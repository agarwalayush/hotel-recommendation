
“Second honeymoon”


Had a wonderful trip. Very relaxing trip. We stayed in beach cottage. Its very cosy environment with all the luxuries. Meal is a bit costly if its not in ur package. But there are many restaurants on the beach with reasonable rates.


“Best Location with better service in Goa”


Location is near Bogmallo beach so obviously it has to be marvelous. I heard from one of my friend that it is govt owned property so I have to say that it has maintained very well. Room service was ok. Dinner was just fabulous what you expect from goa. During odd time you can get room at much more cheep...


More  




“Office party”


We had an office party here in their banquet hall. The food was good. The ambiance was well set and the overall experience was delightful. The drinks served were also well made. The cocktails were also nicely made. Strongly recommended for such parties.


“Nice hotel”


Nice hotel. Good staff. Little too professional. On the beach.
Room was big as compared to other hotels that we've stayed.
Good view of beach from the room.
Breakfast is good. The restaurant area is beautiful. Gives a fresh feel in the morning while having breakfast.
Beach is easily accessible.
Overall a nice place to stay.
A big thank you...


More  




“Wonderful stay & wedding at this hotel”


I had my wedding at this hotel on Dec 29th and enjoyed the excellent and courteous service from the staff. A wonderful beach side hotel with good facilities. Specially will like to thank Mr. Jason who managed my wedding arrangements in the hotel. I will surely recommend this hotel to others.


“A Hotel which seems to have seen better days but now falling through average and heading to bad”


I was in this hotel for 5days / 4 nights recently along with my family. Bogmallo beach resort must have seen better days but currently it is in bad shape, this hotel has everything to become an amazing property but it seems its management does not believes in investing any more and employees too do not see any future either....


More  




“Wedding Fun”


Recently went there for a friend's wedding. Had a wonderful experience! :) The hall was gorgeous and the setting of the resort was immensely beautiful!
The staff was wonderfully helpful as well! It was a good experience overall! Left us feeling satisfied.


“R&R”


My girlfriend and I used this hotel for our first night stay in Goa as it is situated close to the airport and we had been travelling for over 36 hours.
Organised via email a collection from the airport which was there and was cost free. We were in the room no more than 1 hour after landing and were...


More  




“Too Expensive for the Service they provide”


We happened to book this resort as we were looking for some quiet days after spending few days in North Goa ...
The Room rates were expensive : Rs 7000 / night for a standard Room .
I was OK with the money if i would have got a room which would have been worth the money ..
The room...


More  




“A very disappointing hotel”


Our stay didn't start well when we landed at Goa airport and saw a representative of the hotel there with a board for guests, but on identifying ourselves they told us we had to take a taxi as we booked online. It was a disappointing first impression considering the hotel is barely 10 mins away from the airport. When we...


More  




“Amazing location ! Quiet stay”


I stayed at the Bogmallo beach resort with family for 3 nights on all meal plan. Since it was in Dec 1st week it was really costly than I expected. However, we all really loved that place. It is just 10min from the airport and they arranged a pick up. Check-in was smooth and they gave me a room on...


More  




“Great View Crapy Food..”


Scenic view, beautiful ocean facing restaurants, the Bogmallo Resort will definitely score among the top 10 most romantic hotels I've checked into. the more you marvel about the place and ambiance and the helpfulness of the staff there I must agree the first meal i had in the hotel made me wanna check out immediately.
During my stay it was...


More  




“Excellent Hotel and good stay”


We had booked a three nights package and ovrall experience was good. The rooms are comfortable and the property was good having a good pool and beach right next to the hotel. View from the room was good as it was facing the sea and the pool. The service is slow due to which we had a showdown but they...


More  




“Excellent Property”


We stayed at the Bogmallo beach resort for a short break and we really loved the place. It's clean tidy and neat and we have seen the housekeeping staff hard at work to ensure the sand does not interfere with the stay. The hotel has an old world charm and the best part is that from almost everywhere you can...


More  




“bogmallo beach resort”


Excellent hotel; we wanted to stay near airport; this was ideal; staff extremely friendly and helpful. Cannot praise it enough. Fantastic beach and surroundings; we needed to get to the airport at 1.00am . no problem from here; ten minutes away staff had a taxi ready. Lovely end to a three week holiday. Comfort ensured


“Bad Experience”


Unfortunately, our experience of this place was poor. We had booked 2 cottages for 2 nights but couldn't even stand inside the cottages owing to the strong stench. The place looked empty and un-kept. We decided to forgo our entire upfront payment of over Rs 24000/- for the 2 cottages for the 2 nights and walked out of there.


“A lovely beach side hotel with amazing food & Casino Attraction!!”


Recently had a get to gather with my classmates and I choose Bogmallo.. This is about 10 min from airport so you cut down on the travel.. The place is great with all rooms facing the sea. The place has some great food. The staff is very nice. It has a big pool and a lot of place around to...


More  




“very close to the airport, beach 2 minutes away and an in house casino are plus points”


The property is amongst the closest 5 star hotels to the Goa airport. Ideal for those who like to start their holiday without a long drive to reach their hotel! The breakfast is adequate and their ala carte selection is also good.The rooms are comfortable, overlooking the beach and with a balcony attached.
The downside is.. you have to pay...


More  




“Beach resort near Goa airport ”


I think this is one of the nearest beach resort available near Goa airport.
The resort located ideally in front of bogmollo beach and you have rooms facing to the sea.
The resort is quite old but they try to maintain it. I noticed Expats are more in this hotel. Ideal place to stay for week end but quite only...


More  




“nice location”


during my first visit to goa i stayed at this property with my family....it was long back an at that time a beachfront property was a luxury....the rooms were sea facing nice, clean and spacious ...the hotel was a mere 15 min from the airport...the swimming pool is decent enough...the food was ok kinds...you could just walk upto the beach...


More  




“Food is just horrible!!!!!”


our check in was smooth ,
rooms were beach facing with amazing views.
The rooms are very basic and old...
in the morning there was no hot water in the
shower!!!!
Food is just horrible!!!!! The staffs are ok....
Only good about the hotel is the beach and being
close to the airport.
Avoid this hotel....there are many better options...


More  




“''Bogmallo Beach Resort - A Precious GEM in the beautiful state of Goa''”


I stayed here with my wife and little daughter during November 2014, for a 4 night and 5 days package, and it was my first time I visited this beautiful resort!!
I made my bookings through Mr. Amin Khan (handles M.P sales section), who is a very honest, reliable, and trustworthy person. Whatever he committed to, fulfilled the same. A...


More  




“Breath Taking view”


Bogmallo resort has been the only place I have been living in for the last 5 years.
I have checked so many other hotels, but nothing like bogmallo and every year make it a point to visit this place.
every room has an awesome sea view, if one wants to have a romantic dinner the balcony of the room is...


More  




“Review of Bagmallo Beach Resort”


The Hotel is old but all the rooms are facing the sea. Room renovations are going on. The rooms were clean and the food was good. The beach is just one stone away. The lift is very very old. The hotel staff is very friendly and helpful. It is near to the airport


“Superb property with great location”


I stayed in this resort for couple of days in November and it was a great experience. Just around 5 km from the airport, the resort is situated right on bigmallo beach which makes it kinda personal beach of the resort. All rooms are beach facing which gives an awsume view. Rooms are big and quite spacious. Overall an excellent...


More  




“Good service but quite run down”


I was there for a conference, had a nice high floor room with an excellent view. The front desk and staff were helpful and attentive.
However the hotel itself is huge but dated and in serious need for a makeover. It was probably something grand.....30 years ago. Putting on a new layer of paint does nothing to hide obvious leaking...


More  




“Great Location, Fantastic view and a peaceful place !!”


this is a fantastic property in a great location in the calm Bogmallo beach. Overall its a decent hotel, but the service is not all the great. The staff are laid back and take it easy. The pool and other facilities are pretty good. The complimentary breakfast is very average but the view is awesome. Eating at the hotel can...


More  




“Best hotels for late 90's and early 2000's”


The resort has dedicated beach and promising sea view. In some rooms the view is obstructed due to pillars specially the till 3rd floor (unlike other sea facing properties). Customers have no option to have nearby food as the hotel is remote. Spa and rest features of the hotels are not marketed to an extent it should be.
Best hotel...


More  




“A Great Location But Missing Warmth ..”


The Hotel has an excellent location in South Goa and the sea facing rooms offer a panoramic view of the Arabian sea. The rooms are adequately maintained and the property has a serene ambiance. However the hotel staff lacks the much need personal touch and the warmth when they communicate/deal with the clients and the food is nothing to rave...


More  




“A comfortable stay”


Stayed at the holiday in Jan-Feb 14. Firstly was surprised that the hotel vehicle did come to pick me up at the airport - well the same was not the case with some other hotel I had stayed earlier. The location of the hotel is its standout point with rooms (at least mine) offering superb views of the ocean and...


More  




“Had one of my worst holidays”


Well to start off with the pros, is its location. No other hotel will provide you such awesome location. But there's nothing else to be praised. The reception staff was rude when we complained about the non working lights in the room. However it was fixed later. Food isn't the best but average. They claim to be a 5 star...


More  




“A wonderful stay at Bogmallo - Bogmallo Made our trip Evergreen..”


Stayed during 19th Oct - 22nd Oct - 2014.
As soon as we landed at Goa Airport, Staffs are there to receive and arranged us a vehicle to reach hotel in next 15 Minutes. Hotel Lobby and reception was of Palace kind, with beach view. Rooms allotted at 5th floor are completely modernized with uninterrupted beach view throughout our stay....


More  




“Affordable Luxury in Picture Post Card Settings”


It has been our long standing desire to visit Goa and so when Shalom Travels Chennai announced a group tour to Goa in the Pooja holidays from Sep30-Oct3, we jumped at the opportunity and booked 5 seats in the group for my dad, mom, wife, daughter and myself. The group consisted of 32 persons and we were to be accommodated...


More  




“Only one thing - Super Hotel”


Super Location, Old resort but beautiful views. Right on the beach. Best value for money. Highly recommended if you are travelling with family, couples, friends. Nice Pool, good food, near to Bogmalo Market.


“Best view resorts but ok ok hotel”


The best view from any hotel in goa.all rooms are facing to sea. But hotel is an old hotel, all amenities are ok ok type, do not expect modern amenities of 5 star hotel.
Do expect for any classy thing here.one should opt for this hotel only and only if he wants to have sea view room in limited budget...


More  




“Excellent - As Before”


This review of the wonderful Bogmallo Beach Resort is long overdue.
We were there the previous year along with our elderly parents and had such a wonderful stay that there was not doubt about where we would stay on this visit. Bogmallo it was again.
This time, we took rooms on a higher floor with a more sweeping view of...


More  




“Decent stay.”


Checked in 29th Sep n checked out 1st Oct.
Booked by host company.
Frankly, I was worried when I read many negative reviews on this place but it was not that bad !
Distance was 10mins from airport but as this resort was in small village, there was no shopping around the hotel. Shopping places are in North and about...


More  




“A wonderful experience”


I visited Goa on 19th sep 2014 and checked out on 23rd sep 2014 ( 3 nights and 4 days ) . The resort is situated 10 mins from the airport . Took APAI Plan for my family which included everything from breakfast to dinner and sight seeing and all taxes.
About the hotel staff ... they were too good...


More  




“Old hotel on the beach!!!”


Situated 10 min drive from Airport over bogmallo beach, As soon as you will enter the recption you will get feeling as if it is a home of a business typhoon who flourished in 80s with a huge dome like ceiling & chess board kind of flooring, your hair will be continuously disturbed by the round the clock breeze coming...


More  




“Nice experience”


Ultimate sea view from our 5th floor deluxe room. Service is good. Buffet breakfast quality can be better. Also, timings of buffet breakfast should be upto atleast 11 AM, considering people are there on holiday


“Good Location”


Have been travelling with my colleagues every year and to start with get excellent service and deals from their Pune Sales Office ( Ms Marinita Kotinkar) she is undoubtedly the best in prompt service in the hospitality industry... The place is really good ,and the food could be better....
The biggest USP is the location of the resort.,..Every room is...


More  




“Dated but charming”


Upon arrival, Bogmallo Beach Resort has an aura of 70s charm, with the quirky decor and helpful staff. Enjoyed nightly beach walks with beautiful sunsets. The only downside was the catering provided; the buffet seemed to have been reheated on a couple of occasions. Overall a good experience.


“Amazing View”


The best part about this hotel is its location!! the view from the Sea Facing Rooms is simply breath taking..!! The staff is very friendly, charming and helpful. The Food is at the restaurant is quite good...and if ur not happy with the food there are many shacks around that serve good food.


“Lovely experience”


The view from the hotel is quite breathtaking; it is right on the beach and the sunset is beautiful. The food and service is quite good. The restaurant is well maintained and overall is a lovely experience.


“Well located badly maintained”


What can I say about this property ex oberoian property so had to keep up its charm with location had got married their so have the bestvmemories of my life attached with this hotel but should be maintained by a group of hotels can make wonders with this property rooms are spacious but badly maintained staff are not that friendly...


More  




“Horrible place”


I stayed at Bogamallo yet again due to lack of choice and attached are pictures of how this place was when I checked in.
Unclean, with roaches and less than likeable food in the restaurant.
Rude staff and non responsive reception .


“badly maintained, extrem mould on walls, unfriendly!! ATTENTION!!”


We stopped in Bogmalo a few Days ago and tought this could be a nice Hotel. Actually NOT! The Hotel is over 35 years old and so are the rooms. Badly maintained, stinky, old, mould on walls and the smell of mould everywhere.
CHECK your room BEFORE you pay anything. THey like to ask for money in advance which is...


More  




“Terrible place to stay during the off season”


Bogomallo Beach Resort!!!!! dont know what is the definition of resort here, a swimming pool and and Bar????
This hotel has nothing but rooms with good view and proximity to Airport. The view is wonderful, with sea and the airport on looking. The good things ends there.
The rooms are stingy, dirty and small. None of the plug points works...


More  




“Worst reservation team of this hotel”


I am travelling to goa next month from 14 october to 18 oct as it's my honeymoon . I have sent mail to this hotel 1 week ago and today again . After sending the mail to the hotel I called up the reservations department where I was able to communicate my details to a female with the name ophilia...


More  




“Close to the Airport & the Sea!”


The property is located just 10 mins away from the Goa airport and I must say that it has an excellent view of the ocean. You can hear the waves and its really nice. All the staff are extremely friendly!
But then the hotel really needs renovation. There was not even one power plug point that worked in my room;...


More  




“View!!!view!!!!!view!!!!!!”


Location...just besides the airport 10 min drive ...just 250 rupees for taxi!!!
Every room is sea facing
LOVELY VIEWS OF THE ARABIAN SEA
see the sunset in front of u from the balcony.......
Hear the sound of the waves...
IF U LOVE THE SIGHT AND SOUNDS OF THE SEA...THIS IS IT!!!!
ROoms....just o.k.not very luxurious...
It is not as grand...


More  




“Beautiful and Memorable stay ..Just loved it...!”


Me n my husband stayed here in April 2014...this is one of the most beautiful resorts in Goa that offers breath-taking view of the sea from the room and the dining area as well. From the lounge bar also ,you can enjoy the spectacular view of the sea.Rooms are spacious n well maintained with satisfactory room service..The staff is polite...


More  




“Pleasant stay,just awesome i can say...”


We visited Goa recently and we enjoyed every bit of it,the best of our trip is our stay at Bogmallo beach resort.I am very much confused after reading all d reviews that its not been a happening place to stay, very far ,nothing to do much there etc etc.But as we reached Bogmallo , enter the Resort we welcomed warmly...


More  




“Scenic Beach Resort”


We were at Bogmallo for three days and absolutely loved it. As background for destination wedding it was an excellent choice. Each room with sea/beach view, good service,great food, spacious room and balcony was unlike the new cramped hotels coming up in Goa. Excellent well maintained pool and lawns with beach next to the property, huge A/C hall was ideal...


More  




“Excellent Location & Property”


Very good location, just a breadth away from sea.
Good well maintained property.
Spacious rooms with every helping staff.
Very good food with courteous staff.
Had a nice short break from busy life.
Very near from airport (5 - 10 min drive), but far from the city.


“'Bogmalo Rehab Facility' - The Spoiler”


Enter this rehab and you find the magnificent Sea View welcoming you.... And it ENDs there!!!
Next Up -
Dirty swimming pool ( water not changed for almost 15 days)
Unkept Rooms ( Roaches, ants and dirty linen)
House-no-keeping ( no dental kit in rooms, single supply of tea n milk for 3 days, limited tissue rolls)
Staff Misbehaviour (...


More  




“Beautiful resort !”


One of our team members had gone to Goa for a conference with an student organization, who keep having these conferences all around the year at different places in India and booked Bogmallo Resort for this particular conference and it was a beautiful resort. Still remember walking in and being blown away by the proximity of the beach. The view...


More  




“Cockroach's invade your privacy”


The only memory I have with me is cockroaches invading my privacy all the time. To top it all ants dint want to feel left out. Nevertheless food would get 8/10. The hotel needs real makeover and refurbishment. The locks are a tuff nut to work. The staff is good but the old look overshadows all the positives.


“bang on the beach”


Wanted to try out new hotel near the airport. Wanted to stay by the beach, Hence Bogmallo Beach resort. A board proudly proclaims "Tripadvisor" badge. Room allocated to me was beautiful on the fourth floor facing the sea. The view was extremely beautiful. the room had huge windows facing the sea, allowing an uncluttered view. Flip side, Food is priced...


More  




“A very quiet place”


I stayed with my family 3 nights.
It's a very relaxing place.
Breakfast was very good with a lots of fruit, almost everything you can want.
The only drawback is that the hotel is very worn. In no case is a 4 stars hotel.
The staff was friendly, Christmas evening held a pretty nice show.
The main advantage is that...


More  



