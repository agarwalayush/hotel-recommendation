
“really a nice resort........”


this resort is near ti vainguinim beach and the rooms are also very good
the pool is for kids as well as for adults
the food was also good their is also a casino in the resrt and we can get a taxi from the resort itself


“Definitely not a 5 Star Property”


I visited Goa in January 2014. I was looking at a resort with in-house casino but due to heavy rush during that period, this was the only option I was left with. The initial pictures and reviews gave me impression that it would indeed be a very good choice even if it is a little isolated from the main city....


More  




“Definitely more casino than resort . . . .”


I stayed here for four nights on my own as a pre-arranged chill out add-on at the end of a group tour exploring Kerala. On arrival (in the dark) I was allocated room 403 which, though large enough, was fairly basic and had no hotel information whatsoever and overlooked the 24 hour staff and security entrance . It transpired the...


More  




“Really Very Nice Stay”


I visited the hotel on 26 Oct to 29 Oct the place is very nice. The staff is very helpful the resort is really very clean. The casino is nice enjoyment, and the live music is really very good. The breakfast is good. I have tried food as well its really testy. Overall worth for money you can go with...


More  




“Isolated with very poor service”


During my stay of two days there, overall experience was not very pleasant. But I would give them a plus on cleanliness and room sizes.
In room dining and restaurant dining were snail pace slow and food was very average (we asked for many dishes to be replaced because they tasted bad or stale) with only Indian section a little...


More  




“Nice Resort”


Nice and decent place to take a quick break to Goa. Good swimming pool and lovely buffet breakfast, lunch & dinner. The casino has some enjoyable live music which you can enjoy until the wee hrs of the morning.


“Place needs maintanance”


The plus and minus are as below : we booked 3 rooms on a 3n/4d pack between 3-5-14 and 6-5-14.
The Plus : The front office staff Domingo and Natasha were excellent in Hospitality and took a lot of pains to make our stay pleasant and comfortable. They were ever smiling and very co operative.
The minus : The a/c...


More  




“Place needs maintanance”


The plus and minus are as below : we booked 3 rooms on a 3n/4d pack between 3-5-14 and 6-5-14.
The Plus : The front office staff Domingo and Natasha were excellent in Hospitality and took a lot of pains to make our stay pleasant and comfortable. They were ever smiling and very co operative.
The minus : The a/c...


More  




“Average Atmosphere”


I was there to try my luck at the casino. very average crowd, dull atmosphere. though the food was awesome and also they provided unlimited drinks and snacks on your table for reasonable charge but still the spark was missing


“A short break to remember”


I stayed for 2 days at Chances Casino and Resort, Dona Paula. It is a clean and a decent hotel and I think the best in that area for the price. The food was good and the service courteous. The hotel does not have direct access to the beach, but enjoyed staying there as I got to experience a unhurried...


More  




“Calm stay”


We had stayed at Chances Casino and Resort, Goa for 4 days in March 2013. The stay was comfortable. The food was tasty and gives a good experience of Konkan food.the Service was very courteous .
The hotel has no direct access to beach and does not own any area of the beach compared to other hotels nearby. It is...


More  




“What an experience!”


One of the best hotels that provided us with extraordinary service at a very humble price. Stayed with a friend in Deluxe room. Very Spacious and secure. Being a coffee person, i was glad i had a coffee maker in my room. Plus it's a 5 min walk to the beach. Great food, specially if you are a non vegan....


More  




“Good”


Stayed here in a single room. Almost upto 5 star standards.food tasted good, except that room service forgot the chips in fish'n chips. Maybe, it was the healthy version. Didnt go to the casino,as was full of political looking people. Small swimming pool. The place is a bit way out but well connected by road. So carry a car along!...


More  




“fun in goa”


last year i went to goa with my friends by train goa sampark kranti from delhi
karmali is the nearest railway station for this hotel but dont forget to book a taxi in advance to pic up you from here.
hotel is very near to dona paula.. a beautiful beach is also there on back side of hotel..
food quality...


More  




“Better than average.. for sure.. Good. But not very good.”


The location kind of sucks. Its on the back side of Cidade de Goa resort. The beach access is through a road throu the resort. So none of the rooms are beach facing. Its a good 8-10 minute walk. The area near the hotel feels less resorty but more residential.
The rooms are spacious, clean. The hotel is also good....


More  




“A cloud over the beauty of Goa”


The hotel is situated in a part of Goa that is not near anything. The service was not very good, especially during the day. The food was ok with some ethnicity. If you like to gamble, Im sure you would like the casino they have. Our hotel room was not in the main hotel.


“A casino lovers.........Hotel away in Goa”


Location of the Hotel is bit away from the city and not attached to the beach. Eventhough it's close to Cida de Goa, Resort need to walk a lot to reach the beach............Food quality is not very good and the main business seems to be from the Casino in the hotel.
No centralised, AC in the lobby and other areas...


More  




“Awesome experience”


I went there an year ago and it gave me the peace of mind, relaxed stay and awesome food....which were the things most needed at that time.....!
The staff was good and I enjoyed every bit of my stay....best part was the evening dinner beside the pool with live orchestra....A little far off from the city hub but quite chillaxing....!!!


“Value for money.”


It's a good hotel for family and is 3/4 star quality. Good service and big rooms. B/fast spread and quality was good but Goan food was not like other famous restaurants. There is a nice virgin beach behind at 2 minutes walk but no hotel staff informed us about it !
Needs lot to be done about activities and atmosphere...


More  




“good property but primarily a gaming hotel...far from everything else”


stay at this property atleast twice a year
rooms cosy and functional adequate services for the whole family
good reataurants and a full service spa on site
main draw is the casino Chances which is open from 11am to 4am .
as it is literally in a valley with a solitary beach 5 mins walk away, there is really nothing...


More  




“completely satisfied.”


Lets start from the begining when we were planning and searching some good deals in Goa..we saw many resort and hotels and atlast finalising this one with the package of 5nites and 4 days.
Our package includes:
1)Airport Transfers
2)Break fast and dinner
3)Half day sight seen for old goa churches,mangueshi temple & miramar beach
Must say that all of...


More  




“Easily the worst hotel I have stayed in Goa”


We were booked into this hotel by our office on a visit to Goa. Rated as a "5" star luxury hotel - it was anything but. It turned out to be easily the worst hotel I have ever stayed at in Goa. The rooms, though large, were musty and looked old and shabby. The bathrooms were not very clean and...


More  




“good location, bad service”


nice quite place, good casino. the only disappointing thing about the hotel is their service. u will be waiting waiting waiting ........ btw breakfast is really good. nice cozy bar beside their pool. they don't serve much of a variety but its good


“Quiet Place with attached casino”


Dont go by the title resort. It cannot qualify for it however it is has two hotel buidlings. Staff is courteous and if you book in advance and through some promotional website like agonda.com you will get great discount. Ask for packages so that you can get max value. Our package included unlimited entry in casino with dinner and breakfast...


More  




“Good Quite and Nice Hotel”


Hotel is centrally located so those people who are looking for parties or night outs may be disappointed. but they can use a very nice casino in Hotel. Rooms are good and best part of rooms are its AC. Well I got a bit low when I found there are no bath tubs. Food was good really loved the non...


More  




“NOT EVEN A 3* PROPERTY TO BE CALLED”


First of all GOA tourism should investigate how can this hotel be called a 5* property when it is not worth 3* also, the staff was behaving as if they are doing a favor by serving us, the food was pathetic, it seems they had hired raw guys from nearby village & gave them the uniform .
The only thing...


More  




“Worthwhile, Courteous & would love to come back”


We had been to Vainguinm Valley resort in the month of Sept 11.
This was my repeat visit after 5 years. It was really heartening to see the resort in same, rather better condition, in terms of Cleanliness, staff & the customer approach. We really loved the individual attention / service from the hotel staff, which includes people, right from...


More  




“Good Resort”


I visited Goa in Aug'11 end along with my friends. We were 6 couples with kids.
We enjoyed a lot and had a really good stay at Vainguinim Valley Resort.
I liked the hospitality of all staff members, receptionist and manager. The Casino is really wonderful and well managed and its worth visiting.
The only thing which makes this resort...


More  




“Really a good boutique resort to visit”


We went to goa for the very first time and we found it very comfortable due to the very very very good hospitality from the hotel staff and really the restaurant staff was really an excellent with the service and food.
i recommend all the visitor who have criteria of having complete rest can visit this resort.


“really a difficult place to live in”


This place is far off almost like 10 kms from the city, the staff is not corteous and even not trained properly. The swimming pool is a pool just for the name sake.The beach front near is occupied by another property cidade de goa. The only good thing about the place is the casino.


“Never visit the resort nor will allow anyone whom i know to stay here”


I love travelling and had been to many places in and outside India. This resort is the worst i have visited. I have never written anything on web but just to save people from spoiling their holidays, on humanitarian basis, I am compulsioned to write the review.
The services are very poor. Even drinking water was not provided on daily...


More  




“Quality/friendly family run hotel”


The hotel is situated in a peaceful location a few minutes walk from a quite attractive beach. It is a casino so many guests come to gamble. Our room was top quality as are all the facilities especially the spa and swimming pool..Food is excellent.What makes this hotel special is the friendliness and helpfulnes of the owners/staff.


“HOTEL is good for money value.”


hotel is very good for money value. You can get good rates in off season means feb to october. The main thing is hotel has its own Casino Chances casino, a good one. A hotel normally rated in 5 star category but you may not find 5 star facility.
one more thing is it is in mirmar area and too...


More  




“Fantastic hotel...lovely staff and manager”


My friend and I visited the hotel and had a wonderful time. The room had plenty of space and clean. The food was great; we had breakfast and dinner during our stay in Goa. We ate at the restaurants and a wide variety of food. The staff was very friendly and always willing to help.
We used the spa and...


More  




“Loved the food!”


We stayed along with a group of around 40 folks. It was managed well by the hotel staff. The services were good and the food was great. The beach is 5 mins walking distance from the hotel and is shared with other hotels as well. The place is a bit far from beaches like Baga, etc. Overall we had a...


More  




“Will be going back”


We all found it a most enjoyable experience the staff were always smiling and very helpful.
the rooms were clean and serviced twice a day. there was 14 couples and it was a return visit for 10 of the couples, and we would all be returning next year.


“the guys running it were clowns!”


we booked the resort through hostelbookers, arrived mid-afternoon and the manager claimed not to have the booking. We produced the confirmation email, the manager offered us the room at double the advertised price. Unimpressed by this extortion (it was the day before New Year's eve - so I guess he reckoned he could get away with it) we turned down...


More  




“A really nice hotel!”


The best thing about VVR is the location - 2 mins from a lovely beach and 10 mins from Panaji (Goa's capital). You can hire a car for the day and visit the various beaches, Old Goa, the Spice Plantation, go dolphin spotting, go shopping at Bmbay Bazaar. The beach is a long stretch of golden sand and was quite...


More  




“More No Frills & 3 star.”


From dodgy hostels to 5 star hotels I have never before felt compelled to write a review on trip adviser because each place has been just as it advertised. The Vainguinim Valley Resort however is the first place myself and my husband have stayed that has been a complete disapointment - the complete opposite of what is advertised.
Travelling to...


More  




“LESS EXPENSIVE BUT FULL OF LUXURIES..”


Its a beautiful hotel situated just 10 min from the capital of goa "PANJIM"...the rooms of the hotel are very comfortable and the stay proved to be the peacefull as well as the luxurius stay in goa..me and my cusine on the first day of our arrival took dive in the swimming pool of the hotel..the swimming pool is in...


More  




“Best of Glorious Stay next to Beautiful Vainguinim Beach”


Our trip was planned at summer vacations of last year & we chose this resort as it is situated at the most beautiful place. Hardly 8 km from Panaji Bus stand, situated at most beautiful & serene valley of Vainguinim next to Dona Paula having an inhouse casino called Chances where we tryed our luck with Gold rush. Along with...


More  




“An average 3-4 star experience”


This hotel purports itself to be a five star property but we didn't feel that way. It is a small hotel (boutique style 5 star as mentioned by the hotel). The amenities were not up-to 5 star standard neither the room decor was. The breakfast was average. The hotel claims a special access to Vainguinim Beach which turned out to...


More  




“Go Goa...”


Best place to hang out in Goa... I had been for the first time to goa, but the hotel made me so comfortable, that I used to stay all the time at just two places, at beach and at the hotel, ha ha ha...
Awesome experience, one should go through...


“Not bad but not the best location for a holiday resort”


Went on an office sponsored holiday to this hotel. Very basic and quite ok. Nothing too great and the location is not very tourist friendly. Might be a good hotel for someone who just wants to sleep-in and relax.


“Good Hotel with some issues”


This was one of the hotels recommended by one of my friends when I shared my travel plans for Goa with my family for 3-4 days. He himself helped us with the booking as he knew the resort's General manager so everything was well taken care off in regards to the booking etc.
The resort is located in a nice...


More  




“Goa in a valley”


Goa in a valley. The hotel is located in vainguinim valley near the famous Dona Paula and is on a 5 mins walking distance from the vainguinim beach.
Pan Asian Restaurant, Pool Side cafe & a 24 hours coffee shop are at your service to serve you mouth watering and delicious meals including some of the famous goan dishes like...


More  



