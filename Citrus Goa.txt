
“citrus goa”


very beautiful hotel rooms are very neat and clean nice view pool is also very clean and food is very good in short we get everything here and this hotel situated at very nice location staff is very friendly .citrus make your trip more enjoyable and exciting.


“Great stay! ”


It is an amazing resort in the heart of the Calangute. The best part is it is in such a convenient location from where you can commute to other places easily. The room which we booked was neat and clean with TCM and mini bar complementary. The room maintenance done everyday by house keeping department is worth appraising. We stayed...


More  




“GOA 2015”


Hi friends.....i along with my wife and small kid stayed in this hotel from 11-13 jan 2015 .It is a small resort in middle of calangute , approx 10 min distance from beach . Location was not great . Room was ok ....I think it was room no 110 . Balcony was there but not a great view . overall...


More  




“MEMORABLE STAY”


Me An My Wife Stayed In Citrus Goa From 9th jan 2015 TILL 14th jan 2015 In room 213.We Had Such an Excellent Experience Of Our Stay. General Manager Mr Rahul Mehta Is a Wonderful Gentleman and All His Team Members Are gem of A Person, Full Of Hospitality standards .from the check in to check out everything was up...


More  




“Very Good Place”


Me and my wife had been on a Leisure trip to Goa and we had an Amazing time at Hotel Citrus Goa. It is just in the heart of Goa. The Calangute Beach is on 2-3 mins. walking Distance and Baga Beach is also nearby.
The market is also in walkable distance.
The Hotel is very neat & Clean, with...


More  




“Excellent hotel great food”


This was our second stay at Citrus. Comfortable as ever. We had initially booked a superior room, but got a free upgrade to a deluxe room. Very close to Calungute beach, so close to all water activities. Staff was very helpful in particular Deepak. Wifi is a bit flaky which needs to be improved.


“citrus goa review”


stayed at goa citrus for five days in jan 15
excellent time as close to calangute beach (one minute walk)
service rooms staff hospitality food neatness all were top class
rooms condition and bathrooms were maintained well
advice to book through expedia for the best deal
kudos to mr mehta and his team for the excellent show


“Fantastic Location right in the heart of Baga/Calangute”


We had a fabulous stay at this hotel. Firstly the location is excellent, right near everything that you could need and the beach. The hotel is quite modern with the reception area having been renovated. The actual setting of the hotel and gardens are lovely, but probably will need constant attention over time. The pool and surrounding area was lovely...


More  




“Awesome staff and good quality rooms”


It feels very sad to leave this place but everyone leaves with an experience . This review is just to express my gratitude and thanksgiving to Mr Rahul GM citrus Goa , the chief Chef and the kitchen and housekeeping staff for making our stay comfortable . The housekeeping staff are v v efficient.
In breakfast they would serve you...


More  




“A mixed bag of feedback”


First impression of the hotel - if you are coming on your own and are unfamiliar with the roads, you will easily miss the hotel. A tastefully decorated reception and smooth check-in. Really important since you are tired and don't want to spend too much time waiting. Neat and comfortable rooms and good food are the highlights of the hotel....


More  




“Excellent hotel in peak season!”


It was a last minute new year plan that we had and all the hotels we frequent were booked! I chanced upon Citrus and have not regretted it till check out!
The hotel is a 3-4 minute walk to Calangute beach (which is very important in Goa) and the famed restaurant Souza Lobo. Baga beach from this hotel is about...


More  




“Citrus Hotel - Excellent hotel, great food.”


We stayed at Citrus for 2 nights on Jan 4th & 5th.
The rooms were excellent. Food - great & overall ambience of the hotel was too good. Its very close to Calangute beach.
Full points on value for money.


“Lovely property at walking distance for Calangute Beach”


I have stayed at Citrus before at Alleppey and had high expectations from this one too. It certainly did not disappoint. The service is very good and super fast.
Pros:
-Walking distance from Calangute Beach ( just 400 mts )
-Right near the Calangute/Baga circle close to shopping paradise
-Very Neatly maintained. Neat and clean rooms.
-Great Staff, amazing quick...


More  




“Amazing Christmas and New Years at CITRUS!”


Just got back from a great 2 week stay in Goa at Citrus hotel. It was amazing! From the great room, massive shower to all the staff who are very caring and there for you! The reasons I'm giving 5 stars is a waiter called Hemant! He was the best, was a grat bloke very helpful and happy! He made...


More  




“Overpriced and definitely not worth it”


During our travels round India we stayed at lots of hotels and hostels of varrying price and the Citrus Goa was one of the more expensive. It was meant to be our treat after a week of homestays and frankly the homestays were far better.
Our room stunk of mould, the fridge was so noisey that we had to unplug...


More  




“Value for Money”


I went through a friend who has membership, so got a good deal during Peak Season. Its a good place near calungute beach. Good Breakfast spread. Jam the manager from Bangalore was just too good .. great service from him. the staff is very friendly and always willing to help. The in-house dining is expensive then again you should just...


More  




“Excellent hotel with extraordinary quality of service, food and hospitality”


This hotel is unlike other luxury hotels where you are restricted to the hotel property itself, it's centerally located in North Goa. The beach is just 7 mins walk. When you come out of the hotel you have lots of shops and cafes. Mr. Deepak who is the reception manager was very warm and helpful. Rest of the staff also...


More  




“First trip to Goa”


The Citrus Goa was a great, clean place to stay. During this time of year there were plenty of families around. The staff was very helpful and everyone is very friendly whenever you see staff around. The air conditioning was so needed. My only complaint is it's a pain to constantly ask for internet tickets.


“awsome serviceee”


i want to mention that the bar is so nice in this hotel because it close to the pool.
i feeel so romantic feeelinggggggg....
The staff were behaved friendlyyy ......
foood was tastyyyyyyyyy ....
especialy the fish itemmmmm....
Thank u so muchhhhhhh......


“Value for money!”


Nice hotel with good location n good service at reasonable cost.Breakfast was ok.. food was decent.5 mins.from calangute beach and in midst of lot of good eating options.we got an upgrade on check in which was a good gesture. Would have been good if they
arranged for airport transfers at rates comparable to market rates. All in all a good...


More  




“Great place to stay”


It is the best place to stay very clean and friendly staff. Will stay every time I visit goa. Its near the beach and market...
Night music on sunday but of course the food is awesome.
No noice qround the hotel.. good clean room nice staff.
I must suggest everyone visiting to goa should stay once to know the difference...


More  




“Great Stay near to calangute”


Hotel Ambeiance and Services are Very good, near to beach but no beach view.
Preferble stay in Goa. Food in restaurant(breakfast & Dinner) is sooo good, but littel expesive.
Middle for all places. I recommend this hotel.


“The easiest thumbs up I've given!”


My wife and I had decided on this hotel based on some great reviews on Tripadvisor and I tell you folks, Citrus lived up to each word of it. It was a truly wonderful stay here. The rooms are very clean, fresh and modern in design. However, the highlight here was its wonderful staff. Every single one of them were...


More  




“Decent 3star”


Guys don't eat here it's below average ,rooms are good tho expt leakage from a/c .very good location .If your looking for decent 3star it's good .........................,.,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


“Below Average”


I am a frequent visitor to Goa and I visit the place at least once a year. Citrus at Goa is a mediocre hotel. I had booked two rooms for my recent visit in October 2014. At first, one of the rooms had severe seepage problem and the wall of the room was completely spoiled. When I requested for change...


More  




“I don't know if it was just our room but...”


This hotel seemed a bit mouldy (literally). Our lamp was mouldy, our curtain was mouldy and the window next to the shower was a bit..mouldy. I shall put photos up if i feel suitable annoyed enough to get around to it. Also the toilet in our room was kinda broken, it would flush for all eternity unless you yanked the...


More  




“Perfect stay near to beach..I love it...”


Perfect stay near to beach.Its very close to calangute .I love it...Good food ,service and Bear.,To start with they have a smooth check in.Citrus continues to maintain its standard in service and hygiene.. no doubt about that!!


“A perfect place to stay at Goa.... value for money”


Stayed at hotel Citrus, Goa from 6th- 9th Dec-2014. This Citrus group of hotels used to maintain their respective standard, service, hospitality & quality in service..... that's why I always prefer to stay in Citrus, where ever I get an opportunity while traveling inside the country. Coming back to Hotel Citrus at Goa, it's premises is maintained to be too...


More  




“It's a perfect holiday room”


To start with they have a smooth check in... Rooms are spacious and best to stay.... Bed are good...clean bathroom... House keeping staff are very good...the service is super fast and they have a good menu for food... Only draw back is that the pool is small... Orelse nothing is bad... All staff are best here... Surely I wil be...


More  




“Excellent Service with Value for Money”


Citrus continues to maintain its standard in service and hygiene.. no doubt about that!!! Its very close to calangute which is a big plus. Yes parking is an issue... however they have guards who take care of that. Food quality was excellent!! overall an excellent option to opt for when with friends or family.


“Vacation with Family”


I visited Goa for first time . the hotel support staff is courteous. restaurant food really tasty.Room were neat and tidy and was made up well before we return to our room everyday. It was refreshing and rejuvenating 3 nights experience. I would like to thank citrus team for providing us such a wonderful experience.


“My baby's first family vacation. ....”


I drove to Goa from Mumbai along with my wife and 11 month old son.. After 10 hours or tiring journey I was happy to see my son's reaction when we checked in Citrus calangute. .. He was very very happy...he jumped on the bed and hugged the soft pillow. We took him to the pool and he freaked out...


More  




“Family Vacation”


I visited Goa for first time . the hotel support staff is courteous. restaurant food really tasty. it was with family trip and we being vegetarian, we liked veg food here . however front desk staff needs to improve in service . it should be more proactive in terms of guessing guests type , what kind of services they'll prefer...


More  




“Just loved the hotel”


We visited in the month of march and the hotel is simply superb, The ambiance, cleanliness are are very nice.. And specially the pool, My kids just loved it.. we just went one for a beach and spent most of our time in the pool.. i'll highly recommend this hotel


“They made us feel special! Great Location Fantastic Hospitality”


They truly made our trip memorable, by making us feel like royalty on our Anniversary. The food was delicious and all staff very polite and accommodating. Special thanks to Rahul & Monika, the restaurant managers. To our surprise, they got our room decorated and arranged a sweet romantic pool-side candle-light dinner for us making sure everything goes perfect . Totally...


More  




“Excellent service & good food”


Citrus hotel(GOA) is one of the best hotels I have visited so far,their staff is very friendly & will
help you instantly when you need one.There is variety of food available for dinner.Calangute beach is nearby which is somewhat crowded at night also.I would definitely stay at citrus again.


“Good property Great Hospitality”


Stayed at this property for two nights. Beautiful room, very clean and neat. Very close by to the calangute beach and market. I really liked the hospitality. The food served for breakfast was excellent. I was spoilt for the choice available for breakfast. Wonderful and amazing breakfast. The only one negative thing I found was the room being a little...


More  




“Best hotel at best location”


i have visited citrus again with my friends on this November for 4nights. The hotel is at best location near calangute beach . Best thing about the hotel is there staff which are always eager to help you and are very courteous. Breakfast there are having wonderful option but one thing is disappointing they have not included nothing new in...


More  




“Good Stay”


I stayed at this property for 3 days. The property is situated near the Calangute beach with easy access to the beach and the market place. Staff were helpful.Restaurant food was good with multiple choices .Rooms were clean and comfortable. Staff was very helpful and courteous.


“Great Hotel!”


Stayed at Citrus for 11 nights and would gladly stay in this hotel again! Staff are really nice the rooms are very clean and comfortable! The buffet breakfast is very nice with plenty of choices, the location is also really good just 10 minute walk from the nice but extremely busy Calangute beach! If staying here best to walk past...


More  




“Nice Place”


I stayed for 3 nights in this hotel in October, It was very comfortable with excellent behaviour from the front office and the house keeping staff.
The rooms are really soothing so is the ambience in the property, situated close to Callangut beach it has a good location too. It has a good restaurant with a decent spread of breakfast....


More  




“Comfortable Stay”


I stayed at this property for 3 days and had booked a premium room. The property is fantastically situated near the Calangute beach with easy access to the beach and the market place. Staff is helpful in giving suggestions to guests about places to visit and how to reach. Restaurant food was good with courteous staff. A swimming pool although...


More  




“Comfortable stay”


I stayed in this resort for 3 nights during month of October, I had booked deluxe room but was upgraded to premium room which was big enough to accommodate me my wife n two kids, room was spacious and clean, it had a mini bar with beers,soft drinks, red wine along with snacks, which was really cool.
Resort is maintained...


More  




“Excellent Hotel”


An exceptionally good hotel with great staff and good quality amenities. Stayed at this location with family for one week and it was truly a memorable one. The staff were most courteous and obliging and ever willing to help out (extremely quick check-in). The location is great and close to major shopping and restaurants on the famous Calangute/Baga stretch of...


More  




“Gone,Gone,.....GOA!”


This was my maiden trip WITH family to Goa and I don't think I could have made a better choice than Citrus @ Calangute. For families who like to be near local shopping markets such as mall or local flea markets...calangute is surely the choice than any other location at goa.
About the hotel ( citrus) right from the word...


More  




“Great location, cordial staff”


Centrally located. Close to calangute market area. Good choice if one wants to drive over at night. Our room was upgraded to highest category and was a good experience. Bathrooms are tidy and room was spacious. At the property there is nothing much to do. This is understandable, given its location. So off course could not be compared with beach...


More  




“Good Quality of Service”


We were a big group of around 15 people, so we had to choose a hotel which would provide good quality of service to a bigger group. I must say Citrus met all my expectations due to its service quality. The rooms were very nice and the housekeeping, room service and food was pretty good. The staff was always courteous....


More  




“Nice Hospitality”


I must say we had wonderful time in Goa all bcoz of this hotel, we have visited goa so many times, but this was the best stay. Food was good, staff was good, rooms were good. Wifi was an additional surprise, we had stayed in many hotels which say they give good speed but this was the best connectvity


“Excellent Location, Nice Hotel, Good Rooms”


I must say, I had wonderful time at this hotel. The hotel location is excellent (near Famous Calungute beach). It was absolute value for money. The room size was (even though entry level premium class room) sufficient for a family of 3. The staff was very hospitable and courteous. The only disappointment was WiFi & Food. We had to take...


More  




“Pleasant stay near Calangute beach”


Googling 'goa hotels' returned 52m results....'calangute hotels' returned a lower 3m lines. Suffice to say, it was quite a task to pick this property; However, after going through the reviews of earlier travelers, we chose the Citrus. We were 5, booked for 3 days into 2 pleasant, airy & very clean rooms. Efficient service, great food reasonably priced - even...


More  




“itz a class”


good stay. the food was gr8................... good ambience. clean and class hotel.......................................................................................................................................


“Best value hotel”


Best place to stay in town , great variety and quality of food ( Indian , Chinese and continental ), nice luxury well maintain rooms , well maintained gym and spa .good for local shopping . 100% recommended . Staff is courteous and well behaved ... .


“Excellent service”


Because this was our first trip to India we spent a long time choosing our hotel, but we still had feelings of trepidation right up to the time we arrived at the hotel. The airport and the bus journey to Calangute had us wondering what we had let ourselves in for, but once we were in the hotel all doubts...


More  




“nice experience”


we visited so many hotels before confirming this one ... and its my personnel opinion that if you are going with your family this is one of the best place to stay. My son loved this place. Nice ambiance. helpful staff... :)


“Horrible Experience at Citrus Goa”


We stayed at Citrus Goa for 8 nights, we did not consume the snacks kept on the desk and did not use the mini bar for the whole duration. When we went to check out, the lady at the desk said that we consumed snacks and billed us for something we did not use. When we told them that we...


More  




“Bland”


We visit Goa (especially North Goa) every year and this holiday season since most of our favourite places were already full, we chose Citrus, since it seemed to be in in Calangute and had great reviews. Our experience though was mixed and here are the positives and the negatives of the same:
Positives:
The Pool: Its a small pool which...


More  




“Excellent Property in Busy Calangute”


Overall experience in this property is EXCELLENT. Rooms are spacious and well furnished. Bathrooms are very clean, spacious and wellm laid out. Airconditioning is good though creates quick condensation if switched off. Spa is very good, its masseurs well trained. Swimming pool is a bit small and starts from 8AM - this is a little inconvenient. Room service is prompt...


More  




“Good Resort for families to stay”


Hi ! We stayed at Citrus Hotel for 3 nights and overall experience was good. We booked 2 rooms. One was premium which was nice room with minor ac cooling issue. However the other room we booked was basic and it had problem of moisture. Its flooring and wall were wet which made room damp and unpleasant. Breakfast and food...


More  




“Mr Anup Mohta”


Absolutely hopeless front desk, and food service. The front desk provides you with the worst located room till they are available , and if by any mistake You let know your preference, then you are gone , no way you gonna get it.The service at the restaurant is so slow that your stomach never gets filled up.
To top it...


More  




“almost excellent”


Nice rooms, great hotel surroundings, various levels of satisfaction from staff, huge breakfast buffet! Spent there a night due to a express-trip to north Goa. Very beautiful outdoor areas with loads of colorful flowers and a little bridge over the pool! I had an incident with the dinner; expensive and small portion and imaging I'm a small guy. Moreover, they...


More  



