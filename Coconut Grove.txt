
“Disgusting”


Where do I start. The property is run down and Management is absent. It all starts from the top. Marc the general manager is in his own world pretending he has the best hotel on this planet. Wrong buddy. Get up from your chair and take a walk outside to face the reality.
The breakfast is below any standards and...


More  




“Splendid and Relaxed”


The Hotel is great and the ambiance is awesome. The breakfast is wholesome and the pool is fabulous with clean crystal water. The Bar is sweet and sexy.
The overall stay was very comfortable and splendid.


“ok hotel on nice beach part of”


Cocounut grove situated on a nice betalbatim beach, nice hotel but could be so much better with a little more effort from the management has the hotel sadly has a feel of neglect about it and is know looking a little old and tired with its fixtures and fittings and a bit of re novation needed to freshen it up....


More  




“verry relaxing”


just returned from this resort,rooms are a tad tired bathrooms very.good points good location breakfast ok,evening meal hit&miss dining areas not good laid out tables not much ambiance non existence grubby walls just need washing in breakfast area,wifi you have to pay for,good hse keeping staff very friendly no actual leadership,like hey don't you think we wash these dirty walls?...


More  




“Good hotel, shame about the staff”


Firstly, this hotel is actually very nice. There are 40-something rooms, all with balcony or terrace and it's only a short walk to the beach where the sand is very soft and the Arabian Sea is warm and clean.
Unfortunately the staff appeared to view the guests as an inconvenience. We stayed from 15th to 24th December and for the...


More  




“Average, not at all satisfied”


We were there from 4-6 Dec, 2014 to celebrate our anniversary.
Frankly speaking the only advantage is that its very close to the beach.
Breakfast and dinner was included in our package. They were very average with limited options.
its better to eat in the beach shacks at your own expense.
The staff at the reception comprised of 3 ladies...


More  




“Near to the beach”


Close to a very nice beach with excellent beach shacks.The ' service ' in the hotel was very poor and we had to complain to the hotel manager,have stayed in a lot of hotels and have never had to do this before. it did improve a bit after this . There is a reasonable restaurant (Amiquero) nearby and an excellent...


More  




“Average resort”


Its an average resort with bad food though location is good since its closer to the beach, finding
the location is a challenge, so would request the hotel to put proper sign boards on the main road, its a old property, the greenery is good and so is the proximity to the beach


“Ok place - Not a great value for money”


Stayed here just for a night
Food was not awful
Some staff were friendly but were outdone by the ones who were not
Shower curtains were bad and dirty - btw they did not serve the purpose as well
Pool lamps had sharp screws not well screwed in - you could imagine what could happen if you are too close...


More  




“Great property that seems to have seen better days ....”


I have just got back from a mentally refreshing but physically tiring vacation in Goa. The little green state never fails to charm me whenever I go there. From the beautiful views of the sea to the warmth and friendliness of its people. I am not too much of a beach person, but my wife and kids are and as...


More  




“Decent resort.”


We went there in sep last week which is off season in goa. Rooms were decent and service was strictly ok. The big plus point was the location of the resort which is 300 mtrs from the betalbatim beach.The beach is simply awesome with clean soft white sand.The only company is that of starfishes. No beachshacks so completely have to...


More  




“Very nice property. Super close to the beach. Great Service”


We stayed here for 4 nights and loved every bit of it. We reached the hotel late in the night and were a bit skeptical since the access road leading to it was a bit far from the main road and deserted, but all our apprehensions vanished the moment we checked in.
The property was very beautiful. Accomodation was in...


More  




“Close to the Beach”


Arrived here without a reservation in early August, was able to get two rooms. Rooms were spacious, overlooking the garden area. Beside the bed, we had a couch, coffee table, two chairs, large flatscreen TV, closet and small balcony. They are located about a 2 minute walk to the beach, along a path that runs from the back of their...


More  




“Cottages by the beach”


South Goa is definitely more calmer than its northern counterpart. I personally love the beaches of South Goa, and Betalbatim is one of the cleanest I have ever seen. The location of this resort is top notch, being just a 2 minute amble from the beach.
Since we had been to Goa in the rains (my favourite time of the...


More  




“Perfect value for Money Resort”


We stayed for 3N-4D Package. If your purpose if a quiet, undistubed holiday wth picturesque surrounding, this is the place for you. The beach is only 2 minutes and almost deserted so that you can enjoy it peacefully rather than other crowded beach of Goa. Breakfast was a good platter with lot of options. However, more care about cleanliness/hygience shall...


More  




“Mixed feedback on Coconut Grove !”


I once again headed towards Goa this year with my partner to celebrate his birthday and got a very good package at Coconut Grove. I liked the location and the greenery in the resort so booked it with some issues getting thro to reservations. I was happy to get a good deal with breakfast and dinner, airport transfers and one...


More  




“Coconut Grove Hotel, Betalbatim.”


My wife and I stayed at this hotel for one night and were impressed with it no end. Courteous and helpful staff and a beautifully manicured set of gardens made this a very nice stay. The room was very comfortable with one of the largest double beds I've ever seen!


“Fab.....”


The hotel was wonderful. The rooms were clean, comfortable. The best part was beach was so nearby you can watch sunrise every morning. Food was also good. Friendly and very helpful staff
Overall good experience


“Worth a one-time visit!!!”


My family, comprising my husband, my 10-month old son, my 90-year old grandmom, my mother-in-law, my aunt & myself, stayed at Coconut Grove from 18th - 20th April, 2014. As requested, a wheelchair was kept ready for the older family member and two close by ground floor rooms were arranged. We appreciate the hotel staff for heeding to our special...


More  




“Our little paradise”


Back home after yet another wonderful stay at this beautiful resort. Can't wait to go back already !! Have to work my charm on hubby dearest for an 'early vacation '. This resort has the nicest people you will ever meet, always smiling and so very helpful. From the gardeners to the General Manager, all are absolute stars. Thank you...


More  




“Service let's this hotel down”


Well located and very clean and tidy. The hotel gardens were very pretty and well maintained. The food at breakfast was ok but limited choice. Lots of chicken sausages. Which part of a chicken is that?
The staff were unfriendly and unhelpful. Reception was often unattended in the evening.


“Overpriced and disappointing”


Most of the guests spoke Russian, so if you do too that may be fun. There General miserable affect and constant drinking may be off putting. The bed was not comfortable. They have many television options in most major regional Indian languages, but the menu was all Russian. I found the town of benalim, much more friendly and appropriately priced....


More  




“Fantastic Location!!!”


Coconut Grove is an ideal destination if the objective of your trip is to spend some peaceful and quality time with family/friends (don't visit if you are looking for a vibrant Goan experience within/easily accessible from the hotel, else have a hired/owned vehicle for traveling around). The best part about the hotel is the easy accessibility to the beach. Also...


More  




“Disappointed!!”


Well what a disappointing place...Pictures look Great on the Website, but when you actually get there its not what you see on the Tin!!!
The place is looking tired it actually looks like all the money goes on the gardens in the middle off the Complex.. It needs a GOOD CLEAN and a PAINT JOB!!
Went for Breakfast on the...


More  




“Worst experience.. Ever!!”


After staying in ramada.. came to this hotel / resort for a night.. It is rated as 4stars.... I dont know what kind of people would give this 4 stars.. It is 2* according to american standards.. Breakfast terrible.. Found a small cockroach inside a napkin in the restaurant during buffet.. coffee cold.. asked if they have any toothbrush/paste.. not...


More  




“over priced, insect in food, delay in serving”


Went there for lunch as we were staying in a hotel nearby which didn't have the kitchen operational. Reached at 2:00 pm with 7 people including me.
the resort and restaurant looked nice. no AC in the restaurant, not switched on even when requested.
Ordered pineapple and watermelon juice without looking at the menu. was priced exorbitantly at Rs. 130....


More  




“Very comfortable, good service”


This was a quick weekend stay, for a quick break. And its our second stay in the same Hotel after about a year.
Clean, decently furnished rooms but bathtubs could have been in better shape. Preferred the 1st floor to our earlier ground floor rooms.


“Awesome experiance”


The service was good.....food was amazing, specially the prawn biryani and the lobster...thoroughly enjoyed the 2-night stay.
The room was nice and cozy....perfect for a couple getaway....enjoyed our time there at the pool and at the room. Convenient location, can head to colva beach for some hussle and bussle, or just relax at their own beach exit.
In short, was...


More  




“Review of Coconut Grove hotel Betalbatim”


We go every year to Coconut Grove for a weekend stay especially during May-June and we enjoy it so much that we dont bother to go to any other place. The rooms are great,ambience is great, superb food and above all fantastic hospitality with the staff being really friendly ,helpful and prompt to respond to anything. The smiling ,courteous staff...


More  




“Great Time...!”


We wanted to visit south goa and decided to stay here. Thus was our first time and we were glad to make this choice.
Besides the staff we were the only Indians in the resort. So clearly its famous among the westerners.
The resort is well done. Good facilities like restaurant and a swimming pool.
But the best part is...


More  




“good but could be better!”


the resort has beautiful reception and a wonderful garden but somehow i felt that the management is all keen to maintain the lawns and garden only. dont go by the professional pics. the resort is way old property which might have been awesome some 20-30 yrs back but now its kinda after-retirement abode for the old foreign tourists. i will...


More  




“Fantastic Experience”


We had a wonderful time in the 5 days we were there. This place is practically on the beach which is quiet & simply gorgeous especially at sunset. It's not a big hotel but we found the overall services & cleanliness good. The staff at Reception are friendly & helpful & assisted us with all our requests ranging from hiring...


More  




“Super Location”


We were on the lookout for a hotel that fit our definition of a holiday in Goa - a closeby beach, good food, peace & relaxation & a romantic walk on the shore. It was pure luck that we found this cosy little resort tucked away in a peaceful, quiet village, nestled amongst swaying palms !! Our room was quite...


More  




“Wouldn t stay here again”


We stayed in this hotel for 2 weeks, we had stayed here before a few years ago , you can see a big difference in the change of management.The service in this hotel is pretty poor throughout, with breakfast being the worst .Their isn t anyone with any authority around and the staff act like their in a play ground...


More  




“mesmerizing”


I chose this as my honeymoon destination and loved it totally. couldn't be more satisfied. friendly staff, nice hotel, wonderful beach, good food.
very peaceful and beautiful location.
cheers
http://www.beingtraveler.com


“Don't bother”


My wife and myself stayed here for 2 weeks in Feb 2013.On arriving the reception area is inviting and nicely laid out.Our room was a nice size but let down by a mouldy bath and shower curtain and dingy bed sheets.There was a daily inconsistency with the supply of refills of consumables ie shower gels,coffee, tea etc.We had to ring...


More  




“Really not worth the money.”


First impressions were good - the grounds are lovely and the reception staff are very pleasant. However, the cleanliness of the room was shocking. The shower curtain was mouldy, the table in the bedroom was encrusted in dirt, the fridge was filthy and everything else was generally a bit grubby and could have done with a good scrub. I've travelled...


More  




“Service Quality”


Ambience is good. Close to beach hence you save time. You can land on beach early morning spend time , take bath at beach & be free by 10 am and have full day to travel to other places. Good beach shacks at Colva which is close by for sea food and Domincs is good and reasonable. Staff is junior...


More  




“IF ITS GOA ITS COCONUT GROVE”


Coconut Grove Resort Revisited.
Going back to Goa never fails to enchant. Going back to a Resort you have liked is always with a little bit of trepidation. Have things changed, will it be the same, have the staff been moved around, would the stay be different? – a thousand questions attack.
So there are a thousand moments of relief...


More  




“great value for money hotel”


this is a charming little resort - very clean, and good for a family visit. right at the betalbatim beach. the staff off-season is less but still helpful and warm. very reasonable on the pocket. Definitely a must-visit


“COMFORT AND WARM”


This year a happy accident led to the discovery of a great resort, the Coconut Grove Retreat, on Betalbetim beach, a stone throw away from Nanu, which has gone down in the last few years.
The Coconut Grove is a dream come true. Nowhere else in the vicinity can you find the same mix of comfort, service, food at this...


More  




“Nice Hotel,great location”


Enjoyed our stay, It was off season, So we had just a few guests around, the only draw back was since it was off season the staff strength was reduced and service was a bit poor.Though Bonnaventure a steward there, tried his best.
Got a great deal on agoda for its cottages.
The rooms were clean, the pool was also...


More  




“For Peaceful holidays”


We stayed in Coconut Grove from 23rd-26th June 2012 for our 3 nights exploration trip to Goa. It was our first trip to that part of the country & we had a great time, specially beacuase of whether conditions in Goa. as it was monsoon magic :-)
I booked this propoery by chance & must confess, we had great time...


More  




“tranquility and calmness”


I really recommend this small hotel for those who desire peace and quiet (i.e. there is no touristy entertainment or loud guests). It's close to the beach, and it's surrounded by fields and, nomen omen, coconut groves. I loved it - but I wanted to avoid people (you might wonder why I chose India :)
The rooms were clean and...


More  




“Relaxing, fantascic garden, great food”


Stayed 1 week in May 2012. Silent, relaxing atmosphere, polite staff. The most beautiful garden we`ve seen in India. Every room has a private area with comfortable chairs, matraces etc. among palm trees, bush and flowers. Very quiet at night, AC is ok, not so noisy. Rooms & bathrooms are big and well equiped, lots of clean towels.
The breakfast...


More  




“Quiet and cozy”


A small and cozy hotel, very green and very peaceful. Ideal for family holiday. Our air-conditioned room had a big double bed, two armchairs, a coffee table, a sofa, and a fridge. Tea and coffee-making facilities. The room was comfortable although a bit tired and not soundproof – you could hear your neighbors talking. The bathroom with a bathtub was...


More  




“A gem of a place”


visited March 23 - 7 April 2012,
Although a small resort, but wonderfully positioned. The pictures on the wedsite did not do it justice. The surrounding gardens have since taken shape since the photos were taken, and are beautifully laid out. Very quiet, and peaceful. But still lots going on outside. A short walk through the coconut grove to the...


More  




“Tired and noisy”


We stayed at the Coconut Grove from 02/02/2012 for 7 nights as a base to then find our own accommodation for a further 3 weeks holiday. Whilst the hotel looks pretty from the exterior and the gardens are pleasant, the hotel is very tired and needs a refurbishment. The pool is much smaller than it looks from the photographs, and...


More  




“Excellent and mesmerizing”


Chose it a my honeymoon destination after lot of research and was more than satisfied with the hotel. Rooms were clean and good.beach was near hotel. Food was nice, peaceful hotel.have lots of memories attached to this hotel.


“Nice hotel, far from the crowd”


We stayed 3 nights Coconut Grove.
The hotel is small, very clean, a lot of trees and flowers and the garden is well maintained. The beach is very close, with several shacks that rent out sun beds and serve good food. Advertising is in Russian, as this place like many others in Goa serve the Russian Tourist industry looking for...


More  




“Wouldn't go again!”


We have been to Goa anually for the past 6 years and stayed here for the first and last time. Good points: Rooms wereclean, reception staff helpful, close to the beach.
Bad points: The pool was filthy.
Staff were seen to be cleaning it each day yet the water was murky and had things living in it (I have photos)....


More  




“Our second visit to Coconut Grove”


Just like we did last year, we did spend 2 weeks in Goa in the Coconut Grove during our travel in India. of course we looked into other hotels in Goa as well but we liked the Coconut a lot last year so we decided to go there.
It was nice to come back. A warm welcome at the reception...


More  




“Great location but........”


WHATS GOOD: Located 350m from a fab beach with loads of shacks where a curry is £1.50 and a beer a quid. Rooms big clean, free aircon, sheets changed every day, towels for pool. mini bar.
WHATS OK: Pool looks a bit murky (never see anyone swimming in it) and its not as big as it looks from the hotel...


More  




“Beautiful spot”


I stayed here fo 5 nights after travelling around Rajasthan for 3 weeks. It is close to the beach which is great. The food was excellent, drinks cheap. Lovely pool. Staff were fantastic.


“Great Location but Restaurant needs improvement”


I stayed at Coconut Grove Resort on a Business Trip. Location of Resort is excellent and has nice lawns. Rooms as nice and clean. Beach was not at all crowded in morning and just a short walk away from Resort. But the key issue of Resort is it's Restaurant. I shockingly found a cockroach in my breakfast plate; With that...


More  




“Great time”


A pretty good place, if you just want to unwind and relax. The beach is just a stone's throw away and is very serene. One can easily spend hours just walking along the sea for 2-3 kms in either directions, or even roam in the Suru trees along the beach.
The rooms are nice, and 1st floor rooms have a...


More  




“Excellent Place to stay in South Goa”


Had gone to this resort on recommendation of a friend. One of the finest resort i have seen in South Goa. It consists of individual studio rooms instead of a tall building. The beach is just a 5 minute walk at the back of the resort.
The food available for breakfast, lunch and dinner was excellent. The silence was heavenly...


More  




“Wonderful : "Peaceful Paradise in Goa"- Would Surely Return.”


I along with my 2 friends, planned for a re-union trip to Goa.We wanted a peaceful and relaxing vacation.After a lot of research we bumped into Coconut Groove thread on TA.We were surprised to see so many great reviews.We agreed to give it a try.The booking was not a problem.We did it online and immediately received an acknowledgement.Spoke to Avinash...


More  




“An enjoyable trip”


I stayed between 28th and 30th July 2011 at Coconut Beach Resort, Betalbatim, Goa.I The Resort Team is Hospitable. I interacted with
Mr Avinash, Ms Michielle, Ms Malika at the Resort. They are very hospitable.
Mr Mark, General Manager is very supportive and arranged vehicle as per my
convenience.
The resort room is quite good. Housekeeping staff is very hospitable....


More  




“peacfull n amazing..........”


Stayed in coconut grove for the first time and really think that it is one of the best resort in goa. Lets start with resarvation, as it was not a problem because as it was done by Wonder Hub guys in ahmedabad ( Rakesh Mishra ). Once u reached ur station i.e madgao, A person will be waiting for u...


More  



