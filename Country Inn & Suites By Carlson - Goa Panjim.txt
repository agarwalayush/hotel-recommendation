
“Enjoyable stay in goa”


Had a wonderful time in the hotel. Stayed in jan 15 for 3 nights very nice room wonderful hospitality from check in staff, room service, restaurant service. Having wifi was a problem as they give one per room. Staff always ready to help. Had complimentary airport pickup which really help especially when you come from abroad. Breakfast was nice, had...


More  




“If you like to stay in Panjim”


The hotel were located in downtown, relatively strategic place in Panjim. The room were great, the food was okay and the service were excellent. They have a nice and quiet rooftop swimming pool overlooking to the surround area.


“Best service”


We stayed there for 3 nights and our stay was pleasant. Every single staff person was helpful and complimentary breakfast was really good. Manager, reception desk staff was always friendly and eager to help. I am very thankful to the Entire hotel staff who made our trip more enjoyable and comfortable


“Horrible Telephonic Experience”


I had spoken to Mr. Gagan Sharma, regarding a glitch in my reservation. I have booked a total of 10 room nights (2 rooms for 5 nights each), but he is inconsiderate to my requirements. I made the reservations 3 months back, there was no point of mandatory Gala Dinner at that time, now suddenly there is a mandatory gala...


More  




“Worst customer care and service :(”


I had a very bitter service experience with this hotel in panjim especially with their accommodations manager " Gagan Sharma". He was quite arrogant in replying to my request of rescheduling the stay and neither he was ready to understand our concern. I have never encountered such a rude and non-caring behavior ever. I think hotel management should look into...


More  




“worst stay experience ever”


I stayed in this hotel and had my worst exprince ever. I needed a doctor at 8 in the evening urgently. I asked the hotel staff for a doctor and they said he wil be there in one hour. I kept waiting but at 10 doctor called me and said that due to some miscommunication he reached to their othr...


More  




“Excellent Stay”


We had a pleasant stay at the hotel. The room was good, staff was courteous and the overall facilities provided were good. The breakfast spread was also good. May be the a-la carte menu & dishes need to be improved. This hotel is good for short travel with good accessibility. I would certainly recommend this property especially if you are...


More  




“Excellent Customer Service”


My family enjoyed the stay in this hotel. Each and Every staff delighted us with their great customer service. Food was very tasty in the restaurant. The room was smaller, but customer service was excellent.


“Stay not worth value for money”


I stayed at Country Inn at Panaji Goa for 2 nights during 12 th to 14th nov 2014. While the staff is all coutesous, the problem lie with non human stuff. the room ac was making all kinds of horrible sounds which could not be taken care until late night. The room window was also not easily possible to be...


More  




“Amazing food of country inn suites Goa..”


Country Inn & Suites GOA Panjim is Located in heart of Goa. If you are interested to live away from the beach crowd as the Beaches are just near by and the normal commodities are cheaper there compared to the beach belt.Comfortable beds, Free wi-fi...and delicious attractive various kinds of food. Especially the excutive Chef has give us a personal...


More  




“Not as good as its other properties”


The staff was friendly but we were not very happy with the rest of the things - the food was average, the gym opens at 8 am (!) instead of the customary 6 am in other hotels, they switched internet passwords of the two rooms, only one device per room is allowed which beats the point when each room has...


More  




“Nice business hotel”


Typical city country inn hotel....which is more like a business hotel. Stayed 2 nights in july and was not dissappointed. Lovely rooms, but bathroom was a little cramped. Service was good, breakfast spread and service during breakfast was excellent. Overall a nice stay, specially convinient as very near to the lovely casino deltin royale.


“Good Hotel”


We are in room no 201, the room was, well furnished, specious and comfortable. The room is having small balcony with table and chairs. The breakfast is having wide variety to choose. The veg and non veg food is tasty and delicious. The service is good, staff is polite and helpful. Over all the stay in hotel is comfortable.


“A refreshing change”


Got a room with a pillar. Not at all bad as some of the comments suggested. They are part of the standard rooms offered. As I was travelling single I did not face any discomfort. Staff is pleasant and extremely helpful. Stayed only two nights as on a business trip and will definitely return with the family for a longer...


More  




“Excellent stay with delicious food”


Excellent Stay with the staff so helpful & corteous. the food was excellent speccialy the morning buffet wherein we got all the varities veg as well as non veg the rooms was also neat & clean. the hotel location was very nearby the cruise .. the market was also neraby the hotel for geting local stuff overall a very comfortable...


More  




“Country Inn, Panjim - Wastage of money”


We had been given room No 710. There was a pillar in between room. Since, we were already paid and as per hotel staff there was no option available, we have to stay in that room. There is no table in the room for taking food. You have to use your bed for that. No variety in veg Menu as...


More  




“Excellent Hotel”


Hotel offers a variety of food options, clean room, pool overlooked city from the rooftop - was very calm and relaxing. Staff were very attentive and helpful. A lot to see and do in the area - very walkable.


“Good Business Hotel”


The hotel is quiet good to go and hang out with colleagues rather than with friends. The service is quite decent(They din't had idea about why WiFi was not working other than that quiet good service).Had a nice stay. The terrace view from the swimming pool is coooolll. Please, dont miss the morning dump in the pool.


“Nice stay in the hotel!”


I had a very fine and enjoyable stay at the Country Inns and Suites. I would recommend the hotel to all. The staff was very courteous and helpful. The hotel is placed right in the middle of the city near the Panaji Bus Stand and is very accessible.


“Bad Design Good Service..!!!”


Stayed @ This Hotel beetween 25th Sep and 27th sep..
Check Inn was smooth..( Although didnt replied me back on email i sent 5 days ago )
Rooms R basic n Big Drawback Is Pillar just beside bed u have to b very careful.
Tv Reception was Bad.
Service is Top Notch..
Breakfast was Superb with Smiles around and feedback...


More  




“perfect for business”


a nice hotel very much located in the central of the city . It is very next to Bust stop and taxi stand . Travelling in and around in very easy from the location of the hotel. The breakfast was very nice and elaborated. Service was very prompt and friendly. The elevator of the hotel has a different feel ....


More  




“Had Awesome stay @country inn suits &carlson goa”


hello guys &girls i had recently been to this property had a good time and the staff were so helpful and the hotel is centrally located in the heart of the city of goa ... specially the casino is just five mins walk from the hotel and the locality is very clean and good .as u get out of the...


More  




“poor facilities and room design”


the room had pillar which breaks our head once we wake from bed shows poor interior design,also the swimming pool and gym fitness is for name sake on top 8th floor as it is very small,so food price is very high compare to quality and quantity


“Opinion”


A good hotel to stay. Located in a quite locality but at the same time fairly close to the happening spots in Panjim. Hotel is clean and well maintained, however service standards needs a bit of improvement.


“Total value for money”


This hotel is located right in the heart of Goa s business District . The river is a walk away . Beautiful river view rooms make your stay even more worthwhile . Boat cruises , casinos , the beach , all things you come to Goa for are a stone throw away from the hotel .
Breakfast was complimentary ,...


More  




“Had a nice stay in Country Inn”


We had a nice stay in Country Inn. The staff is very helpful. The location of the hotel is excellent, right in the center of the city and near to the bus depo. Rooms were neat and clean. Room interior was well done.


“Clean Boutique Hotel in Panjim”


I checked in and stayed at the Country Inn & Suites for 3 days and 2 nights. My experience at the hotel was beyond my expectations considering the price i had paid. From cleanliness, bathroom facilities to the new and clean linen, to the good spread of breakfast, i was very satisfied. Reservations attended to my need of a nicer...


More  




“Excellent Experience...Complete Value For Value”


We stayed in Country Inn & Suites, Panaji Goa in our 2014 trip to Goa...We wanted a good place to stay close to Panaji (since we are more interested in traveling around in Goa rather than centering around one area)....This is a new property of Carlson group. The major positives according to us are -
1) Good location and reasonably...


More  




“Good place to Stay”


Great hotel in the heart of the city and staff was very helpful. It was so heavily raining but hotel authority ensured that the airport pick up happens on time. I really appreciate they due deligience.


“Overall a Nice Hotel to Stay”


Nice Location as its in the Heart of the City. Easy to roam across. Cabs and Bikes are easily available just outside the facility. Courteous Staff. Nice Ambience and Delicious Food! Value for Money! Also its a newly constructed Hotel so everything is new. Felt good to stay here. Would like to be back again!


“Value for Money”


Very good hotel at the heart of the city. The food and the staff was very good. Great value for money. It was very close to the Panjim market area and the river. The staff was very courteous. Would highly recommend this hotel.


“smallest room ever, horrible food, crooked taxi services”


Water doesn't flow out of bathrooms, it comes into bed rooms. Screwed up view from the windows. Pool is so small, that water spills out if kids are swimming too. Did you ever see 8 AM to 8 PM for gym. This hotel has it???!! Every dish in complimentary breakfast has been tailor made not to consume it. Bacons were...


More  




“Awesome Hotel”


We had great experience while staying at this place, notable things are the hotels staff friendliness, hotel is very tidy and clean and its prime location, main beaches like Baga, calangute, Anjuna vagator, and Old goa are within range of 15 kms and its located nearby banks of river Mandovi. I will definitely consider this hotel again for next visit...


More  




“Excellent Stay”


Superb place to stay for business travellers the location is also good. at the centre of the city so pretty accessible. the staff is very courteous and humble. excellent food quality. would recommend everyone.


“good”


Hotel was fine at service. I loved my stay at the hotel. I t was well within the reach of all major spots in the city. all together i enjoyed my stay at your hotel. Country INN and suites is a very good hotel with all facilities required for any class of people.


“My stay”


Good people. Room not convenient with a pillar in middle; not changed even when requested.Swimming pool too small and the timings not convenient.Food was good; breakfast was very good.Location is not typical Goa! No outside view!


“Very comfirtable - value for money”


I checked in to the hotel at one rainy afternoon of August. It was an excellent and very friendly environment. Right from the security to the reception all ready to provide me all comfort that they can offer. I specifically want to mention Abhishek Goswami in the reception who always there to help for any need; and the chef of...


More  




“good”


Good location.. Great staff... Good food.. U must try the chicken xacuti and chicken cafrel. Also try the bongo mocktail.. Perfect..
Everything is nearby.. Had a comfortable stay.. Prices r reasonable.. I wud recommend to everyone.. Nicely maintained.. Clean.. Close to market..


“Great hotel in the business district of Panjim”


Very nice hotel with well appointed rooms. Hotel staff was courteous and very helpful. They were almost eager to please us. The hotel is located at the heart of the business district of Panjim. Since we were on a leisure trip, we wanted to go to the downtown area for a dinner. This was a long walk. This was the...


More  




“Recommended for Business travellers”


Good location. Good Service and Room facilities in line with Business travelers requirement. Other facilities like gymnasium, swimming pool available.The staff of the hotel was courteous and helpful. Would recommend to business travelers and Family.


“excellent service !!”


Excellent service & feed back taken for every service given which I found different. Over all good ambience and good hotel for business stay in panjim also the rates a reasonable. Location is also perfect.


“Nice Clean Hotel”


Pros :
1. Very Well Maintained property.
2. Close to City Center.
3. Good Breakfast & Food.
4. Super like the staff.
Cons:
1. Spa & Pool are quite small.
My personal experience has been good with this hotel.
The Front Desk Manager Noticed that it was my birthday and when we returned to our room in the night we...


More  




“Bussiness or leisure, this is the best hotel in the capital city”


We have stayed in many hotels & resorts in this region but this time we wanted to try something new, thus we came across this beautiful hotel " country inn & suites by Carlson" in Panjim. This is a newest property in the town and extremely affordable, and yes they have kept their brand standard very well maintained.
I actually...


More  




“Great brand but certainly doesnt live up the expectations”


Let me start with perhaps the only two good things - location and the buffet breakfast. The rest, RIP.
Firstly, it was a task in itself to locate the place as not many people were familiar with the place. The cabbie didn’t know it either. Upon reaching the gate, interestingly there was no IN and OUT boards on the gates...


More  




“Good experience!”


For casino lovers, this hotel is absolutely perfectly located at a 5 min walk from the off shore casinos. It's a no frills hotel but comfortable and pleasant. The breakfast buffet is lavish like any other Carlson hotel. Staff is courteous n helpful. Overall a good choice.


“Excellent value for money”


Location advantage at Panjim makes it a good option. The rooms are good. Clean and well maintained. Hosts a good buffet in the morning. Well connected across Goa. If you are on a holiday and plan to try the boat cruise, this is a walk from the Mandovi river- boating point.
Has a clean pool on its 8-th floor in...


More  




“Very good hotel and friendly staff”


Had a very good experience staying at this hotel. We reached the hotel atleast two hours prior to the standard check in, but the front desk let us in without any hassle. The rooms are impeccably clean and organised.
The best part about this hotel is its location. Located at a walking distance from all major attractions at Panjim, the...


More  




“Hotel staff delays to act on cancellation request and hotel declines refund of charges”


I booked the hotel for 2 nights but had to cancel my trip to Goa for some reasons. As per the reservation confirmation, I were to be charged for 1 night unless I cancel the reservation 3 days in advance. If I did not cancel the reservation at all then I were to be charged for both nights.
I called...


More  




“Amazing stay at dirt cheap rates”


We were in Goa for 2 days (budget stay) but we had no place to stay.. we searched many budget places in and around Panjim but even the shady places were charging a bomb during the off season. Then we went to Country Inn only to check the prices for a room knowing well that we will not be able...


More  




“Nice city hotel for business travellers as well as families”


We stayed at this hotel for 2 days in mid-May. Nice clean rooms with good ambience, free and fast wi-fi and sumptious break-fast. Also noticed the hotel is patronised by some entrepreneurial organisations - everyday about 50 people came from the conference floor in large groups landed up at the buffet breakfast around 10 am .. when I checked I...


More  




“Great Stay”


I stayed at Country Inn Panjim in June 2014, it was one of the best stays I have had in months, I am a frequent travellers and I must say it was a real pleasure staying in Country Inn in Panjim, great environment, excellent services, wonderful staff and aoverall a great experience.


“A Typical City Hotel - Value for Money”


We had never really stayed in Panjim during our numerous trips to Goa. So this time,we decided to spend two days in the city of Panjim. The hotel is located, slightly off the usual hustle bustle of the city - which some you may find good and others may not like it. It is located very near the State transport...


More  




“Fantastic Place. Very Friendly Staff. Good Food & Breakfast”


I would like to thank the staff of Country Inn & Suites, Panjim Goa for the wonderful stay I had at their property. I was on vacation with my family consisting of 2 small kids. The entire staff ensured that the special needs of my kids were taken care off. Mr. Gagan Sharma (front office manager) would check on us...


More  




“Good brand but average hotel”


Was here on business. In the heart of Panjim the location is average but OK by business traveler standards. Rooms seem dated and worn out and aged.
Internet connectivity was average.
In room dining was good, fast and good value for money.
Panjim is approx an hour away from the airport.


“Astounding Stay !!!”


I've been to this hotel couple of times for my business trip, but this was the first time I booked it for my personal holiday. Coming to the hotel, staff is extremely helpful and cooperative and they will leave no stone unturned to make you feel comfortable.
I want to appreciate following people from their staff, who went an extra...


More  




“Very nice property in the city”


Was there for a seminar and this was walking distance to the venue. The property was nice and service was prompt. The breakfast was horrible with not much choice and tasteless food. Avoided having food there and chose to eat elsewhere.


“Value for money”


Stay here everytime when i m in goa.Nice hospitality, co-operative & friendly staff. Some of the guys i know personally in staff team such as Prabhat,Sanjeev, Mr.Chauhan. all are nice. Overall great property.


“very good”


staff is very supportive and location is good. because of staff co-operation stay becomes very pleasant. Only thing for improvement is to add more variety in main course menu. overall it was really good


“No special charm of a hotel. No swimming pools, no club, no bathtub.”


The hotel location is not great at all. No amenities like gym, pub, swimming pool. Stay in the hotel is boring. The breakfast buffet wasn't customised for guests. Majorly south indian dishes. Not cherished the stay. Not a value for money. Next time I would go for some other hotel near to beaches.

