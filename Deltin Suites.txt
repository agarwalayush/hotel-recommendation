
“Overall a Satisfying stay”


I shortlisted and booked this hotel in Goa for my Anniversary trip. The Hotel didn't disappoint me. I booked the package through their website and everything promised was given to us and there were no hidden charges. :-)
Transfers from Airport was very prompt and timely. Even though my flight was delayed twice and I informed them about this at...


More  




“Great 5 star hotel experience”


I stayed at Deltin Suits last week for 3 nights. Had great experience. Good food. Welcoming and caring staff. I was visiting the hotel after 6 months, Yet all my choices were remembered and catered promptly without mentioning for those.
Food quality has been improved a lot with some new fantastic menus.
Reception is cordial. Housekeeping are fast and prompt....


More  




“Warm & welcoming”


I stayed at Deltin for New year this year. The property is close to Candolim beach & approx 45min from Dabolim airport.
The property owners went an extra mile to decorate the property for X-mas & new year which was nice.
The suites are reasonably priced & optimally furnished.
Breakfast buffet has plenty of options for vegetarians. Food at hotel...


More  




“Bachelors trip to Goa!”


When 10 guys go to Goa for a short 3/4 day trip and that too to celebrate the final bachelor days of a buddy, all they need is a few good hours of sleep at the hotel and some nice breakfast. We got that and more at the Deltin Suites!
Located in the heart of North Goa, the hotel has...


More  




“Great hotel. Excellent customer service!”


I stayed at the Deltin Suites across the New Year period. It was a great hotel. Clean. Good customer service. It had 2 good restaurants (particularly recommend the Emperor). We had a standard suite which was spacious with sofa, lots of cupboard space, shower, satellite TV, balcony etc. etc.
The only thing I would say is that the wifi was...


More  




“Exilent hotel”


I stay in this hotel in 7night,,,this is a very good hotel nice breakfast clean water swimming pool nice gym pool table spa gym and all....room are very spacious with little kitchen,lockers ,sofa set dryer and all basic needs.....staff is very good and specially the breakfast was delicious with veg and non veg...


“Vry good experience”


Gr8 hotel & location breakfast/ dinner is also good vt varieties..locatin is the center btvn beaches & panaji
Hotel is very clean & rooms are well maintain
Person nxt to hotel provide bike & taxi on reasonable rate..
Over all good experience.
Even service staff is also good


“Nice place with excellent service and sumptuous food.”


Superb place.breakfast and hospitality worth a mention .extremely cooperative front office and house keeping.had a wonderful time on my birthday with the family.worth a stay.cuisine is splendid with good South indian food and surprisingly nice Punjabi delicacies. Kids room and bar is worth a mention but above all is the front offices serving 24 hours with a smile.


“nice place”


good hotel... extremely well priced..... and good for people wanting to go to the casinos.... nice big rooms.... clean... good staff Nd good food.... only negative if any is probably the location one needs to drive for a few km to get to the nearest markets or beaches...


“5* luxury @ 3* price... would definitely recommend”


We have stayed this week for 2 nights, had few issues which were taken care by the lady who is managing the hotel (RDM i believe)... we check out and stayed in one of the 5* in candolim and again came back to deltin for the last 2 days... it is centrally located.. 5 minutes drive to candolim, 15 minutes...


More  




“Good time with friends”


We 3 friends stayed from 1st to 2nd November 2014. Had great fun. Breakfast was amazing, plenty of vegetarian options. Rooms are Suites with a living room, bedroom and balcony overlooking magnificent pool. Hotel is built on Portuguese theme. Pooja at front desk was helpful in check out.
Being a hotelier, I know that it is a great pick when...


More  




“Good Clean Hotel”


I spent a night at the Deltin Suites last week and was really impressed by the hotel. The frontage of the hotel is a HUGE Portugal Mansion which houses the reception as well as a bar and the rooms have been build behind this huge mansion. The location of hotel is really good as it is in close proximity to...


More  




“Deltin Suites, Goa: Excellent Hotel”


The hotel located very strategically. The staff is excellent, rooms are very nice and spacious. Though it is not a resort property, however, they try to give you an experience of that sorts, which one will surely love. The only negative was that there was some construction going on, hence can become a little noisy, but that is not much...


More  




“Excellent Hospitality”


Our happy experience started right from the Airport with a team of smart executives checking us in, tagging our baggage and getting us seated.
A welcome drink at the entrance and we were in our rooms with the baggage neatly stowed.
For the three days we were there the staff gave us no opportunity to ask, we looked and we...


More  




“good place to stay”


The hotel is clean, neat and having a spacious rooms to stay, The staff are ready to help you in all the possible way as possible. They have arrange a private vehicle to drop us to station when there was a strike of taxis and tourist vehicle without any cost, really thankful to them. In short a Good place to...


More  




“Best Property For avid Casino Fans or Otherwise too”


After a two month long haul I shortlisted Deltin Suites Nerul for our small get-together which we 6 friends plan every year and seems like we hit a jackpot this time by sorting down on Deltin. Its one the best place to be in goa.
First Thing first
The Booking Process was Smooth- Thnx to Chaula at Sales office and...


More  




“Deltin Suites , Just Lovely”


Excellent property , delicious food and high quality hospitality .Located in lush green area . Lots of plantations are seen in campus . Rooms are beautifully designed. Optimal use of space .
The swimming pool is very good.


“Good Spacious rooms. Nice experience”


I had stayed at Deltin Suites on July 18-21.. The hotel is part of the Deltin Chain which runs the Deltin Royale Casino.. The Rooms are quiet spacious and best suited for family and groups since they have a sit out area in every room with partition done. The swimming pool is clean and located right in the centre of...


More  




“Good rooms..very average service”


Travelled to Deltin Suites in July 2014..here's my review:-
Pluses: Room size, good food, brisk check in, free wifi, good pool, complimentary entry & transfers to DELTINE ROYAL casino.
Negatives: room service
The moment you enter your room you will be surprised by the sheer size of it. Rooms with pool view are advisable. Other rooms have no view, actually....


More  




“Spacious rooms... Below par service”


The rooms are spacious ...and beautiful...though only one side of the hotel has decent view..make sure to get the right one!! The room service takes like forever to be at your service...this needs urgent notice.


“Good Experience..”


Awesome Breakfast, Lunch & Dinner menu with all Veg and Non Veg delicacies.
The best part is suites itself with great comfort, service, decor and ambiance
Not so far from beaches like Siquirim, Candolim and other good beaches.
Good amenities and very clean swimming pool.
I would really recommend this hotel to stay in Goa..


“GOOD FOR CONFERENCES, EVENTS AND SEMINARS”


The DELTIN SUITES on Nerul Candolim Road is newly constructed with the swimming pool the center of attraction. The well equipped Whisky Bar overlooks the pool & so do fifty percent of the suites. The Chef and food are simply outstanding. The suites are spacious with a kitchenette and mini fridge.
Buffet Breakfast is sumptous. The hotel has a nice...


More  




“Good Hotel but a lot of Power outages”


I have just come back from my trip to Goa where we stayed at the Deltin Suites in Nerul. The hotel is very good with all the amenities provided. We checked in on the 6th of June till the 9th of June. We were a group of 7 people including 2 couples. The suites were good but one thing that...


More  




“Very nice hotel at an affordable price”


Absolutely no complains. Everything we requested for was given to us and the staff was most accommodating. They were friendly, helpful and knew their job very well. They could speak good english. Food was average but no complains. The rooms are well furnished with everything you need, 2 AC's, cable tv, hot plate, kettle, etc. The layout of the room...


More  




“Stay at Deltin Suits”


Not quite warm welcome at reception. There was very low occupancy when I checked in . Still the receptionist offered me a most remote and un preferable suit that was on 3rd floor and at the end of a long unlighted lobby. Very small and slow elevators. The offered suit was smelling bad due to closure for may be weeks...


More  




“Super location! Super rooms! Clean pool! Nice staff! Great food!”


This hotel is good in all ways. location wise, cleanliness wise, room wise...vehicles and taxis are available very convinently right at the hotel exit! Its near from all good hangout places...the breakfast buffet is really good and extensive. Staff was polite and helpful. Service was quick. The pool is kept very clean. Best part was the casino! Had lots of...


More  




“Clean, quiet with great location”


After staying in many hotels in India in our month of travels, this was one of the best places we stayed.
Most rooms overlook the sizable pool which was kept very clean. The rooms are also clean, and fairly new with lots of room for our huge backpacks. There was hot watever whenever we needed it which was very hard...


More  




“Relaxing.....”


At good location... near to city and Candolim Beach. Very good staff. Very helpful and excellent service. A delicious food. And neat and clean swimming pool. Only thing to be improved at this hotel is lift and spa, Rooms are reasonably good sized and well maintained. A was with my friends and family in December-13. Had a good time and...


More  




“A truly nice place to stay......”


Been there for work purpose for 4 times in last 1 year and found the place to be improving each time....
Rooms are quite airy and classy with all the requisite amenities at right place.
Food quality is improving each time.....
This time was bowled out by peach artichoke salad, French omelette and bibinca....
Bar is also an nice place...


More  




“Over-rated”


If you are from North India don't dare think opting for this hotel. Food is cooked in western spices and everything you eat is sweet, even the dal tadka & biryani.
Ac in the room is fixed in wrong position. Very average amenities considering the price. Location of the hotel is a disappointment. Getting taxis to travel around Goa is...


More  




“Disappoint with service and amenities”


We were shifted to this hotel for 2 nights, On hotel's website there were many amenities mentioned.
but when we moved in i will say pathetic services. Many amenities were missing. when we asked for them they gave us just one reply we are trying for that. - that made me disappoint.
About Room - wrong position of AC, they...


More  




“Superb Experience @ Deltin Suites”


What better place for a honeymoon then the beach paradise "GOA" stay at Deltin Suites has been the best time that we could spend lovely food and a wonderful staff always ready to express their most warmth service especially at the Front office looking forward to many more visits here...


“Awesome Experiance”


Its Classy, fab and perfect location.. i had awesome time in here... and the location is such that you can go either to old goa or towards baga..Nice relaxing hotel with good pool..Excellent service and recommended for travel !


“Enjoyed our honeymoon!”


It was a nice hotel with very nicely equipped rooms. Staff was friendly. Only con was limited option for food. I m a great fan of pizza which they do not have. :)
It is far from beach but overall nice property in center of north Goa


“Good Relaxing Hotel... !!!!”


Nice relaxing hotel with good pool... Staff is nice and courteous... Buffet breakfast is average... Location of the hotel is little towards interiors (deserted lane during the night) and is about 3 kms from the Candolim Beach...
Rooms are nice and cozy... however toiletries provided (bottles of shampoo, conditioner in the bathroom) were not filled completely... seemed as if they...


More  




“Deltinn Review”


The hotel is very good and hospitable staff - Rooms are excellent. Food served is very good. It is value for money as other rooms around the area are expensive. Excellent service and recommended for travel !


“Deltin Suites a Admirable place to stay at GOA”


I had been to GOA several times and stayed with various hotels which are on the Beach or near to a Beach but this time had all planes to relax and spend time with the family. Though the hotel is at little distanced from candolim beach but we did not missed the fun of GOA. The hotel was equipped with...


More  




“Made my trip memorable”


Deltin made my 1st trip to GOA a memorable one. Very beautiful property includes a great gym a nice swimming pool , TT room, Pool table along with bar. Hotel room was very neat and clean , toilets were in excellent condition. A small kitchen in the room.
Staff was very nice & cordial , food was good. I will...


More  




“Awesome Place to stay away from home”


We, with our friends and families stayed at Deltin suites for 3 nights and 4 days. Its a very clean, beautiful place to stay with family and friends. The staff is very friendly and cooperative. We were there for Christmas holidays and the hotel and experience we had was very good. The live band on Christmas brought about a lot...


More  




“General”


There is much to praise & little to complain. The property is a new one with all modern amenities. Casino Royale owned by them is really a good one. If somebody is thinking of a relaxing holiday in GOA away from madding crowds of December but at the same time all wants all happening events nearby then they can look...


More  




“Mixed Feeling...Will need strong recommendation to visit again”


To start with Check-In its a little sloppy and quite slow. The staff was not very happy that we came at least I felt so. There was no smile no warm greeting.
I was there with my family, and my inlaws as well as my brother in law. I specifically preferred this hotel as they have all room with a...


More  




“Awesome rooms”


The rooms are spacious and the great Service. The staffs are courteous.The food is good not that great. The Pool is well maintained. I stayed in the Executive Suite which is good & Comfy. It was a semi suite.


“Good property, poor services and food !!!”


This is a very good property but is going to a waste because of the poor services and food. This place is severely understaffed with just 2 people handling the entire restaurant and you keep asking them to service you multiple number of times. Food items kept on the buffet which gets exhausted goes unnoticed by the staff and they...


More  




“All suites-Comfort-But not too much of resort”


Place is good. Comfortable. All rooms are suites. Rooms are comfortable and big. Probable semi suites. Staff is good and courteous. Hemant at reception was quite co-operative and friendly.
Some negative points that hotel should look upon;
1. Highly understaffed. Hotel was fully booked and there was inadequate staff to handle. We reached 9 am and had to wait 4...


More  




“not worth in both the front service n food”


i stayed here for 3nights from 16th nov with high expectations
check in was problematic had to stress for pool facing room till i used the name of general manager , then i was alloted the pool facing room
breakfast was pathetic ,they hardly know the term hospitality ,had to pull a waiter for a glass of water as there...


More  




“NOT WORTH FOR THE PRICE CHARGED BUT FOR ITS STAFF”


I had booked into this property for the purpose of deltin royale casino and for having a good time in goa. The room given first did not have a good view and food was not upto the mark for the price paid in the package. But once this issue was raised to SABIR manager. He automatically changed the room with...


More  




“comfortable stay”


Situated near condolim. around 3 to 4 km from candolim on an internal road. So people staying here should have a vehicle. Stayed at this beautiful property during our trip to goa. Unlike other resorts, the rooms shown in the website is what you get !! rooms and bathrooms are very spacious and is worth the money spent. There is...


More  




“best hotel!!!”


i had booked a room for newly weds, and they had an awesome stay for 01 night, everything perfectly arrangd, the bed deco was awesome.. i must say, the best place for a complete holiday.....
KEEP UP THE GOOD WORK!!!! :) :)


“Luxury Rooms But Poor Food”


We have been to this hotel from 8.11.2013 to 12.11.2013. Right from our start of Goa trip, we have to wait for 1.30 hours at railway station for pick-up. At the reception, we waited for four hours for check-in as there rooms were not ready. Secondly there is a need to improve the food. They have nothing for vegetarians. It...


More  




“Memorable experience”


Me and my hubby visited the Hotel between 10th to 13th October 2013.
The location is very close to major beaches so it’s definitely a plus point
Coming back to the rooms we had opted for a suite and it was indeed beautiful and the white’s beige and brown calmed the room’s ambience. Also the cleanliness and upkeep is maintained...


More  




“Excelent”


Awesome ambience, Rooms really neat and clean, Room service excellent, and never the less…. Reception really cooperative and quick response – special mention Roopa (front office executive) u made our holiday….


“Great stay @ Deltin Suites”


We had a wonderful and memorable stay @ Deltin Suites Goa on our recent visit during Oct 2013. We went from Hyderabad and I think we got the best place to stay which is in close proximity to major beaches and other attractions in Goa as commuting is the best from this place. We stayed for 4 days and had...


More  




“great place to stay”


Located a 10 mins north of panjim. it is very close to the city and close to baga and calangut as well. the hotel staff is super. they are very helpful. the food is good at their restaurant called Vegas and their sports bar is fun as well. they provide great connectivity to and back from the Casino. airport pick...


More  




“WONDERFUL STAY OF OUR COURSE GET TOGETHER - 18 - 20 OCT 2013”


We as a group of 194 persons, all senior Citizens( me and my wife stayed in room number 124) stayed in Hotel Deltin Suites from 18 - 20 Oct 2013 and experienced a very nice and memorable stay in Deltin Suites. It was all because of very fine arrangements of looking after guests, well laid out and excellently furnished rooms,...


More  




“SERVICE WITH A SMILE”


DELTIN SUITES is a new hotel cum resort at Nerul, North Goa close to the beach.
The hotel is very well maintained with an amazing swimming pool. The food is good, the rooms very clean and tidy and above all the staff is ever ready to go the extra mile with a smile. A resort worth revisiting.
Any one looking...


More  




“Get together”


I had an opportunity to stay at Deltin suites from 18th oct to 21st oct.
Really enjoyed the stay. Ambience was great, food was exotic and staff really took good care of us.
Beautiful swimming pool added to its glory.
Would recommend every body who plans to visit Goa to stay over there.
S D Singh


“a great experience”


from 18-20 oct we were a group of 100 couples who enjoyed the hospitality of Deltin suites. My observations are as under.
1.the pick-up and drop arrangements from airport/railway station were flawless.
2. the entire staff was very courteous and hospitable
3. the breakfast was excellent.
4. the staff at the reception counter esp Roopa, Ridhima, hemant and som were...


More  




“An Excellent stay for our Course Re-union”


We had booked the entire hotel for our Army-Navy-Air Force course re-union (around 100 couples - all men in their 60s) from 18-21 Oct 2013, and it was an excellent choice. All arrangements including pick up and drops from airport and railway station, organizing various events and facilities in the rooms and dining were very good. The rooms are very...


More  




“What a AMAZING vacation”


What a amazing vacation in Goa from 18 to 21 Oct 2013,,,,,,,,, There were over 200 of us in this group,,,, mainly senior citizens and some on their first trip to Goa. From the pick up at the airport or Railway station to the smooth check in for so many people was the first of many challenges met by the...


More  




“Reunion made fantastic”


We had organised a reunion at Deltin Suites... Total attendance 210 heads....for three nights we stayed there..... It goes to the credit of the management, staff that they catered for our requirements to the T..... Though they had never had such a large number at one time.......some shortfalls....but they were ready to correct and improve that made our stay very...


More  



