
“Budget Guest House Near Baga Beach”


4 Star for the location.
It is located very close to Baga, almost walking distance.
The rooms are small , though sufficient for a couple. Dont expect any luxuries in the room.The rooms( Non-Ac) are simple basic rooms, with a double bed and a neat bathroom.
Nancy , the owner is very good.Though her dad was a bit rude.
You...


More  




“a good budget guest house”


A nice homely budget guest house near baga creek. Far from traffic. Clean ambiance. Just a stone throw away from Baga beach. Quite clean . My room was just ok for we three. With a fridge, cupboard and tv. Running hot water was also there. The owners stays on ground floor.Nice catholic family with full of information about goa. You...


More  




“My stay at divine guest house Baga”


I am planning to Return to Baga in January 2015 , on my return i will be staying at the Devine Guest house . I stayed there a couple of years back and was the most amazing and friendly place you could imagine. i traveled to Goa with family but they were all staying at another hotel down the road...


More  




“Very nice place, helpful people, felt like home”


I stayed at Divine Guest House, Baga, in March 2014. I came to know of this place from the Lonely Planet South India & Kerala guide, which had a very favourable review of this place.
I booked my room on the internet. I exchanged emails with the proprietor, Ms. Nancy Fernandes. I deposited a part of my room tariff as...


More  




“Home!!”


I was in Divine somewhere around Sept last year.
This is my 3rd stay here and since Gavin n me including our friends were on a low budget, we decided 2 book the non a/c rooms.
There is not much of a difference between the a/c & d non a/c...there is no scope of disappointment whatsoever.
It's as good as...


More  




“Tranquility and serenity at the Divine Guesthouse”


The most friendly, respectful, peaceful and easygoing guest house I have ever visited in Goa. If you’re looking for late night parties, the club scene or other nocturnal activities this probably isn’t the place for you. If looking for peace and quiet, a homely and relaxed atmosphere plus a very kind and genuine service then I can’t recommend more. A...


More  




“Lovely Familliar Guest House”


I really enjoyed my time at the Divine Guest House. It is nicely located across the river and is the place to go if you want calm and quiet sleep. The room was well furnished with a comfortable bed, AC, fan, TV and even a fridge. Try to book in advance, because it can get pretty full. I was lucky...


More  




“lovely family guesthouse”


Brilliant base for my stay in Baga. This family run guesthouse lives up to its name - the family were friendly and welcoming while the accomodation is clean and safe. It is within easy walking distance of a few bars/restaurants along the beach but seperated from the main Baga strip by a small river so is removed from the busier...


More  




“Home away from home”


I can't describe how much I felt like family at this guest house. All of my friends and family were worried about me being along in Goa for new years, but as a single woman traveler I felt completely safe at Divine Guest House. Annie, Nancy, and the whole family made me feel right at home. I am an animal...


More  




“Very nice family guesthouse”


This was the first place I stayed on my arrival in India, and I was extremely pleased with it! Seperated from the main Baga drag which is very commercial / touristy, the guesthouse sits across a river in a much more peaceful setting... (excluding the sound of barking dogs, of course: an everpresent soundtrack in India and S.E.Asia!). I hired...


More  




“Great place, family run guest house!”


The family that runs this guest house are extremely warm hearted and hospitable. Being a solo traveller in Goa for an extended period of time, they were very warm towards me, making me feel like part of their family. They would even take the effort to make small talk with you to make you feel more at home. Mr. Raymond...


More  




“Just too good a place to stay if you are at Goa”


Divine Guest House is a great option to stay at Goa. A refreshing change from the sophisticated, business like hotels, where service is often impersonal and formal. At Divine, service is rendered with love and smile. Here one feels 'at home' thanks to a bunch of warm and lovely people - Uncle Raymond, Annie Aunty and Nancy - who own...


More  




“Warm place with warm hearted people !”


If you are looking for a quiet sweet beautifully green place on an economy budget this is the place to go.
I interacted with the owner nancy multiple times and I found her to be a gem of a person. We actually reached a little early before the check in time , but were exhausted from the travel and heat...


More  




“Great place for the price”


The family who owns this b&b are so sweet. Although some things are not available, such as landlines in the rooms or wi-fi that works consistently, they did wjat they could to make things smoother for us (making local calls for us themselves, and letting us use their reception computer for internet access. For such an affordable price, we found...


More  




“Divine Guest House review”


Very homely, peaceful and people are very hospitable. We really enjoyed our stay here. The rooms were very clean and neatly maintained.
Also the guest house is near the center place which made our commute very easy to other places.


“Budget Travellers - Look no further”


If you are budget travellers, this is the place for you. You get simple, clean rooms in pleasant surroundings at a stones throw from Baga Beach. The important thing is the rooms & bathrooms were extremely clean that it even passed the high standards of my hygiene freak wife. The rooms are basic with a comfortable bed and bare essentials...


More  




“It's the best place to stay in Goa”


My bf and I had been to Divine Guest house last Thursday, I read the reviews on here n decided to check it out.
Not very far from Mapusa taxi/bus stand, easy to locate. The place looks the same like shown in the photos here.
Its run by Raimundo and Annie, amazing and extremely sweet people. Not once I felt...


More  




“nice homely guest house near beach and river.”


The owners of this guest house were lovely. We arrived in the off season and were the only guests. Rooms were decorated in bright colours. Bathroom a bit claustrophobic and basic but clean. Fridge in room a bonus! Quiet street away from the hustle and bustle of goa. Still easy walk into town, dark so bring flashlight or get tuc...


More  




“Great stay for a budget traveller!”


It is hard to find places like this in Goa where most stays are either awful or too expensive. I got the smallest room on the property for a real bargain price and didn't expect much. But it was a room sparkling with cleanliness, the family that runs the place are simply wonderful and hospitable people and you get to...


More  




“Great quite and comfortable stay with very hospitable people”


I stayed at Divine guest house over a weekend from 29th Mar to 01st April. The rooms are well maintained, clean and come with a refrigerator and TV!! The family running the house are very very nice people and will do all they can to make your stay comfortable. They even booked my ticket for returning to Pune over the...


More  




“Nice,calm place within the budget.”


We stayed in Divine Guest House last weekend and it was a very pleasant stay. Rooms are extremely clean,with attached bathroom n a small passage. The hosts are sweet and professional and very helpful. Nancy can be a best guide too , if you don't have any idea of places to visit. :) The guest house is pretty close to...


More  




“Best Place to Stay in Goa - Divine!”


I have been staying at Divine Guest House for many years - every time I return to Goa. The Fernandez Family are beautiful people and wonderful hosts. The Guest House is always clean and the Family are always there to help out with anything you need. There are various types of rooms to choose from and they are all clean...


More  




“My good times”


I had in Divine Guest House some of the best holydays in my life.I hope coming in Divine a e lot of times.Annie Raymond and son and daughters are vero good family.DGH is vero nice place in tropicale gardens ,clean confortable friendly site.Good sleep and very fashionable open space fior relax.Ver good for some relax days in peace with yourselves.Religious...


More  




“Divine Guest House”


Gill & I enjoyed our month long stay at Divine. I have just read through the one negative review it has received and I'm amazed - how could anyone say they were treat with rudeness (unless perhaps they were less than polite themselves?) - Nancy and her family were most welcoming and helpful and best of all they introduced us...


More  




“Best Budget Guest House in Goa”


They keep the property impeccable, unlike some other budget hotels/guest houses. Our room was very well kept and had everything that we needed. Nice bed, hot water, cupboards. Felt like home, the best part is it is near to the most happening beach baga(5 mins walk) yet very peaceful and sweet. Divine by name and most certainly divine in truth....


More  




“Divine---My home away from home!”


On my first trip to India i was flying into Goa, the passenger next to me asked me if i had a place to stay, i said, No. She said, i'll take you to a place i think you'll like. She brought me to Divine Guest house. Needless, to say. I loved it and have been coming here every year...


More  




“Silvia Duarte-Divine Therapy”


I felt in paradise ..the way i was received by Nancy and Annie. All family very kind and alwyas ready to serve.
Annie she is an incredible helpfull and hard worker lady that i had the pleasure to met.
The awakening in Divine was a therapy for me.
And I will never forget the way that they treat the animals....always...


More  




“My Paradise”


From the first time I was in Divine Guest House I felt like Paradise. Annie is like a sister to me the most helpfull and strong little lady I ever met in the whole world. She work hard every day to serve all the guest in her place and clean so all the rooms are shinny. She look after her...


More  




“A safe haven amongst the chaos of Calangute/Baga”


The staff at Divine Guest House was incredibly accommodating, humble, and kind. They were willing to help us book bus tickets, rent motorbikes, hire taxis, and navigate our way around the neighborhood. We ended up spending half of our last day sitting around and talking to the staff because they were so interesting and fun to hang out with.
They...


More  




“After visiting and living in Goa for 15years myself and my wife finally found a heaven on earth.”


Divine by name and most certainly Divine in truth, the Divine Guest House offers visitors that very rare feeling of well being. This family owned and run Guest house is more like being an honoured guest in a true goan family home. Nothing is too much trouble for the family who go out of their way to make your stay...


More  




“excellent service”


My friend and I stayed there for more than a week , and believe me , the welcome was unrivalled , all 4 of them , not to forget the dogs and cats ! a quiet place , not far from BAGA beach where we could walk early morning 5.30-6.30 a.m .This " divine " place takes on a homely...


More  




“not a good experience”


I n my husband and our friends(couple) stay in tis guest house in november 2012,the experince is not good.room is small and bathroom is very small.no proper service is provided by the owner as well as his behaviour is also not friendly n co-operative.he is not talking properly n so rude even not provide any help or guidance we asked...


More  




“5 star service at this place”


I have stayed at Divine Guest House with my husband and we were truly delighted to have great hosts who were the owners at the guest house, the check in procedure was really smooth...the a/c room we stayed was impeccable and so clean...no complaints at all....we had morning coffees prepared by the owner itself....The Baga beach is 5-6 minutes walk...


More  




“Delightful cozy & quaint”


This place is an absolute delight! Both Aunty Annie and Uncle Raymundo made us feel welcome and were so hospitable! The place is situated away from all the hustle and bustle and yet in close proximity to all the happening places in Baga! The dogs 'Welcome' and 'Suzie' were simply adorable and not to for get the cats!Lovely, quaint and...


More  




“Good stay in Baga”


Stayed here from recommendation by the Lonely Planet.
Was a great little guest house tucked away from the main drag (about 15mins walk from the beach). Perhaps not the best location if you want to be in the thick of it but we found it a nice welcome break.
Room was bright and cheerful and great for the budget prices.


“Cheap, a bit away from the parties, and nice folks”


We called up guidebook places en route from Agonda in the South to Baga-area, and they had a room when no one else did and we quickly snatched it up! Clean and isolated, there was no free wi-fi but we were too busy scooting down to Candolim and Panjim to notice. So much to do in this area, and glad...


More  




“peaceful location across baga river”


i used this guest house in the 90s,its left over the old concrete bridge on your right across the river a few minutes walk to mayonas live music ..lovely owners and a very safe nice place to stay at the very far end of baga, great omeletes fruit juices etc and very clean rooms (updated now looking at the photos)i...


More  



