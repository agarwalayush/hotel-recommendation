
“Very Disappointed”


We have stayed at this hotel on a number occasions prior to our visit early January 2015 and enjoyed it, hence another visit. I have to say this visit was somewhat different to say the least. Every evening all the tables are pulled together so everyone could sit in a huddle and if you did not want to be part...


More  




“Keeps improving”


This is our 3rd visit to Goa & we have stayed at Dona Sa Maria on each occasion. This time our room is in the Farmhouse, it is a large , spacious room with a balcony that overlooks the countryside & is excellent for birdwatching,it is so quiet & peaceful.
Again the hotel is clean & comfortable. The staff are...


More  




“Just gets better”


Brilliant months stay,it's just as johnnie one leg says,improving all the time!.got scooter from Joe just down the road, tried eating out this time top places-kinara -lennys veinola does a special burger goa style,not Big Mac crap.masa restaurant ,Susse gado all in tamborim area.very quiet not many Russians about,police are a pain roadside stops and silly fines so take a...


More  




“Moving forward”


My 20th time at this hotel and things have moved on the restaurant has been modernised now no more quieng for tea and coffee. Flasks full of coffee-tea and hot water are now available for each table. Also their is a new toaster conveyer type also thick sliced bread so toast is now nice and soft. The FH house at...


More  




“paul and ann's fab holiday”


we are a couple of days back home after 3 great weeks at this hotel, its our second visit and we just love the place,the staff are wonderful and the hotel is always spotless. special thanks to puree and serafino for arranging to have our wedding rings blessed and puree for taking the pics for us see you all next...


More  




“Don't go on first impressions”


OMG was our first thought, what have we done. This was completely opposite to our normal holidays but we wanted to see Goa and this was just a base we told ourselves. The room are basic as basic can be, we had 103 ground floor facing the pool so nice location. Arrived in the middle of the night so just...


More  




“Friendly, relaxing, Goan holiday!”


My partner and I stayed here for a week in January, and despite being nervous about the negative reviews, we had a fantastic holiday. So much so that we have just booked to return in January 2015 for two weeks!
Accommodation is basic but very clean. Pool is spotlessly clean. Grounds are so well cared for and beautiful. Breakfast is...


More  




“Great family holiday”


We visited here in November 2013 and had a great time. The rooms are basic but clean and comfortable and the grounds are lovely. The hotel restaurant was excellent and the staff were so friendly! If we go back to Goa we would definitely return. We would like to say a massive thank you to everybody at Dona sa maria...


More  




“peaceful friendly and a little sanctury”


We returned to the Dona Sa Maria for an unexpected but very enjoyable, 2 week holiday in late march 2014. The season was coming to a close with many rooms becoming empty, however, the standards did not slip, the rooms were cleaned every day and towels changed daily bedding being changed every other day. The staff as always were happy...


More  




“see for yourself. you will not be disappointed”


i have just returned from Goa. i stayed at the Dona Sa Maria. lovely place, lovely people. nice clean rooms(cleaned everyday,sheets changed every other day) clean swimming pool. just the thing for a relaxing holiday. enjoyed the walk to and from the beech. made many friends along the way. if you want to shop and not be ripped off then...


More  




“Disappointing hotel but local beach lovely”


Room was in dire need of a good clean up and refurb (paint etc), net curtains were grey, bed very hard, location not as described on Booking.com as walk to beach is a good half hour in open hot sun, owner/manager overcharged us and we paid for 5 nights but only stayed for 2 (for the above reasons) - we...


More  




“Nothing was too much trouble”


Just returned today from a 2 week stay at Dona Sa Maria and we could not have stayed in a more friendlier place, all the staff were lovely and made us so welcome, yes it is a basic hotel, you get what you pay for but the rooms were clean, although the mattresses were a little hard but you get...


More  




“Dirty run down”


Will start with the half board it is not you get an allowance of 250 rupees each per day and with the overpriced and poor food it's false advertising on the part of the tour operator.As for the rooms we were put in the cell block at the back with an employee sleeping on a mattress out side the door...


More  




“Just what we wanted”


This was our first visit to the Dona Sa Maria and we were very happy with it as a base for a great holiday.
We chose it because we wanted something not too big and authentic - we weren't after gated five star luxury.
We got exactly what we wanted at Dona Sa Maria - it is a small hotel...


More  




“2nd visit”


This was our 2nd visit, after staying there last year. Once again we found the staff open welcome and friendly.Rooms clean, water hot and linen changed regularly.most people we met at the hotel were returnees which speaks for itself,If you want to stay in an impersonal 5 star hotel this is not the place for you but if you want...


More  




“Star Budget Island in South Goa”


After staying 3-4 days in the North, we wanted to experience the Southern atmosphere. We initially contacted several hotels/ BnB in south Goa and the only hotel which has a swimming pool for our budget was Dona Sa Maria. We were intimidated to have read some terrible and poor ratings. Despite all that, we ventured in and to our surprise,...


More  




“Very pleasantly suprised”


After reading reviews before we arrived was very pleasantly suprised . Lovely family run guest house . Staff nothing to much trouble . Always a nice greeting at breakfast by savio . Anthony and mahesh . Fishing good few catfish caught in pond out back of guest house much to the excitement of local kids . Very nice walk to...


More  




“Honest unbiased review”


This hotel is of a hostel or backpackers standard, we visited Goa first two weeks March 2014, needed a base to drop off bags, get our bearings then set about exploring South Goa, this hotel was the best option to get a cheap flight and digs included, price did include half board but all we had was breakfast the days...


More  




“Relaxing peaceful holiday”


We are a family of four who recently stayed at the Dona Sa Maria hotel, we booked through Thomas Cook and the hotel was exactly as it stated in the brochure.
We knew that it was a simple style hotel and that it was a walk through a small hamlet to the beach.
The staff at the hotel were very...


More  




“Hotel that enhanced our holiday experience.”


Where to begin, as my heading explains the complete holiday experience was fully down to the hotel,area, staff and management, (Serafino & Puri ).
In my opinion, having travelled extensively in the world in 4&5 star hotels,for example, Egypt,Tunisia, Turkey, Barbados, Mexico, Dominican Republic, Jamacia, Brazil and many other European countries, (which excludes me from the stated Costa Del Sol...


More  




“The truth about Dona Sa Maria”


Firstly, In answer to negative reviews, did not the people complaining on trip advisor have the opportunity to discuss and resolve problems with owners Puri and Serafino whilst there.Why wait until returning home and giving no chance for owners to put matters right at the time.The rooms are basic but spotlessly clean,pool is clean and very well looked after. Staff...


More  




“Mamoot”


Dont believe the good reviews. If this had been our first time in goa it would have been our last.
Hotel uncomfortable and filthy.most of the staff Staff rude and unhelpful.travel providers need to revisit this hotel and see the truth.
This holiday cost nearly ��2000 for two of us .we chose it after reading all the great reviews. We...


More  




“heaven on earth”


We have returned from our 13th visit to the Dona Sa Maria and have enjoyed ourselves thoroughly and already planning our 14th visit. This year we stayed over Christmas and new year with our daughter son in law and two grandsons. I was a little apprehensive as a family because when holidaying together we usually go to Spain or Portugal...


More  




“Get a life”


I loved my stay at the dona sa Maria as seems a lot more do but you can't please everyone. A lot of the sad moaning brigade just can't help theirselves they probably fall out with their neighbours at home. It's true the hotel has it's faults like every hotel people just like complain for a living. The management purie...


More  




“Not at all bad for the price!”


My wife and I stayed at the Dona Sa Maria during peak season Christmas/New Year 2013/14, chosen mainly as a base to explore that area of India, to avoid Calungute, and because of some surprisingly good reviews. Compared with what we thought we might get we were pleasantly surprised. LOTS of return visitors, almost exclusively retired couples from 'up north',...


More  




“We'll be Back”


Visited Dona Sa Maria for two weeks in January 2014. We stayed in an annexe down the road which was spacious, clean and quiet. We enjoyed the wide variety of evening meals on offer which were excellent value. We found the basic breakfast adequate for us although there was a selection of additional items available at very reasonable prices. The...


More  




“unfriendly and lacks atmosphere - nightmare”


Having read all of the latest reviews I have to agree with Nicky B
'In many years of travelling, this is the first time I have felt compelled to write a hotel review. During previous trips around the world I have stayed at various types of accommodation from back packers upwards, so I am well aware of the general standard...


More  




“keep up the good work”


this is the second visit for us. both.serifino and his lovely wife made us feel realy welcome.before i go any further i have read all the recent negative remarks and i can see where the writers are coming from,what they maybe need to understand is goa paticulary sth goa is maybe a bit behind other places.if they had taken the...


More  




“Relaxing haven”


We went to this hotel just before Xmas 2013, I think it's the most relaxed holiday we've ever had. It's got a lovely pool area where you can spend the day in gorgeous sunshine having a swim and a coffee or cold drink from the restaurant at the side of the pool, or maybe have a massage.The food in the...


More  




“Don't be put off by first impressions!”


Just returned from an 11 night stay on our first trip to Goa. Our initial impressions of our room were in keeping with some of the comments I've read in that it is very basic. However once you get to know the place, after the first day, we started to realise that underneath the superficial surface we were staying in...


More  




“overbooked”


Many hotels overbook as a matter of course but these are usually business hotels were a similar alternative can be arranged. It is not common practice for resort hotels.
Around a week before our arrival Serafino e mailed me to ask if we were still intending to come. I replied by return re-confirming date and time of arrival. When we...


More  




“We'll be back”


We are just back from 2 weeks at Dona Sa Maria. This is the 5th time in 9 years we have stayed here and we realise why this hotel has the highest return rate of any that the various operators to India have on their books.
It is a family run hotel where the friendly staff are so happy to...


More  




“Dona sa Nightmare!”


In many years of travelling, this is the first time I have felt compelled to write a hotel review. This trip was my second to Goa and third to India -during previous trips I have stayed at various types of accommodation from back packers upwards, so I am well aware of the general standard of hotels.
Our expectations were not...


More  




“Awful accommodation, very disappointing”


Ok so we booked to stay in 2 star accommodation in India, we were expecting basic, very basic in fact, and to be fair, some of the rooms we were eventually shown were to this standard, however, the room, or should i say 'outbuilding' we had be allocated was sadly nowhere near this level of basic living you would expect...


More  




“Fond Memories”


First timers to Goa, so all new experiences. Found the first couple of days a massive culture shock but soon got into the groove of this quiet area and friendly hotel.
80% of the guests were happy, friendly, down to earth and happy to give us tips about everything Goa. Perhaps the other 20% will one day find a paradoxical...


More  




“Never again”


Booked this hotel last April on arrival no room at the hotel but put over the road in frog villa, shortly after our arrival hubby got a bad chest infection I got food poisoning not from the hotel but one of the shacks. The food at the hotel very under par compared to goan food outside the hotel once we...


More  




“home from home”


This is our sixth holiday staying in Dona As Maria.We can't give one reason for returning six times.
Maybe it's the warm welcome we receive from owners Puri and Serafino,maybe it's the staff, who will do anything to ensure we have a great holiday.
There's many reasons why this excellently family run hotel has more return visitors than anywhere
else...


More  




“Don't let the moaners put you off”


I am just back after spending two wonderful Christmas and New year weeks here.
After reading some of the reviews on here I wonder if they were at the same hotel as me,the trouble with some people is,they pay 2 star prices and want 5 start treatment.
First of all let me say ,the pool was spotlessly clean,it was cleaned...


More  




“A Family Run Gem!”


We have just returned from a festive fortnight, having stayed at the Dona Sa Maria - this was our first time in Goa travelling with our two boys aged 8 and 12, so there was a fair degree of anxiety about our first trip to India, despite travelling with parents who are now on their 13th year at this lovely...


More  




“Very basic hotel”


We travel to Goa and stayed in Dona Sa Maria in december 2013. We bought this package with half board basis, so we thought we can get breakfast and dinner, however for breakfast we get only 2x toasts with jam, and the worst coffee I ever drank, for dinner you have very limited menu (2,5£ to spend and we heard...


More  




“Don't bother.........”


If you like sitting by a tiny, dirty pool (washed by rubbing the sides of the pool to clean off the dirty grime around the sides every morning....this grime goes back into the water!!!), stereotypical Anglo-Indian food served by bored, lethargic waiters then this is the place for you. What a disappointment. We booked the holiday through Cosmos and that...


More  




“rip off”


My wife booked this one for us to have a base for exploring Goa - my first time in India, she's been a few times. We knew it was going to be a base as we don't usually do package holidays and the experience has told us - never again.
On the plus side - room basic but clean, rural...


More  




“Good but far from the beach”


It is a nice basic hotel, the rooms with the vieuw on the swimmingpool are often noisy at night because the bar from the restaurant is there and it is only closed when the guests are tired and that can be very late 3.30 a.m for example.
The swimmingpool is nice but could be cleaned better!! Restaurant food is good...


More  




“complete chillout”


another enjoyable stay at the dona sa maria,always rely on agood peacefull holiday here,friendly helpful staff and an excellent management team,good food very clean,always sure of a good nights nights sleep.that good will return in February. ps just down the road Joes bikes a must if you can handle a scooter or motorcycle .Eric T


“Cheap in everything but price.”


Looking for flights to Goa for our winter escape next January, we came across a Monarch holiday at £308 for two weeks H/B at this hotel leaving in November, cheaper than staying at home we thought. Looking at the trip advisor reviews and the hotel seemed fine, basic but clean seemed to be the message and lots of returners so...


More  




“Excellent budget hotel in quiet, pleasant area”


This is a highly reccommended budget hotel. The room was extremely clean with air conditioning and small balcony. Towels changed every day and we didn't see any other hotel in the immediate area that we would prefer to stay in more. The staff are very friendly and approachable. The hotel has a small pool and is about 15 minutes walk...


More  




“Relaxed and friendly hotel”


We loved our stay at dona sa maria and we be going back for sure. Lovely budget hotel with great staff who we will miss. beautiful grounds with large clean swimming pool and exellent restaraunt. Thankyou to all of the staff for makeing our holiday a special one


“Dona Sa Maria”


I concur with everything John Glarvey says. This coming February will be our 17th visit to Goa since 2001 and have stayed in the same area every time. When not staying in an apartment the only hotel we would choose to stay in is the Dona Sa Maria and have done so on many occasions. It is a family run...


More  




“Dona Sa Maria: Mother Nature’s favourite hotel.”


No TV. Check.
No network. Check.
No waste. No plastic. Check. Check. Check.
Gentlemen, put away those smart phones. Switch off the idiot box. And detox yourself with some chilled beer at Dona Sa Maria.
This many roomed cottage is beautifully maintained by Mr. Serafino and his manager. Large and portly, Mr. Serafino is up for a joke every now...


More  




“Return Visit to Goa and Dona Sa Maria”


This was our 8th visit to Goa and several years ago we found the Dona Sa Maria and since we have always chosen to return to the hotel. All our previous visits were taken at Easter (I was a teacher now retired) so it was interesting to take this holiday in January. We arrived a couple of days early and...


More  




“Best Holiday Ever”


4th time at Dona Sa Maria - fabulous holiday as always. All staff friendly and welcoming its like coming home every time we stay. Serafino and Purie are NOT selling the business and people who make comments like that have really upset them. Savio and Anita are always helpful and friendly. Already provisionally booked for next year.


“Return to Donna Sa Maria with wife and grand-daughter”


We have been visiting India for the last 18 years and first stayed at this hotel 16 years ago. This year we returned with our grand-daughter for a months holiday knowing that this south Indian hotel would be ideal for the young child (20 months old) and can confirm that our expectations were exceeded by the hospitality of Serafino and...


More  




“Good Hotel”


Stayed two nights here and enjoyed having a large pool in which to swim as the sea is not great for swimming. Put off initially by numerous signs about not doing things, and extra charges, but these seemed to be irrelevant in practice. The hotel is used by package companies, which might explain the signs, but most of the very...


More  




“3rd visit looking forward to the 4th :)”


Just got back from yet another lovely stay at the hotel. Still as good as ever and had a lovely welcome back from all the staff. The rooms are still basic but cleaned daily with clean towels and sheets, although the bed did seem more comfortable this year, maybe were just gettting used to the beds . Met some new...


More  




“basic is the word”


As in the title this hotel is very basic, with this in mind we found it one of the most friendly places we have visited. Tamborim is very small I found it to quiet for myself my wife loved it.as stated by others the rooms are cleaned daily sheets towels changed etc.Bar and eating area was great Thank you SAVIO...


More  




“Lovely holiday”


First time stay at Dona Sa Maria hotel, it is a lovely friendly ,family run hotel ,all the staff are great , rooms basic but very clean had every think we needed and we were in the farm house which had A/C and it was lovely and quiet . Met some lovely people and when I was poorly for a...


More  




“Could not have had a better stay...”


This family run hotel is lovely and has a strong personal touch. We felt very welcome by the staff - especially Savio, who has been working there since the hotel was opened over a decade ago. Serafino, the owner, is very knowledgeable and extremely helpful. The food is delicious, particularly on Tandoori nights, and the bar is open for as...


More  




“3rd visit”


This time we stayed for 8weeks.First we would like to thank Serafino and Puri for taking good care of us and making our stay brilliant.We arrived 05.00 and our room was ready even though check-in time is 12.00 mid-day which we were very thankful.All the staff are wonderful thet really take care of you and always ready with a smile.Anita...


More  




“relaxing friendly hotel”


basic but very friendly hotel, staff and guests make you feel very welcome .many guests return year after year and some visit for long periods at a time .food is lovely especially the tandoori nights .The local beach is a gentle 1/2 hr stroll away and is beautiful clean and spacious , not crowded at all. you must call into...


More  




“Fantastic”


We are regulars & stayed for 4 weeks.had a great time.met old friends and new ones.everyone is made to feel welcome here.a 20 minute walk to the beach or hire a cycle or motor scooter.nothing changes much here which is great.why change anything when its the ideal family run 1star small hotel.anything that doesnt suit you is soon put right.so...


More  



