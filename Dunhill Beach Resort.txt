
“Best spot in Agonda”


I stayed in the magical Cabanas at the Dunhill in Agonda. It is an incredibly spot with absolutely beautiful views along the beach. The rooms were maintained beautifully (incense sticks were put out each night and fresh flowers hung from the entrance) and all the staff were very attentive and friendly. It is by some distance the best place to...


More  




“Good at Agonda”


This was a great way to start our Indian holiday. We stayed in the family room for 2 nights which is away from the beach but about a 1 minute walk at most. We then spent 4 nights in the Cabanna's on the beach front and i don't think you will get better than this in Agonda. The food is...


More  




“Fabulous Cabanas. Clean and awesome service. keep going!!!”


The 3 day stay in cabanas was simply superb. Very clean. The manager Karthik is pretty cool by nature and also warm towards the guests.Location is also superb. Just perfect serene resort with extremely good individuals manning it. But my request to Karthik will be to keep improving on every front. Would want to see it even cleaner, tidier and...


More  




“Awesome Resort best in Agonda”


This was a great way to start our Indian holiday. We stayed in the family room for 2 nights which is away from the beach but about a 1 minute walk at most. We then spent 4 nights in the Cabanna's on the beach front and i don't think you will get better than this in Agonda.
The food is...


More  




“slightly dissapointing”


Have just stayed a week in one of the beach cabanas, they have 6 on the beach in total with 3 in the front and 3 behind. We stayed in one of the rear huts. It was cleaned each day with fresh towels and sheets and plenty of hot water and although slightly cramped have no complaints about the hut...


More  




“Best Place to Stay in Agonda!!”


Stayed at Dunhill resort with family in Dec '14. This was our second visit to Agonda and first time at Dunhill. We had an amazing time at the resort and the beach.
Rooms:
The rooms (Garden rooms) were equipped with a queen size bed and had all the basic amenities available, though they didn't have running hot water but it...


More  




“Best place to stay in Agonda”


We stayed three nights at one of the beach cabanas. Really convenient hotel, close to lots of restaurants, and right on the beach. Breakfast was OK. It was really nice to have a place to sit down and read or enjoy a Lavazza coffee.


“A relaxing weekend at Dunhill...”


It was my first time in South Goa, I wanted to spend a quieter and laid back weekend this time at Goa rather than the maddening night outs at North Goa. Agonda was my first choice. Well, the place did not disappoint me at all. It was a perfect spot for me to chill out. For my accommodation I chose...


More  




“Classy accommodation at lovely beach”


We stayed at the courtyard rooms which are just across the road, few meters away from beach which is absolutely fine. In case you want to be right at the beach, mention this while booking. The room was well furnished and designed - all new. Staff is friendly and helpful. Restaurant has some delicious food and you can spend a...


More  




“Almost great”


We took one of the new courtyard rooms for around 5000 INR a night. Bear in mind that only the beach cabanas are on the beachfront property. The garden rooms are all across the road directly across from the restaurant and beach cabanas. Not an issue.
It's a lovely spot, definitely the pick for those not on a budget. Friendly...


More  




“Nice place for a relaxing trip”


We stayed for 4 nights in mid-November 2014. It is located at one of the loveliest beach in Goa – Agonda beach. We booked a garden view courtyard AC room just next to the road. The room was spacious with nice sitting place and table outside the room. The room was well maintained and cleaned daily. The resort also has...


More  




“Value for money”


We stayed for 3 nights in mid November 2014. Air conditioned room just across the road from the beach was ideal for us at 3000INR per night. The room was big and had a nice front porch with a table and chairs. The only thing that let the place down for us was that it had no mini fridge in...


More  




“*****AMAZING*****”


We stayed in Cabana One for 4 nights at the start of November. Dunhill is situated on one of the loveliest beaches which isn't crowded and you're not bothered by people trying to sell stuff. The beach is well maintained, the cabanas are of a great standard and are serviced each day. The food in the restaurant is faultless. The...


More  




“great value”


It's a good package.. Excellent place to stay with superb beach access.. Agonda has one of the best and clean beaches..
Great food.. Big portions.. Yummy preparations.. OK on your wallet.. Restaurant offers a wonderful view to the beach and very relaxing..
Spacious, clean rooms.. Hot water was a problem... Nice bed, fresh sheets and decent bath.. They don't provide...


More  




“good”


Food is good.Only one thing is horrible ,that is manager who is very rude.Place is ok.I tried to meet G.M, but manager told that there is no G.M. We were staying in Baga.We came there just to see beach.


“Awesome food”


The place has beach facing rooms, which are clean and neat. The restaurant is the best part about this place. The food is great - salads, pancakes, drinks, Indian, conti… Service is very polite and helpful.. One can just get out of the room and go for a long run on the beach… Chilled out & very laid back crowd.....


More  




“Located on the best beach in Goa”


We ran into a few people staying at other places around Goa. They all had complaints about noisy or drunk tourists on the beaches, sub-par quality food or road noise. Dunhill resort was perfect - we ate in the hotel restaurant almost every night and easily spent a week just wandering up and down the beach along the front of...


More  




“Best place in Goa!”


This place is a hidden gem. If you want a relaxing atmosphere Agonda beach is the best in Goa. Dunhill Beach Resort has a great restaurant, beach, rooms, and is close by to shops. Only concern is a longer drive from the airport than other beaches - but in my opinion definitely worth it!


“paradise”


I spent 3 nights in this wonderful hotel last years. It was my first time into a cabana. Service was excellent, the room was clean and the location was simply a paradise. I love India and if I will come back I'm sure I will step by Dunhill once again.


“An Old Favourite”


Dunhill Beach Resort has been a favourite of mine since the 1990s. So it was really sad to hear that Menino, the original owner of the resort had passed away. RIP Menino. I shall miss your gracious hospitality.
The resort has been revamped and upgraded with better rooms and facilities such as wifi in the restaurant. The location is outstanding...


More  




“Beautiful Resort on a Beautiful Beach”


I read a lot about the places to see, restaurants to eat at in Goa..and when i was here i was just wondering that how is that i never read about his amazing place.
We just happened to land here when we decided to visit Agonda before visiting Palolem. After reaching agonda, we had lunch at dunhill. Prawn curry, Asian...


More  




“Lovely stay at a perfect beach”


Helps that Dunhill is located on Agonda beach - probably the best beach in India. Perfect resort to chill out on a really lovely beach.
The view from the restaurant is lovely and access to the beach is easy. The service was excellent and food was great. We stayed in a spacious family room which can accommodate 4 people with...


More  




“Fantastic...”


Agonda Beach is a beautiful, idyllic location in the South of Goa. It is a 10 minute journey by scooter, cab or rickshaw from the busier Palolem.
We chose a beach hut with air conditioning (which was excellent quality). The bed was extremely clean and comfortable with a good quality shower. The glazed front allows for picturesque ocean views and...


More  




“Reservation issues!!”


I haven't been to the resort but was desperately trying to make reservation for January 2015, I tried calling Mr. Kartik various number of times, he just answered the call once that too in a hurry, but he seemed least interested in making the booking.. I offered full money in advance aswell, I even spoke to other guys mentioned on...


More  




“Wonderfull”


Had a fantastic three nights in Dunhill. What a fantastic way to wake up in the morning, to open up curtains and look out onto the beach and the sea beyond.
Our biggest decision of the day was weather to move onto the chairs on the balcony, or onto the beach.
We walked along the beach and watched the fishermen...


More  




“Lovely resort right on the beach”


I arrived from Goa airport mid afternoon, the only small issue I had on my 4 night stay was finding reception when I did arrive everything else after that was great. A small sign is all they need below the hotel sign advising which way reception was, I did talk to the Manager about that. I was taken swiftly to...


More  




“Worth a visit”


We didn't use the accommodation, which looks very nice. I am reviewing the restaurant, which we went to several times. The food is mostly very good, it's a bit more pricey than some of the other eateries but with justification. It's a good location on the beach. My only gripe would be that the waiters are the least friendly in...


More  




“Fantastic food”


After reading reviews on TA we decided to give this place a try for dinner 1 evening and it was only a 10 minute stroll away from where we were staying.We arrived approx 7 30pm and it was practically full and we thought that was a good sign.The food was probablly the best we had eaten in our 2 nights...


More  




“Overpriced and therefore packed but not worth it.”


We went once to Dunhill restaurant with friends who we staying at Agonda cottages which is much better. Even though we stayed in Agonda for over a month we did not return.
The restaurant is full every night but I think its because it is the most expensive resort and certain people and nationalities will only go to these places...


More  




“At the beach - on the beach - by the beach!”


Dunhill has a great location as you might get from the title of this review. We spent 4 relaxing days at the "resort" in the new bungalows. The bungalows where neat and clean, and nicely decorated with good bath/shower room, the beds gave us a good night sleep. Waking up in the morning strolling down to the restaurant for breakfast...


More  




““vacation stay-in Goa””


we stayed at the resort for 3 day and had a great time. The staff was able to accommodate our last minute plans and made it a very memorable stay. The service was great and we were very happy with the food and the room as well. Evening time the they are ready to serve food near beach side till...


More  




“One of the best beaches in India”


excellent stay...great staff.
One of the last remaining clean beaches in Goa...untouched by the rampant commercialization that plagues Goa of today.
The staff is friendly and the location-excellent....


“Ok but not great”


We booked in advance as this resort was recommended to us, we expected the beach to be more deserted and thought there would onlky be a few accommodation options but instead it was completely packed with accommodation and busier than we had expected. Dunhill was more expensive than other places, it was pleasant but the WiFi did not work in...


More  




“Excellent a real treat away from the crowds”


We stayed for five nights on the roadside rooms. A bit expensive for what it is. Excellent standards and service. The rooms on the beachside are even more expensive and can be a bit noisy from the restaurant. Food good choice high standards and service not too expensive. Well situated. Will go again I hope for longer next year. Could...


More  




“good...but”


We stayed here for 5 nights paying 5000 rupees a night for one of the new courtyard rooms...this didn't include breakfast!!
The rooms are v good...like good western hotel rooms.
Comfortable bed ...good shower..and good furnishings.
However for this price...which is expensive by Indian standards I would expect more.this is the same price you would pay for a 4 star...


More  




“Seaside serendipity!”


Our most wonderful experience in all of Goa - our beachfront cabana was superb, so romantic & chic, the adjoining Dunhill restaurant is excellent including the complimentary breakfasts, we also ate-in for many lunches, dinners, beachside snacks & cocktails. Clean & beautifully maintained rooms & gardens; extra care is taken to put out mosquito repellents, candles, coals & incense for...


More  




“Never again”


Hi Guys.
Never again.
We stayed at Dunhill. We loved it before it was taken over. I was woken up with flashing lights through the room. Voices coming from everywhere. It was scary. People were removing sand in the early hours.


“Goan chic”


This New Year we saluted on Agonda beach in Dunhill Beach Resort .
We stayed at the Beach Cabana # 3, as last February.
It's awesome, but the entire beach complex, including six cabanas and the restaurant looked like it is brand new! Later we learned from the managing director Raul that they demount all the buildings at the end...


More  




“very new - way to go”


Good points
1 excellent room, in the new apartments.
2 as expected clean and good quality furnishings.
3 excellent shower and toilet facilities
4 near to beach
5 restaurant has a varied menu service good, can be slow but it is very busy and the food is lovely.
Things to be to be addressed
1 no kettle - it's a...


More  




“Reservation Issues”


I have not stayed here. However , I booked a room with them for the a week in the month of December 2013. I got a mail confirming my reservation. After 24 hours I received another mail stating that my reservation was never confirmed!!
I even forwarded my confirmation mail and still there was no apology or response. So please...


More  




“Clean, complete and chilled”


Love the rooms, the staff are lovely and very trusting, we signed once a day for lunch and dinner and bar! There is no security issues and Agonda beach is the grown up Goa, not noisy, not party town just a but of classy sunshine.
My only additional information would be Dunhill restaurant gets very busy and they only just...


More  




“Pre-Christmas Holiday”


My idea of paradise. Beautiful and clean beach. Mountains coming down to the sea made for a spectacular setting. The waves were fun for body surfing but not so strong as to be dangerous. The water temperature was perfect! Cool enough to cool you down but not at all cold. The water was clean and clear. Our room was right...


More  




“Can't wait to go back!”


I spent two weeks in Agonda during Christmas time and ate in Dunhill restaurant at least once a day. I can only say good things about this place:
- Food: the menu is rich and variegated (I was not even able to try everything)! Food is simply tasty – I love their palak paneer and their special nans (potatoes/mint and...


More  




“Amazing”


What a place! Best food we had in agonda....... In goa infact, and it was all pretty good!! The kitchens were clean, staff helpful and attentive, we found all service to b relaxed in south goa, so didn't expect dunhill to be any different, but were pleasantly surprised by the speed and efficiency of the staff. A cracking venue that...


More  




“See the sunset from your bed”


We stayed in one of the huts right on the beach. It was a small, clean hut with a comfortable bed. You could actually open up the all the doors and lay on the bed watching the sunset. So nice!
The Strawberry Daiquiri was they served was the best I have had.
Staff was very service minded and the the...


More  




“4 star beach resort”


Fabulous room, with very modern bathroom & shower. Soft bed clean, modern. Definitely not camping but glamping. Reasonably priced. We had a bungalow room on the new site around the court yard & I'd recommend.
The restaurant is across the road & very busy most of the time. We had a lovely meal but service very slow. Although on our...


More  




“Great Food”


We visited Agonda beach as a day trip. This was one of the better looking restaurants that we found just on the beach.The food was good but we found the service very slow. Atleast it was worth the wait.


“Lovely family vacation...”


Good hotel, right on the beach, calm, less crowds on the beach and great restaurant with excellent food.
Areas of improvement : Should try to serve food faster and waiters should smile often :) :)
Overall, nice trip.


“ideal place to escape hectic aspects of India”


Dunhill is small and quiet, ideal if you are looking to relax on an uncrowded beach. If you want night-life and 'buzz', go to other beaches in Goa.
Dunhill has three cabanas in the front row, all with uninterrupted outlook over the beach. There are another three in the second row which are 1,000 Rs cheaper. Their view is the...


More  




“great location, great accommodations and wonderful staff”


One of the best places I've been stayed
On one of the best beaches in Goa, quiet, lovely environment and a wonderful resort
Don't miss these paradise, while isn't a crowd place
I'll recommend to everyone without any doubt
*****


“Best place to stay on Agonda beach ”


The best rooms you can find on agonda beach. Balinese style cabana rooms are an absolute pleasure to stay in. Couples retreat more than bunch of friends. Right on the beach, don't need to leave the room for hours sometimes an entire day. Wifi doesn't work all the time but once you are here you won't care much for any...


More  




“Peaceful, clean and relaxing”


My husband and I stayed here just for two nights. The lodging is no doubt the nicest you can find in agonda beach ( one hour drive from the airport ).We took a front beach cabana which costed 7000 rupees plus taxes . Certainly more then the neighbouring guesthouses , but that s because the place is nicer.
We enjoyed...


More  




“Great view”


Our room had 180 degree view of the sea, it's on the beach and one of the few beaches where one can really enjoy the peace and the beauty of Goa minus the loud music. They have a lovely cafe with great food. Rooms were small but nicely done. Staff was polite and courteous


“Lovely location, great service, professional approach”


We had been staying at another resort down the beach but had to move on. At Dunhill, the beach front cabanas are IR1500 cheaper, and a lot better than H2O down the beach. The service is efficient and friendly. We actually stayed in one of the rooms in the house back from the beach over the road. At IR3500, it...


More  




“I got more then expected”


Usually I am a 5-stars hotels traveller but there is not a hotel like this in Agonda, then I had to stay there for one month in the ayruvedic school, so I stayed in Dunhill resort then I dont regret at all. I got one of the beach huts, not a front one but in the second raw. The huts...


More  




“WOW”


Spent 3 nights at the Dunhill Beach resort the 2nd week in november, we paid the extra money and stayed in a beach front cabana and it was certainly worth the extra, complete relaxation, lying on the bed watching the dolphins in the sea in the morning, fantastic. The restaurant was superb and the staff were lovely, the place was...


More  




“Fantastic stay”


We spent a perfect week at Dunhill. The location couldn't be better, the staff are attentive and friendly, the view outstanding. We stayed in a beach front cabana - in my opinion worth the extra money. Didn't have a bad meal at the restaurant and couldn't fault the waiting staff. Really don't have a bad word to say about the...


More  




“Great place, great location.”


We have been to the Dunhill Beach Resort four times in the last three years and each time it gets better. The accommodation is much improved and the food is just amazing. It is just on the beach in a quiet area. It a great place to stay in Agonda but it is getting expensive. Go while it is affordable,...


More  




“A real treat !”


Location - couldn't get better, right on the beach. Very peaceful !
Cabanas - Beautiful interiors, amazing view, lovely private outdoor seating area
Staff - Polite and well trained
Food - Amazing ! Try the Indian...
If you are looking for some peace and quiet in a beautiful serene location, then this is the place to be at.


“A spectacular eating experience!”


We ate at Dunhill's, and it was great. Fresh fish, delicious specials, excellent service, and a great environment! Would return in a heartbeat.
We did not stay at the hotel (which looks nice) but Trip Advisor does not have a restaurant listing for the Dunhill.

