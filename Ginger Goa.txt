
“Nice hotel”


This wasn't my first trip to a Ginger facility but the staff here was more courteous and friendly. Only suggestion is that the travel desk should provide more options as not always you want a cab for 8hrs. Also it would be great to see a complimentary pick up and drop to airport.


“Satisfactory Stay”


This hotel is located near the Panjim Bus stand. I have booked the rooms through the call centre & the call centre executive was very supportive & explained tariff & available schemes & options. Check in was very fast.
Mr. Roopam of Front desk/Reception was very supportive & helpful. Restaurant staffs were very courteous. Taste & Quality of food in...


More  




“decent hotel with easy access”


Hotel is located in a central district, a good place to launch your goa adventure from. Taxis and buss stand are very near at walk-able distance. Staff is friendly, food is decent, cafe coffee day is a blessing!


“Will visit again”


Ginger Goa is centrally located with easy access to city center.
Check in process was quick. Rooms are of small sizes but they serve a purpose well. CCD counter near hotel reception has a good spread of menu considering it's not a full fledge facility.
Overall it was a pleasant stay. We chose this hotel considering it's location and we...


More  




“Good Hotel BUT near garbagge dumb”


i visited goa in early jan 2015. hotel was good.staff was cooperating. Good location. But at night there is lot of bad smell in the atmosphere because the is city garbage dump near hotel. it is very difficult to breath there.


“Affordable luxury hotel”


This hotel is very close to Panaji Bus stand. The area is very clean and well maintained. The rooms are well designed. Rooms are small but is enough for a good stay (No one comes to Goa to sit inside the room  ). All the famous beaches are only 10-20 KM away from this hotel
Staffs are very helpful....


More  




“A very good budget hotel.”


The hotel is located in Panaji, which helps you access most of the places in Goa easily. Once you have been to any other Ginger hotels, you need not produce documents. That's an advantage,as hotel has all the necessary computerized documents required for security purpose. The hotel is clean. One has to rely on self service in all the Ginger...


More  




“Good Budget Hotel”


I was on my official visit to Goa in May 2014 for 4 days and needed a place to check in, in and around Panjim area. Since I have stayed in Ginger Hotels before, in Ahmedabad and Baroda (India) back in 2010, I was pretty okay with getting in there and did not want to keep searching & thinking for...


More  




“Below average”


We stayed at Ginger and were so disappointed. The only positive about Ginger is its Location so we could visit many places in North Goa. Also its very close to many restaurants.
The stench from the nearby garbage dump is unbearable and it was all over in the room,floors and lobby. The room has basic stuff. The lights in the...


More  




“Pleasant”


I visited during a working week and found the hotel quiet busy due to its central location and proximity to local features.
The hotel is equidistant to North Goa and South Goa. Has adequate amenities and concierge facilities. Has a renowned pub/lounge DOWN THE ROAD, down the road. You can find bike and jeeps on rent nearby. Evening ferry tickets...


More  




“Stay only if you don't have a better choice”


I chose ginger to stay as i was travelling on business and had to stay at Panaji .I regret having stayed at this property. It appeared more like a lodge than a hotel . There is no service quality level of a TATA enterprise..maybe more like a 'nano '. TheA/C was not working..it was quite warm inside the room .The...


More  




“average!!”


stayed at hotel twice ..this year...had room 508..both the times..rooms bsic clean,,cool for wht kinda pice paid..hotel is nere to city centre,,can get easy access to texi & cabs to aiport..overall good for price paid..


“Is this really a TATA enterprise?”


This hotel chain was launched in the "Smart Basics Hotels" category by TATAs with innovative advertising strategy. They also promised smart check-in and check-out facilities for these hotels. Alas, what a shame on the TATA brand name!
The check in process was slow as the system was "slow". The room (320) had funky smell coming from the bathroom. The was...


More  




“Business or bachelor friendly hotel”


The Ginger hotels as usual are Business or bachelor friendly hotels. The AC should had been a split one . They are still using the window AC. The working condition was good. Not advised for family with Kids.


“Average stay”


Ginger was always on my list of stays and I finally got the opportunity this month when I had to travel to Goa for a wedding. The property sits right near the heart of the capital so a guest will have the natural advantage of having everything - beaches, markets, theaters, bus stand, offices, rent bikes - within a radius...


More  




“The Brand and the quality standards do not match”


Knowing that Ginger is a budget hotel, my expectations were not much though it carries the TATA brand. Even then, disappointment was in store during the stay. I reached late at night at about 1 pm after a looong drive from the airport but was smoothly checked in by the lone staff at hand. The room was neat and clean...


More  




“Strictly Business, Value for Money”


Ginger as concept suggests, its built for professionals. The rooms have bear minimum amenities. Rooms are neat & clean. For me the coded key never worked & I had to call bell boy all the time to open my room door. The furniture looked dated & it was getting deteriorated due to humidity. The AC was working but with lots...


More  




“Its my mistake that I took a decision to stay at Ginger”


Despite the reviews on TripAdvisor shouting loud and clear about the smell and the limitations of the Restaurant I decides to stay at Ginger Goa and the moment I enter its compound I realized that I had made a mistake.
The Hotel though a TATA property has three BIG negatives-
1) The smell from the nearby dump yard. Please dont...


More  




“Value to money”


Hotel is at good location. Very close to Panji Bus stand 2 min walk time. All transport facility are available near by hotel. Market place is also close 10 15 min walking distance. Good resuretruent in vicinity. Casino also very close.


“Good Budget Hotel”


I stayed in this hotel for 3 Nights from 28th Nov to 1st Dec 2014.
Sleep Quality: Hotel is neat and clean. It’s not a Luxury Hotel but gives good value for money.
Location: It’s near to Panjim Bus Stand and Panjim market is across Mandvi River. You can visit near by tourist spots/attractions such as Old Goa Churches/ Dona...


More  




“Decent place for leisure & corporate travellers”


I stayed there for 3 nights as part of leisure trip. Decently priced & fairly well maintained. The best part of the hotel is its location which is a walking distance from Mandovi River ( which houses all the live gaming casinos). So if visit to casino is on your mind, then this hotel is recommended. Its also close to...


More  




“Good - If you get a discount price”


Stayed for 2 nights, as part of a business trip. The rack rate of the hotel is quite high, compared to the amenities offered. But most of the guests we saw were on business trips & were probably paying a discounted price.
Overall, the hotel is quite clean & minimalist in style. The rooms are on the smaller side &...


More  




“Neat and clean value for money keeps TATA flag flying”


I like this hotel i was 3rd time there in this year. This is not a luxury hotel but i like the location all basic amenities are within the reach , The staff is very good , my special thanks to Mr Parth sarathi Hotel manager for his personal care and attention.
I recommend this hotel strongly as the price...


More  




“BEST VALUE”


hi there stayed in august for 3 nights 20th to 22nd with friends enjoyed the stay very economical well maintened rooms it was interconnected too so gives a great time to stay connected with group location is very much centred to panji easily accesable to all happening lifestyle of goa . overall worth and decent stay.


“cheap decent stay”


Cheap decent business stay hotel. The rooms were clean. Not many amenities but it was good value for what I paid. I was transiting through panjim and needed to spend the night. The location is a bit secluded. However the ktc bustop is a short 5 minute walk from the hotel.


“Not worth the money you spend !”


This is my first review on any site ever.After reading my review, you will realize why I wrote this review.Stay away from Ginger Hotels, if you wish not to loose your money!!!
Before I start let me write the pros n cons;
PROS:
-Hotel is by TATA :-)
-The name Ginger is good :)
CONS:
-Take ID's of all pax.(they...


More  




“Budget hotel if booked early”


Its 2 mins by car to deltin royale casino..
Breakfast was good spread...
Clean and decent rooms..
Huge king size bed - enough for a couple and a child upto 8 yrs can fit..
Bathroom small but managablem
Can b visited for business purpose
Only problem is tht the entry gate level is too high tht honda city cars bump...


More  




“Typical Ginger Hotel”


This is a typical no-frills Ginger hotel. Rooms were small, just to accommodate one (I was in single occupancy). Window AC settings cannot be changed. It only works as On/Off. Restaurant serve decent food.


“Good budget stay”


Located very near to ktc bus stand. Area is not that good. Adjoining garbage dump yard foul smell in vicinity. However once you enter the building everything seems OK. Ac is old and noisy otherwise everything is OK. I had problems with water not draining properly in bathroom although they fixed it. Rooms are a bit small but considering other...


More  




“Best business hotel with low price”


I stayed at Ginger hotel in July’14 for one night during my business trip (it was on 11/12 July) I can say this was my best choice.
check-in & check-out was very prompt and hotel manager personally welcome each guest on arrival.
it was completely raining two days here so could not enjoyed lot, but yes finished my work within...


More  




“Good stay at Panaji”


Stayed in Ginger Goa on Nov 13th-16th 2013.
Positives
*Well placed in Panjim junction. Quiet and serene location. Close to the Panjim bus junction. Walkable distance from Panjim market.
*Rooms were clean and well maintained
*Good parking space
*The restaurant at the ground floor was excellent in terms of food quality. I tried several different dishes over the span of...


More  




“Stay in Goa @Ginger hotel”


Only AC was old but it was in running condition. Except this every thing was fine. The biggest plus point was it was near to the bus stand and in the middle of the panjim area. And the food was excellent.


“Not worth the value.”


Easy to locate and very close to Panaji KTC Bus Stop, Taxi and Auto Stand.
It is located very close to a dump yard so it stinks badly at times nearby.
Though the front office was decent and polite enough, the check-in process was in a mess.. system was down thus check-in got delayed and even room keys could not...


More  




“good with location”


We a group of college friends decided to have get together @ Goa, so we have decided to stay at Ginger itseems it is located in the center of the goa, and from here the distance for north/south/east/west Goa is almost the same. So spending ur day at one part of Goa beaches, again you come back to hotel and...


More  




“Dirty, overpriced room, gateway for terrible experience”


We stayed at Ginger hotel out of necessity and not out of choice. Were travelling from Mumbai to south Goa by road and had to take an unscheduled stop at Panaji. So we had to do a last minute booking at this hotel. Their whole team - Reception/customer care has no co-ordination. When we called to check for availability of...


More  




“Decent stay, could improve on service”


This is a decent place to stay.
Rooms are ok. A/C is noisy because it is window a/c and not split a/c.
Food in restaurant is limited. Breakfast buffet has very limited options. A wash basin in restaurant is recommended. Walking a fair distance to common area bathroom from restaurant is not good. That common area bathroom was stinking badly...


More  




“Comfortable stay”


I was there during August 11-15th. Comfortable stay I can say.Food was ok,location is good. Service and maintenance is good. I prefer to stay again. Good budget accommodation hotel. Except pool. I feel pool should be there.
-Sandeep


“okay Just for staying ..”


I have stayed here from 15th to 19 th August 2014. If you have planned your Goa tour and you just need place to stay at night then its perfect place.
It is very central location but far away from most of the beaches hence after enjoying sea water you feel its to long to travel with sticky cloths and...


More  




“Onother Good hotel of Ginger Chain at Goa”


I have stayed many times in this hotel.
The peoples are very nice, Polite and Helpful.
The Breakfast is testy and with lots of choice.
This August on 13th I was at Panjim to spend time with Dempo Opp this hotel hence went for only Break fast.
I like all ginger chain of hotel, Specially in Jamshedpur, Baroda, Ahm, Delhi...


More  




“Value for money”


Excellent hotel I must say..I booked at off-season price that included buffet breakfast. If you are looking to stay in Panji, this probably is the bast place. The rooms, bed, air-conditioning,food,room-service...everything was perfect.


“One of the best "No frills hotel"”


It is quite cheap considering its location (located very close to the bus stand and different bike hiring shops). However its distance from the beach is a little put off. U will need to hire a bike to go to the nearest beach( autos overcharge and taxis are very expensive). The resturant is amazing with excellent food and fantastic service....


More  




“Best location and great service!”


i stayed dere from 1 aug to 5 aug ...nice rooms and got great service from room attendant "ABU"
who cleaned my room daily. front desk is also very courteous i felt home away from home. had enjoyed my vacations... thanx to ginger....hats off to ur services.....


“Good Value”


Located in Patto, Panaji close to Bus stand. Nice Udipi restaurants in vicinity for vegetarians as Goa is known for Non veg food so this helps. Rooms are comfortable but as its self service it becomes difficult sometimes. Tea coffee maker is available in rooms. Food options are very limited in room service. Buffet breakfast is decent but lunch menu...


More  




“Very good location, rooms could be better”


Stayed there between 24 - 25/07. Liked the hotel being centrally located to the business center at Patto Plaza. Walkable to Mandovi River Cruise and few good eateries too. WiFi worked well during our stay.
Would have preferred Central A/C to window A/C which is usually noise prone. Though it's a no-frills hotel, better mattress would have been worth the...


More  




“paid high for less”


i was visited this hotel on 25 jul 2014, hotel was good, food was good, service was good but its overpriced. they not provide dental, shaving kit in room, Its provide on MRP. No soap , conditioner , shampoo in bathroom. No split AC, Window AC was lots of Noise. But happy with service & staff, Welcome by Manager, Front...


More  




“Good... Hotel... Best Location.”


Good Hotel in Panjim... Rooms Neat & Clean. Bathrooms r Clean... Overall... A Good Choice... Restaurant inside Hotel, Travel Desk, CCD Cafe Coffee Day inside Hotel... North & South Goa both can be reached... This Hotel is Located in Center of both ends... So, Best to Stay here, and visit North or South Goa as per your wish... Recommended...


“best budget hotel for families”


i visited this hotel in 2011jan with my family for 7 days.this hotel is located in panjim city.panjim is in between north goa and south goa.i booked this hotel directly.rooms and bathrooms r neat and clean,housekeeping regularly did their job very efficiently.king size bed is real king sized.tea maker,fridge ,led tv,ac,writing desk r basic amenities and r available.there is ironboard...


More  




“Slightly overpriced Given that it is No-Frills”


Very comfortable hotel at a very good location. Somewhere I feel safety wise it is worth the money (as i was travelling alone), but then it is a no frills by TATA, with that in mind it appeared to be overpriced.
Rooms were comfortable. Laundry services were good. But no frills right? Carry your laundry to reception and deposit.
Markets...


More  




“below basic facilities in high prices”


1. On outskirts of Panjim City. not good if you are on a vacation. specially when its rainy season and you cant go out walking. but just next to many government and private offices so a good location for business travellers.
2. very high tariff as compared to the services they are offering. basic rooms with no amenities. average housekeeping....


More  




“Very expensive in comparision”


We had booked one room for 2 days in Ginger hotel. We wanted one more room for our friend who hadn't made any bookings. When we checked in we found that the entire hotel was undergoing renovation and there was an unbearable stinking smell everywhere. We entered the hotel around 2 PM and found a very dim and gloomy atmosphere....


More  




“Centrally located”


Nice clean hotel,Just in the center of the Panjim ,Value for money.
I was looking for Goan food and Chef suggested to go for Goan Thali and it was really awesome and very economical .
Beaches are just 4 to 5 KM away from the hotel.
surrounding near the hotel gives the impression that you are in Foreign country.
Nice...


More  




“stay at Ginger”


Worth staying. I have stayed at Ginger, Jamshedpur & Mangalore also, but this one at Goa is different. The area where the hotel is situated is good, you feel as if you are in some foreign location with a clean road and nice buildings, with wonderful parking marking and facilities. The room is quiet decent like all other Ginger property....


More  




“Great Stay. Great Experience”


Stayed at the Ginger Goa yet again. A great stay and the most important thing being its proximity to the city centre in Panjim. The staff was understanding and polite. The rooms and the housekeeping was good as usual. The breakfast menu at the restaurant in the mornings was good. Ample choice was available in the buffet breakfast menu. The...


More  




“Ginger Hotel Goa”


We stayed in Ginger Goa for 4 nights, the staff is very hospitable. The best part of the hotel is very centrally located in Panaji, very close to bus stand and market. Beaches are just 4 to 5 KM away from the hotel. The morning breakfast is just average. The Ala Cart lunch and dinner are very good. Do not...


More  




“Unanticipated Hospitality”


I booked this hotel via Goibibo and had a very smooth experience starting from Check-In till Final Check-Out. Rooms were really value for money. The only odd part of this hotel is that you have to carry your luggage on our own to your rooms as there is no bell boy policy (its good when you have a lesser luggage)....


More  




“incredible... i m Loving it :)”


GOA...Awesome place and with awesome hotel..its like cherry on a cake.. location of the hotel is very good surrounded by neat & clean high rise building in heart of panjim city. Sea Food is yummy. Had a good stay.thank you Ginger


“Beautiful”


I went to Goa with my friends to have fun. As Goa is beautiful place, the location of this hotel is very good. It has nearby beaches as well. Service was good. Management is very helpful. Food is good.


“Excellent!!!! Stay at ginger”


I was on a vacation with my family and found the hotel to to amazing the service is crisp. another advantage is the location of the hotel the hotel is located in the heart of the city which is definitely essential when shopping !!!!


“Clinical”


Ginger hotels are quietly efficient but has a "clinical" ambiance.The rooms, though small, are good. The location of this hotel is good as it is in the business district of Goa. All services are efficiently executed. Good hotel for business travelers.


“excellent property”


location of the hotel is very good surrounded by neat & clean high rise building in heart of panjim city.staff of the hotel is very co-operative.food in restaurant is excellent and good part is that the chef himself interacts with the guests and inquire about food taste..my special thanks to MADAM ALKA who not only handles the cash counter and...


More  



