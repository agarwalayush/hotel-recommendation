
“Average stay, higher than average prices.”


Large room, quite uncomfortable bed. Huge ants in the room that were pretty much everywhere including on the bed. Location is good. Prices offered to me were hugely overpriced at 2000 rupees a night when I found accommodation of the same quality but with less ants for 800 rupees a night (Goa is quiet this season so there is plenty...


More  




“Nice Guesthouse, 5 mins walking distance from Arambol Beach”


Best place to stay for Bachelors and Backpackers. The owner Mr. John is very helpful and friendly. Rooms are basic, but so clean and neat, Good priced. They even have a restaurant with great cuisine. At night the sound of waves made my sleep pretty well. Mine 3 nights stay was amazing.
Cons:- Wi-fi is chargeable @ 40 rs/hour and...


More  




“an overall pleasant stay”


i stayed at one of there huts with attached bathrooms for 3 nights in jan '14. for the price and location the rooms are awesome the bathrooms pretty decent as well. the highlight of this place is the friendly staff and the restaurant. the food is pretty good and hygienic and they play some decent music in tune with the...


More  




“Poor facilities, good location”


I have booked in advance for 6 nights, but i have stayed there for only 2 nights, as the facilities of the place and the condition of the room were very poor.
No hot water, no even a mirror in the toilet room, no mosquito net, the mattress was hard like a stone, dirty all over. See the photos of...


More  




“Great! Book in advance for a room though!”


I have been here twice and stayed in the same room. I really like the family who are always friendly even if they don't speak much English and all of the guesthouse guests were invited to the wedding of one of the family in February. Clean, tidy, value for money and quiet location. I hope I'll go back!!


“Gods Gift Holiday home”


Gods gift holiday home
We stayed at this holiday home, very friendly people and always willing to help you. The owner of the home is very helpful trustworthy
We did not pay a deposit but still held our room for us.
The room is not always cleaned everyday but i still kept it clean and tidy. The owners are wonderful...


More  



