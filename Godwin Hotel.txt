
“Mr Hanit Nijhawan ”


Godwin
I stayed with my family expirence of stay was great...
Property...
Around 45-60 minutes from airport
is well located near to market, candilum beach, many Resturants nearby ,well maintained rooms sizes with balcony ,swimming pool too maintained and very nice..
Hospitality
Helpful and polite staff...
Food Quality
Good
Pricing
Reasonable


“good stay”


This is not on beach. but you can walk for about 10-15 minutes and reach there. Its a clean hotel with nice, quiet ambiance. Shopping and eating places are close by. There are also medical shops in case you need and also a place to hire two wheeler for your travel to nearby places. The only problem (at the time...


More  




“Value for Money”


GodWin hotel is a well maintained hotel at main Candolim - Calangute Connecting road. Lots of points of public facilities like newton market and may eating points are located on same stretch. Candolim Beach is just about 200 meters away and walking. Staff is very helpful. You miss for a gym and this miss out is further stretched when swim...


More  




“We had a wonderful stay in godwin hotel, Goa”


It was my second trip to goa. Earlier i was stayed in Calangute but it was not up to the standard but this time i stayed in Godwin hotel in Candolim. It was a wonderful hotel, their ambience, lobby area, front desk was so pleasant, my room was so specious and well maintained, wathroom was so neat and clean. But...


More  




“Visit once again”


Lovely hotel. Our rooms were road faceing .
Rooms are modern and spacious with a little patio. Bathrooms are clean .
! Lots of enjoyment in hotel . Breakfast was good. Service here a bit lacking but the food is good. And if you're a sweet tooth,
Chicken Hydrabadi were exceptionally good. Location is okay. A street away from all...


More  




“fantastic Stay”


i stayed in godwin hotel 6 dec 2014 i liked the hotel very much
as the hotel is on the road side looks very good . All staff are also very good
location wise and room wise very good
we stayed for 3 nights and we enjoyed lot the roos and decor was too fine
hope we will be back...


More  




“heaven stay”


Execellent hotel, nice staff, quality food service no words to explain this was my fantastic stay, value for money, very much approach to two best beaches of goa, market very much near,bar was good, restaurent was also nice


“Beach Location”


This hotel is nearby to Candolim beach but the rooms were pretty ok and we had got breakfast and dinner but it was not up to the mark. Only the good thing about this hotel was it is nearby to the Candolim beach and the staff was also friendly. People going with some budget in mind can book this hotel.


“Good Choice! Read the Review”


We spent a night at hotel Godwin and were pretty pleased! The hotel is in Candolim, right on the main road.
The first rule you need to learn is "DO NOT walk in without a reservation". The reception guys are likely to charge you much, much more than what you would have paid online on a 3rd-party Website. So, find...


More  




“Godwin- God Save!”


Wonder why this place calls itself a 4 star hotel? Located very close candolium beach and fairly near to the baaga beach, these guys do not understand how to utilise their locational strength!
Extremely poor is customer service, infact would say they do not have any service orientation. Reception, bar and restaurant all are at the entrance, one doesn't gets...


More  




“A Place to stay”


Hotel is in Ideal location.Indian Food is very good.The security guard give us very good local information.You get scooter on rent for INR250 for a day just in front of the hotel.But not sure it is a 4* property or not as the pool is closed & the lobby is small.Overall OK.


“Dont keep your expectations too high.”


I stayed in Godwin from 7th feb to 12th feb.
Overall it was a pleasant stay.
I was told by my tour operator that it is a four star property but I got disappointed.
The pool was closed because of some maintenance issues.
The food was good. So overall I would rate it 3.5/5.


“Lower your expectations”


Hi I am Jyotsna Bhatia from Belgaum.My husband and me booked Hotel Godwin for the weekend of July4th to July 7th through Yatra thinking its a four star property as per advertised by them. i am sorry to say it is not at all four star and they fall far short of the amenities and services they claim to provide.The...


More  




“The worst Experience I ever had at this hotel just because of MAKE MY TRIP!!!”


AS always EVEN this time i planned to go in the same HOTEL but this time it was a complete dissapointment with the hotel staff and the main culprit MAKE MY TRIP. Since i am completely dissapointed with MAKE MY TRIP team ,as i confirmed my booking through MAKE MY TRIP HAVING BOOKING ID NH710766256396, but this time it turned...


More  




“Good hotel”


Godwin hotel offered me a good stay with nice breakfast and dinner. The location is its USP. They provide a good pick and drop city tour as well which is an add on. Also its proximity to Calungute is an add on for party people. Overall I had a very good experience and would suggest to travelers choose this hotel...


More  




“nice cozy stay with great room space”


Godwin offered me a good stay with nice breakfast and lunch spread. It is very near to candolim and calangute beach.
Good on accessibility...
Good Food...
a swimming pool with a nice view
Pick-up and drop facility to and from airport
Need to improve upon the room service in terms of time factor; although the service staff was good...


“Good value for money.”


It doesn't get any better at this price! The location is the USP. the service is prompt, we had some issue with the refrigerator and it was changed in no time, food quality is good, market is near by, even the beach is not far. overall good experience.


“very good hotel”


I stayed at this hotel from 12th may to 16th may.The best hotel for budget holiday. The hotel is very near to the Candolim beach( best beach for couples). Food quality is too good.Although the choices are less . Hotel staff is professional and cooperative.
The only disappointment of the hotel is its pool. It is only 4 feet deep.


“very very good hotel”


i stayed at 5 april for 3 nights i was great stay
the staff and manager are very good . Room were also very nice neat and clean
good pool and food was also very nice hope i will be back soon
i also like the bed in the room its very nice we feel like not to go any...


More  




“Poor service, bed bugs in our room....pathetic experience”


I am writing in order to share my terrible experience while I was staying at Godwin goa from 4to 8April, 2014.
There were bed bugs in our room and we couldn't sleep the whole night add the hotel was full abdno other room wasvacant. It was sopainful. I was staying with my husband and 2year old son. All of us...


More  




“Godwin Hotel - Not a good experience”


I was at Godwin hotel in goa....I was surprised to find the landlines of the hotels were dead for a week and no mobile contacts were shared....I had booked 2 rooms and my booking on 100% advance was not booked. My second room was not given citing leakages in room as reason and I had to stay in a hotel...


More  




“Good Hotel”


This is a very good hotel clean ,friendly staff with a beautiful gardens and pool area.Rooms are clean and have all basic facility. they provides free - WiFi , had small bear bar attached.This Hotel is about 600 m from the Candolim Beach.


“first time in goa”


just returned from two weeks in goa, stayed at the godwin hotel with my partner and 6 other friendsThis is avery good hotel clean ,friendly staff with a beautiful gardens and pool area.The rooms are huge stayed room no 104 kingsize bed great bathroom and shower rooms cleaned every day and towels changed .Only stayed b&b witch i must say...


More  




“WONDERFUL STAY”


We, a group of 9 senior citizens, visited Goa from 12th March to 16th March, 2014. We took a packaged tour through Travel Agency" D Paul's Tour and Travels". We stayed for 4 nights at GODWIN Hotel. This Hotel is about 600 m from the Candolim Beach which is a nice beach. We enjoyed our morning walks and spent some...


More  




“I was been there for 3 days n 4 night”


Hi
I was been at Hotel for 4 days. Its batter for its hospitality, services and cleanness. Had good clean swimming pool. Its near to Calangute beach. Rooms are clean and have all basic facility. they provides free - Wifi , had small bear bar attached.


“Pleasantly surprised!”


We had a chance to enjoy Godwin Hotel's hospitality in Jan 2014, when we extended our stay in Goa and had to quickly find a hotel to stay in. We were lucky that Godwin had a room free, since our stay coincided with the International Bike Week.
It has a great ambience for a small stand-alone hotel. The X'Mas décor...


More  




“Very good...Value for money”


Very Clean and spacious rooms.. Nearer to the beaches... good complimentary breakfast...Swimming pool and lobby are also good.
Staff is good and cooperative. They tried their best for our early check-in.


“simply awesome....”


Just loved the experience at this wonderful hotel....good location....rooms , bathrooms are amazing...its grand man..and service is too good....without a doubt book this hotel....go for the All inclusive package....cant wait to be back ...Cheers


“great hotel”


We stayed in Hotel Godwin for 4 nights and this Hotel is luvly place to stay in. Ambience is very good.....The location is 10 min walk from Candolim Beach ....very nice rooms with small balconys...The room has modern facilities i.e. 40 Inch LCD, centralized aircon etc.


“Best Stay...”


We stayed in Hotel Godwin for 4 days and this Hotel is luvly place to stay in. Ambience is very good.Food is very good.Had a nice clean big pool. The location is 10 min walk from Candolim Beach and all the best beaches are within the range of 15 -20 min Drive. You can also enjoy the street restaurants and...


More  




“Great amentities”


I stayed here in for 8 days in November and enjoyed my stay. All of the facilities were very nice. Clean pool, decent bar, good restaurant and very nice rooms with small balconys. The only reason I did not give an excellent was because hallway noise was easily heard in rooms. Other guests would leave their door ajar and there...


More  




“Average Hotel”


Goa is one of my favorite vacation destination in India and this time it was my 4th visit.
Hotel is situated very near to Candolim beach (10mins walk) and around 15mins drive to city center Panjim. Most of the known beaches (Calangure, Baga, Anjuna) are within reach.
Check-in/check-out process with smooth. The room has modern facilities i.e. 40 Inch LCD,...


More  




“Needs Improvement ”


Stayed here for six nights. My first impression was positive. The room was good although the cleaners seemed to have pulled the covers up instead of making the bed up again completely and the pillow cases were never changed OMG the proof was in the nights before dribble stains lol surely they would have noticed. I had to ask but...


More  




“Filthy and bed bugs!”


This hotel had come recommended so we were surprised that it turned out to be the worst hotel I have ever stayed in.
The bedroom floor was so sticky we had to wear shoes all the time and the pug holes were clogged up with hair.
The worst thing were the bed bugs which left my partner covered in bites...


More  




“worst ever hospitality”


if u r planning to go to godwin goa, please rethink cz its the worst ever hotel we found.
The staff behaviour is the worst ever , its worse then a 2 star rated hotel.
The manager itself is non co-operative. Issues happened...
1. We had a complimentary cake in our package but manager refused for that at time we...


More  




“Very good option, especially for Families”


A very nice, comfortable hotel with spacious and comfortable rooms... The staff was extremely courteous and helpful and ensured that we were well taken care of.. While the food was good, we were a little disappointed with the limited menu on offer and repetition of certain dishes over our stay... However, it is a great hotel to stay at, considering...


More  




“TOO GOOD for the PRICE WE PAID..”


we just returned from Goa on 28th Sept 2013..our stay at Godwin can be described as follows:-
Pluses:-
- Good location. Everything is close enough by car. CCD bang opposite the hotel :-)
- Great food, hats off to chef.
- Reception staff was well informed & always ready to help.
- Big rooms for the price we paid. Have...


More  




“Nothing special, but nice”


Had a New Year party at this hotel)))it was nice, a lot of Indians, i've got a feeling that there were staying only Russians and Indians))) locations is on the road, the way to the beach lies between private villas, or you can have a walk along the street with a really full of traffic. There are some restaurants and...


More  




“Good Hotel,great food”


We had a three night stay in April 2013 at Hotel Godwin.It was surprisingly a very good experience staying in this hotel.Staff is courteous & food served over here is very good,although spread at buffet was limited but whatever was served was of great quality.rooms are clean & staff is very well behaved.


“to good hotel and service and food”


we stayed in Godwin for 3 day and that experience was great and remarkable, hotel is really beautiful, clean, inside the room are very much comfortable, staff is very help full and food is extra ordinary
i would like to say you couldn't fault in the room and hotel
i would like to give you 100/100


“GOOD LOCATION”


I check in godwin goa on 20 march 13. hotel is good . location is very good nearby condolin beach walking distance max 10 min. food & breakfast is average. room is good. baga beach abt 10 km from hotel straight road.hotel staff is co-operative.


“wonderful holiday.....”


We stayed in Godwin for 3 days and had great time.The enterence of the hotel is beautifully designed and the ambience is fantastic.The hotel is clean n the staff is friendly n helpful.The staff at the hotel always greet you with a smile on their face.The rooms are well cared for and
comfortable.The food is good n tasty..The bar is...


More  




“great stay at the godwin”


we stayed at the godwin from 26th jan- 9th feb, we found it to be a lovely hotel in very central location. couldnt fault the room, very comfortable. the breakfast was good, buffet style and a chef on hand making eggs. we didnt have lunch or dinner there as we ate out every night, the pool area was tidy, clean...


More  




“A good place to relax in Goa India”


This hotel is close to most of key Goa activities. The restaurant is excellent. The complimentary buffet breakfast was something to look forward to. The room was well appointed and extra clean. The hotel had good parking for our driver. The reception staff was kind and helpful.


“Noisy”


Good decent property with helpful staff and nicely done rooms. Its near to the Bagha beach that was one of the reasons my travel agent recommended it , no problem with that. The real problem with this hotel start is the size ; its way to small to accommodate the amount of people that it books.
The restraunt cannot hold...


More  




“Godwin's - Fawlty Towers”


This is the second time I have stayed at Godwins and I am half in love with the place and half driven nuts by the madness that prevails! Okay good stuff first 1. Rooms are extremely comfortable. 2. Most of the staff are very willing/polite and keen to help 3. The pool is amongst the best in Candolim and is...


More  




“Is this a Hotel or a Lodge?”


This place is a 5 star lodge and half star hotel. We had 3 rooms in this lodge the rooms were fine however the only good part of this lodge were the rooms. If u dont give any training to your staff they r bound to be a bunch of monkeys running around without direction.
Service was rank bad. Hotel...


More  




“bad service at godwin”


Property is average. What is worse are the policies and attitude of the staff and management who are extremely inflexible and not customer friendly. The lifts don't work and the pool is dirty. In summary not worth the price at all


“Nice hotel - Well maintained.”


We stayed here when this hotel had just begun operations. Staff is very courteous. Rooms are very impressive for the price paid. We had taken a suite with breakfast package.
However breakfast is not served as a buffet. You need to place an order. The restaurant is not separated from the the reception area, so a little incovenient.
Well maintained...


More  




“nice place - good standard”


myself and my wife visited this hotel for a three day tour to goa. the hotel reviews and photoes were not much attractive. but believe me the hotel is very nice. the service here also is very good. house keeping team needs special appreciation. the restaurant staff also needs special appreciation. they were well behaved. but the stadd at reception...


More  




“Family Vacation at Goa Nov-12”


This is an exotic place where you can really relax . the entrance of the hotel is beautifully designed and the ambience is fantastic. the amenities are very good. the hotel is located close to candolim beach where one can really relax, can enjoy in beach waters every single minute.
the food is very good at godwin. overall a very...


More  




“Overbooked Hotel - People are shifted to other Hotels”


Booking: Checked in Tripadvisor & Booked a 3N/4D package through Yatra.com. All the reviews on Tripadvisor were Very Good/Excellent.
4 Star: This is not at all a 4 Star property, can rate it max 3 Star & that too not comparable with the better Hotel chains.
Welcome/ Rooms: Did not feel welcome at all, people at the reception were not...


More  




“Wonderful Hotel....”


Our stay at Godwin, Goa was one of the best stays we have had in long time. We went there after scanning a few hotels in Goa as it was our first visit in Goa. The hotel was tidy from outside and inside. We checked in and found the staff to be very pleasent. Our rooms were way better than...


More  




“Luxurious stay”


We stayed at Godwin for 5 days and four nights.The location of the hotel is very good,just a 10 minute walk and you are at the Candolim Beach. The food was amazing...., many a times we came back to the Hotel just to have their delicious preparations.The staff of the Hotel is courteous and friendly.Would definitely like to go back...


More  




“Honeymoon @ Godwin”


We stayed in Godwin for 3 days. Welcomed by the courteous front office staff, we checked in into the hotel in the morning. Housekeeping of the hotel was brilliant. The food was amazing and the service excellent. It was a total "paisa vasool." Special mention needs to be said about the hotel staff behaviour, they were courteous, helpful, and guided...


More  




“very good hotel”


i was stayed in Godwin Hotel in month of Feb 2012. it was my fantastic stay with my family. manager Ashish Sharma is very helpful and polite as well as team Godwin is very helpful and friendly, specially front office staff is very good. And food part from the breakfast to dinner all meals are very tasty. rooms are very...


More  




“fantastic room and great soft bed,volunteer with monkeys”


what a great little hotel loved the pool and the very comfortable king bed, room is four star defiantly, haggled and paid £50 a night. Indian style breakfast was really nice and eggs to order. but it has some major issues going on with management. the door system to your room has an alarm on it so if its not...


More  




“Wonderful Stay”


We had our stay in the Godwin Hotel & from our entry in Goa airport till our drop at Airport we were totally satisfied with the hospitality offered to us by team Godwin.
The moment you enter Godwin the state of cleanliness & beauty allures you. Everybody is there to help you both in a personal manner as well as...


More  




“GOOD BUSINESS HOTEL!”


The newly opened Godwin Hotel is quite a boon for those on a business trip. It offers chic rooms for a reasonable price, on the main Candolim-Calangute road. There are glitches however. The room service is very dodgy. I had to call three times for a bottle of water. And the staff could do with fresh clothes. The breakfast is...


More  




“Wonderful Stay”


Goa is the land of sun and sand.. Godwin is our favourite hotel for a perfect goa holiday.
The rooms are very well done up and are equipped with all the lastest intsruments and appliances.
Rich and royal interiors and intricate woodwork make this hotel give away a very majestic and warm look.
The breakfast, in room dining and the...


More  



