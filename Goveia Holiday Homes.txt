
“Would not recommend”


I would say that the hotel is fine, but not good.
The bed is terrible - mattress was as hard as a plank of wood, pillows were thin and lumpy and there was no top sheet on the bed, just a blanket. Bathroom is dark and not very nice.
However, the rooms are all very clean and tidy.
Reception staff...


More  




“Bad choice”


Where to begin? I will be as objective as possible:
- The place is dirty, bathroom not clean and coackroches everywhere.
- The bed is as hard as a price of wood.
- The pool water is not clear.
- The building and rooms are very old, smelly and not welcoming.
- You have to walk at least 20 minutes...


More  




“Poorly maintained”


We were a group of six people occupying a 2bhk in Goveia Holiday Homes from 27th to 30th dec and paid quite a high amount as rent for a shabbily maintained place.Even though the kitchen is set up to function for guests,the toaster wasn't working and stove knobs were too tight to use.The shower wasn't working properly and the sofa...


More  




“Value For Money”


Its a nice hotel with decent interiors and service. Rooms are fine... not great. Rooms are clean and Reception people are co-operative. They are ready to help. Near to Calungute beach... around 2 kms. Walk able distance. Worth visiting.


“Average to good hotel”


The hotel provided a spaceous room with separated sleeping room. We had breakfast and dinner buffet in the hotel`s restaurant. They served different dishes, both veg and non-veg. We tried just the veg food which was delicious but pretty spicy for our European taste. In average the service was good but strongly dependant on the single staff member. Candolim beach...


More  




“Nice Hotel”


Been there in Dec 2014 for three nights, It is 15 min from the Candolim beach , well maintained property, good food, Rooms are good, swimming pool is good. Enjoyed Rajasthani folk in evening, Staff are helpful, definitely recommend to every one.


“One of the best stay and the staff”


First time in Goa.
I spent 10 days in this resort. Best part of this property is Mr. Sagar, Anup & Frazer.
Hotel staff was very kind.
Food was really good
Rooms were good.
I would say 7 out of 10 for this.
For a Budget property this is going to be choice for my next visit.


“Execellent Hotel”


The hotel is excellent in many ways. Location is very good and Candolim beach is 10 min walkable. Cleanness is very good and staff is very help full. Special for Mr. Sagar & Anup.
Food quality also very good.


“Excellent Hotel”


The hotel is excellent in many ways - location is awesome, the Candolim beach is 15 min walkable and very nice to relax (its not very commercial) and the staff is very polite and helpful especially anup from front office.. I had been with my wife and 20 months old daughter and we loved the stay. The staff at multiple...


More  




“Never even think of staying here.”


Had a really bad experience. The hotel staff is extremely rude. The cleaning staff never changes the bedsheets and the bathroom is never cleaned. If there is any dirt on the towels(yes, they expect you to keep them clean) , they don't let you leave unless you pay a good amount as a penalty.


“A really bad place to stay at.”


We were a group of 110 people. We stayed at this resort for four days. The services at this hotel are the worst ever. And the staff is not curteous at all. They are rude to the customers are don't treat them well.
While checking out, the hotel fined atkeast three rooms for things the people staying in the room...


More  




“No sense of hospitality, poor service quality, bad managing staff”


I stayed at the hotel for four days during my college trip. The hotel is fine but the quality of service is unacceptable... They dont treat the guests properly.They charged us for a towel we didnt even use, seriously for a towel. I would personally not recommend staying here. They treat us like they have paid us and not the...


More  




“Above Average...”


Our Irony started when we book Villa for stay of my family. Though everything looks very pleasing in pics but things weren't that good once we reach this place. Paints of bedroom was all peeled off, no utensils, no gas or microwave in kitchen, lights will go off randomly with staff having no clue of it(leaving you in perfect darkness)....


More  




“Worse Resort”


Worse Resort I have ever seen who say that we don't provide good service to Indians because they do not behave properly.Buffet breakfast was pathetic,swimming pool was not cleaned even there was an open electrical wire inside the pool even after complaining this problem was not solved poor service was provided even we had to pay fine for not keeping...


More  




“Poor below average hotel”


The hotel is far from beach and its not a professionally managed hotel the pool is not maintained and the building is old I read the other reviews which said its a good hotel but when we had been the staff is not friendly and the breakfast is horrible and the spread is only powa some breads and cornflakes and...


More  




“Best budget 3 star resort in Candolim”


I have stayed for 8 Nights at Goveia Holiday Homes which is managed by Ginger Group. I enjoyed the stay since the resort is located on one of the famous but less crowded beach of goa ie Candolim. People who are looking for fun on beach without too much crowed I advice them to stay only in candolim since other...


More  




“my 8 week stay”


the Goveia Holiday homes is situated off the mail road in Candolim we stopped ther for 8 weeks mid of January to March, been to Goa many many times first time to Candilim the hotel is a good 20 mins walk to main road so need taxi most of time after 3 weeks we found a short cut only takes...


More  




“Just one word "awesome"”


Exactly the place what we were looking for!!! We were 5 couples & 3 children we enjoy each and every mint over their!!! We got 2 villas at very good price also! U will get each and everything inside the villa from cooking to waching machine everything u will get inside without any extra cost!!


“Needs a lot of Improvement...”


The hotel has many things to improve on. The restaurant is the worst part. The Restaurant and front desk Staff is the second part. Made us feel as it was due to compulsion we were in that Hotel. Torn bedsheets....Torn Blankets...no cleanliness...No service.....filthy swimming pool.....much more could be added but need to forget it....


“Worst among 3 star hotels”


Friends, me and my wife have booked this hotel under Make My Trip package. This might be the worst hotel we have stayed so far.
Some of the points you may need to consider booking this hotel:
1. Even executive ac rooms here has cockroach inside cupboards
2. Don't go by photos posted here, they are modified and in actual...


More  




“Very good”


Second time we visited goa and stayed in the same hotel. Good environment, clean pool, good food but less variety, good service. We enjoyed our stay at this hotel. No complimentary drinks received on arrival.


“Independent Studio Apartment”


Well maintain, neet and clean, easily approachable, not too far from beach. Beach is at walkin distance. To get two wheeler or four wheeler is easily available near the apartment. Kitchen with all utensils is an additional benefit.


“Worst hotel possible Never ever go here.”


I travel a lot and this is the worst hotel I have ever seen... The rooms are stinking, old rooms, unclean wash rooms.. simply horrible. The rooms give a feeling of a free choultry in a temple town, then a hotel in a tourist place like Goa.
Worst reception, no manners or decency... Wonder how they are into hotel industry....


More  




“v nice stay resort group”


really v nice view and location silent zone and staff very helpfull and food nice , room size very big and not any complain for resort ,rate wise very low but service wise best and only problem for bus way not in main road so step only 200 mts walking , i hope stay all and good experience


“Nice ...but need to work certain ares”


Pros:
1. Nice Location
2. Friendly and full co-operative Staff
3. Food Taste is Good
4. Peaceful environment
5. Spacious executive room
Cons:
1. Un-Hygienic Dinning Place & Washrooms
To Management of the resort, kindly update the website with updated photos of rooms.
Over-all nice place to stay..


“Holiday in goa ---July”


Overall experience of the hotel was good.Nice ambience ..clean rooms..
Breakfast is good but variety is less .They claim Wifi but it is available only at reception and no mobile signal in rooms.
swimming pool is clean


“Great Stay at Goveia”


We stayed from 18th Jul to 22nd Jul.14 at Goveia Resort, Overall stay was good at the price what they charge.Staff is very co.operative. Thanks Mr.Anup for helping us whenever we needed your help. We surely visit again in near future.


“Great Property @ Candolim, Goa”


The property is excellent; amenities are good; worth the money spent; will definitely visit the place again; hospitality of the staff and the management is great; food is of good quality; a no-nonsense place for a mid-level classy hotel choice;


“Everything is fine”


I need everything no. 1 in like stay and rooms and location and privacy and services and staff etc. I enjoyed your govea resorts like everything and I hope I will come their next time and I enjoyed everything.


“Good but never book it through vacations gateway..they are frauds”


Nice hotel but 250 MTS from main road.. Location is good but do not forget to hire a vehicle right from the time u enter the hotel or else u will break ur legs walking miles n miles.. I booked it through vacations gateway which is a big fraud.


“To be avoided”


i had week end visit to the location, exterior painting of the hotel is nice...... but everything in the rooms need urgent attention...cots have expired its life..... AC noice is unberable....!!!! restaurant is dirty and full of fly... there is no proper lighting in the restaurant. food quality is worst. overall environment is dull.... to be avoided....!!!! dont go by...


More  




“very nive place and well mannered staff”


i stayed here for 3 days and 4 nights. whole place is very good and staff is very polite. One night we did'nt liked the food and complained abt that and hotel manager Mr. Fazer immidiatly resolved the issue by providing us the food of our choice.
Thank alot Mr. Fazer.
Regards
Satwinder Singh
Stayed in Feb 2014


“Very poor response of Staff and rooms not clean”


On my first day, I came at 7PM and i request to reception that we all are very hungry so, we want our dinner fast. They told that dinner will come at 8PM otherwise u hv to pay dinner amount if u want early. and, Actually dinner start at 8:40PM (Date : 18 MAY)
- Rooms are not clean, next...


More  




“Don't be Fooled By Free Breakfast / Free Wi-Fi”


I selected this option for my team of four, knowing that the stay would be a "budget" one and since most of our trip was about crashing on the beach only, we'd get good value of money by being able to use Wi-Fi / Breakfast. Following are negatives you need to prepare for if you wanna make this hotel your...


More  




“Pathetic experience”


Just visited goa and stayed at goveia holiday homes for 3 nights...below my points why I didn't like the hotel
1. 1st day just entered room called restaurant to order food ...They said they can't bring food for the next half and hr as there was a hugh group they have to cater to ( as if they were doing...


More  




“Amazing every time you visit”


Its always a pleasure to be here. Everytime i visit Goa, this place is the only one i look forward to for staying.
I'd advise to book the place at least a week in advance, as it has so many visitors even in off season.. Not only the place is amazing, but they have a gem of staff as well....


More  




“Budget but brilliant stay”


The hotel is budget, but that's what we booked, the room are clean and comfy, Indian breakfast buffet good most days, coffee not so good!
Other food made to order was excellent.
The staff are all incredibly friendly and helpful all the time making your visit a pleasure. We stayed for 3 weeks and we could of easily stayed for...


More  




“Hotel services”


we had spent almost 1 week in the hotel and we are very happy with facilty of hotel and therir services ,punctuality,hospitality, room services, behave of staff is very good for coustomer and there is no botheration of any this we feel the hotel like our home ,this is best hotel in goa very very good,all the beach are very...


More  




“Wicked stay.”


Well me and my family had a beautiful time. Staff made us very welcome. Appartment clean and tidy. The Manager friendly and welcoming. He came to our daughters wedding. See you next year. 21 days this time....love Snaz Eve. Billy Simon Lizzie Neil Raj. Say hi to all the staff xx


“Budget Stay”


Location is nice but not the best. Rooms are normal and food is ok ok. Looks more like an society apartment. Pool is good. Staff is helpful. Looking at the price is a good deal but not for one who has a classic taste. for boys stay and family its good but not for honeymooners as you have great options.


“Avoidable, unless you are in a large group”


This was our first trip to Goa, and we had chosen this place for a 4 day stay (Summer 2011). We had chosen this over other hotels because we were told that it gets many more domestic tourists, and hence, the food would be more Indian in taste. Not so!
While the overall property is decent, the service is not...


More  




“very unhappy guest”


We stayed at Goveia Holiday homes in January and had a very bad service. My experience is below and when the hotel owner was notified about the below by our operator in India he refused to refund any money or assist in any way.
* The receptionist called me the morning that we were to check out to
find out...


More  




“Good for meetings and conferences”


I was part of a scientific conference organized that was attended by about 60 people (about 25 foreigners). The resort is located in Candolim in a small street, 5 min walk from the main road. The Condolim beach is about 15 min walk and other beaches like Calangute, Baga and Fort Aguada are within 5 km. We stayed at Goveia...


More  




“Dripping AC, under quality breakfast”


Hard pillows. No daily cleaning provided. One water bottle provided on arrival thats it.
AC was found dripping, they did not check room before giving it to us. Maintenance guy worked on it for an hour, wasting our time. Fitted it back, but dripping restarted after some time. And this was the case with executive suite.


“Excellent experience”


We were in Goveia and the nearbyy by Ginger place for a conference with about 60 people. We all stayed in Goveia and it was a great place to stay. The staff was very friendly, food was good, drinks were cheap, and the rooms were big enough and clean. The beach is about 15-20 minutes walk from the hotel and...


More  




“Good hotel”


we are went on 6 feb 2014 to goa and Goveia Holiday Homes is very good hotel to stay and there staff are very friendly and in very good nature. the food was very great taste and in very special manner to serve the food. the hotel near by Candolim beach of distance of 15-20 min of walk the room...


More  




“Absolutely awful”


Do not go there could not even get a cup of tea!! Plastic brown stinkin cups.we should av moved but no my husband didnt want fuss he scrubbed bathroom.we had to ask f sheets only one grubby blanket.full of indians every wkend busloads jumping fully dressed in the dirty pool.had to walk bk n fore lonely lane to get main...


More  




“goveia holiday homes”


we went to goveia for two weeks, we took two little ones aged 2 and 3 , we had origionally booked an apartment but was upgraded to a villa, we had a fantastic two weeks,the complex was clean and tidy and the villa was above our expectations for goa, we had booked through travel adviser and ultimately a guy called...


More  




“The crew”


We went there in august 2013.we drove all the way from delhi to goa.we did not do any booking so once we got there we looked at few places we rented a 2bedroom apartment at govei it was very big clean and location was also very well loved it .going back again and staying at the same place.thank you Mr...


More  




“AVOID ALL THE GINGER TREE GRP HOTELS”


Fact:
Ambiguous details on web page. It is not opp Café day Coffee – the road to the hotel is opposite. The sign states 200mtrs – its approx. 2100mtrs. The beach is then approx. 2km from the main busy road by transport or 1500mtrs walking down un-lit dirt tracks/alleyways of peoples home.
This is a group of 3 hotels The...


More  




“Penny and Chris Brodie”


We stayed for 3 weeks in January and can only applaud the Management and staff on the improvements made since last year! All of the staff were polite and always ready to help with any request we made -especially Tofik in the restaurant and Bhagwan who helped to keep our room clean. The menu and especially the Indian dishes are...


More  




“Horrible”


V stayed at 'new ginger tree' which is in d same campus as this goevia resort.both places are jst 'ok ok' types.do not expect anythng nice here.the crowd is typical indian holiday groups,couples also form a large part.d pool was dirty.n d rooms wer jst tolerable.n it is surely overpriced!!!wil nvr go here again!!


“avoid at all cost”


My wife and i had the missfortune to stay at Govia holiday Homes from 12Dec 2013 to 7th Jan 2014.Although we were not staying in the hotel we were in the same "resort". First thing I would say is that it is a long walk to Candolim centre i would say 30 mins for a fit person, definately a taxi...


More  




“grt place”


nice hotel the managmnt is really helpul n provides evertng at evry point of tym u require. clean rooms n great atmosphere which makes the people more like stayng in d hotel premises den roaming here n there


“Comfort at goveia resort”


Sanju rohra
Stayed in the hotel from 11 dec to 15 dec.
Room was very good.staff were very helpfull
Very nearby to the beach
It was very comfort stay for me.
I visited goa fr 1st time
Food was also good
Thank to mr sagar
Keep it up.
Will visit again


“The best time ever.”


Hi Guys,
We went goa from 5th Dec to 12th Dec 2013 for honeymoon and Our stay in Goveia Holiday Homes was good, we enjoyed a lot there. Safe and secured for family stay. The staff was excellent - very helpful and quick, always with a smile! The food that we got at the restaurant was amazing! so yummy. This...


More  




“Goa i come back”


nice little hotel with a big garden outside
it takes 10 min to the beach and to the centre of candolim
big size rooms with useful Equipment ( fridge, tv, balcony )
breakfast was very easy but enough ( it was not the european Standard)
the personal are very friendly and helpful
i come the last 3 years back and...


More  




“this hotel is very poor. and service is so gulty. bakvas hotel in goa”


this hotel is very poor. and service is so guilty. bakvas hotel in Goa. we have stay 3 days in this hotel in august. service is third class. really bakvas hotel.we could see visitors complaining about the abysmal service and what was scary was that the resort manager was shouting back at the visitors at the top of his voice...


More  




“One Of The Best Hotel In Goa”


We have around 120 person group visited this property between 23rd November to 30th November.
Please is really superb.
Plus Point
Check In Process & Front Desk Is very co-operative (Specially Mr. Anup)
House Keeping Staff is also good
Restaurant :- (BF/Lunch/Dinner) always on time with new menu. (Specially Mr. Frazer) is nice and helpful person.
Mr. Sagar Owner of...


More  




“Hellish dirty place with shameless/pathetic service”


This place ensured that our trip had the worst beginning ever. Right from the time we entered the "resort" we could see visitors complaining about the abysmal service and what was scary was that the resort manager was shouting back at the visitors at the top of his voice while flinging their belongings! Ignore that and you'll get a menu...


More  



