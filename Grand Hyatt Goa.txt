
“Worst Grand Hyatt property”


We recently stayed in hotel grand Hyatt Goa and trust me it is one of the worst Grand Hyatt properties one can stay in. The main lobby and restaurants are very very sad and have no feel and character to them and so is the food and service, totally unappealing. We took the bay facing rooms which were nice but...


More  




“Grand Experience!!”


A wonderful stay for my Wedding Anniversary, the property is sprawing, the pool amazing & the view breath taking. Amazing Breakfast. Was my thrid stay at Grand Hyatt Goa. Only minus was - strict checkin time policy, and complimentry upgrades or services ar a BIG NO... Unlike other hotels like Accor, who generally do offer a lot on special occasions....


More  




“Amazing place! Perfect for chill & take a rest, but the beach :/...”


Nice & amazing hotel with perfect service; fine room with nice view od the sea! We trayed all restaurant a was perfect!!! We spend here the Christmas time - really nice stay, perfect, like a fairytale :) Also we visited the joga lesson, massage and we must recommend it! And also the wi-fi is everywhere:)
but... The beach :/ if...


More  




“Perfect stay”


You enter in this huge lobby and You are already in paradise...
I had a club room... So directly taken in a club car to go to The grand club guesthouse... Just amayzing.
All the places of this hotel is decorated with great taste.. The garden, The pool, The spa, The bars and restaurant are amayzing.
My biggest surprise was...


More  




“Simply awesome”


Awesome ! Loved it.
Initially we had some problems with the reservations. I had written off this property from my list. However, after speaking to the GM Mr Le Roux we decided to take the risk of staying in this property. I must say it was really worth taking the risk. We were really pampered. The staff is incredibly hospitable....


More  




“Lovley relaxed stay!”


We were on honeymoon and chose this luxury hotel to spend some relaxed quality time together. Since I had informed them that we were on honeymoon, so they had arranged a small delicious chocolate cake along with rose petals and towel decorations which was classy! The staff especially the turndown service staff were very polite and efficient. He made a...


More  




“Well needed luxury, lovely place to stay”


After 2 weeks of beach hut experiences we were hoping and praying for something special here. We spent a lot of time weighing up our options when planning months earlier between this hotel and its nearest five star rivals. We chose this one and once we found it ( wasn't easy ) we felt like royalty, what a fantastic reception...


More  




“Great hotel with amazing spa facilities”


We only spent two nights/ three days at the hotel in January. The room was spacious, the bathroom huge. They have a twice-daily linen service and it was the first of four hotels in India where tipping was more of a 'thank you, you have been great' rather than 'we are in India, we have to tip everyone for everything...


More  




“Grand Hyatt Goa_Serene Hotel”


A Huge property with Own beach. Lush green lawns with lot of spaces to just sit & relax.
Awesome swimming pool.
Hotel room service is little slow with bit of waiting time. Probably as we had a huge group checked in. However that's what such huge hotels are meant for & hence should come up to such luxury standards as...


More  




“Wish we could have stayed longer”


We booked a long weekend at Grand Hyatt as a break in the middle of our longer stay in Mumbai. We are so glad that we did. The hotel did their best to make us feel welcome, right from the moment we checked in (the rose water to cool down was a lovely touch). We had a pool-facing room, and...


More  




“Great Beachfront Property”


We stayed at the Grand Hyatt for 1 night, after having stayed at the Park Hyatt Resort Goa for 3 nights. This Hyatt property is very good, with excellent service by the staff. The location of the property is on the shore of the Bambolim bay, which means its not exactly open sea facing, and the sea is very calm,...


More  




“Stay in Hyatt”


Staying at Grand Hyatt Goa is excellents, enjoyed very much in Hyatt. Facilities and staff is fantastic. I recommend this to all my colleagues and friends Would like to visit this place again in future


“Great family visit”


We were 3 families with 4 young kids. It was beautiful weather and a beautiful outdoor pool. Nice clean huge rooms and decent service. Only downside was the commuting from reception and main building to our room, it was tedious and far and also restaurants got a bit boring after a couple of days as we are vegetarians. Apart from...


More  




“Amazing Stay”


After many weeks of travelling around India we finnaly landed at Grand Hyatt on the last leg of our trip. Our whole trip had been in lower end accomodation so we wanted to finish the holiday with a little luxury, and we were not dissapointed! From the moment we arrived the staff made our experience faultless with each staff member...


More  




“Really good family holiday place”


Last May , we visited Grand Hyatt . We took the elite - premier package for 6 days and was a super worth it .
Food is extremely affordable . Indian restaurant , Chulha is the best Indian value for money place in luxury hotels . Loved it . My kids loved the continental and break buffet too .
The...


More  




“Beautiful Property. Self Sufficient”


Hello Everyone,
It's my pleasure to write about this property which I had visited this month with my wife. We stayed here for 4 nights and every day was an all new experience. First of all, the property is very well made and the staff is very helpful. As soon as you enter the place you get a feeling that...


More  




“Amazing experience!!”


Visited for a business purpose...decent property, nice rooms, but the hospitality was amazing. Special mention for Chef Ishaq, a very warm person who goes that extra mile for meeting guests' needs.... A must visit. We all had a blast over all.


“Grand Hyatt Goa”


An excellent hotel for holiday with family. The facilities were outstanding. The rooms were very good and the service was outstanding. The quality of food at all the resturants was amazing. It was real value for money. I will like to go there again with my family. truly a memorable holiday with the family ..one which i would repeat very...


More  




“Good location in Goa”


I stayed there for 3 days. The rooms were nice but little small for a hotel of this stature. You are literally cramped for space although there is a dressing room and a verandah attached to every room but the sleeping area was little small. The food was superb if you order from A la Carte. I attended Christmas Eve...


More  




“Spectacular Stay!!”


Stayed at the grand hyatt goa for 3 days for my honeymoon. The hotel upgraded us to a grand suite with a jacuzzi on the balcony. The service and the staff were amazing and very helpful. I would recommend this hotel without any reservations!


“Bay Beauty”


And that's exactly what it is..possibly the prettiest bay views in Goa and one of the cleanest and tranquil beaches as well. While the Beach is one of the finest in Goa the murky waters and the rocky beach bed are less than inviting. The Beach is best explored early morning and at sunset. What works for the hotel -...


More  




“Beautiful property but something was missing.”


Hard to describe this place. Goa is my birthplace but I grew up in NYC. On my trip to Goa I opted to stay at this property vs with my family. Greeting was polite and initial concierge was pleasent but seemed as hotel was overstaffed without anyone actually knowing what's going on. Lots of trainees perhaps. No golf cart service...


More  




“Paradise in Goa”


Lovely resort on the Goa coast.
Service is excellent - really some of the best we've ever experienced.
Rooms are amazing, pool is very nice.
The food quality overall is very good, in the main buffet restaurant, the room service and the a la carte restaurants too.
Spa and Gym are both excellent.
Couldn't really fault the hotel or staff.


“Fabulous hotel with top-notch facilities and semi-private beach”


I went with 5 members of my family and all of us (seasoned hotel visitors all over the world) were impressed with the beautifully maintained and expansive facilities, the top notch service at the pool and restaurants and super comfortable, large rooms. But the most amazing aspect was the semi private beach with a calm bay- totally undersold by the...


More  




“Fantastic Spa”


I was staying elsewhere and just came here to use the spa facilities. I had a Thai massage which was absolutely fantastic, and other members of my family enjoyed a full body massage - one even said the best massage they have ever had.
Unfortunately the actual spa lacked personality and the waiting room felt like more of a conference...


More  




“TERRIBLE!!”


Housekeeping is terrible at this Hotel. Had to change my room 3 times. 1st room was dirty, full of dust. Curtains didn't close properly. Someone came to repair the curtain at 12am, took his shoes off, stepped on the coffee table, didn't even bother to clean it after that. Bathroom got flooded, had to follow up 3 times for someone...


More  




“Terrible Service”


It's an excellent hotel with beautiful grounds. But the hotel lacked in good service. The Grand Hyatt Goa staff need to be trained at Park Hyatt Goa. The only place that we enjoyed ourselves was at Chulha. Aquasail were excellent and we really enjoyed kayaking with Tonny. They were very accomodating for my 6year old daughter. I will definitely recommend...


More  




“Average hotel”


We stayed at the Grand Hyatt in Goa in Sept 2014 and thought it was an over the top hotel. The service and rooms were average. The food too was nothing to write home about so would not recommend it. Prefer the Park Hyatt any day.


“What a hygiene fiasco!!!!”


Recently we have been to grand hyatt in goa and according to their reputation we did not get any service and hygiene was a major issue. We ordered fresh orange juice at the pool and you would be surprised to hear that the orange juice was having worms floating on top. We again complained to restaurant in charge and they...


More  




“Worst hotel”


Never book in this hotel the service is terrible extremely slow rude staff and charge for wifi while their rates are exorbitant I will never recommend this hotel to anyone the room doesn't get cleaned unless u call them the turndown service is not provided unless u call them they take forever to bring your luggage or collect your luggage


“Excellent hotel stay”


We were at the Grand Hyatt for a night and have had an excellent experience to remember. The property, the services and the stay was just great. We were out the next day waiting for hours for our next activity and this restful stay and experience was a great help for it.


“No hot water when checking in at night!”


When my husband and I decided to get married in Goa we were recommended that we spend our wedding night at the Grand Hyatt Goa. Unfortunately, the experience we had was far from the high standard we'd expect from a five star hotel in one of the world's most popular tourist destinations.
We arrived around 2 am after coming directly...


More  




“Amazing property and beat family vacation!”


Our family of 5 stayed here for 5 nights over the Christmas holiday after a stop in Delhi and had the best time. We planned on visiting other areas in Goa but after being at the incredible resort, we never wanted to leave! (We did however sneak away for a few hours to Panjim for shopping and a change of...


More  




“Good place to holiday lazily”


A very good hotel in a good surrounding. Its a beach resort done up in Villa Style complete with quintessintial balconies to match.The rooms are quite big and luxurious.The breakfast spread is lavish with very good dessert spread. Was pleasantly surprised to find kodaisutir kochuri and cholar dal (Bengali breakfast dishes).The pool is nice big and inviting. The hotel can...


More  




“Great family hotel”


The setting is lovely - grand houses make up the hotel and there are huge grounds leading up to a private beach. The rooms are large and luxurious. The breakfast spread is huge and delicious - the most delectable pastries I have had in India! Service is great - we were there over Christmas when they were fully booked -...


More  




“Quite hotel and unique style”


Washroom is bigger than room which is not good.... We stayed in Guest house 7.. And there was amazing beach view ..
Club access is for no use...and not much option for breakfast in club... So go in main building..
Loved the food in Indian resturtent Chola or chula ..forgot... But amazing food and jalebi
One of the staff workingin...


More  




“Good beach resort”


This is a pretty new hotel and located right in front of the Bomboli beach of Goa. The hotel has typical beach resort villa style and has full facilities including a very nice swimming pool. Everything is pretty clean as it is a new hotel I think.
Room is not super great, but still good and clean. All (or most)...


More  




“not a good experience”


The room was good and was pool facing but there were too mosquitos in the room. The air conditioning stopped working in the middle of the night and with the baby it was very difficult night for us.
The buffet in the ground floor restaurant was pathetic and tasteless, esp the salads were stale . The Chula restaurant was decent...


More  




“Great time @ grand hyatt in gorgeous goa”


At Grand hyatt now celebrating Christmas and family vacation. Amazing weather, great architecture , calm property, secluded beach with sailing and canoeing , morning yoga , excellent rooms. Kids having a blast so parent doing great.
Only suggested improvement is a request for more Indian vegetarian food at the dining room


“Excellent Hotel for family travellers”


This is a great luxury hotel in Goa. Centrally located. Property is large and well built. Excellent facility. Beach is not clean though. Loved staying there. Food is also very nice specially breakfast.


“Great luxury hotel”


had a nice stay and the staff was courteious to our need. The service was upto date. Had great fun. I only hope that they had more beach area for childrens to play. The room was very clean and spotless.


“Live admist old world charm”


The hotel is well situated at the sea shore of Bambolim and conveniently located near the shopping area of Panjim. Simple old world architecture lends the property a unique charm. One clearly feels the sense of space and openness throughout the property. The rooms are superb, big and very well equipped. If you are fond of walking, ask for a...


More  




“Live Life Portuguese Style”


The hotel is about 20 km from the airport and is about a 40 min drive. The hotel is 3 years old and has 316 rooms. Despite being a new hotel I loved the architecture. The building & woodwork has an old world charm. Each building has only 2 floors and has a large balcony overlooking the sea. The rooms...


More  




“GREAT EXPERIENCE”


The best experience I have had till date in Goa !!!
The room was excellent and the view of the sea was the icing on the cake.
Service provided by staff as exemplary and I look forward to visiting it again !


“Gorgeous, huge hotel; luxury in Goa!”


This hotel is first-rate, and a very welcomed experience in Goa! The property is gigantic, but that only means extra service, extra food options, and extra luxury! My husband and I stayed here on our honeymoon and we were truly impressed with the service, the food, the accommodations, and the comfort. The hotel is extremely clean and well-maintained, the food...


More  




“Super hotel”


Visited this property with a few friends. Over all a great property not too far from the airport, yet very secluded. The rooms are great, the open bathrooms are not great if you are sharing the room with friends.
There are many restaurant options with great food. At breakfast time though they seem very understaffed and the service was very...


More  




“Dec 2014 - family from London”


Great hotel .. great ambiance ...felt very tranquil. Staff are very friendly and helpful. The rooms were clean. The extra service charges and taxes on meals and especially spa treatments & WIFI was a lot.


“Wonderful”


Travelled in January 2013 Absolutely amazing no words to describe from the staff to room,service ,food everything. Definitely go back anytime. location is excellent not far from busy centre and in the hotel is so serine and calm.


“Awesome hotel with unsatisfactory service.”


Love Grand Hyatt Goa. However, few problems persists inspite of them being 3 years old. Also, the weddings at the hotel killed part of my stay experience and rest was by the service offered/ experienced. They need to have more variety for the vegetarians.


“Nice Place, BUT!”


I stayed in Grand Hyatt Goa for 10 nights in November, it was a nice stay and the place is so great, staff, service, environment, everything was excellent, and I wrote a review earlier but I found myself willing to write another review to explain my case.
I booked through booking.com first, after that I surprised that Grand Hyatt deducted...


More  




“Amazing property with great grounds”


I stayed here as part of the company offsite and had an amazing time at the hotel. The rooms are really beautiful with back doors that open to the lawns overlooking the beach. The paved road along the beach is very pretty and there is a path at the end leading on the beach, where the hotel has also built...


More  




“excellent hotel however left us with a shock”


great service , beautiful property , good staff ( Most of them ) Beach is not much to talk about ( Black Sand ) , Food was excellent , however a great experience ended on the last day with a rusted nail in the pool almost hurting my 2 year old and the staff being very casual about it


“For someone who has stayed in many Hyatt's and other premium hotels/ resorts”


For someone who has stayed in many Hyatt's and other premium hotels/ resorts... Grand Hyatt Goa is certainly up there with the best I've stayed at! My wife and I who are extensive global travelers for business and pleasure highly recommend this one!!!


“Less hotel, More resort..!!”


This hotel perhaps is one of the most prefered ones for events and conferences. One, the banquet is massive, and two, they have priced their services a tad better than the usual suspects in Goa.
The place is very conveniently located, in bambolim, and if you happen to catch the morning flights, it's just 30 mins drive. Apart from the...


More  




“Luxury and Sea at their best.”


The way to reach Grand Hyatt Goa may be misleading. But the opulence of the hotel will impress you. Lavishly spread out hotels and resorts in Goa is a common feature but Grand Hyatt despite being spread out is very well maintained.
The club rooms are well appointed and pamper you with the space and amenities.


“Luxury at its best - Grand Hyatt Goa”


I visited Goa with my family (wife & an 11-month infant) in September 2014; it was a family leisure trip for 3 nights & 4 days at Grand Hyatt Goa.
I had opted for an all meal package including airport pick & drop. The airport pick up & drop was on time & they provide Toyota Innova for transfers.
Once...


More  




“Seaside elegance”


The hotel is on the extension of the famous Dona Paula beach. The access road is yet to be developed. The beach view is surreal, overlooking the Mormugao harbour at a distance. There is total seclusion for a romantic holiday, as the hotel is nearly five Kms from Panaji city, about thirty Kms from the airport. The "Dining Room" restaurant...


More  




“Excellent”


Rooms and bathrooms both are perfect.it is a huge property with different villas spa was also very nice.our room was facing the beach and had big balcony to relax.very comfortable and good service.just 25 mins from the airport.


“Timeless”


Great hotel for relaxing stay. Excellent timeless décor amenities and service. It is really a resort rather than just a hotel. However if you want to experience an outside of the hotel and a best that Goa has to offer, a car service is excellent and very reasonable. You can hire a hotel car to take you to places and...


More  




“Once again, an outstanding experience.”


My second time now at the Grand Hyatt, I returned with my fiancé after having a great experience with my family earlier in the year.
The hotel does not disappoint for a couples getaway. We took a pool facing room on the third floor which gave us a great feeling of being part of the resort whilst having complete privacy....


More  



