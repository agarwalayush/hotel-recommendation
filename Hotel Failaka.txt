
“Great Hotel”


This hotel is very well run by a wonderful family, who care for their guests needs. Nothing is too much trouble for them. It is very clean and comfortable. The hotel is near two banks, ATMs, supermarkets, transport, pharmacy, shops and several restaurants. The beach is about 18 minutes away and a peaceful one too. The hotel has a restaurant...


More  




“Wonderful hotel with wonderful management”


I stayed here for 6 days in ac room in may'14. The room was fantastic with road view. Beach is very near to hotel..(hardly 15-20 min walking). Mr. Edwin is the best director I ever met in hotel industry. He is very helpful and kind. WOuld like to thank ROny, Pankaj, Edwin for making my stay wonderful. Will surely visit...


More  




“Good Budget Stay”


I had stay 3-4 days in this hotel. This hotel is a 15 minutes walk from the Benaulim beach and is nicely located near the market where you can find daily needs, transportation, ATM, market and a two-wheeler renting place.The hotel is very clean with all facilities. Very good service - specially the breakfast. Visited there in 2012 with a...


More  




“very pleasant stay”


i stayed for 10 nights in a non-ac room in october 2013. the room had an average size and was freshly painted in cheerful colors (orange, yellow). the staff was very friendly and helpful (if i had a hotel i would try to hire the two myself). the hotel and the rooms are very clean with a frequent change of...


More  




“Value for money but not much else”


Its a extremely cheap lodge kind of a place located pretty close to the scenic Benaulim beach. The rooms are extremely small and well maintained. But the quality of furniture and furnishings is pretty average. The people managing the place are good and can serve a decent breafast.


“Wonderful”


At last I've managed to find Failaka on here. We have styled here twice, both times over Christmas and new year. We love it and would highly recommend it. Great place to stay on a smaller budget. Having said that, it is very clean and well kept. You have all you want or need in this wonderful part of the...


More  




“Good Budget Stay in South Goa”


The hotel is very clean with all facilities. Very good service - specially the breakfast. Visited there in 2012 with a bunch of people. Could see that all were having fun - expats, Indians, families, friends, couples. A very good hotel to stay in a reasonable amount.


“Nice hotel at a very good location”


The hotel is a 15 minutes walk from the Benaulim beach and is nicely located near the market where you can find daily needs, transportation, ATM, market and a two-wheeler renting place.
We stayed for 2 nights in the hotel and had a nice time there, especially because of its close proximity to the beach.
The rooms are OK, and...


More  




“Not Bad hotel”


This hotel is well situated near the Maria Hall. We get all kinds of transportation from here cabs,buses and scooters on rent.We get Cabs and Buses only between 8am till 7pm. Did't see any bus or cab after that. The room and toilet was clean but i expected more from the money they charged for the non AC room. The...


More  




“Comfy & homely”


I wanted to spend a couple of days Colva/Benaulim. My purpose was to just chill, soak in the atmosphere of the place, carry a book to the beach and laze around at the beach-side shacks with a chilled beer at hand and looking at the waves. I wanted a paisa-vasool, as we say in Hindi (value-for-money) budget hotel kind of...


More  




“Great and funny welcome! Great value!”


Hamilton , one of the owners, has a cheeky sense of humour and he and his wife,Doris, both have a very welcoming and helpful manner. My non air com room on second floor was spotlessly clean, hot shower, soap, good fan and nice balcony. The hotel also does breakfast....and a book-swap. A small amount of traffic noise only in the...


More  




“Loved it!!”


OK where to start.
We have been to Goa on about 7 occasions in the past and have always stayed in 3 & 4 star accommodation.
This time decided to try something more basic! Well how wrong could we have been, yes the Hotel is not 3 or 4 star but the treatment we received was 5 star.
Doris, Hamilton...


More  




“Happy Stay”


During the stay at Failaka ( an island if not mistaken) the feeling that we were away from home in some hotel room, did not come to mind. It was as if we were in our house. Nice Hotel. May stay again with family next time.


“Small friendly family run hotel”


Have just returned from our eigth stay at this levely small family run hotel. Hamilton, his wife Doris and the rest of the family looked after us very well. Accomodation is basic but clean and well maintained and prices are good value. We had our usual room, 207, which has 2 balconies, A.C. and flat screen T.V. Breakfast is taken...


More  




“BEST little place there to live...!!”


Personally i loved ths place to live...rooms are clean,warm,hav a perfect ambience..even the small balcony whr u can sit i cud hear the live music played in tht restuarent besides..whch was again a cherry on the cake.....evn the bathroom is efficiently big..n the beanaulim beach is jst 15 min away..probably the one quite clean n rejuvenating beach in goa...so plz...


More  




“Homely Hotel”


If you are going to Goa and staying in the south then please do yourself a favor and stay at Failaka. It is both clean and well managed. The staff are efficient and the owners are friendly. It is a family run hotel and soon you feel like you belong to the family. Nothing is too much for them to...


More  




“Awesome”


I personally recommend everyone to stay in Failaka Hotel if you are visiting Goa - good service, friendly & helping people, nice ambiance, perfect location just 10 minutes walk from one of the best beach of Goa(infact Benaulim beach is the only best beach compare to Baga/ Calanguta or Colva beach as Benaulim beach is clean & safe & no...


More  




“Clean rooms and friendly service”


The rooms have a good size and i even had a balcony. The bathroom was fine and clean. Roomservice did a good Job everyday. It was always nice to come back in the room. The Hotel is a Bit far from the beach but you can rent bycicles everwhere in the village. I didnt miss a thing at failaka. My...


More  




“Like staying with a Goan family!”


We landed in Goa without much of preparation and found ourselves booking two rooms in Benaulim's Failaka Hotel. We had no particular expectations since the we were coming from Delhi following a series of horrible hotels. Failaka was a breath of fresh air in India. The place is very nice. The rooms were very clean, tidy and very affordable. The...


More  




“Best place near by Beanaulim”


Over all experience was good. It is the best place for the couples or family as it is run by the family. It was very well maintained and cleaned regularly. If u r looking out for a decent place in a budget price then this is the one :)


“Delight of South Goa!”


WoW! This place is a total delight and everthing we could hope for here - the staff is over and above expectations with cheerful family run environment that make you really feel like friends the second you arrive! Alot of colour and happiness here, the rooms are really clean and very pleasant, good bathrooms and you even get your own...


More  




“Basic, clean, and friendly”


Stayed here with my daughter over Christmas.
If you're content with accommodation that is clean and basic, I'd recommend Failaka.
Small, with only 16 rooms, half of which are allocated to packages. Lovely small garden in which to enjoy breakfast or relax.
Owners and staff are lovely, friendly people who will do everything they can to make your stay enjoyable,...


More  




“Good budget hotel, don't expect any frills”


This is a well situated guesthouse/hotel just off the Maria Hall cross roads in Benaulim, and aprox 15 minute walk to the beach or a 60 rupee trip in a rickshaw. This is definately for the budget traveller, it was a little too basic for us, however, the staff were great, basic breakfast served, toast, tea, coffee, fruit juice, but...


More  




“Very clean family run hotel.”


This guest house is spotlessly clean and very good value for money. Breakfast is basic, served in a lovely little garden at the side of the hotel. The staff are friendly and very obliging.
Only down side is the music bar/restaurant two blocks away, which can be noisy at night especially at the weekend or a locals party. We had...


More  




“holiday”


Nice family run hotel. Basic, but kept clean and if like me all you need is a bed, shower and balcony then its ideal. The family Agostin and his wife along with sons Edwin and Hamilton are very welcoming and helpful. Two young lads Richard and Malcolm serve breakfast and keep the rooms ect clean. Became good friends during our...


More  




“A friendly family-run place”


As we expected, the hotel was very basic and the room was small with a little balcony overlooking the garden. There was a ceiling fan and the temperature was perfect. The management and staff were very pleasant and helpful and responded quickly to any requests e.g. when our shower head needed fixing. The place was spotless so it was a...


More  




“Lovely Hotel but can be noisy at night”


As the previous reviewer says the hotel is lovely the staff are really friendly and the place is spotlessly clean but in the evening when the club which is very near starts up the noise reverbalates right through the hotel. it is very difficult to have a quiet evening drink on the balcony it is not the quiet family location...


More  




“nice hotel,but loud”


the hotel is good and the staff better,but should have been informed of the open air disco-live band venue 30 meters from the hotel.lasted 3 nights before had to move due to lack of sleep.would be more suitable to younger visiters who like to party till the early hours.


“Great hotel BAD location”


Just got back from a jewel in the crown holiday at this hotel. The hotel is charming, clean and the staff extremely friendly and helpful rooms are lovely for this level of hotel. BUTand quite a big but no one seems to have mentioned that very nearly next door is Filipis a restaurant and "entertainment centre" which plays loud disco...


More  




“A most relaxing hotel atmosphere”


Fantastic simple hotel. Family and staff brilliant, rooms exceptionally clean. We were made to feel like one of the family.
Definately recommend.
Love to you all from John (Iain) and Myra Clark, the scottish part of the "grumpy club"
Happy New Year!


“Great Location, Warm people”


I went on a business trip to this hotel however I was accompanied this time by my husband!!
Being a budget hotel it does not have air-conditioners or show off stuff. But the hotel rooms are absolutely clean. The beds are comfortable. You have wardrobes. The bathroom was absolutely tidy. The staff cleans it everyday. I went their in May...


More  




“Delightful hotel in Goa”


A very nice hotel, and very reasonably priced, with clean rooms and most importantly, delightful owners and staff. By far the friendliest of any of the many hotels I have stayed at over the years in Goa. I arrived with an upset stomach and was rather indisposed on the first day and they very kindly brought me tea and toast...


More  




“Budget price - 5 star family and staff!”


Our first trip to South Goa, but won't be our last. Quieter, more relaxed, and better value than North Goa.
Accommodation was simple, but very clean indeed. The family and their staff were among the nicest we've met anywhere in India (and we've been many times). We arrived as guests, and left as friends. Not pretentious, but you couldn't find...


More  



