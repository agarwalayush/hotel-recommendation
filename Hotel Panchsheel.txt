
“Nice budget hotel”


Hotel Panchsheel is located within walking distance to the Margao market and Municipal garden. The hotel is well connected as there are regular buses running between the Margao bus station and Municipal garden which is around 0.5km from the hotel. There are quite a few restaurants and places to eat in this area, Hotel Kamat is a very good option...


More  




“Very Bad experiance”


Hi All,
I was at this hotel from 4pm, 31st Dec-14 to 1 pm, 3rd Jan-2015. Hotel is ok ok. The normal tariff is INR 1200 for A/C and INR 1100 for Non A/C Double Occupancy. But these hotel booking sites ask for 2K, 3K and so on.
Ok, Now why I rated so bad to this hotel. I asked...


More  




“Very good hotel to stay with Family in Madgao”


This Hotel is really very good option those who are traveling to madgao or to visit goa. Hotel is near by station hardly 1 Km. from platform no. 2 . Hotel has good room service and supportive staff. yummy breakfast in morning. Not a costly hotel compare to the quality of the room.
Also the location is near madgao market...


More  




“hotel panchsheel”


The A.C. suits needed to get equipped with Split A.C. , the window A.C. was having very much sound of blower. Cleanliness was good.
Break fast is not having much choice but quality is good.
car parking is limited


“After staying here with my family for 3 days I regretted why I missed this good hotel during my last visits.”


We stayed during Oct 2014 first week. After staying here with my family for 3 days I regretted why I missed this good hotel during my last visits. Hotel is maintained well, good service, rooms cleaned daily, delicious breakfast which is complementary. A nice experience.
N. Jayaram, Bangalore.


“Good Hotel for family tour”


While we visited Goa as part of family tour of KSEBOA, we stayed in this hotel. This is a very neat hotel. Complimentary buffet breakfast was just outstanding.
Staff were very polite and the location was very nice.


“Best hotel”


I recommend this hotel to every one. The staff is very cooperative. The complementary breakfast is good.. Panchsheel has a reasturant also were u can enjoy delicious food. They can also provide u Airport transfer for just Rs 700.


“One of the best Budget hotel in Margao”


Just one Kilometer away from Margao railway station { from Platform 3 }, the rooms are basic but neat and clean, the tariff very reasonable, breakfast included , their restaurant was closed, located in walkable distance to city center , this hotel is absolute value for money


“good hotel in madgaon”


I have visited Goa and stayed at Panchsheel Hotel in Madgaon on 28th april with my family.The staff is very co-operative and supportive.Hotel is neat and clean and near to Madgaon Railways station.buffet breakfast is included in price and we get idli sambhar,tea and fruits ,bread and jam,as much as you can eat.The hotel arrange the tour by bus or...


More  




“Average”


i find the room average, the washrooms were clean but bit smelly as the exhaust was not working properly. The staff was not supportive, my AC was not working and staff only came after i called them 4th time. The only thing i liked was the breakfast. They provide free buffet breakfast which was good. If u want to take...


More  




“great Living Room”


Great Living rooms are clean and comfortable, relatively low cost, very helpful staff colaziane great chance to hire bikes and scooters to go to the beaches, public transportation within walking distance and highly recommended


“Nice place for family Visit”


I have visited Goa and stayed at Panchsheel Hotel in Madgaon .The staff is very co-operative and supportive.Hotel is neat and clean and near to Madgaon Railways station.The hotel arrange the tour by bus or taxi which ever you like according to your budget.I recommend this hotel.


“Worst Customer Service, Embarrassing...”


Arrived here with friends booked in advance, needed the room for only one night. Upon arrival was told that two girls and two guys cannot stay in the same room. We had booked a deluxe suite which accommodates four people and they were okay when the room was booked. Guy at the reception was rude, simply said look some where...


More  




“Large, clean room”


Lovely hotel in Margao within walking distance of the railway station. Pleasant staff on reception. Room was spacious and clean but the bed was hard! Also the coloured flashing lights outside our window kept me awake. Our package included breakfast but unfortunately breakfast only started at the late time of 8.30am so we didn't have time to eat before our...


More  




“hotel review”


Good hotel to stay . Me and my friend have a nice stay of 2 nights and three days in panchsheel. the complementary break fast is also good. I think this is a good hotel for those who want to spend a few days in Goa for picnic.Thanks.


“Nice hotel to stay”


Clean Rooms, Lift, Free breakfast ( food quality is average ), Good vegetarian hotel nearby hotel if you are vegetarian, the one restaurant near hotel is costly and serves non-veg and also has a bar.. overall satisfied..


“Average but no jet/pipe in western commode !”


I stayed in this resort from 24-27 Dec 2013. One of the OK hotels I have been to.
Let me list out + ves and -ves of this hotel :
+ Ves
1. Closest to Margao market..only 5 mins..whenever u feel ..go to market...
2. Good complimentary breakfast.
-ves
1. There is no jet or pipe in western commode. Yes...


More  




“Good hotel poor staff & worst management”


We visited this hotel a couple of times. First time the rooms were regularly maintained but standards seemed to come down on our second visit. The restaurant adjacent was no exception. The main problem with the hotel seems to be it's staff particularly the reception. Wonder if they got any training at all in dealing with customers and Goa being...


More  




“an average experience”


We chose this hotel because it was cheaper than most others in Margao. The hotel is nicely located.Its a few kms from the bus stand but a stones throw from Railway station. There was some renovations/maintenance work going on when we visited at some places when we visited. The non-AC rooms were large spacious and well decorated.The towels and bedsheets...


More  




“Good budget hotel”


the hotel is good. But parking is a problem. Rooms are spacious especially the AC rooms. But it would have been nice if they instead of throwing in complementary b'fast had reduced the room rate that bit as the complementray b'fast was not up to the mark. Good veg resturants nearby is a plus. It is quite in the heart...


More  




“Budget Hotel, Not so Bad - Not so Good”


The Rooms are good. but the service is bad. Staff is not so good. they do not care. AC's are old and needs to be replaced as it's Pathetic - Noise making AC.
The Manager does not listen to your query. they are very reluctant to make changes to make customer's feel comfortable.
I was on a business trip for...


More  




“Budget Hotel, Not so Bad - Not so Good. Jet Privilege No. 141241354”


Hotel panchsheel is a budget hotel, so don't expect anything luxurious.
Let me start with the hotel's location. The location of the hotel is about 3 km from bus stand and railway station. The taxi stand is about 7 min walk able distance.
I booked the hotel through makemytrip.com I checked in with my wife and kid. Instead of having...


More  




“Good Location, nice rooms and reasonably priced”


Nice hotel for a budget trip and quite comfortable rooms .I feel the location is good and near to all beaches in that part of Goa, especially Majorda beach which is a great beach with less crowd. Also those who are vegetarians this is the best place to stay.


“Truly embarrassing customer service”


After arriving at Hotel Panchsheel in Margao at about 6am, we tried to check ourselves in. Although we were booked for 11 nights, and they freely admitted our room was already available, they kept pointing (smugly, in fact) to the sign on their reception saying check-in at 12 noon. The only way they were prepared to let us into the...


More  




“terrible customer service”


When we arrived at Hotel Panchsheel it was 6.05 am, on arrival they told us our room was ready but that they would charge us extra to go to our room before check in we said that we did not want to pay their half day rates for early check in so instead we would just wait in their lobby...


More  




“Don't use this hotel”


Sadly, after stupidly using a travel agent for a trip to the south of India, myself and 2 friends ended up in this hotel.
Let me get the positives out of the way first, the breakfast was tasty and the room was clean, upon arrival at least as the hotel made no effort to keep it that way...
We stayed...


More  




“Decent Stay”


It is at good location in terms of the reach to the bus stand, railway station and also any essential needs that may be needed. They come under budget hotels category and can provide with the basic necessity in the locality and not the luxury , pomp an other facets.


“Waste of Money..All izz bad”


Me and 2 of my friends decided to book this during new year eve...the hotel location is too bad..we took almost 30 mins to locate the hotel..hotel staffs attitude is too bad...they don't even pick the intercom...the towels and bed sheet were dull and dirty..we asked for an extra bed which was half broken...the complimentary breakfast really sucks...u can't even...


More  




“Elegant stay in Madgaon Goa”


I was with my family and went to Goa. While returning I wanted to get Konkan railway from Madgaon. This hotel is close to railway station (around 1.5 km) and can be walked down with less luggage. We approached the hotel from Madgaon municipal building where bus leaves you as last stop. We had to inquire a lot to get...


More  




“Sleep on the road but Don't stay at Panchsheel Hotel”


I had the misfortune of staying in Hotel Panchsheel near Madgaon in South Goa. I have seen good hotels, I have seen bad hotels and I have seen average hotels but this hotel should come in the category of worst hotels.
I had booked their most expensive air-conditioned suite and this is what I got:
The ants were crawling all...


More  




“Good Hotel to Stay”


Hotel is good and neat and clean.Only tea is served in room.For breakfast,lunch and dinner have to go to the restaurant which is below.


“not worth it”


big stains (blood?) on the wall and in the bed, dusty, wires are hanging out of the wall, uncozy, bathroom was clean (but stinky), neonlights, dirty linen (hair plus stains)... even the pictures on the wall are hanging crooked... and it was hard to understand the staff.
doubleroom with fan/no ac: 750 indian rupees


“Decent Budget Hotel”


I am a regular Business traveler to Goa. Generally I stay at the Ginger in Panjim.As this trip involved a larger part of my work to be in Madgaon, I sought out a Budget hotel in Margao. A business acquaintance suggested Panchsheel. I checked in and was pleasantly surprised at the room and its cleanliness. The rates were reasonable -...


More  




“Hospitality and quality is marvelous feel good as at home”


the hotel is cheap and best for stay with homely feelings


“HOTEL IS VERY FAR FROM TOURIST SPOTS”


If you want to visit GOA as a tourist i will not prefer or recomend this hotel because it is very far from tourist spots of goa.maximum tourist spots of GOA are situated at NORTH GOA and this hotel is situated at south goa.you have to spend too much of money and time to approach tourist spots from here. so...


More  




“loving trip”


so beautiful hotel and services to fast in this hotel


“Dont Stay at Hotel Panchsheel”


First and foremost, don't stay in madgaon if u want to enjoy your stay in Goa. Every thing worth seeing is too far away. The money you save in Hotel Tariff will be nowhere near to what you will spend on transportation.
As for the Hotel mentioned above, the only good thing is room. There is no fridge even in...


More  




“Fine arena”


The hotel is neat & clean. The room furnishings are beautiful.
The service is fine...


“just fine”


My husband and I stayed at the Panchsheel hotel in Madgaon for 1 night. We were travelling sorta backpack style but staying in mid-range hotels. And this was a good a mid-range Indian hotel - reasonably clean, hot water and even a tv.
Madgaon itself was great and I recommend a 1 day stop here to visit to see a...


More  




“Service beyond expected!”


We were a group of 7 people stayed here for the new year celebrations. We had a great time staying here. Food and Serveice was excellent. The rooms were kept clean more than we expected. Service provided by Mr. Venu and hotel staffs is definitely worth mentioning. Over all we had a great holiday & Stay.
Regards,
Shreedhar S


“Good Hotel at Margao”


While searching on Internet for a good hotel at Margao Goa, I came to know about Hotel Panchsheel. Although there was insufficient information available over Internet about this hotel, I took the risk for reserving Deluxe A/C suite in this hotel for four days through website TRAVELGURU. Before reaching at hotel, I was very furious abt the hotel. After reaching...


More  



