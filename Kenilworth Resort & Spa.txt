
“Fantastic Pool”


I was looking for a quiet place to unwind before Christmas and the Kenilworth ticked the box. The grounds are huge and the pool is fantastic ! Unfortunately I was there in wedding season which meant on the days there were no weddings the service was great and the days when there was a wedding the service dropped dramatically. The...


More  




“Excellent Resort with all amenities and the most helpful and pleasing staff”


I and my family just visited Kenilworth, to my surprise they have had the place renovated to give it a more palacial look, the staff there is exemplary and very soft spoken, the best quality is that they have a very compassionate and caring management specially Mr. Vishal (Resort Manager) and Mr. Sanjay (Front office manager). they are very helpful...


More  




“Awesomeness”


One of the best resort of goa with excellent property.. Check in was smooth. It was a birthday so they offered a complementary cake at 12 in the night.. The property was so awesome that we couldn't get time to go to the beach. Comfortable room with all required accessories and toiletries.. The food is good if u are a...


More  




“Incredible activities, disappointing food”


Absolutely wonderful place to visit in a group. Lot of activities/sports at your disposal. Courteous staff. The food is very disappointing though. Walk down to Zeebop for lunch and dinner. Amazing pool, beach football, etc. Tranquil ocean in most seasons.


“Lovely well maintained property in south Goa”


We stayed for 3 nights. Good rooms...excellent breakfast....
Lush green lawns....
Clean swimming pool....
Good spa. Little expensive tough.
We had 3 rooms. The presidential suite is luxury at its best. With ocean view.
Courteous staff.
Didn't much care for the lunch we had.
We mainly ate out.
Zeebop. Fishermans wharf. Martins corner.
Secluded...tranquil.... Scenic..... Value for money.....superb property in...


More  




“This is not what a 5* Hotel should be like.”


My family and I stayed in room 123 at the Kenilworth in Utorda Beach, Goa, for the nights of 19th and 20th December.
The catalogue of problems of varying degrees of severity we encountered came as a huge surprise after 12 days of faultless hotel experiences elsewhere in Goa (Dunhill Resort in Agonda Beach stands out).
So what were the...


More  




“Paradise”


Dear viewers,
First I Thank to Thillai Tours and Travels, Chidambaram - for their excellent guidance to accommodate in KENIL WORTH, GOA
Resort is Paradise / Heaven / Luxury for all.
Facilities for kids recreation like ( games, home theater, cycling, etc) . also volley ball, tennis, billiards etc
Very safest place for all.
Food were too excellent.
@@@ Over...


More  




“Fabulous hotel”


This truly is a wonderful hotel, located in the quieter - and in many respects, nicer - South Goa. We were here for a wedding and as anyone who has attended one at a resort will tell you, they can become jamborees where patience is often required whilst dealing with overworked hotel staff. Not at the Kenilworth, though, where we...


More  




“Fantastic Facilities.”


I didn't stay at the hotel but my mother Margaret is a spa member there and I was fortunate enough to visit the hotel a few times with her in December when I was on holiday.
The gym is excellent, the grounds are beautiful and very well looked after, the food from all the outlets is of a high standard...


More  




“Pleasant, Refreshing”


We chose this for family vacation and were not disappointed. With direct access to a beach which has watersports (which is not common in South Goa), as a family we had a great time. Parents could relax as well, either inside or in the beach. Refreshing to walk around, pleasant by evening, and relatively larger property with a large lawn...


More  




“Family Vacation ”


The KENILWORTH Resort in Goa is Truly a home away from home for me and my family.
This is probably the fourth or fifth time we have stayed here and each stay has been better than the previous.
The food is fantastic, ambience is perfect and the staff very helpful and courteous
For us this is a preferred getaway destination...


More  




“Location and settings are outstanding. Service is not”


So we travelled here for a wedding and we thought the facilities and the look was amazing - 1 of the best 5 star hotels I had been in and although I hate the term, it really was paradise on Earth
But when you look closer, you start to notice the poor service this place has
1 day a club...


More  




“Do not see the websites and follow the hotel as there are many hidden charges compared to the rest of the 5 star resorts”


Checked in for the New Years eve package for 3 nights paying with 3 rooms at the high season rates, but after coming we realised that the hotel being a five star is charging for all its amenities, even the steam and sauna which all the five star hotel gives free this hotel is charging for it, tried speaking to...


More  




“KENILWORTH OUR HOME FROM HOME”


We have a house in south Goa but frequently stay at the hotel and use its beautiful facilities- Gym and Spa, direct access to the beach, best pool in Goa etc. The staff are really friendly and very keen to ensure that clients enjoy their stay.


“Best wedding destination hotel in Goa!”


Simply put, an outstanding resort with wonderful staff that go out of their way to provide warm and professional service!
My wedding reception was held at this resort, and in spite of the fact that 4 other weddings were being held the same week, and was quite busy, we were never ever let down by the level of service! There...


More  




“So beautiful”


Just got home from this wonderful hotel, it was beautiful, just like paradise! The pools were stunning, an amazing spa. The food was great! The rooms were fab, the laundry was good & quick. The airport shuttle was spot on. The service could prob do with some attention but it is India after all. Couldn't really fault this place, want...


More  




“Special”


I came with the wedding group, the wedding was done amazingly, and the staff working their are so helpful, specially the spa area, the girls were so kind, the breakfast boys were very welcoming every morning and my favourite were the boys from the pool area, Anil was a great guy, and really deserves to get an award or even...


More  




“one of the best resort in India”


I would say Kenilworth as on one of the best resort in India , it has best in class service and the food was really good . I enjoyed my stay there , specially the property of hotel was full of different options for guest.


“Excellent service”


The Kenilworth Resort and Spa,Goa is one of the best hotels I, have ever visited .We were given a very warm welcome by one of the staff members .All the staffs were very hospitable and provided excellent services specially Mr Nityannand Kumar (sous Chef) and Mrs Gousebi (Trip Advisor) .The hotel was very neat and clean and well maintained even...


More  




“A complete package ...”


Travelling in a big group becomes a nightmare if the place you are staying is not able to manage individual and collective needs. We stayed at Kenilworth and were very happy with that experience. Here are some key highlights/tips that should help others...
Reservations - The hotel has done very well to employ great staff to take care of reservations....


More  




“A little bit of heaven by the Beach”


The Kenilworth Resort and Spa is one of the most impressive hotels I have been to in Goa. The wonderful entrance lobby with some great pieces of art makes it such a welcoming experience. We were greeted with a shower of Rose Petals, now that is what I call an impressive entrance. That is just the beginning, the rooms are...


More  




“Amazing holiday relaxing”


The hotel is elegant and extremely clean. Staff reception is excellent always willing to go out of their way. It was very warming that staff remember faces and needs. We took my father who is wheelchair bound every effort was made to make his stay comfortable and enjoyable.
The food is very tasty various menus to choose from.
Swimming facilities...


More  




“It is worthy !!!!!”


It was good hotel and it have A big swimming pool and this hotel specially on beach is very good thing all things in hotel is good playzone is good but the food is not that much good If you want to go for relaxing then this hotel is best in goa but this hotel is far from north goa...


More  




“Beauty at its best!!”


The stay at kenilworth last time in 2012 was memorable one however this time again the stay at Kenilworth got just all the more better with services getting enhanced. Now the Hotel boast a good Movie Hall, gaming Centre amongst others. The already amazing food at the buffet also got just better.


“Hospitality at its best”


Had a memorable stay. Staff very generous and takes care of all your needs. Beach is just outside the hotel premises. Simply enjoyed our stay. Very clean swimming pool. Nicely equipped children activity area. Highly recommened.


“Loved the stay”


We have been to Goa several times, but this was our first holiday in Goa with our little one and we wanted to make it special for her. The minimum criteria for selecting our hotel was access to beach, swimming pool, and away from the crowded areas. While meeting the bare minimum criteria, it also exceeded our expectations on service...


More  




“So Quiet”


This is an amazing place if you a peace of mind. Huge property with beautiful garden and a ready access to the beach. When you dont want to go to the beach, the pool is there to help you out and its not bad either. Morning breakfast is amazing helped by the neatly placed restaurant near the garden.
However, this...


More  




“Enjoyed”


Nice cool place. Saw a great dance performed by tribals. And gazal evening was great.
Nice food. Enjoyed delicious breakfast...
Bartender was ao nice...he prepared a cool cocktail of his own when I was confused...
It has their own beach...its bit far but clean...overall a happy experience.


“Excellent Property coupled with Awesome Staff”


I traveled with my wife to celebrate her Birthday in Goa. I am sure this has been her best birthday ever thanks to awesome hospitality of the staff at Kenilworth and the beauty of the property. The upside - pool with the sunken bar, food, rooms, Utorda beach, club, gym, cleanliness, people, value for money... everything. The downside - absolutely...


More  




“Something For Everyone”


After my seventh visit to goa (everytime to a different hotel).... i am compelled to write a review about this particular hotel... (Resort better describes it actually)
just about 20-25 min from airport this resort gives a perfect look n feel of white sandy south goa, which happens to be just 2-3 min walk from our room...
the entrance i...


More  




“Great experience at Kenilworth”


Stayed here for a week with family in early Oct... great resort with courteous staff... some initial glitches aside (minor housekeeping issues on Day1, most restaurants under renovation), it was a fabulous experience. Highlights:
~ Great beach - clean, pristine - Utorda is one of the best beaches in Goa
~ Great property & grounds
~ Awesome swimming pool with...


More  




“CONFIRMED BOOKING BOTCHED UP”


This review is bit overdue. Of course what happened to us cannot be reversed now.
1. We reached the Kenilworth Goa hotel reception after a long tiring rail & road journey on 21st Oct’14. After wasting a good deal of time, to our utter surprise and shock, the reception staff informed that they could allocate only ONE room for our...


More  




“Good Location but management requires major overhaul !”


10 of us visited this hotel for a family break and whilst the extensive renovations were disruptive we were prepared to overlook most of the negatives -
Bar was closed for first 2 days of 7 day vacation.
Only 1 restaurant open for the whole duration with repetitive menu every night .
Overcrowding due to lack of restaurant facilities
Poor...


More  




“good location, good property but still not able to make it perfect”


Location is good, property is also good but still lot of thing missing to make it complete. The reception process needs to be more smooth. There was only 1 restaurant functioning that also a temporary arrangement, was really a mess ....i think there is too much renovation going on and one needs to wait till everything is over.


“Awsome resort!”


Nice property right at the beach. It has little old kind of room arrangement. This place has all futuristic amenities like fantastic swimming pool, gym and spa. Food needs a bit improvement. Staff is friendly. There was some repair work was going on in lobby but didn't affect much.


“Amazing resort!!! Worth every single penny!”


This is an amazing resort located between north ans south goa. The rooms are little small but very well maintained and have all the amenities. The staff is very curtious. Swimming pool is excellent with a floating bar. The beach adjoining is also really awesome. Qhote silverry sand, clean beach. Not very crowded also. I found this beach much better...


More  




“Fantastic property”


I have spend 2nights with my family at KKenilworth. Beautiful ambience, amazing food, friendly staff,big pool,clean beach,well equipped gym, what else you need? I have spent very memorable moments here. My kids really enjoyed.


“Good Hotel”


This beautiful hotel is located along the utorda beach. An old building has been converted into hotel. The rooms are spacious and clean. The beach is excellent. The swimming pools are excellent but they allow people without proper costume and shower. Pools are available for all age groups. Food is good. Outdoor activities are available in this huge resort. Service...


More  




“Good but not the top”


I have been recently with my family and our impression (not extremly positive) is also due to the fact that during our presence there were some activities in progress. The Hotel was not ready to receive guests even if it was sold out. The weakness point is the organization, I spent more than half an hour for paying. Generally speaking...


More  




“Peaceful location, Great resort!”


I and my wife really enjoyed our stay earlier in October . Kenilworth is at a great location with a very peaceful atmosphere. Well maintained rooms, very warm staff and amazing food. Some of the areas inside the resort were getting renovated: pool bars and also lobby area which was not creating any disturbance as such. Food at Mallika restaurant...


More  




“Amazing....Resort ...Beautiful Lawns ...Awesome Beach”


This resort is Really Amazing ... Beautiful Entrance , Lawns , Interconnecting Swimming Pools with Sunken Bar and Open Jacuzzi ... Big Play Area and activity Center for Kids Food is Awesome ...Unforgettable White Sand Beach Out of the World experience ... Not too far from Airport i will definitely recommend for a Perfect Family Holiday...Go Goa ...Go Kenilworth


“Great Place for a Perfect Holiday Experience”


The resort is an awesome property extremely well maintained - lush green lawns , a large pool - The Open air Jacuzzi is a great place to enjoy...
Everything was excellent - Guest service , Food & Beverages, housekeeping etc.
The food was a grand feast at all the times .The Gala Dinner on the Diwali Night was great &...


More  




“Best Place for Luxury and Fun”


To begin with this was our first time in a 5* Hotel..and believe me it was totally up to the mark..
1. Prompt pickup at the airport.
2. Grand welcome at the Porsche of the hotel.
3. Extra warm service by the staff.
4.Extra spacious rooms.
5.swimming pool with attached bar.
6. Beach at the arms length.
Food excellent..
Totally...


More  




“Good Holiday location, Kenilworth Goa”


The Hotel is located bang across the Beach, and the property is large with massive swimming pool and lush lawns.
Best part is the friendly and cooperative staff, who are always interacting with the guests to make them comfortable and seek valuable feedback.
The buffet spread for breakfast is very good as well.


“Great Property and service”


My wife n myself went to bring in our anniversary. ..
Hotel property is beautiful n all online bookings were well taken care of.
Hotel staff was very good n helped in arranging cabs at good rates for us to travel to old goa n north goa.
Food is great. ..
Only hassle we faced was the overflowing toilet as...


More  




“we fell in live with you and you worth it!!”


My family means fun they go to any cirner of the world to have a great time of togetherness.this october's first week we drove our car filled with family and wishes to be in the laidback lap of goa.we stayed for 6 nights in goa although went for 3 .kennilworth resort utorda was our destined lap of beautiful nature around....


More  




“Not so good”


I visited this hotel with my family for a holiday trip. The location of the hotel is very good. The beach is not very crowded and enjoyable. The food in this hotel is horrible. Only one restaurant was operating. The buffet was served in a temporary shed full of flies. The rooms were not sound proof hence noisy. You can...


More  




“lovely”


The greatest thing about this hotel is that you can walk to a fantastic beach right from the hotel virtually without leaving it. The swimming pool is the best I have seen yet. We really had a peacefu l and yet fun filled week there. The staff there is polite and efficient. They seem non existent yet everything is always...


More  




“Good PROPERTY”


Good landscaped property with a beach but some work needs to be done on overall hospitality and menu options. Good place for a short holiday with friends and family. Kids will enjoy the Jacuzzi and the swimming pool and elders a drink by the pool side


“Our best ever Goa trip”


Landed on time, we headed straight to Kenilworth Resort & Spa. We were upgraded to suite room, courtesy, Mr. Khosla & Mr. Sanjay – they made sure that we have a gala time. And Kenilworth was indeed worth it! Sprawling resort with immense open space and greenery, awesome pool, beach facing room – every bit was mesmerizing. And to top...


More  




“Kenilworth Goa is the best”


This review is long pending.
I and my wife along with my in laws who always wanted to see Goa booked two rooms for 4 days in August this year. Also it was my wife's birthday. We decided to go back to Kenilworth where we had an equally amazing experience 2 years back.
Right from being greeted by the hotel...


More  




“Could be better”


Stayed here in the first week of October.
Pro
Huge and nice pool you do not realise from the photographs. Nice 3 star rooms. Nice activities. Lovely and most importantly a clean beach.
Con
Food is below average. Only one restaurant open at that time. Expensive food at 1000 a plate for buffet.
Lot of renovation work going on.
House...


More  




“Relaxed stay”


If you are looking for a place to have some quiet time then this is apt for you. Its located away from the busy touristy beaches and places. It has a nice pool and lush green lawns which lead to a gate towards the beach. The beach is not too occupied. There is one or two shacks as well.
Hotel...


More  




“Mixed feelings”


Really mixed feelings about our recent stay at kenilworth. Some great aspects, some not so.
Let me start with the good;
On first impressions we were in awe. Hotel and property much bigger than expected.
Greeted with smiles on entrance.
Able to use hotel facilities until our room was ready. We were early.
Fantastic swimming pool. Staff at pool,especially Anil...


More  




“Disappointed”


We just came back from our holidays at Kenilworth. Stayed 4 nights and booked 4 rooms in advance.
Trouble started from Airport pickup. They messed up big time that we waited for 25 mins at the airport with our infant to gt our taxi.
Check in was very slow. Took atleast 20-25 mins to get the key. On of the...


More  




“Tranquil location for relaxing”


This resort is located in a tranquil and peaceful place in Utorda (Salcete) in South Goa. The resort has a private access to the beach and a very well maintained garden/ lawn with lots of greenery.
The swimming pool is very good. They have a basket ball court also apart from a gym.
So a family can have a very...


More  




“Perfect Stay”


I had a fabulous experience at Kenilworth, Goa. I stayed there for 4 days.
Overall Pros:
1) Excellent hospitality (especially by Veena and Gousebi).
They went a step ahead to make the room decoration and cake setup for the surprise planned by my wife.
2) Decent spread of breakfast menu (although a little repeating)
3) Easy and quick approach to...


More  




“Pleasant Peaceful stay in Kenilworth Ultroda Goa”


My self, my wife stayed in this property along with my 2 grand kids for four nights, we had a pleasant and peaceful stay. It was surrounded by green paddy fields & Coconut groves, the beach is in back yard. The food served in Buffet Breakfast, Lunch and Dinner is widespread and good. Except a small glitch, the front office...


More  




“Awesome stay”


A serene, exciting and best experience in our 2 night stay in Goa.
Had been to this place in Sep,2014. Best hospitality & care !!
Excellent facilities.
Such a scenic beach- at hopping distance.
Food was awesome
Enjoyed evey bit of stay.


“Good but could be better”


We had great stay at kennilworth with my family.Initially we were not happy by room hospitality offered at the restaurant but after that We had no complaints. As it was my birthday they had beautifully decorated our room.swimming pools are something really best if you r looking for.resort have excellent property with beautiful sea beach connected.they have excellent tennis courts,basketball...


More  



