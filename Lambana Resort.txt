
“It did the job”


We stayed at Lambana resort from 30 December 2014 to 9 January 2015. We arrived at the hotel which was in near darkness in the reception area, we got to our room which was on the side of the hotel and was very noisy from the traffic outside, we did complain and asked to be moved but were told this...


More  




“Was OK”


Hotel was ok however very unsafe if it's only two girls. The reception guy acted so cheap by coming to our room with some excuses and not only that till 3am he was knocking in my room by name. Rooms are attached so he is also knocking that door for me to go to him and have a drink and...


More  




“Great hotel”


This was mine and my fiancée's first trip too Goa and we really had a great holiday! The hotel itself was ideal for us because the rooms was just standard with fan & air and also had a mini fridge. Which the prices for drinks are so cheap it's unbelievable! We only ate once in the hotel and the food...


More  




“Very clean spacious room”


This hotel was very clean. It included a fridge / mini bar with good prices. The was construction going on next door but that didn't seem to last all day. We had a problem with the remote control for the tv which the batteries needed changing. We showed staff, he sat on the edge of bed and got it to...


More  




“Group Stay at Lambana Goa”


After some research in Tripadvisor and other places we, a group of 10 guys planned a stay at Lamabana resort in Goa. This was a fun trip with friends and we had taken 3 rooms for 10 guys. The resort can be found easily and there was no trouble finding it (used Google Navigation on android phone).
The resort is...


More  




“Close to beach, budget accommodation”


Its more of a hotel than a resort. We went in November and prices were unnecessarily high in most of the places we checked. Rooms and toilets are neat, though a bit old. Pool is small, but clean.
Breakfast buffet is terrible. Avoid, if possible. Food from restaurant(you have to pay) was good. We had ordered a sandwich. It was...


More  




“Budget accommodation”


This is more of a hotel than a resort, and by that I don't mean to say that it is a bad place. However, one cannot expect the trappings of a resort from this place. It is at best a budget hotel for travellers looking for an economical stay!
The rooms were standard type with air-conditioning. We discovered our bedding...


More  




“clean hotel at good location”


There was loud music in restaurant when we reached the hotel, so we were worried if it would be peaceful in the room, but we got a room on 2nd floor n it was very quiet. ( no lift). Room was decnt size with an extra bed as we had requested. Walking distance to beach made it a great location....


More  




“Overrated ”


Despite having an on the dot location, I was quite disappointed with the resort. Especially after seeing all the pictures and reading up all the reviews. Don't let the pictures fool you. It is less of a resort and more of a hotel. Tiny pool. Barely adequate menu cars. Rooms are quite decent, considering everything. But the AC is freakishly...


More  




“" Value for Money "”


Real Value for money.
Service - Good & prompt
Rooms - Clean rooms
Location - Very convenient location, walking distance from Calangute beach & located in the market place where you get everything.
Food - Very Good
USP is ofcourse location but not for those who want a quite and peaceful holiday.


“Truly value for money!”


We stayed at Lambana resorts during our trip to Goa in August 2014. Situated in a prime locality, within walking distance from Calangute beach. Its in a busy area with several shops, restaurants & taxi rental shops around and is very convenient from a tourist perspective.
Rooms are comfortable. I would specially like to mention that the food from the...


More  




“Great Place !!!”


Awesome Place !! Awesome LOcation !!
5-7 Mins walk to Calangute Beach is its USP,,
Situated on Calangute Baga Road,,
Rooms are good,
Service is good,
Clean Swimming Pool,
and recently property being coloured.
Value for Money !! Bargain for the Best Rate and you will get a good deal .


“great holiday in a gem of a hotel”


We have recently arrived home from a two week stay at the Lambana,The hotel is on a busy road, our room faced the front of the hotel, we did get some noise but not enough to spoil our stay,The rooms are large and comfortable,and very clean with air con, The pool area is small but very pleasant, and was kept...


More  




“Wonderful Hotel and Staff!!”


Our recent stay in the Lambana Resort Hotel in Goa, was excellant,the staff are lovely and always helpful, we had the room cleaned when we chose and it was of a high standard, the room we had was with a pool view and we loved it, so much so that we are planning to return and stay in the same...


More  




“Good Budget Hotel”


A good budget hotel with all the basic amenities, a decent pool and a fantastic location close to calangute market, walking distance from calangute beach....rooms could have been more cleaner and although staff was polite...it could do better....although we checked in as couple...we were allotted room with two single beds joined as one....breakfast spread was limited but tasty.... overall the...


More  




“Peacefulness surrounded by chaos”


We stayed here for 2 weeks in March, the rooms were basic, very clean, with good air-con and fan. We had a mini bar with cheap prices and the hotel restaurant and bar were also very reasonable. The staff were very attentive at breakfast and in the restaurant, all food was really good. Our room was facing the pool (...


More  




“Nice Budget Hotel”


We stayed at this hotel in September. Took a pool facing room which was a good choice as other rooms would face the road side and wont be that quiet. Its situated on the main Baga beach road, so quite bustling with traffic and people. However, the location is very central with markets, beach and restaurants all close by.
The...


More  




“Good basic hotel.”


The Lambana is on the busy Baga road and is well positioned to enjoy everything Calangute has to offer, beaches, shops and restaurants are all within close proximity. Breakfast was a buffet offering the usual drinks, cornflakes, toast and eggs done any way you desire. The hotel is basic and has a good bar offering very reasonably priced food and...


More  




“wonderful memories”


This is not the best hotel that you will ever stay in but for a small hotel on what can only be described as the M25 it is wonderful.from the moment you arrive you are met by smiling faces.nothing is too much trouble for the staff.every single member of staff are ready to greet you everyday with a smile and...


More  




“Everything you need”


Just back from our first trip to Goa and felt Lambana Resort really deserved a good review.
Staff were nothing but polite, friendly and always gave great service.
Rooms, bathroom and balcony were all spacious and clean. Shower room is very big with a powerful shower that was always hot. Rooms with A/C as standard which was a godsend and...


More  




“Best hotel”


Just got back from the Lambana and must say its the best hotel we've had in Goa. Everything about it , the staff, rooms and the food. If there is 1 criticism the bar closes at 11pm but Madhus isn't far away. Baz


“Fab holiday”


Stayed at the lambana calangute by recommendation from my daughter who had been the previous nov. Hotel is in a great location 5 mins from the beach, and right on the main rd to baga.we had a room at the side of the hotel nr the main rd. we found it not as noisy as we thought. Rooms were basic...


More  




“Lambana Lambana Lambana :) ”


Fab wee hotel. Has own genorator for all those 'power cuts' that you get in Goa. Rooms are very spacious could do with a lick of paint, and some new mugs/cups as they are all chipped, but all in all cant complain about this place. They dont have wifi. Very friendly staff.


“Rude and Unhelpful Staff”


We booked two rooms for two days during Feb 14th weekend. The booking was done through agoda.com (on Feb 12th). We paid 16k upfront on agoda.com and received the hotel booking confirmation.
When we reached the hotel on 14th Feb morning, we were in for a rude shock. The hotel receptionist told us there was no hotel booking by our...


More  




“Wish I Stayed Here Years Ago!”


The Lambana has the best staff you could wish for, they make your holiday! Friendly and always up for a good laugh, the rooms have everything you need. They are very clean and are cleaned on a daily basis as is the pool. Hotel is in a perfect position surrounded by shops, bars , restaurants with the beach literally a...


More  




“Lambana. Hidden jewel of calangute”


Arrived at the hotel in the early hours of the morning. We were met by some of the friendliest reception staff we have had the privilege to meet. We were shown to our rooms and given the standard tour of the amenities. The rooms are basic but more than adequate for my needs. A well stocked mini bar at very...


More  




“lambanna”


hotel clean room cleaned daily if you wanted staff friendly especially peter an raj and the security guy at front had a room over looking pool which was quite food was good especially the pizza breakfast mostly consited of eggs anyway you liked an toast but wasgood overall a good holliday and will go back again next year


“Cheap and cheerful”


I will say from the start that we normally stay in 3 or 4 star hotels in Goa, so maybe we have been spoilt. But we wanted to stay in Calangute for the New Year celebrations,
and our normal sort of hotels’ prices were far too high. So we saved a lot of spending money, but we got what we...


More  




“Great staff”


This was our 7 th time to Goa and 1st time staying at the Lambana, staying for two weeks over the busy Xmas & New year period. We picked this hotel because of its location it is walking distance to everything we like to do,knowing how hectic this time of year can be.
We was travelling in a large group...


More  




“Lambana”


Had a lovely 2 week stay at the lambana. The view from the room was not great but as it is onlt a base to sleep I did not let it bother me to much. The pool area was very clean with towels provided. Breakfast was good and served by friendly staff Peter an Raj were lovely. Nothing was too...


More  




“Holiday”


Went to Lambana Rosort for Christmas and the New year this hotel was the best place I have stayed in on my third visit to Goa the floors was very dusty maybe as they were working on the pavement but told staff and it was all washed .The staff was wonderful would go back to this hotel but ask for...


More  




“Friendly Staff, Central Location, Nice Rooms”


Very pleased with our stay here, large room, air conditioned, coffee making facilities, reasonably priced mini bar. Pool area in sun all of the day, plentiful sun beds & very clean. Side rooms are larger than the standard rooms (although no view to get excited about). Rooms at the rear overlook the pool & would be a lot quieter than...


More  




“Did not let it spoil our holiday”


Not a lot to be said in favour of this hotel.
Arrived in the early hours to find all the staff asleep, we were shown to our room but not offered any hospitality after what was a very exhausting day. We had requested a quiet room but was given a room at the front which was on the main road...


More  




“So Sad - Lost its Sparkle”


This was our second visit to the Lambana - last year I gave it full marks - but unfortunately standards have slipped and I cannot give it the same thumbs up. The poolside cleaning is not done every day - had to request they do something on my third day which is unacceptable really as there is a plague of...


More  




“Dissapointed”


Arrived at about 0300hrs as the plane was late taking off. On arrival staff were asleep, no drinks or refreshments were offered. Room and toilet stunk of urine, room was on the side giving an excellent view of the rubbish. I have never seen so many shades of white in the sheets. Blankets were full of cigerette burns. Staff lovely....


More  




“Good facilities, very poor service.”


As far as the price goes it's a real deal. The room is spacious with a balcony and has a nice hot shower.
This is my second visit to this hotel. But the service has dropped a fair bit. The staff could care less for your business if you don't look like a foreigner. The room linens are quite stained...


More  




“All about the staff”


Me and my girlfriend spent 2 weeks in the Lambana in early November. After spending some time looking through reviews on Tripadvisor, we decided on the Lambana and were very happy with our choice.
If you're expecting a fancy hotel in Goa, probably best to stay away from Calangute and go to South Goa. What we wanted was a clean...


More  




“Chilled out hotel with friendly staff”


A clean and tidy hotel, with friendly staff and a fun environment. Myself and my partner stayed there for 14 nights had a fantastic time. Our first trip to Goa and it will definitely not be our last. The staff could not do enough for you. They were friendly, fun and entertaining. Also the rep at the hotel Lloyd was...


More  




“DESCENT PLACE AT GOOD PRICE”


Its a small hotel in Goa.Its a descent place near Calungate beach.All the restaurants and shopping places are nearby.The hotel has a good swimming pool and it is kept clean.The hotel mostly has Russian and Scottish tourists.The rooms are kept cleanand, the staff is friendly.The breakfast is good and the staff also takes care of personal needs in breakfast.Though the...


More  




“Excellent location and value for money”


We stayed at the Lambaba last November and have booked to return agiain this November and March 2014
Very welcoming and friendly staff, the Lamabana offers excellent value money ,this medium size accommodation.


“Good stay”


I had been in this hotel in 2007, in 2010 and would be going again now . I like the location of this hotel which is in the center of all the happenings for which a tourist usually goes to goa . Calangute is so close by and also candolim and other beaches if u have a car .
u...


More  




“A wonderful gem found after 15 visits to the area!!”


Located at the top of busy Baga Road, just 200 yards from the roundabout in Calangute, this hotel has about 27 rooms over floors 1-3 with ground floor taken up with Reception & front and rear bar/restaurant areas. There is no lift so those who may have difficulty with the stairs should seek a lower floor room. Regulars to Goa...


More  




“Good but...”


This is the 4th time we have visited this hotel and there have been several changes over this time.
Thanks to the following members of staff who without these the hotel would be a complete shambles.
Reception - Melissa and crew
Housekeeping - Sagun head housekeeper and his crew
Waiters - Raj and Peter
Pool boy
Reception area - excellent...


More  




“Good introduction to Goa.”


We stayed at the Lambana from 14th Feb for two weeks,me the wife and fifteen year old daughter.The Lambana is in a great location on the Madness that is the Baga road,anyway we found the hotel to be really comfortable could do with a lick of paint here and there nothing major though,the beds were comfy shower/wet room really good,the...


More  




“what a shame”


This hotel is very tired and is in desperate need of re-decorating.
Excluding the service delivered by Peter, Raj and Malisa. The service was very very poor, the waiter's more intreasted in watching the TV or chatting at the bar..... you got a drink if you went to find them.
Breakfast is advised at 7 till 10.30 - We were...


More  




“Super Time Go for it”


Back from Goa - this was our first time, stayed in the Lambana Resort, situated on Baga Road, which seems to be the vibrant hub of the whole town, both sides of the road crammed full of shops, stalls, bars, restaurants, etc.
On route to the hotel we were told by our rep that most beds in India are as...


More  




“A little gem”


A really friendly, small hotel. The staff are fabulous - they created some fantastic towel art during our stay - including a full size father Christmas ! We are regular visitors to Goa and although we tend to spend most of our time in Candolim, we will have already booked to return to the Lambana next year. The hotel is...


More  




“Excellent service brill staff and hotel”


We have just returned from the lambana resorts goa. We had a great hoilday . We visit goa every year. This hotel was very clean rooms cleaned everyday if wanted. Pool area very clea the pool boy is very busy and thr pool and pool areas are cleaned everyday and he certainlt looks after you whilst at the pool. the...


More  




“Excellent Budget Hotel”


Just back from a 3 week stay at the Lambana hotel Jan 8th-Jan 30th 2013. The Lambana is a small friendly hotel located on the main Baga road. The rooms were clean and comfortably albeit, the beds ar rather hard but I understand that's normal!. If you request room clean daily your sheets and towels are changed each day. Hot...


More  




“Can't Wait to Return!”


This was our first holiday to Goa, so we were unsure what to expect of the hotels; en route to Calangute from the airport, my partner & I noted many hotels along the way, most of which looked much neglected and some were downright derelict! Therefore, on arrival at the Lambana Hotel Resort, we were pleasantly surprised and relieved to...


More  




“1st time in goa”


After arriviing at the hotel about midnight, we got off to a bad start when we were told they had no booking for us.They put us in a taxi and sent us to their sister hotel (the somy resort) where we had to spend the night. Not a nice place the room was very very basic, we spent most of...


More  




“Nice hotel, busy location”


Stayed at the Lambana November / December 2012, the room was nice & the air conditioning was also powerful & you didn’t have to pay extra for it which was a bonus.
The pool area was nice but there are only a few sunbeds (about 10) & they could all do with upgrading, the pool boy was friendly & always...


More  




“lovely hotel”


we stayed at this hotel for three weeks over the new year.the lambana is a great hotel we stayed in room 102 overlooking the pool the room was plenty big enough with ample wardrobe space and was very clean.the hotel is in a great location and all of the staff are so very friendly and helpfull the breakfast is very...


More  




“Excellent Hotel”


We stayed at this hotel over New Year and had a brilliant time. Admittedly the traffic outside was mental all day and night leading upto new year but come the 2nd of Jan it quietens down and the wandering cows return!!
The hotel is brilliant, all the staff in the hotel and adjoining restaurant are all exceptionally friendly. The room...


More  




“Right hotel, wrong location”


We stayed for two weeks in December. After a good flight with Thomas Cook, we arrived at 2am and the reception staff checked us in very efficiently and showed us to a room overlooking the pool. The hotel is on a very busy main road and the traffic can be horrendous at times. There is almost always some noise even...


More  




“Best Choice in a Budget”


Positive Points: was well maintained and clean, you can ask for daily housekeeping if you are fiddle, the lady at the reception is very helpful, the swimming pool is well maintained and clean with available towels... there is a mini bar and a coffee maker (charges applicable) in each room, a restaurant bar at the entrance, and breakfast included on...


More  




“fantastic time carol forfar”


Just back from my first trip to goa, I was with with family and friends and I must say I had a fantastic time. We stayed at the lambana and what a great place, the staff are so friendly and helpfull they couldn't do enough for you, the room was more than adequate clean, with mini bar stocked each day...


More  




“Lovely Lambana”


Just returned from a two week stay at the Lambana Resort, Calangute. Must confess arriving at 2.30 in the morning after a 10 hour flight, tired and emotional - my first thoughts of Calangute were Oh No - hopefully the reviews that told us we'd get over this would be true. They were - within a couple of days we...


More  




“Avoid - Stay somewhere else simple !”


Just retuned to the Uk following a seven night stay at this hotel. Having travelled to Goa on a number of occasions I expected the hotel to be fairly basic however this was below that !
Day 1 after a long flight from the UK arriving at 0330am we were given room 102. We left our suitcases on the floor...


More  




“Fantastic staff, great clean and comfortable hotel :-)”


My partner and I chose the Lambana hotel due to the great trip advisor comments and we were not disappointed. This was our first trip outside of Europe and our whole 2 week holiday in goa was so good we are visiting the Lambana hotel again in February 2013 so we can finish seeing all the places we missed last...


More  



