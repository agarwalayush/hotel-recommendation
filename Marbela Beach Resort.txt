
“VERY GOOD STAY”


My stay was really comfortable & had great fun...all staff was so friendly....specially the bar tender Ashish was really good.the arrangement was good...about the songs we w're disappointed on 2nd day.


“Charged Card but Cancelled Reservation”


I am extremely annoyed with Marbela, Morjim without having stayed at the hotel!
I chose to book this property based on recommendations and the generally positive reviews on tripadvisor. What I experienced instead was mind boggling or even bordering unethical!
As most anyone would, I too made a reservation via their official website (http://marbelabeach.com/). As per their website they confirmed...


More  




“White and luxurious but noisy!”


Spent a few days here over nye period. The place is stunning with the white tents and amazing sea-views. The luxurious tents are really something and quite a unique experience. The breakfast was 5 star and staff helpful enough. Now for the noise. The stunning layout and location make Marbela the number one place to party in the area. We...


More  




“Luxary in Goa”


Even though it is difficult to get there, it is worth the drive - we came there a lot during our short stay in Goa. It is a fabulous place - and Vikram, the waiter we had a couple of times, is simply amazing. We enjoyed staying there day and especially at nights. Their drinks are amazing. Their food as...


More  




“Came back to hang out, great time!”


Wow! We went over as a family to Mirabelle after aiming to go to a neighboring business that was closed for the season and I am very happy that fate gave us the opportunity to visit this establishment. It feels like one of the fancy places you see in movies with the white beds/canopy on the beach. Mirabelle appears to...


More  




“An average stay”


Though Hotel appears a bit small but it has very friendly & helpful staff, amazing food and clean & well kept rooms.Anybody looking for some quiet time and peace among the beach , dont take this hotel as an option


“Truely Fantastic”


hotel is a far from the goa airport but once you are there, its fantastic. the cottages are cute and self sufficient. the breakfast needs improvement and lunch/dinner menu needs a bit of more choices because if you are to eat 3 meals for 3-4 days then the choices are limited. the white theme is fantastic and the beach is...


More  




“Superb, watchable...”


This resort is very good.its cleanliness, surroundings are really mesmerizing. I went there with my family and when they saw they really said me whatt a resort.there is beach and what the cool vibes blow there.fabulous!! Great place for diminishing tension and is a way of getting rid from hectic schedule.


“Perfect location for a sun downer”


Was here for a private party on the 9 of Nov 2014. lovely location, clear waters and clean beach with lovely beach beds to relax and simply enjoy the beautiful waters and white sand. The continental food and cocktails were good. Good music and ambience right by the sea.


“Heavenly place to relax”


We had the ultimate rejuvenating beach holiday at this resort....the entire resort is class and beauty combined into one! They have a beautiful stretch of white sand beach with lovely beach beds to relax and simply enjoy the beautiful waters and white sand.
The cottages are cute and their restaurants offer a stunning view of the beach and you can...


More  




“Relaxing place for a family”


Went to Marbela for lunch with family. The unique concept of this property is peaceful and relaxing. Anup, the Manager informed that the flat bed of the sea is very safe for swimming. The continental food and cocktails were good.
The access to this place is difficult and a car can barely reach the hotel entrance. The tee-pees and the...


More  




“Heavenly!”


The first time I came here was actually nearly 3 years back with a couple of friends. The stay was so perfect we ended up extending our trip by a day! Their USP is definitely their property, they have these luxury tents set up on their lawns, with their restaurant and bar right on the sea front. The entire lounge...


More  




“Good One something different ”


Just what is shown in pictures.... It's a nice beach resort and the Food is also good.. And nice natural ambience everything here is build in white which gives excellent exposure on the view of the resort.. We had great time at Marbela Beach Resort... Sorry to write so late.. Farooque


“Absolutely loved this place”


Loved the resort! Beaches were clean and resort was lovely. Food was great too. Only thing was, there was no swimming pool and neither was there a bath tub in the bathrooms.
Breakfast buffet was just average.


“Quiet in morning loud at night”


Located very far in north Goa.Beautifully made, loved the beach and the beds on the beach.Big place, food is okay.Very pleasant setting in daylight while equally loud setting at night.Very Disturbing.Rooms are very expensive not worth it.Road to the resort is bad.Only coz of loud music cannot recommend anyone it's too unbearable to sleep as its loud all night.


“White all around on Black Sand”


What a beautiful place to relax. White Bed on sea shore is just amazing. The resort is a concept on its own. Located far away from hustle bustle of Goa..just contained in itself. Excellent blend of comfort and serenity. Solitude is what we feel at the resort. Only drawback is price and taxes of food offerred is really high. also...


More  




“Overpriced tent in a great location”


When I came here I myst admit I was expecting so much more from the resort. The rooms are clean and tidy but the sheets have stains on and are very worn and itchy. Just found ants in my bed whilst writing this! The staff were very unhelpful and everything is very overpriced considering the staffs laziness. There were a...


More  




“Just what we wanted”


Stayed here last week and I must say it was even better than I expected. Only downside was we arrived during the local elections so no alcohol server for 3 of the 4 days we stayed there. mini bar had some beers in when we arrived but it was never topped back up.
The staff were fab. Always friendly (especially...


More  




“Horrible and scary DDirty”


All pictures in Website is delusional and FAKE...our honeymoon was turned to a horror movie . the Road to the Hotel is Horrible , not even taxis will drive you there. The reception is just not there. The rooms are terrible , horrible , very very dirty. The sheets are old and dirty . THE TENTS are lilliput size and...


More  




“The perfect Hotel choice for Morjim beach”


This was my first trip to Goa and I was not sure what to expect at the beaches and the resorts there. But now I can confidently say that I have found my Hotel in Goa (Morjim). Morjim is one of the best beaches in Goa- not too crowded, not too desolate, just right. And this resort is an ideal...


More  




“Terrible! Don't stay here!”


I am fortunate enough to travel a lot & stay in many hotels worldwide. Don't be fooled by the website as this hotel is of a very poor standard & if luxury is what you are looking for you will be very disappointed. The tents are small & cramped & the general feel of the complex is that it is...


More  




“Best hotel in Morjim if you know what to expect.”


Marbela is truly a piece of heaven for us in Mmorjim. It is the only beach facing resort I know off with room service and air conditioning that actually works. They also have the nicest cabanas I have seen anywhere on a goa beach.
Yes I know it isn't the quietest place around but most of us go to goa...


More  




“Great music and food”


We visited this place in search of a good party place in Morjim. Initially it was like any other place but slowly as DJ and live music performance picked up the place was really happening.
Though it was bit pricey but overall food quality n ambience compensated every penny.
A must go place.


“Average”


We had wanted to spend this Goa holiday in a relaxing manner - being on the beach most of the times with the kids - hence we chose Marbela. Though our main Objective was satisfied, the service and food quality were a major let down. However, the Cabanas on the beach made up for alomost everything - being spacious and...


More  




“Mixed Views”


Confusing as the Title may sound, it exactly what we felt. Travelled with wife and 2 kids and had taken 2 cottages.
The Pros : As soon as we entered, we felt bliss with the "Whites" around us and the peacefull atmoshphere in the morning. The Cabanas on the Beach were excellent, had quite a few naps there. As soon...


More  




“Great place for a sundowner!”


Must go for a sundowner here! We also came back at night to party... if you like techno/ trance then this is the place to be! They also mixed it up with a live tabla player which was awesome. Amazing ambience to party right on the beach where you can sea and hear the waves of the sea and under...


More  




“Just Gorgeous”


Marbela is one of the best places to stay in Morjim. It gives you a mix of right amount of relaxation, great food and nice parties. The beach on this side is gorgeous and Jaydeep and Sheetal are great hosts. I have stayed here multiple times and each time felt like not checking out. The food is great, even though...


More  




“Beautiful Property, Amazing beach”


All white, amazing looking resort with Airconditioned Luxury Tents and also few regular rooms, all flowing down nicely landscaped greens down to one of the most beautiful beach's in North Goa.
While we did not stay there, I did roam around the place and checked it out. Looked as good as in the photos I had seen. We did have...


More  




“Disgusting !”


We arrived in the morning to have a coffee. We went to the toilets ... it was a real nightmare ... such a disgusting place .. I never saw that in my life .... .We imagined how could be the kitchen .... We ran outside and flied from this place .... FOR EVER.


“Hidden paradise”


Winding roads & a long drive to Morjim almost had me tired. But as I stepped through the gates I was like teleported to another place. Independent tent dwellings with five star amenities within. A spa- feel shower. Quiet. Restful. Manicured lawns with stone paths leading to the beach. Pristine beach. Dj & afrocuban drummers. Excellent service. One often looks...


More  




“Not a resort. more like a cheap party place for Drunks”


After trying to find a suitable place to visit GOA for the new year period, we selected Marbela, This was due to their New years eve party and good looking pictures on the website of the resort!!
Well it was very disappointing,
To start with
My sister in-law was worried about the music prior to booking, as she was taking...


More  




“worst hotel experience ever”


Having read previous complaints on this hotel, I can see the manager always protecting herself that the client did not make a complaint during time of stay. I am therefore posting this complaint online today into my 4th day of stay. I still have 4 more nights to go here so lets see what the hotel will do.
I am...


More  




“One of the best in North Goa!”


We stayed here for 3 nights and it was awesome. The rooms are good, but who are we kidding - it's all about the location. The beach is just sublime. Restaurant and bar is also good.
Good staff, clean rooms, clean property. Highly recommend it.


“AMAZING!”


This is definitely one of the best properties in Goa. Great food. Great People. Great Staff. Beautiful location. The rooms/tents are superb. Was there for a wedding. had a most amazing time. This is a luxury resort... so its VERY expensive compared to options available around Morjim.


“Lovely place, amazing food, great beach.”


I didnt stay overnight but spent a whole day at Marabella. The place is charming and staff very attentive. The food, especially pizza, is the best in Goa. For the beachside - nothing like it. The white modern furniture design is lovely. Towels are provided by Marabella.
For everyone who wants some lux, good food at the best beach in...


More  




“Marbella”


This was my second visit to the Beach Resort, First one being for New Years.
On new years we were with friends and this time around it was family time & both the times we had fun to the core..
The Service and the attention given to us at both the times was Superb !!!
The rooms are amazing and...


More  




“Beautiful Experience”


Beautiful Ambiance, Excellent Food, Friendly Staff, and Awesome Rooms. Couldn't have asked for more. The peace and constant sound of the sea was so blissful that we didn't want to leave. The Bar is equipped with the best of liquor and wines. Kids LOVED the food and the personal attention that the chef gave to our needs was really commendable....


More  




“Awesome Place.....great beach”


this is THE place to hang out in Goa these days. the rooms are cool and the restaurant/bar was amazing. however, it is not a child friendly place and there is NO SWIMMING POOL.
other than that....ighly recommended.


“Grimy and overpriced”


I was really disappointed by our stay at Marbela beach resort. The website photographer is really talented, but it was just not reality. We are on our honeymoon, and there was no love had here.
The place was filthy. When you're going for white, you need to have a good cleaning routine. We found the sheets stained and worn, the...


More  




“Excellent Location”


The beach is awesome here and the hotel is bang on the beach, just a min walk to the beach from all the tents/rooms. Tents were small but very comfortable. I especially loved the rain shower. The Bar and food were very good too especially the wood fired pizzas. Ashwem beach has lots of other great places to eat nearby...


More  




“Lovely Luxury tents and white sands await”


Recently stayed at lovely Marbela Beach in a Luxury Tent. This quiet resort is set on a lovely white sand beach and has cabanas and outdoor dining. I saw a certificate awarding their restaurant as the best sea food restaurant in North Goa. I highly recommend the thin crust yummy pizzas. Friendly staff, well laid out spacious tents with air...


More  




“Trendy, chic and happening”


Beautifully crafted and conceptualised, Marbela sits pretty on one of the most pristine white sand beach in Goa. It's all white decor is appealing and soothing to the eye. Once you enter this marvellous property you automatically calm down and relax. The staff is warm and friendly and the rooms well appointed. The delicious food is another feather in the...


More  




“Most memorable evening!”


With A bunch of friends, we spent a long evening here, it was our memorable one! A view to die for.. Great music and yumilicious food n drinks!
Away from the noisy goa, great way to spend time, talk, drink n be merry!


“Could be better, could be worse”


I don't know what one should expect in Goa for Rs. 6,000/night, but I think it's a little more than this place offers. It's not dirty or run-down, but it's missing any touch of luxury or elegance. The beach is fine but so are the beaches everywhere else in Goa. I only had breakfast here but it was quite basic--no...


More  




“Good hotel on the beach”


Good hotel on the cleanest beach in Goa ! The cabanas which the hotel has made on the beach are great. Really enjoyed chilling on the beach of the hotel... very calm and clean.... Tents are a good size for 2 adults only... I would go back !
Only need to improve on their food !


“One of the Best Places to Stay in Goa”


Marbela beach resort is on morjim beach. The tented accomodation is very comfortable for 2 adults and maybe 2 small kids but that might be a bit tight. The staff is absolutely fantastic. The beach is awesome. They don't yet have a pool but we did not need one. The spa is fabulous and we got plently of massages on...


More  




“Heavenly abode when staying as a couple or with friends and group”


Nestled in white sand russian beach Morjim, the place is beautiful with private beach shacks, beautiful rooms and tens priced reasonably offering decent food, great party atmosphere and music int he evening, great view of sunrise and sunset, excellent service the place is a bliss for those who like calm, quiet beaches with class and finesse and are not too...


More  




“Fantastic place... if”


The hotel and its belongings are pristine, the beach is one of the best of the area, guarded. Staff is nice, food is good, hotel is almost as beautiful as in the advertisement pix.
The only but very real problem to me is that Marbela beach is both a hotel and ... a club (marbelabeachclub). So if you are into...


More  




“Nice but not worth it”


Stayed here for two days after taj panaji and was so dissapointed. Lets start with the pros- nice location since a ten min walk leads u to bardo, other cleaner prettier beaches. this ones nice too but too crowded. beach villas and tents were nice enough but not that great. they were pretty but the tents too small, the villa...


More  




“Serene and beautiful”


Morjim is pretty far from the usual hustle bustle of North Goa! Far from the usual beaches this place comes as a total surprise!
Getting here is a task, so make sure you have your own conveyance. The directions on the street will ensure you get to this place alright. We didn't stay at this property but spent the whole...


More  




“Peace and calm”


Stayed at Marbela in Feb, 2013 in my first ever visit to Goa. Much like at any other place there were both pros and cons of staying here. I'll start with the downside first so that you know what you're getting into.
The location - approach road to Marbela isn't exactly the best and their will be puddles galore if...


More  




“Lovely place - must visit!”


Beautiful resort & beautiful surroundings! My hubby surprised me with a birthday getaway to this place. Welcome drink, lovely tent room, green lawns & a very calm ambiance overall, so good first impression. Ordered pizza & pasta for lunch, but it wasn't hot when it reached us. It still tasted so we let it go. The towels my hubby used...


More  




“Fantastic!”


This is a real good and amazing place. The staff is great, as well as the food, the facilities, the instalations... If you want to get real relax, this is the perfect place. Try to book before, as they used to be full. Don't miss the landscape around this hotel, if you can, take a walk. Fantastic.


“A piece of paradise”


After much research, I picked Marbela Beach Resort despite some of the negative reviews. It was one of my best stays in Goa! I was there in October and so not everything had opened up yet (ex, La Plage, one of my favorite restaurants just a stone's throw away from this resort). However, the tented beach beds more than made...


More  




“YUK! Horrible place!”


That is one horrible place and not worth that money!!! Dont believe those pictures you see on their web site or whatever!! The sirvice is so slow and they just dont want to work there!! Food is horrible we had to go to the nearest restaurants all the time! Horrible horrible! I travel a lot so trust me! Yuk Yuk...


More  




“Outstanding...”


What an amazing place... exceptional huts... outstanding food and bar facilities... great staff... Will definitely be back... had the most relaxing holiday.


“Don't do it.”


I love trip advisor and always use this site whenever I'm traveling. I feel compel to share honest opinions with fellow travelers, as others have done. There is much to say about the Marbela Beach Resort. I'll start by saying this place is exceedingly disappointing. We actually believed the resort would equal the pictures on the resort website. No such...


More  




“What a fantastic place!”


So lovely! So relaxing! So inspiring! But most of all, so nice staff, from the waiter to the manager to the owner. I love those Indian guys who really take care of you. Will be there agian!
Very nice experience to live in "tents" compared with hotel rooms.


“Amazing beach hotel - a must visit and stay place in Goa”


I was at Marbela Beach Hotel with my fellow MBA colleagues at the end of January 2012. It was definitely heaven on earth. The hotel is situated right on the beach and all you need is your flip flops and swim suit. The beach is amazing, nice sandy, clean and not to crowded. The hotel has their own beach tents...


More  




“The worst experience ever”


The reason we stayed at this hotel was my friends wedding there otherwise I'd have shifted from this place next day for sure. First of all the food there is not good at all. I spent there 7 days and ate at hotel's restaurant twice! First time that was the night I arrived as was starving i had to eat...


More  



