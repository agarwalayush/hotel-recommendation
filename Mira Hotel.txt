
“2nd time i the mira”


just returned from our 2nd stay at the mira, just like our first trip 2 years ago we loved it. stayed in p8 overlooking the pool and found no problems. room was cleaned every day, fresh towels & bedding every other day. cant fault it, staff were really nice, breakfast consisted of jam n toast although you could have a...


More  




“Brilliant holiday”


This is the second time we have stayed at the Mira. We had a great time . We asked to change rooms on our first day ,the staff were very helpful and changed us that day. The room was basic but that isn't unusual for this area. It was cleaned daiy all requests we had were met with a smile...


More  




“Great place to stay”


The Mira was in a great location for us, close to the beach and great restaurants/bars. The room was small but cleaned daily and the staff friendly. It's basic, but I presume all 3 star hotels in goa are. I'd definitely stay there again.


“dissapointed!”


just returned from our holiday at the mira having spent xmas and new year there. We returned after a prevous stay 3 years ago and we loved it but this year found it was under new management and was extremely dissapointed-on arrival they had tried to give us a room that no one could stay in-padlocks on the door and...


More  




“Would go again!”


My 3rd time staying in the Mira would defiantly go back again! Great hotel service! Perfect location! Highly recommend. Great staff, managed to get us in at short notice as we had to leave the previous hotel as it was disgraceful!


“Very central tourist hotel.”


Central to town of calangute but also just a short walk from beach. Rooms are noisy if not over looking pool! - but all quiet after midnight so not too bad. Staff are amazing! Yes- all I them!! A few language difficulties with some but they will make your stay very pleasant and happy!
Rooms are basic but a good...


More  




“Overall it is a good hotel at prime location.”


I stayed in P3 room for three days with my wife during October. As i booked this hotel many days before my visit to goa so i could not find any issue in check in. Just drop a mail and they will manage and inform you with their best service. Hotel is great but no proper parking for 4 wheeler....


More  




“Great Holidays”


We stayed here 3 days in second week of Dec'14.This hotel is in the center of Calangute with easy access to both Baga and Calangute. We stayed P2 room which is in back block facing the pool.Checkin was smooth and it took only 2 mins.
The sheets and towels in the rooms were obviously new and were changed every day...


More  




“In the Centre of the Mayhem”


This hotel is in the centre of Calangute with easy access to both Baga and Candolim. The rooms in the back block facing the pool are not noisy, I'm guessing some in the front block would be
We were first to arrive at the hotel for the season. The sunbeds were not out and the chairs were still being prepared...


More  




“Fabulous stay”


We stayed here for 2 days what a wonderful place close to calangute beach.everything just good.clean hotel n swimming pool.love to stay here as staff is friendly.issue being only restaurants are not open for long time but nevertheless ther are many restaurants closeby.


“awesome stay”


Hotel room were good and moreover better as very close to beach, swimming pool where we can chill out with beer,though dining facility is limited, pricing is average and affordable compared to other hotels in calangute, overall must stay here.


“Great little hotel”


I've stayed here many times (at least 3times) on last minute get aways. It's a great place to stop and use a base to get around north Goa. The staff are warm friendly and helpful. The rooms are great, the best are around the pool, simple, clean and user friendly. The food is as good as any of the local...


More  




“Nice Economy Hotel”


I have stayed in the hotel for 3 times and had very nice experience. The hotel is located very near to the Calangute Beach. Rooms are clean and the hotel also has a swimming pool. A very good hotel for its price.


“15 good holidays”


My wife and I have stayed at this superb hotel 15 times in 7 and a half years. Twice a year in March and November. Its a family run hotel and all the staff are only there to be helpful to its customers. The boss pops in from time to time, a very pleasant man and also a film star....


More  




“You get what you pay for ........”


Stayed in this hotel 21st Feb for 2 weeks this year, a very basic hotel very friendly staff, area around the pool has very little to offer a few sun loungers no parasols.The one thing I was disapointed in, when I returned home and was checking through my phone I discovered the cleaner of my room had taken photos of...


More  




“small basic but great location”


this is my second stay at the Mira , arrived march the 9th for two weeks and was greeted by such warm friendly staff some whom i remember from two years ago including simon at reception who will always be on hand . with any worries/concerns you may have and who has the biggest happy smile that could lit up...


More  




“basic clean hotel”


Stayed at the mira for 2 weeks in March with my husband i had emailed them beforehand to ask for a highfloor and were given a top floor room which was basic as appropriate for a 2star rating small wardrobe only one tiny draw the usual thin hard mattresses ,television with football matches all div ,balcony furniture and a lack...


More  




“Holiday of a lifetime”


I stayed here 19-3-14 - 30-3-14, and had a great time, I travelled alone and met some lovely people. The hotel staff were great, and very friendly and approachable. The area was lovely, and I felt very safe.


“mira hotel”


stayed here in november 2013 for 3 weeks....arrived late and was checked in, asked about air con and was told everything is chargable!!! then shown to our room which was small and the door was a bit unsecure.....upon entering i noticed something on the floor....the boy who brought up our suitcases also saw something went over to it and stamped...


More  




“Mira Hotel”


stayed here for two weeks 26th feb 2014 till 13th feb basic hotel the staff are very helpful and friendly and will do all they can to help you. they even arranged for a taxi driver for us one that they trust and this made our holiday very special he took us on some lovely trips that cost next to...


More  




“Basic but comfortable”


Basic hotel,good value for the price ,Stayed 19th Jan till 2nd Feb 2014, The staff are very friendly and any problems are sorted out immidiatley, well worth the cost ,would stay there again no problem,Good location and is handy for everything.


“Clean place to rest your head”


Firstly, let me start by saying this was my first visit to India and I was excited and terrified in equal measure! I'd read reviews on here and like many wanted to see some of the worst of it! Well, some of these people obviously have high expectations from a cheap hotel in India! The room was a good size,...


More  




“Very good hotel near calangute”


The service was very good. We stayed from 30th jan to 1st feb 2014.
Initially after checkin we found a faulty AC. They repaired it within 2 hours. Thats a great service. Overall the hotel was very good.


“An evening Meal”


We didn't stay here but just called in with some friends for a meal on a recommendation, we weren't disappointed. We had a lovely meal, treated like special guests! It was so clean, the food was good and service excellent.


“just ok”


been to goa 7 times now, this was are first time in the mira hotel, i do not know how it gets higher rating than the osborne hotel, location of the mira good, pool area very small, rooms clean but again small, but we don't spend that much time in room so not a problem, what i didnt like was...


More  




“Lovely clean hotel, lovely atmos”


It was my first time in Goa, so en route to the hotel Mira, I was terrified. Mosquitos, rats and other horrifyingly debauched insects lingered in my imagination. But when I arrived, I needn't have worried. There was nothing untoward, insect wise, in the Hotel Mira and I don't think there were any rats. The bathroom and beds were perfectly...


More  




“Lovely place!”


Bedrooms regularly cleaned, the bar is great, love the fishes in their tank! Staff there are helpful and friendly. The swimming pool is nice and private, if you wanted to go to the beach instead, its not far!


“Lovely Hotel”


Thanks for a great stay hotel mira, just what you need it's basic...but very clean, tidy and great shower. Would advise to take a plug in to ward the mosi's off...as it worked great for us. Smashing location and go to Davids place down the road - best restaurant in the area : ) xx Would definately stay here again


“Awesome and affordable hotel”


walking distance from beach. superb hotel and nice staff.
all basic things you need is available.
clean room and pool.
it was a great experience to stay here.
i am defiantly gonna stay here on my next visit to Goa.


“Mira Hotel... Just awesome”


just 200 mt from calangute beach.... awesome room , awesome service , nice and coordinating staff....only concern was limited options in food...but that was manageable many restro are nearby.....Bike are available in front of hotel.... i m just loving it...:)


“Amazing”


Room was basic but what do you need really shower and bed. All i can say is the hotel Staff brilliant so helpful.
Got a Christmas meal there that was amazing brilliant cook who ever it was cooking it and free then a firework show.
Did the same at New Years but sadly I went home on the 30th dec...


More  




“Happening Place at Calangute”


This is a nice, clean and happening hotel in Calangute, Goa. Have stayed here on several occasions and booked a room over the phone while driving from Mumbai and also got a room facing the pool. Mira has a nice little bar and serves tasty Goan food by the pool side. The staff is very helpful though parking place is...


More  




“Great Experience !!! :)”


I have been there with my mates and it was really really good experience to been there.the location is cool, just like five minute away from the market and beach too.
I would love to go again and i would recommended my others family members and my known peoples to stay there. Simply great value for money.


“Goa Trips”


We are happy with the service more over is closed to beach just working distance,The staffs are warms and very kind, I would like to go again and again to stay in this Mira hotel.one should never go through the negative"s reviews.Go personally and enjoye the stay over there.


“Excellent Experience”


Mira is a great budget hotel for the money. The rooms are spacious and the staff is very helpful and friendly. The hotel is located on a very quiet road at a very convenient location with 2 mins walk to the main road market, 5 mins walk to an amazing beach. I recommend this place.


“Nice and central x”


Family run hotel & very helpful, would stay here again, although all rooms should really have air com, staff very friendly And helpful, we asked for an air con room and they moved us, also the dog was super cute xx


“Good Budget Hotel”


If you are looking for a lower priced budget hotel in the centre of Calangute with no frills but is clean and has friendly helpful staff, then the Mira Hotel. Is perfect.
We were fortunate and were allocated a second floor room overlooking the pool ( p2 ) which was basic but very clean with a good sized bathroom and...


More  




“Excellent”


I just returned after a three day stay at Hotel Mira. The facilities were excellent. Good Reception. Well managed and organized hotel. Staff were excellent. The location very near to beach makes this hotel very attractive for stay.
ANIL NAIR.GANDHINAGAR, GUJARAT.


“Not as good as it used to be......”


We returned to the Mira after a 12 year absence and picked the Mira as it used to be a really great little friendly hotel, this was our 5th visit to Goa. We asked for a poolside room and were given one so a good start. The rooms and furniture really do need updating the same furniture and beds from...


More  




“Not good”


No value for money , poor service , bad rooms ,location not grt , far from beach , not great for family. Lot of noise. Bad food and worst breakfast . Hygiene not maintained. Expensive and not worth it


“Fantastic base”


We stayed in Mira hotel for 13 nights as a group of 7 fantastic staff and clean rooms. Look for Seby taxi diver ( lives next door but 1) he took us on lots of trips- cheaper than booking through an agent.


“Average hotel with avergae service”


Stayed here with friends and had a very average experience. We booked four rooms for the same price and 2 of us got swimming pool facing room and 2 of us got rooms on third floor, which was not fair. Plus they charge you same for single occupancy and double oocupancy, so definately they have got their tarrif pricing structure...


More  




“Dec 06 2012 excellent holiday”


This was our 1st time at this hotel & I just wanted to say a huge thanks for everyone working at the Mira hotel. From the 1st steps into the hotel till the last steps out the staff was excellent.
The hotel was clean & tidy at all times.


“Good for what you pay!!!”


When we arrived very early in the morning the staff was very helpful and even got his some water from the bar even tho it was closed!!! throughout our stay we were treated so well and felt like part of the family at this hotel !!
The food here is horrible but its so cheap to eat out that we...


More  




“Mira hotel calangute”


flew out to Goa on the 22nd of november with my son, who is a grown man now, we stayed at the Mira in calangute 2nd time at this lovely hotel, the staff are brill and can't do enough to help.
i could not belive how much calangute has changed since my last visit, it's so busy, but it's a...


More  




“Second visit, still as good”


This time we got a pool room, definitely quieter than the main block which is on the street. Hotel is basic but very clean with rooms serviced every day. Staff very friendly. Pool is clean but limited room for sunbeds in the sun. Great location near to the centre of Calangute.


“Basic, but clean & cost effective.”


The hotel is situated 200 yards from the main strip of bars and restaurants so no need to worry about finding taxis. The staff really do make this hotel,a pleasure to stay in, and do make up for the lack of luxury. There is a clean medium sized pool that is a bit starved of sunlight, a bar that opens...


More  




“loved the mira”


just returned from our very first holiday in goa. we stayed in the mira for 2 weeks from the 8th of november. reading some reviews on here really does prove to me that some people want to pay 2 star prices for 5 star accomadation (2 bob millionaires). we stayed in room p7 which was basic but spotless. the room...


More  




“Extended Family!!!!”


My husband and myself have stayed at the Mira Hotel 3 times now and have loved it every time. The rooms are just the right size and are kept clean and tidy. Our last visit started at another hotel just outside Calangute (we booked with Thompsons and they do not have rooms at the Mira) but we were very disappointed...


More  




“Excellent hotel”


Stayed here December 2011. Our first time in Goa. At first it didn't seem great but this is India and it is a good standard for the price and location. Had a room on third floor which was a bit of a trek up the stairs in the heat but the view was entertaining... hawkers, elephants, sacred cows, rabid dogs.......


More  




“Go goa- stay in mira”


i went goa in april 2012 with my wife and an another couple. My friend recommended me for mira hotel. It is around 400 meter from calangute. We stay in room no P6 and P7. We had very pleasant experience of this hotel. They dont ask you why u came late in night. Bar is good, attractive. you can have...


More  




“Everything you need in a budget hotel”


I just returned from a Week-long stay in Goa at the Mira hotel in Calangute.
The location of the hotel is perfect. The beach is just a 5 min walk away and all the popular eating places are also within walking distance. The hotel itself is very well maintained. The staff were very courteous.
The rooms were clean and spacious...


More  




“GLAD TO BE BACK IN ENGLAND”


The hotel is cheap to stay at and we booked for three weeks. We didn't do any reaserch so ended up with all sleepless nights.The rooms are ok, (EVEN THOUGH THE DOOR HANDLE FELL OFF ON ARRIVAL).
We did have two good nights sleep, although this was on our stay at two excursions. We slept in a mud hut and...


More  




“Back to mira.”


Well! my self and my daughter stayed at the mira in calangute jan this year 2012. As expected the hotel was yet again marvellous. Unfortunatley this year our holiday was somewhat blighted, with the massive influx of russian tourists. Now let me make myself clear! i am not tarring everyone with the same brush here, but the majority of the...


More  




“Basic Clean”


This hotel is basic but many are in Goa and you have to look past that. It was clean and staff were friendly. Located right in the centre of the hustle and bustle of Calangute. This was our 3rd visit to Goa and this was our least favourite hotel. We like being around the pool with occasional trips to the...


More  




“Excellent little Hotel”


We have been to Goa 3 times and stayed here twice. Most of the guests are repeat visitors and I was surprised to see so many welcoming faces from the previous year. The rooms are big enough would recommend paying for an air conditioned room. Bathrooms are more like wet rooms but again are more than adequate. The Hotel is...


More  




“Hidden gem!”


Me and my OH stopped at this hotel from the 2nd March for 3 weeks. The hotel is a great base as it is located just off the roundabout. The hotel itself is basic but very clean. We found it accomadated all our needs.
There is a small bar area at the front of the hotel where you can sit...


More  




“Small, comfortable and friendly hotel in a great place to visit.”


I have stayed at this hotel on several occasions and absolutely loved the place. Don't turn up expected the Ritz because you will be sadly dissapointed. It is a basic hotel but thats pretty much the standard in Goa, it is however clean and comfortable. It has a nice pool, the staff are fantastic and make your stay extra special....


More  




“mira hotel”


just got back from a two week stay at the Mira Hotel 16.02.2012 if your are looking for luxury then this is not the hotel for you, if you looking for clean rooms excellent service then the Mira is for you, this is my first time in Goa it was amazing, lovely staff and nothing was too much trouble. I...


More  




“Same as always”


Hello again.
We have recently come back from 2 weeks at the Mira Hotel 19.02.2012. We stayed at the Mira hotel 2 years ago. Even though we prefer Candolim we went back to Calangute as it is cheaper and the taxis are next to nothing. The comments that some people make about the Mira are false. In 2 years the...


More  



