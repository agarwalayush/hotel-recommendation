
“Gud location”


Good property... ...free from extortion of south goa taxi driver(which v experienced in "club mahindra" as well as in "Ramada") ....... near to calgunte and baga beach ...staff is attentive.... rooms are big and clean...breakfast was OK .... but fish is good .... Overall good experience.....


“Staff need training”


We stayed at Neelams for a week over new year and found it to be a very nice hotel in a well situated area of Calangute.The room we had in block d was number 13 facing the pool and gardens and was a lovely room with balcony which easily fulfilled our needs.The price we paid was probably high by Indian...


More  




“Rude Staff.. Average Hotel..”


Dates Visited: 12th January 2015 - 15th January 2015
The only good thing that I can say about the hotel is that it is about 5 - 10 mins from Calungute Beach.
The Reasons why I rate this hotel as Average are as follows:
1. Reception Staff: The staff at the reception never greated us either on the day we...


More  




“Very Good Place to Stay”


Rooms are really good, spacious and clean. Breakfast has very good spread & Good Food, It is located at a very perfect place, Restaurants, Bike hire, Chemist & other shops is very close to the hotel. 10 mins walk to the Calangute beach.


“dont expect too much, just a very decent place to stay”


this was my third stay at neelams, staying in room b21 from 18th to 31st dec, everything was ok, nothing really changed in years...they still don't have wifi in room...bathroom flush not working properly, service is ok...cant complain much as it was new year peak season time and i understand how badly Indians behave at that time in goa...so hotel...


More  




“"Very Good place"”


I stayed at 'Neelam the Gran'd with my husband from 25th till 28th Dec and we got a very good room with swimming pool view. It would have been better if we have a balcony as well with it but we were fine as we had a good view from it. We liked everything about the hotel - the room...


More  




“Amazing Holiday”


Really Amazing Experience. Rooms are really good, spacious and clean. Staff is really good and respectful. The hotel has a spa & very reasonable. Breakfast has very good spread & superb food. Free WIFI in the lobby.
It is located at a very perfect place. Restaurants, Bike hire, Chemist & other shops is very close to the hotel.
10 mins...


More  




“Good property for family stay”


I had stayed in Neelams The Grand with my family during the period 26 December 2014 to 29 December 2014. We were a group of 5 adults and took 2 rooms in the first floor of A block.
Let me start with the good things about this hotel first.
1. The hotel check-in process was simple and smooth, with a...


More  




“Good place”


Its was an excellent stay. Good location, good food and overall excellent place. Best food joints all around the corner.. i would give it a 4 out of 5.... yes the room sercice is a bit slow but still a good place..


“total value for money”


its a four star hotel and totally value for money. took the rooms for 4.5 k per night. the breakfast is inclusive and is very tasty. stayed for 2 nights and seemed very nice. rooms very sticky first cos they sprayed disinfactant but then it went off. overall its nice


“Decent Property but needs a massive service overhaul”


This was our second stay at Neelams in the last 7 yrs. They really need to bring their service levels up. The rooms are nicely done. Spacious, clean with all the basic amenities in place. But service level!! Oh my god! They are in the pits. There are delays every where. The pool side bar takes eons to get a...


More  




“Nice stay; good hotel but needs improvement”


It was a nice and comfortable experience overall. The hotel is very far from airport but centrally located near the main market and restaurants, close to Calangute beach. We stayed there for 5 nights in super deluxe rooms of E block. The block is quite old with not so clean staircase. However, the rooms are good and spacious with all...


More  




“Nice Stay....:-)”


We stayed in Neelam's Grand for 4 days and 3 nights. We had a booking of superior room and when we reached we have been upgraded to suit. Wow !!! .. that was super. Room was having a drawing room, bed room and bathroom. Balcony was there from drawing room side. The hotel is very close to Calangute beach. It...


More  




“Hassle free stay”


Hotel is fun to stay with. Staff is supporting, location is good, veg food is good, overall value for money...stayed for our honeymoon n it was fun..Room service is almost good however some housekeeping staff needs to be more humble..


“Overall good experience”


It's a good hotel with clean & spacious rooms, courteous staff, prompt room delivery & soothing ambience. It is at a walking distance from the Calangute beach. Markets & restaurants are also at a walking distance. The minus points are wifi doesnt connect in rooms & breakfast buffet offers limited variety. We were a family of 31 persons including children....


More  




“grand”


i revisited Goa after 25 years .. it has changed for good for sure .. new , modern options to stay over for few days , grand is aptly located and a quiet ambiance to it .breakfast in morning is cherry on the cake , it offers different buffet everyday .though the room service is little delayed , but overall...


More  




“Beautiful Hotel, perfect location.”


The hotel is really Amazing. The rooms are really good and clean. The staff is good and respectful. It has a swimming pool with many plants around and benches to chill out near the pool. There is one restaurant near the poll side where you can enjoy good snacks/food.The hotel has a spa yet we din't had a chance to...


More  




“Cool & Lively”


I was really confused between Grand & Glitz and am glad I chose Grand. It was livelier than Glitz and the environment was very positive. The staff was very courteous and rooms were nice enough to enjoy your stay there. The fact that they serve scrumptious breakfast is another reason why I loved the place. There is free WIFI in...


More  




“Costly but worth”


Hotel location is really good, rates are lil bit costly than others but you will get better satisfaction of money you spent. Enviornment in night is very lavish at the pool side. Room service is good. Only thing they lacking is having intercommunion between grand and glitz due to which i had to book one extra room on same time...


More  




“An Average Hotel, for Couples and families”


We stayed here in room No. 14 in first week of December 2014. We went for our anniversary celebration.
The cost is way too high for this hotel. We had great expectation from the hotel, but somehow we felt that the price was too high for this hotel. We had paid around 16K for 4 nights(after all online discounts).
PROS...


More  




“Value For Money Hotel with all 3 Star Amenity”


like name Hotel Enterance is Realy Grand,
Welcome Drink on Arrival, Baggage Transfer to hotel room, Good Pleasant Feeling while entering hotel. Easy Check In and Check Out.
Walking Distance from calangute Market Place,
Rent A Bike in Front of Hotel.
Good Enf Parking Lot.
Nice Big Hotel with Big Kids and Main Swimming Pool With Attach Bar Counter.
Stay...


More  




“POOR SERVICE”


Hi,
I stayed in this property from 8th to 11th dec
The rooms are good the superior delux room but no view from the room
The room service is the worst according to the star category of the hotel
I didnt have any numbers in my room to call room service or house keeping
There was no menu card for...


More  




“Good Hotel but..”


Its a good hotel located in the center of calangute. even though it looks grand at the outset but the maintenance is not up-to the mark. buffet breakfast is ok. Staff is great.
Place is kind of noisy and you cant complain as it is in the center of the city.The swimming pool is ordinary and is surrounded by hotel...


More  




“Good hotel with excellent location”


Hotel is good with following pros n cons
Pros: 1. Excellent location, proximity to calangute n baga beaches.
2. Good rooms
3. Good food
4. Has an ok swimming pool.
Cons : 1. Just above average services, don't feel 4 star.


“Worth money spent..!!”


Awesome hotel with nice and cooperative staff. Good location, very close to the beach. money wise on little higher side but service is worth money spent. if you eat sea food, do try it out at their restaurant.


“Good Hotel to stay”


Stayed here for 4 Nights. this Hotels is near to Calanguate Beach just walking distance around 10-15 minutes. Hotels has very good Rooms, very friendly and helpful staff but the best part of the hotel was their food. We had breakfast their and that was amazing.


“Nice and average...”


The check in process was nice and quick. The room was very average, bed was slightly too stiff, the key hole was a little dodgy, there was a slight leakage from the roof and the walls were thin. However, it is so fairly priced, it is very central, good service, surprisingly lovely and simple buffet breakfast, helpful staff and pretty...


More  




“Family Fun”


Want to go for a family holiday ? This is the one if you care looking for good accommodation along with service and quality food. That's all one need for family vacation.
Mind it they have a nice garden restaurant for a perfect candle light dinner too. You get it here what you are looking for your family vacation.


“Don't waste your time and money..”


On first approach the hotel looked okay but situated on one of the most congested roads.
On approach to the front desk of the hotel we were met by unfriendly and unhelpful staff who looked liked they really hated their job. We asked for help booking a train ticket because I couldn't book online with being a non Indian resident,...


More  




“Wonderful Experiance”


Good Staff, Clean Hotel. Very near to Beach. Tasty breakfast. We have traveled with family. Opposite their is a good shop for everyday need food, You can also hire two wheeler from the main gate opposite.


“Great Place to Stay”


Hi,
This was my first Visit to GOA.
Had made the reservations through Makemytrip.com for a family Holiday at Neelam's The Grand for 3N/4D.
We reached the hotel at 10:30 am , we were aware that the checkin was at 2:00 pm.
We were given a warm welcome and the checkin formalities were done immidiately. We asked if we can...


More  




“Nice Property”


Very-Good place to stay, Very Good Breakfast Buffet . KFC & local liqueur Shops just round the corner, having a quick snack outside was also easily possible.Friendly Staff. The beach though is requires at least 15-20 min of walking.


“Good place to stay”


Good place to stay, even the food quality was good. With a KFC just round the corner, having a quick snack outside was also easily possible. The staff was also friendly. The beach though is requires at least 10-15 min of walking.


“very good”


awesome place with all amenities with relax mood would love to come back again rooms are comfortable staff friendly food ok but good location and Mr. Chaturvedi is very attentive and good host wish you people all the best keep it up.


“Poor Hospitality”


I stayed in the hotel for 4 days, firstly I was damn excited, as the property was highly recommended in many a sites, but after checking in at the hotel, my views changed totally.. The washroom sucks.. I had a problem of water clogging in washroom, which was solved after a good 8 hours that too involved repeated calls to...


More  




“Comfortable stay”


We were a group of 18 ppl and we stayed in this hotel during the last weekend. The stay was comfortable and great. Upon arrival we were informed that we have been upgraded to suite which was quite a nice gesture from the part of the hotel. The Lunch and the breakfast was just great and tasty. The rooms were...


More  




“Hopeless Hotel-Not a 4 star property at all”


I went to Neelam's the grand this 1-4 October, with my family. We pre-booked the rooms around 4 months ago. But, to our surprise, the hotel guys did not have our bookings. We had to wait in the lobby for half an hour to sort out the matter.
We were given the room sin "B" block-21/22/23. There was no lift!!...


More  




“Great Property”


Enjoyed our stay from 3rd to 6th of October..Service was excellent and breakfast was very well spread out..Buffet Lunch and Dinner also had lot of options..Overall the property is very well maintained..I would suggest every staff of the Hotel to keep up the good show and I would request to the management to get the WI-FI's in the rooms as...


More  




“worst hotel experienced”


it was very bad experience .it totally spoiled our holiday.stay was very uncomfortable.even the basic facilities like toilet flush was not working properly.i have to call housekeeping 3 times . then they attended my call. bathroom was partially blocked,basin water flow was poor bathroom hygiene was very very poor.room flooring was not clean.they do not clean the floor properly.water was...


More  




“The Grand in Calangute, GOA”


I have been here before several times and each time hey have added and improved.. Neelam's The Grand is situated in the heart of buzzing Calangute in North Goa. It's not a beach front hotel. It is in town among all the shops and the commercial centre. The beach is only a few minutes drive or about 12-15 minutes walk....


More  




“Worst possible experience in goa”


Everything starting from the gate guards, lobby manager, rooms, breakfast is horrible and worst possible in hotel industry. Totally inflexible and my experience was so bad that it did not hurt to check out a day in advance. I would recommend to stay away from this hotel.


“Worst hotel in Goa!!!”


To start with had a rude Lobby Manager while checking in, Rooms are not even near 4 star standards as advertised.. Breakfast buffet is badly managed and you would prefer to eat outside rather than talking to the filthy staff that they have.
They parked my car in the near by hotel and called my room the next morning at...


More  




“Good Hotel - Bad Water”


Stayed at Neelam The Grand for the long weekend (15th August -18th August), good hotel, good pool, good service. Horrible quality of water though, the water in the shower & faucet smell bad. You will have to use Bisleri water for brushing your teeth & they have a strict 2 bottle a day policy.
The concierge was very helpful &...


More  




“Party Fiesta”


Amazing rooms, abrupt services but overpriced food.
It was worth every single penny spent on the the lodging.
Was amazed by the pickup and drop service provided by the hotel.
The location of the hotel was just perfect. Hotel staffs were polite and had no issues as such.


“**** Awesome four days stay ******”


We stayed this hotel for four days and very happy. Staff and their manager was so cooperative and ready every time to help you. Room was very decent pool is also very beautiful. Breakfast was excellent many options for every people. Hotel is also very near the baga and calingute beach and market is also on walking distance. Anyone must...


More  




“An excellent business cum family hotel”


I have checked into this hotel on 23rd July with my family and friends during our trip to Goa and was given rooms H15 and H 17. Though I was little sceptical in the beginning as this hotel is 1.2 km away from the beach, I was very happy at the end as we were given a complete comfort. The...


More  




“Wonderful stay”


Simply a wonderful stay with a beautiful hotel... perfect...
the rooms were clean.... pool was huge and staff was co-operative... breakfast is average but that much is manageable... n plz book online as it is really cheap compared to over the counter price....


“its not a 4 star hotel it sucks”


i have never seen a 4 star hotel where there is no elevator .. staf member dont have any respect for guest .. i will never prefer this hotel in my life . bath room nt working. n hotel management should come an vist the room n then do booking ac is 2000000 years old dis is nt a 4...


More  




“Excellent”


Hotel was Nice...Staff Also Nice..Rooms Also Nice..Food Also Nice....but one old Lady Was Very rude In The Reception..i dnt know her name..but really she is rude to everyone..she is looking old and short in height ....pls take action because if one fish dirty whole sea then u know...wht happnd....but thanks for beautiful hotel and the room.....


“Neelam the Grand is a nice place to stay in Goa”


Neelam the Grand is a nice place to stay in Goa, specially for those who want to be near to the markets and eating joints.
The food was very tasty and varied, and the staff friendly. There are two lovely pools.
KFC is just next door and Pizza Hut at a walking distance.
One can hire scooters/bikes and cars from...


More  




“Boring”


Stayed here for a couple of days and decided to shift b'coz found the place quite boring. Breakfast buffet was good. Parking is available but is a bit of an issue due to structure of the hotel. By next day morning u will find loads of bird droppings on ur car which cannot be avoided at all. Room was decent...


More  




“Great Stay awesome Hotel”


I am just back from goa, we were 2 couples with 2 kids in each room, The Hotel staff was very helpful, The room size was great we were assigned room no E 22 & E24, Check in on 17jun and checked out on 21Jun.
Breakfast was much tempting and had variety for every one. This included North Indian/South Indian...


More  




“A good three star property with untrained staff”


We visited in June 2014. The propety is good with big rooms and clean rooms. The staff seriously needs training. it gives a feeling you are walking in a government peoperty. They do things when asked to give you a feeling they would not have done it if they had a chance. Relunctant service. from the reception to the roomservice,...


More  




“neelam goa”


location is best. hotel needs renovation of interiors. needs to improve service and staff behavior. rest of things are ok. old building and cleanness required attention.......................................................................


“Good”


Nice property well maintain an also memebers of hotels where very co-operative. But parking created a great trouble n even similar breakfast was mood spoiling......overall nice stay we enjoyed well................


“no bath tubs.”


the photographs of the hotel on your website showed that the bathrooms have bath tubs. but there were none. complete dampener. moreover we booked a poolside view room from clear trip but we were not offered the same for at least two days.


“Average”


Good rooms but the worst lousy food. The room floor has stains which they let go without cleaning but the stains are too much to go unnoticed. The vegetable dishes are strange and terrible and cannot be eaten. The hotel is owned by Narayan Rane when he was Revenue Minister and so money was just put in by him but...


More  




“Terrible”


Dodgy. Pictures don't look like on the internet and people were argumentative. Bed linen not clean. We left as can get better for that money. Also didn't refund us after we left. Family and friends having a get together outside our bedroom door until middle of the night and would turn tv off.


“Basic property for 4 star rates”


the rooms and property is pretty basic. the rooms in old wing are untidy. only positive is location. its near to calangute beach.. walking distance.. breakfast is very basic and lacs variety and taste. Service is okay. wont stay again


“Excellent Hotel and Service”


We experienced truely wonderful hospitality at the Neelam's Grand and would like to visit again. Room was really good. Hotel was also good with very nice and helpful staff. Hotel also has a decent restaurant which serves you good food. We also had a jolly good time at the swimming pool.

