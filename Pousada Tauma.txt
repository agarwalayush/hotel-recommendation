
“Excellent”


A fantastic hotel in the busy and wonderful Calangute. It was my first trip to india, but it will not be my last. And when i do come i will only stay in this slice of paradise. Amazing owner who makes sure all your requirements are met, a very hands on and wonderful gentleman. Highly recommend to anyone thinking of...


More  




“A Truly Heartwarming Experience!”


I stayed at Pousada Tauma for 10 days during the Christmas Holidays with my family and my boyfriend. It was like finding a small gem, an oasis, in the midst of all the craziness that is happening in North Goa. Imagine, having a little private slice of Heaven for yourself, a place where serenity and peace overrule the outside noises....


More  




“Best Boutique hotel in north Goa”


Stayed at Pousada Tauma for New Years holidays. The location is fantastic in terms of having best beaches and happenings in 15-20 min. Neville(Owner) and his crew,Mathew and Neville's sons will make sure that you will have an amazing time. The food is amazing at Pousada and you do not have to go anywhere else to find a healthy ,...


More  




“Wish I could give more stars!!!!!!!”


Pousada Tauma is a slice of Heaven on Earth!!!!!!! As soon as my husband and I walked through the gates we were in awe. The detail put into the creation of this one-of-a-kind boutiques hotel is clear to anyone who stays there. We were greeted by the wonderful Mathew. We were taken to a gorgeous room right off the pool...


More  




“Paradise!”


It was like you died and entered Paradise! The last time we were in Goa was January 2009 ---- fast forward to December 2014 – almost 9 years later – we were looking for a taste of that “village” feeling, surrounded with lush vegetation, a balcao to listen to the birds and sip coffee, or later in the day, indulge...


More  




“Pousada By The Beach: Can't get better than this!”


My wife and me went to Pousada By The Beach last year in May 2013 and were surprised by the amazing hospitality provided by Neville and his staff. Neville introduced us to his entire staff which made us feel at home. Kevin is a Masterchef and the food prepared by him was amazing. While visiting Goa this year, we made...


More  




“A Pleasure, an Oasis.”


I loved the grace and beauty. Staying at Pousada Tauma was a delight. We felt peaceful in the gardens and nourished by the food. Neville and his sons were lovely hosts. As we recollect on our trip to India, staying at Pousada is definitely one of the highlights. My son did not want to leave:))
I'm sure this is the...


More  




“setting new boundaries”


it must be said from the outset, the service was impeccable. i have never experienced this type of personal service as i did during my 3 day stay at Pousada Tauma. We got in late in the evening tired & after taking 2 flights & we were told that check-in could be done the next day...what a pleasure! That set...


More  




“Home away from home...”


It's a bit hard to describe our 5 days at Pousada Tauma. We really just felt totally at home. The property is serene and gorgeous. The beautiful pool centers the property, and it's so nice to just sit there all day! Neville the owner is amazing - personable, helpful, welcoming, accommodating, the list goes on. He has built this place...


More  




“Quiet & rustic”


Tucked in the heart of Calangute... and yet away from all the humdrum and noise that is typical of North Goa.... is this quaint little traveller's delight known as Pousada Tauma. We stayed here during the first week of Oct and simply put, it was a great experience.
The resort is spread across 2 acres with 12 suites, each with...


More  




“BEST HOTEL EXPERIENCE ANYWHERE”


My wife and I are well traveled. In 2014 alone we have flown over 100,000 miles and have stayed at Hilton, Hyatt, Ritz-Carlton, W Hotel, Crown Plaza, Sofitel, Taj, Leela Palace and several other small boutique hotels... so it is with some experience that I will call Pousada Tauma the best experience anywhere. The only other comparable experience was when...


More  




“A restfull private experience”


There are quite a few adorable boutique hotels hidden in Goa but what made this quaint hotel stand out was the privacy and service especially during this first week of October weekend when all the hotels were running to full capacity in Goa, and it was obvious that this hotel is well managed by Neville and team. It was very...


More  




“jewel of Goa”


Spacious setting, luxurious rooms, wonderful and accommodating staff, terrific food.
All the staff--including the driver, the chef, the property manager, and the owner--contributed to gracious hospitality. Overall, a superb retreat. Congratulations to Neville for creating such a jewel.


“A Family Retreat”


Indeed, an oasis and retreat for an exhausted, world-travelling family. Perfect beginning to our Goan experience. Owner Neville served as our outstanding guide (an excellent and interesting communicator whose family also welcomed us) and driver, who happily shuffled us when we wanted between our wonderful, clean and cozy poolside suite to the resort's beachside restaurant, a key feature to the...


More  




“A hidden oasis”


First of all I must apologise to Neville for taking so long to write this review.
My husband and I stayed here at the end of March this year. It was our second visit to Calangute but our first time in this hotel. We spent the final few days of our holiday here and it was the perfect end to...


More  




“A gem in Goa”


Stayed here with friends for 4 nights from April 29th 2014. The place is just a gem, a paradise in the midst of busy Calangute area. The suites are all individual, clean and spacy. Really nicely designed. The lush gardens, pavillion are just so colourful and calming. The food was so fantastic. All these coupled with the hospitality and the...


More  




“Amazing stay”


Stayed here February 2014. After a whirlwind tour of India, a restful
5 days was in order. The Tauma Pousada is an oasis amidst the frantic
streets of Goa. The large rooms are quiet, comfortable and surrounded
by lush tropical flora and fauna and all centered around the beautiful
pool.
The staff anticipated our every need. The manager, Mathew took...


More  




“Paradise”


Had a truly wonderful few days at Pousada. This is a piece of paradise caringly created by the most charming owner; Neville. Highly recommended as a great base for exploring this particularly fun area of Goa and especially if you are looking for a stunning hide-away for romance or relaxation.
The staff at Pousada are genuinely friendly, attentive and demonstrate...


More  




“Quiet and piecefull boutique hotel”


According to us this hotel is more then any five star hotel in Goa. It's a compact boutique hotel. Very homely atmosphere. Their food quality is very good and excellent service , very healthy breakfast with fresh juices. The rooms were also decorated with scented candles. I was surprised that the owner of the hotel Mr Neville served us Goan...


More  




“Amazing ”


Wonderful hotel with great familiar environment.
Very good food (typical goan), excelent service (great staff) impecable rooms. Pool area is excellent.
They provide support for transportation to beach or local markets.
If I'll go back to Goa, definately would love to stay at Pousada again.
It's a top choice and I Know I wouldn't regret it.


“Heaven on Earth”


I highly recommend Pousada Tauma to anyone looking for a quiet and relaxing vacation spot in Goa. Our flight did not arrive until 10 pm. On the way to the hotel, the driver gave us the food menu and told us that in 15 minutes the hotel would be calling to get our dinner selections. It was thoughtful of them...


More  




“Great long weekend break”


We have just returned from our first visit to Goa and stayed at the Pousada Tauma following Tripadvisor reviews. We went from Saturday to Tuesday. We had three lovely rooms around the pool area (2 adults + 4 teenage children). The Pousada Tauma felt instantly like home, we were made very welcome by Neville and his staff. The rooms were...


More  




“Just Superb”


This place has been a labour of love for Neville, it's owner. he has built this unique hotel and now presides over it, maintaining a very high standard. He is always at hand checking the guests have everything they require.
The rooms, are all very individual and are situated so that you have a very good amount of privacy, with...


More  




“TA sets high expectations, but it still delivers”


Excellent, personal customer service is what makes this place so good. You can see from their website everything else. I give 5 stars rarely but pousada tauma was a wonderful experience.
It really is a must visit on any goa trip if you can afford it.
I know Neville and the team are always looking to improve and so in...


More  




“Elizabeth de Lessner A hidden gem - Pousada by the Beach”


Holidaying in Goa two years ago we came across this hidden gem, a restaurant by the beach with facilities to spend the whole day there. All set in exquisite gardens on the edge of Calangute Beach. The restaurant is well furnished with quality teak furniture and a wonderful view out towards the Goan sea. Kevin the Head Chef is a...


More  




“Seeing is believeing - just fantastic.”


Words cannot explain this place, its absolutly fantastic,both locations one on the beach and the other in the center of Calangute, i have been to most hotels in GOA from 5 star to 1 star and this location puts the cherry on the icecream, the personal touch no one will ever find anywhere.


“Copper Bowl Restaurant within Poussada Tauma Hotel”


Whilst we have never stayed at The Poussada Tauma Hotel, we have frequently visited the property and have noted the exceptional style and quality of the accomodation. Indeed our experience is of visits to the Hoels Copper Bowl Restaurant and to their associated Beach Restaurant which are the main focus of our annual visit to Goa.
En route to the...


More  




“Beautiful and Relaxed with Excellent Service”


Thanks to Tripadvisor and reviews on the website we discovered Pousada Tauma on a last minute decision to stay in Goa (as we were on a road trip heading from Pune to Mangalore).
As soon as we arrived we were made at home and were informed that there were rooms available. It was a quick check-in and were shown to...


More  




“Simply serene. Experience -Positively Unforgettable!”


My husband and I visited Pousada Tauma in February and the overall experience was unforgettable. A variety of dense plantations across the site and a beautiful pool were the highlights. At night, the dim lights enhanced this beauty and made it look very ravishing.
The resort holds a very hospitable staff - yet ones that respect privacy of the guests...


More  




“The perfect place.”


I went to a short trip to Goa with my friends and we booked this hotel first time. It was really an amazing experience for all of us. We really enjoyed a lot. It was so relaxing place. The staff was so good and very helpful. The service was so quick and good. The rooms were so beautiful and spacy....


More  




“This gave us a true and relaxing Goan vacation”


After a tumultuous 2 weeks in India, we showed up at the Pousada Tauma ready for - nay, in desperate need of - some relaxation. This was the 7th hotel we had stayed in; we had stayed in a range from large hotel to boutique and from really nice to horrible, so while I had previously been very excited to...


More  




“Absolutely perfect”


I booked a short trip to Goa at the end of April/beginning of May, and had foolishly taken advice from a work colleague to stay in another hotel in Calangute. I checked out after just 2 nights (my first hotel was awful) and came straight to Pousada Tauma after reading excellent reviews on TripAdvisor. Yes, it's more expensive than other...


More  




“Were misled by other reviews”


On second trip to Goa after Leela Goa in December last decided to change and went for this small "boutique " hotel in north Goa . Had stayed in very very nice boutique hotel in Sri Lanka in November (Serene Pavilions in Wadduwa) and expected something similar .
It proved very disappointing and checked out after two nights . As...


More  




“Quiet oasis with great service”


A lovely, very quiet hotel with superior service. Calangute is a dynamic and noisy holiday village. You will find shelter in this hotel which is quiet, chilly, clean and you will be able to rest there. People who work there are friendly, can adjust to your needs, will make your wish, will help and make you feel really well serviced....


More  




“Best boutique + cultural hotel of Goa”


Had a fantastic stay @ Pousada Tauma. I thoroughly enjoyed every minute of it. All fundamentals of the hotel are of very high standard. Full of bamboo fencing, minuscule streams and plants completely everywhere. The pool is fabulous; huge, rambling around corners with large rocks in piles around the edges.
Special mention of Viagoa.com which gave us a good deal...


More  




“Tranquility Unmatched”


I still can't come to the terms with the reality of the stay i was subjected to...magnificent...simply majestic...it takes a while to come to terms with the pricing of the suites available n i wud recommend the owner to look into dis aspect...but to everyone out there looking for a peaceful n romantic time wid ur wife or fiancee...dis is...


More  




“simply the best in goa!”


Pousada Tauma is definitely the heaven on earth. I can not find the right words to describe the beauty of the garden. The owner Neville put his vision and hearth on this beautiful property and they deserve all the compliments. they are taking care of all their guests on individual basis and make sure that you receive an excellent service....


More  




“nice place to stay out pumy pigs”


Food is awsme, genuine price with good ambience. i personally like the view and place near to the calangute beach.
specially the doggy, chill bear, nice food. 1 must go.
as it was our 1st trip in goa, i find bit of difficulty finding the place. but, reaching there it was a good experience.
i wish i will go ther...


More  




“Heaven on earth in Goa”


We stayed in Pousada Tauma for 5 nights and we loved every second of it. It is gorgeous, peaceful and intimate. The food is delicious, it's beach private, and the service and approach very personal.
Why only 4 out of 5 stars, may you ask? Well, the only reason is that we think it is slightly overpriced. The whole experience...


More  




“Simply Paradise”


My wife and I spent last weekend at Pousada Tauma and after reading the reviews we had very high expectations. We were not disappointed. Neville and Mathew were incredibly gracious. The hotel itself is spectacular, like stepping into a tiny piece of heaven in the middle of Calangute. If you are looking for a place to sit back, relax, read...


More  




“Unique oasis”


We made a round trip in India and the last 6 days we stayed at Pousada Tauma in Goa. It is a unique small hotel in a beautiful and romantic garden areal. It is clearly an oasis in the town Calangute and the privacy in the hotel is awesome. You can really relax and enjoy this small little paradise. The...


More  




“Authentic and tranquil”


Pousada Tauma's simple, calm refinement reflects its urbane host, Neville. Our uncomplicated needs - afternoons at the beach sandwiched between relaxing sleep, languid breakfasts and quality dining - were fully satisfied. True, the rooms are not sumptuous, but that is part of its authenticity. More important was the attentive service provided without compromising seclusion and privacy. The menu quite rightly...


More  




“A personal paradise in the heart of north Goa”


We just came back from a mini-vacation in Goa to celebrate our wedding anniversary. This was the first time we visited Goa and apart from the awesome beaches of Morjim and Mandrem, the one place which touched us both is Pousada Tauma-it's like a home away from home. I wouldn't like to call it a hotel or a resort because...


More  




“Something different”


Pousada Tauma is a true oasis and a unique experience. It is on the edge of Calangute - not everyone's cup-of-tea - but the clever design and lush tropical planting mean that you are totally insulated from the hussle and bustle of the crowds. However this proximity does allow you to sample the fleshpots of the town and then retreat...


More  




“BEAUTIFUL”


A very special experience. I travelled a lot in India. This is my 18th time, every time for one month, sometimes more.I soent in Posasada 4 nights in january 2013. About it i can say that it is a perfect combination of style and special atmosphere together. All the staff care you a lot. The room is clean and if...


More  




“Great Place - ignore the Never Again”


I stayed at Pousada Tauma during December and found the Hotel exceptional. The staff were of the friendliest I have ever encountered. Neville, the owner, was exceptional in his attentiveness to the guests. I can truly say that he touched each one of us every day and made us feel special.
I am at a loss and feel bound to...


More  




“That one negative review below is weird to me!!!”


The negative review below is so, so, so the opposite of my recent experience it just seems so weird and off the mark to me. This place is an absolute delight. I have traveled extensively and this place stands out for me by combining a 5-star experience with simple down home friendliness and caring. The feeling was like being in...


More  




“Fountain of youth”


Meanwhile I am here since 2003 every year except 2006 and 2011 because for reasons of health.
Before I found this place I have seen together with my husband a big part of the world and luxury hotels at many places.
The only reason that I come again despite of the strenuous traveling - I am not the youngest -...


More  




“The kangaroo and the pom”


We spent 8 days at the Pousada Tauma - wish it could have been 8 weeks.Just sensational.
We agreed that our bathroom needed updating but our decor and four poster bed was traditional, which we loved - all was very clean,the staff were very attentive and the hotel is a haven of peace, quiet and tranquillity and feels so remote...


More  




“Amazing! The perfect getaway.”


Staying at Posada Toama was an idyllic and amazing experience. I wouldn’t just say it was just a “boutique” or "heritage" hotel, because it defines and surpasses those monikers. It is the only place I would stay at in Goa. I arrived off-season in October and was greeted warmly (an understatement) by the staff by name by Matthew, Kevin the...


More  




“Fantastic tranquil weekend”


My wife and I just returned from a fantastic stay in Goa. We'd never been before and I picked Pousada Tauma based on the consistently excellent Tripadvisor reviews.
I arranged the stay by email and Neville couldn't have been more flexible and helpful in making the arrangements.
From the excellent local taxi driver who escorted us from Goa airport, to...


More  




“Paradise in Goa”


After many hours of research Maureen and I decided on the Pousada Tauma for our 6 day break before returning to Australia in December from a cold and wet UK. We were not disappointed as Neville and his great Team exceeded our expectations by providing excellent service and hospitality. The hotel food was excellent and served in the lovely garden...


More  




“Fantastic experience”


If you r looking for a relaxing holiday....then this is just the place to be.....the ambience is divine and the service and the food are excellent...highly recommend this to people who are looking for a getaway from the daily hustle.


“Couldn't ask for a better place to stay!”


My friend and I visited Pousada Tauma over the 2012 Christmas holidays, and couldn't have had a better experience. From the beauty of the premises to the astounding service; Pousada Tauma is an amazing oasis of luxury. Neville, the owner, has done a great job of building and maintaining a gorgeous resort. The architecture, as you can see in the...


More  




“Oasis”


I had been traveling for over 3 weeks diving in the Maldives and visiting Sri Lanka. I was tired by the time I reached Goa and didn't know what to expect as I drove through the crowded chaotic streets to my hotel. I was greeted by kind people and given a beautiful spacious meticulous room in a tasteful jungle temple...


More  




“A perfect, peaceful oasis with the most tasteful design.”


Arriving in Calungute can be a shocker, but the moment you walk through the gate at Pousada Tauma you know you lucked out with the best boutique hotel selection ever. The staff are lovely, kind and discrete. Neville, the owner, has exceptional taste and has created a setting that makes you feel special yet so comfortable. The pool is perfect...


More  




“A slice of heaven amongst the chaos ...”


I recently stayed at the Pousada with my friend and Aunt and must say I am truly surprised by some of the reviews below. To us this was a beautiful boutique hotel that is a vision of tranquility and relaxation and was a welcome break from the chaos and noise of Calangute. Infact we loved it so much, that despite...


More  




“Idyllic, the perfect retreat”


Had a wonderful in Goa recently, the main reason being this wonderful hotel. The holiday didn't start well when we had to queue for 90 mins to get through Indian Customs. The airport is way too small and they check each Visa in great depth. The new terminal being built can't come soon enough. However, and this is a very...


More  




“Couldn't be happier”


I booked this hotel for a family trip based on reviews on Tripadvisor and an article in Vogue India.
Even though the reviews were great, I was still apprehensive, since sometimes the hotel can be below par, but the price makes it number one on the hotels list.
My husband and stepkids are used to fancy resorts; if they hated...


More  




“"A little piece of heaven"”


Possibly our best holiday ever!!! Pousada Tauma (or our little piece of heaven) was incredible. Everything from the staff, service, location, food, accommodation was perfect. We couldn't have asked for a more beautiful hotel. Magnificent landscape personally maintained by Neville (the owner). He was always around to make us feel so very welcome. A very special person with a big...


More  



