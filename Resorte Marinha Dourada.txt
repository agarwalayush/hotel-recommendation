
“Marinha Dourada”


Great location but I suspect the owners are relying on this to attract visitors.
Best bits are the lakes where you can sit and enjoy a peaceful day away from the hustle & bustle of Baga and the beach. Rooms kept nice and clean. Watching kingfishers fishing in the lakes and walking over the road to the ‘natural’ lakes and...


More  




“first experience of staying in a hotel in Goa..”


Was a little dubious as previously stayed at a friend's home. The accommodation, was very roomy considering there were 3 adults in 1 room. The cleanliness was impeccable, fresh towels everyday and bedding changed every other day. The entertainment was fantastic and the food was fresh and lovely.
The grounds and pool and all surroundings were kept beautiful by the...


More  




“Third year”


Third year here and it improves every year (except the breakfast). The welcome was fantastic. The staff brilliant and friendly. The grounds and views beautiful. We love this place and have not found anywhere that compares. Still good value, but they need to sort out the breakfast issue.


“Good Location but...”


The hotel is in a good location by a lake and close to the beach but it is aimed at package holiday guests. There is entertainment every night which is extremely loud and can be heard in the rooms and this goes on until midnight some nights. The rooms are a decent size but are in need of decorating. The...


More  




“Good Value for Money, Quite & Peaceful hotel”


Good value for Money, Quite & Peaceful hotel.
Little far from Baga beach - a 25-30 Minutes walk, though they have free hotel shuttle to baga each every hour from 9:00AM-6:00PM.
Very close to Saturday market Bazar.
Apart from little far from Baga beach. Rest all were good at hotel.
Decent swimming pool, an average room quality, an average toilet...


More  




“nice hotel”


stayed here for 3 weeks over the Christmas and new year.had an excellent stay,the breakfast included in the price was good.to good swimming pools.will return again next year.this hotel is situated near the banks of bagga river,and walking distance to the night market and arpora village,


“Apart from the view we were rather disappointed really!”


Looking at the Trip Advisor's me and my partner were very excited to stay at this hotel. I wouldn’t want anyone wasting their money staying here I am not just being a moaner this is total honesty. I am 20 and my partner is 22 both from North West and would not recommend this to a young couple or young...


More  




“Good value for money quiet peaceful location helpful management and staff would recommend”


Recommended to stay at this hotel by family rooms closer to reception are more up to date and nicer. Fabulous stay with no problems wot so ever could be a bit noisy on market nights or wedding receptions at hotel but in goa very quiet area to stay and doesn't go on to late. perfect base to all beaches and...


More  




“Superb hotell”


It was a pleasure to stay at this hotel , a fabulous hotel surrounded by a lake stunning views, the staff were all very helpful and always eager to please , couldn't wish for anything more , overall fantastic!


“Fading giant”


Came back from the Marinah at Christmas. It s the first time we ve been back to the hotel for about four years. It was about 2/3 empty. The location is still incredible but standards have dropped.
If you plug anything in to a socket in your room it will fall straight out . The room lads , although the...


More  




“Good location..decent Rooms”


Stayed in this resort during mid-October 2014. The location is good. hardly 2km from famous Baga beach and around 4 km from Calangute.
Though it is spread out in very vast space and has two lakes and two swimming pool but the only problem is with the management and service...
..view is really great from rooms facing lakes and pool......


More  




“yet another stay at the Marina Dourada goa”


We have just returned from another stay at the Marina Dourada. We had a refurbished room which was great. This is not a posh hotel but it is very friendly the rooms are clean, beds are comfortable and the shower hot and powerful. It is in a beautiful setting and we shall return again . Thank you for an enjoyable...


More  




“Bad experience ”


Stayed here for two days. For sure overrated resort. Only good thing about hotel is landscape (Lake and trees) and decent staff. Poor quality rooms(I had to change mine due to ants all over my bed). Worst breakfast options. ( have to pay extra for fruits). No safe in room.


“Good location decent rooms”


Stayed in this resort during min-Decemeber 2014. The resort is about 16 kms from Panaji city.
Pros:
The location is good. It is around 2km from Baga beach, 4 km from calangute and 6-7 from candolim beach.
Hotel provides hourly shuttle to-and-fro to Baga beach till 7pm.
If you feel lazy to leave resort you will get beds in swimming...


More  




“Lovely stay indeed”


Good location, safe and well maintained property.Lots of green lush lawns and grass patches. Small circuit inside the premises for a peaceful early morning, evening or night walks. Spacious rooms, a small balcony, clean poolside and good service makes Marinha Dourada a good package to enjoy in Goa.
Good food for a vegetarian like me and good service as well....


More  




“Good value, well-positioned hotel in Arpora, Goa”


The hotel was rather an oasis aesthetically compared with the busy and tatty area of Baga. Local areas easily accessible but ideally suited away from the bustle. If you are a birdwatcher you will meet fellow birders there: look out for optical equipment! The service was excellent and very friendly. Rooms cleaned and bedding, towels changed daily.


“A comfortable place to stay in Arpora”


I liked the Marinha Dourada because its position away from the coast made it quieter and calmer than some other locations. For birders like me it's also convenient for the Baga-Arpora birding spots (which despite development, still turn up good birds.) The hotel is something of a fading giant, but I found it comfortable and the service was helpful and...


More  




“Good value for money option in baga”


We stayed here for a couple of days last month. The rooms are very basic but spacious and well maintained. Its a huge resort with close to 200 rooms. The property is located right next to a lake. No direct access to beach, but you can drive down to the beach or other places near baga (5 minutes drive to...


More  




“Nyc hotel at good price”


I stayed for 4 nights. Very nyc ambience close to natural beauty. Although in house facilities are not much (free) but the place is value for money. Lake , pool and songs are just add ons for perfect stay.


“Basic hotel for a good price”


I stayed here for 3 weeks from November to December 2014. This hotel is very basic. I stayed in a double room. It was a very large room. It had a balcony. The bed wasn't the comfiest I have slept on. It was quite hard and all the padding was around the edge of the mattress. I wasn't impressed with...


More  




“Super Setting”


Yet a another stay in our favourite Goan Hotel, basic rooms but kept very clean, really friendly staff, wonderful views from balcony, back again in 2015.
Couple of down points : It seems to attract a lot of noisy Russians and take care with your flight times as the Hotel charge 3 thousand Rupees to keep your room on until...


More  




“Awesome :)”


One of the best resorts if you'd like to spend some good time with your family or partner. The staff's really nice and courteous. The rooms are lovely and clean. You'd have your complementary stuff delivered at your doorstep every morning. The food is good but pricy.The location is awesome with 2 lakes and beautiful greenary. Away from the bustling...


More  




“Wonderful Marinha Dourada”


After our first visit to a hotel in Candolim which was not great, found this hotel on the web and been here at least 8 times now, it is fabulous in so many ways. The price for what you get is brilliant. The rooms overlooking the lakes are spacious, have all you need, adequate bathroom, large comfy bed, air con,...


More  




“Good Hotel with Excellent Staff”


We booked this hotel from tripadvisor->makemytrip. We were 2 adults and 1 kid(3 years) visited from 15th November 2014 - 20th November 2014.
Pros
1) Near to Flea Market, almost near to all the beaches and a beautiful church
2) Have 2 lakes and a big Kids area
3) Very courteous and Helpful staff.
4) Daily Shuttle service to Baga...


More  




“superb..”


the perfect example of excellence.. best hotel in north goa.. what a hotel what a quality food clean rooms.. suoerb service.. swimming pool awesome for family stay.. natural atmosphere.. beautiful hotel ..just awesome.. I suggest everyone..


“Security issue”


Nice, well maintained resort. Only big issue is parking place for bike and cars. Security is standing 24/7 on entry gate. When is stolen bike 3 meters away them, they dont see that. Management dont help you, cameras missing there, only advice take taxi and go to police station. Up to three urgencies not replay from security night shift. Really...


More  




“Good place to stay but avoid lake facing rooms and try to skip breakfast”


This is a good place to stay and will fit your budget but avoid lake facing rooms and try to skip breakfast.
Lake facing rooms: too far from the main gate and don;t have great washrooms. Pretty dirty washrooms. But they do have a good morning view, which you can anyways go and see.
Would suggest you go for pool...


More  




“" Very Poor Service "”


We have visited here from 27.10.14 to 31.10.14 with family and booked 3 bed room but they have provided only two blanket and when i called for extra blanket they said their house keeping will closed at 8pm u will get next day only 'how can new guest will know the timing of housekeeping'......complimentary breakfast is also for the namesake...


More  




“NICE PLACE”


Book this place but please mention for new rooms, their old rooms bathrooms are pretty small. Breakfast is really not that great. Rest everything of this hotel is great. It is value for money every disc and pub is around it and the lakes provide a great ambiance in the night. This is the second time we are staying in...


More  




“Perfect place for a relaxing holiday”


Lovely quiet location but just a short 20 minute stroll to the beach or free hotel shuttle bus if you prefer ,rooms are clean and are in the process of being upgraded ,there are a number of very good eating places within walking distance of the hotel


“A very well maintained property but rooms are basic”


Stayed in this property for 2N (4th to 6th Oct). I must a say very clean and beautiful property with 2 swimming pool and 2 lakes. They have 1 pool side bar and one lake side bar. Lake side bar is open till 10 PM whereas pool side one is open until 1AM. 1 restaurant. We stayed in wing 2...


More  




“A bit dissapointed this time!”


This was my second time in this hotel. The last time I came with my friends was in Feb 2014. My first experience was quite good and that's why I opted for this hotel again.
Rooms and beds were quite clean but fridge in the rooms were old and rusty from inside with no mini bar. This created a bad...


More  




“Value for Money, Good Location”


We stayed in this resort in first week of October-2014 (season's peak). The resort is at a very convenient and peaceful location, approx 1.5 KM away from Baga beach. It's a very beautiful resort, away from the crowded beach area and at the same time not too far, assuming you are anyways going to rent a vehicle. Few of my...


More  




“Excellent Experience & Pleasant Stay!!!”


We stayed from 1st to 4th of October. The resort is well maintained and has beautiful scenic view with lake and hills. Service was prompt and complimentary breakfast was tasty. Overall we had a good experience and worth staying. Hardly 2 KM from Baga beach. Very quite and calm place to stay for families.


“Excellent Facility for Stay..Don't Expect Much from Service..!!”


We stayed here in Oct 2014, the Location close to Calangute and Baga beaches, Spacious Room and balcony, All rooms have a Lake view in New Annexe Builing are pretty cool things..we Especially liked the private lakes and their views from our balcony.. Perfect place if you have kids with you, they have a small cave with a small kids...


More  




“Stay here only if you want to enjoy a good room at low rates!”


We stayed at this property on our honeymoon! We had not booked any hotel for our stay in Goa and decided upon browsing random hotels that came our way. We were driving past this area and we could see Sarovar Portico at a distance, however we thought that Sarovar would obviously be expensive; then we stumbled upon this place. It...


More  




“Nice property average 'Resorte'”


It was a get together of friends so we were not too much worried about the quality of the hotel. First we had a tough time locating the last mile connectivity to the hotel. Google map could not be of great help. With some local help we did reach the property. It certainly is a great piece of property with...


More  




“Not so customer friendly”


I have stayed in this hotel from 18 Sep to 20 Sep,2014. I have booked it through goibibo. I found that hotel property is good and quite big. Hotel has two private lakes and swimming pool ,kids play area and bar.
I found several issues while dealing with the hotel staff.
1. After reaching to hotel we request them the...


More  




“Money value time”


I had spend 5 days here.... Which r the best part of my every holidays.... Staff is so cooperative well mannered ....
I got my value for money here.... Location is the best part of this resort.Its have 2 personal lake n swimming pools....miss u marinha dourada.


“Excellent service and condition of room”


My stay at the hotel was nothing less than excellent in terms of service, staff attitude, which was very helpful. The hotel even helped booked the south and north Goa tour by coach so I did not miss any important places.


“Excellent stay experianced.”


The resort was a soothing place to spend your vacations.
Staff was evertime ready to help in. Got complimentary breakfast and it was tasty in all aspects.
Although the resort was not even half occupied by the guests.
In all it was a nice time in resort Marinha Dourada.


“My great time at Marinha Dourada”


I like this place a lot for its location and its been maintained absolutely well.
Staff can be a little more friendly and you gotta make Wife Free and stop charging for water.
Every re fill of water is charged Rs.10 .
Thats cheap!


“2nd and last stay at the Marinha Dourada!”


Myself and my sister stayed here for 10 days in March/April 2013 and returned again for 2 weeks the same time this year. Our dad had recommended this hotel to us staying here a couple of times himself.
After a 16 hour journey to arrive at the hotel we waited in reception for nearly an hour for check in, to...


More  




“Great service”


Service persons are really good and cleanliness is remarkable. The property is vast and well decorated. Due to off season, food offerings are limited, but foods are good. I would like to recommend this resorte to my family and friends.


“"Comfortable and Refreshing stay"”


Its my second trip to Goa with my Hubby.Before coming had already gone through the pics of hotel.But i must say excellent place,more beautiful than actual pics.Perfect place to relax around the lake.Low number of guests as we went at the end of Aug.we stayed on the second floor.Room was nice.But have to mention they dont provide Hair Dryer as...


More  




“my first trip in goa”


my first trip to the place with my wife, and i feel like coming again here. best location, very good food quality, very good service, and room space are to good. but no toiletry........... Only two complimentary water bottles will be provided for the the stay......


“Good Stay”


Third trip to the place & feel like coming again & again here. The hotel was hardly occupied so it was more quiet....They had closed the best part of the property for maintenance but we still strolled around the area. Breakfast was very disappointing...Only one option to choose from...Guess due to low number of guests....But overall a pleasant stay &...


More  




“Comfortable, Cozy stay in rain”


When I reached this place it was quiet raining and somehow got an accomadation at this place. It turned out expectation++, had a warm and cozy stay at this place on August 16th 2014. Bit expensive at Rs.3,500/- per night stay. But the complementary breakfast was not upto the standards expected. Place is clean, well maintained swimming pool. Lots of...


More  




“Lovely hotel”


came to India for the first time. were pleasantly surprised! the area is large and beautiful. price for India is not expensive. friendly staff, we had a room overlooking the pool and lake. large balcony. from the room you can watch the concert, which was held in the evenings. This year the winter would fly again in this hotel. I...


More  




“1ST TIME IN GOA”


This has got to be one of the finest resorts in this part of goa a wonderful and lush green lawn and a beautiful pond with paddle boats what more could u ask for on a holiday..as every one even i has my reservation about the resort, but all were laid to rest once we settled down by the pool....


More  




“Worst food and service issues”


Hotel has kind of adequate amenities but issue is they have kind of weird policies. 2 water bottles only provided for long or short trips. I had stayed for 4 days and they raised their hand saying that they will not provide more than 2 water of bottles for whole stay. Whenever we reach for complimentary breakfast quality issue, they...


More  




“Just Average !!!!”


I visited along with my wife on 13th Aug 14 and stayed for 3 days here... Though the property is huge and spread across in a big area, but rooms are just average... no where near a 3 star property which the management claims.. Only two complimentary water bottles will be provided for the the stay, rest you have to...


More  




“Independence Day”


We went to marinha dourada on the independence weekend. The location was awesome great place for fishing enthusiasts. The food was not that great considering if one stays at a place like this will spend a lot if time in the resort and a lot of money in the room service.
The staff was very polite.


“Whattaview! ”


So I went to this place to celebrate my friends birthday during independence day weekend. The moment i entered the resort all I had in my mind was the images that I had seen of this resort on the internet and other travel portals. Buy take a break, this place is much beautiful than on the internet. Me and my...


More  




“4 star campus , 2 star room, 1 star service”


Sad to see a property with a fabulous campus in prime part of Goa decay and move to lows of mediocrity due to its vision less management. First a list of 'No' s at the resort.
1 - No shampoo pouch or anything at your bathroom.
2- No wifi
3 - No remote control for Air Conditioners
4 - No...


More  




“Beauty at the Beach !”


I still relish the morning walk between the 2 large ponds within the resort itself. Its probably the most scenic resort in Arpora and also located at convenience. I was here with family and friends and the experience is unforgettable, I have'nt seen that big Children's play area in a resort as well. To be precise, the key points of...


More  




“Pleasantly Surprised !”


Perfect ! Its a great place to stay at an amazing price. Decent staff and service.
The property is mesmerising , very scenic and aesthetically pleasing . All in all it was a very good experience , i would certainly comeback here !


“Great Location But Very Sad Staff”


I Booked this place as i wanted to go here from quite some time. The location is great and peaceful and the property has two Lakes inside and its very nice view. 2 Swimming pools and shuttle to baga every hour is great. below listed are the things i did not like about this place.
1. They do not provide...


More  




“Loved it”


Great location, about 1km from baga beach, and set beside 2 lakes. The hotel was fantastic. Very reasonable pricing, great food for breakfast in the morning. A shuttle bus service to the beach every hour which was complimentary. A couple of swimming pools.
I'm sure the place would be super for a family holiday, but better still for a romantic...


More  



