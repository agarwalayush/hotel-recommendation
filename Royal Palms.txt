
“Good value for money”


For us it is a second home as we come here every year. Property is at a good location with all basic amenities.Beach is around 2 kms . Kids activities are very well organised and they have a kids area with many games. Staff is efficient.Kitchenette has all got all basic items . highly recommended for people travelling with kids


“Super service”


Betty was extremely helpful and all other staff are co-operative as well. Service is too good at this location unlike Royal club. At Royal club staff are pretty rude and do not help customers much. But at Royal Palms front desk is excellent.


“good property with great amenities”


Hotel has friendly staff who are also very courteous. The hotel is well maintained and the rooms are in good condition. Rooms have a bedroom and a living room with kitchenette. There is also a store that open in the day and sells items at MRP. Next to the hotel you will also find all the things you would need...


More  




“Had poor time@Royal Palm”


On arrival, I was told - "Sorry you do not have a booking with you". Though it was booked in 2 months advance through Hotels.com.
After wasting 1 hour, they could find my booking and finally I got the room.
Secondly, They did not have any staff to collect luggage from taxi and that was kept half an hour with...


More  




“Good for Holiday Homes”


I stayed here six months and was allotted a studio. It is a well compact room with all modern amenities in it. Though the property needs refurbishment the staff makes up for it. The GM here leads by example. It is a home away from home. My wife was very impressed with the hospitality given to her. All the facilities...


More  




“Had Wonderful Time with family@Royal Palms, Goa”


Very Good and well maintained property.
Stayed with family including parents from 6th Jan'15 to 10th Jan'15 in 1 Bedroom apartment with balcony overlooking the swimming pool.
Apartment was very nice, spacious and clean. It included all required amenities along with fully equipped kitchen.
Staff was very warm and made sure that we had a great stay.


“Annual Holiday”


I stayed in three resorts during my annual holiday in Goa. I and my family enjoyed most in Royal Palms . Thank you Neha (GM) for making it memorable . Everything was perfect.Rooms were good,specious and clean. Room services needs improvement. Near to beach and market.Good Luck Neha


“christmas & new year at royal palms”


the general manager & her staff do everything possible to make your stay relaxing and enjoyable
they maintain a frendly homely atmosphere where nothing is to much trouble if you require anything i have already booked for next year as i think this is a great resort


“For truly enjoying Goa “Staying at Royal Palms is incredible“”


For truly enjoying Goa “Staying at Royal Palms is incredible“
This is our 4th visit to Resort during Winter session staying one week from 27th Dec’14 till 3rd Jan’15 in Studio apartment along with my second son and wife, which was given to us with our balcony was overlooking to the swimming pool.
The resort is well spread out, and...


More  




“Nice property at a good location”


I stayed at Royal Palms from 3rd Jan. to 6th Jan. 2015 with my family. This resort is very well maintained & has all the modern amenities in it. First of all i would like to thank its staff (especially Ms. Perry) for the cooperation & support they provided us. Unfortunately 4 days before coming here my daughter met with...


More  




“A return visit”


I stayed at Royal Palms on my first visit to Goa almost 10 years ago. This Christmas I returned to Royal Palms and was amazed at the improvements. The Imperial rooms are really something with full facilities in a very modern setting. The restaurant food is at the usual high standard of Royal Resorts. The restaurant is very pleasant and...


More  




“Holidays spolied”


i dont feel like giving one star also. The staff was non cooperative. We were given free coupons to stay for four days after taking ten thousand as a promotional activity. The customer care followed us like anything saying that its luxury resort. When we went there it was opposite luxury. There was not even a proper blanket. The blanket...


More  




“Extremely pathetic horrible”


I have never come across a resort like this so horribly managed. I was with my small baby. I was given a room on 3rd floor. When called housekeeping for water some lady very rudely said there is water cooler on ground floor and i should collect or fill water from there.Mattresses so uncomfortable that they seem to be some...


More  




“Excellent Stay & good staff”


We were on a Promotional stay at Royal Palms last year, but found very good people in the staff.Kids were very fond of Madan at the activity area or the poolside & totally enjoyed to be there with him.Right from front office to the cleaning staff everybody was always there to help you with a smile.
I would specially like...


More  




“Descent Stay”


Far from beach - almost 1.5 kms, may not be walk-able. Rooms are clean. Although had to live with few unwanted guests like cockroach and bugs. Staffs were good - ready to help. Parking was a issue- probably because I visit during peak season.


“Pleasant stay and experience”


Noce experience staying in this resort, rooms were nice and clean and the staff was efficient as well. Nice facilities within the room to cook for yourself, pretty close to the beach as well with basic shopping stuff available nearby. Surely recommended.


“Nice clean decent rooms”


Part of resort is under renovation. You should request for refurbished rooms or villas which are really good.
kitchenette with enough facilities to make tea, coffee or to cook some food.
Beach is nearby just 10 mnts walking.
lot of restaurants at walking distance.


“Nice and clean room”


Resort facilties are nice.You should ask for a refurbished and pool side facing room.The view from the topmost floor of the pool facing room was amazing.It was refurbished and very clean.There are a lot of small insects in the rooms.But I loved the rooms and hope to visit this resort again.


“A very pleasant and serene holiday at the Royal Palms”


This was my third stay at the Royal Palms and it was much better than my previous stays there. For one, the rooms were nicely refurbished and comfortable. But what made it very pleasant and serene was the marvelous new landscape and flowering plants so well kept and maintained. It was such a pleasure to just sit at the window...


More  




“A fantastic experience!”


We decided to go here after great speculation for my daughter's 2nd bday- and I must say it was worth it! My husband and I went with our 2 adorable daughters- age 2 and the other just 4 months old and the team out there made sure that everything was comfortable.
Since we were with babies, we were given the...


More  




“Stay was good, but restaurant food was a bad experience ”


Royal 👑 palms stay was one of the memorable one, we booked a suite resort, which had a bed room, kitchen, sofacum bed, neat hall......and the ambience was fantastic...... But only restaurant food was not palatable, expensive and service was very very slow and bad, attitude of staff was also bad, and they serve not hot......


“My stay at Royal Palms, Benaulim, Goa”


Studio room was excellent, well equipped, clean. While a lot of construction was going in the complex, it was well maintained. Staff is helpful. Restaurant food is way better compared to all other nearby places. However restaurant staff aren't skilled enough - bungle orders in in-room service. But overall a very nice experience. Compared to sky-high prices elsewhere, it was...


More  




“The Resort Not A Worth”


Dividing the review into two parts,
The first being the stay i.e is the cleanliness of the room and other facilities is good
The second but the most important part that is the food service at the restaurant is disgusting. I waited over an hour for my food and so did the customers around me.


“Life time reminiscences….!”


I was here for 4 days, it was really nice experience. As it was a X-mas ambience was very nice. Pool is clean and in good condition. Most of the people are Indian families as it is time share property. Rooms are very big and very much comfortable. All basic amenities are there, you can cook your own food here...


More  




“Good resort with horible management”


After reading the review of all the reviewers with great effort I had agread to go to this resort on a promotional offer from thier telesales. Till the booking everything was fine. I have stayed in thier Beach Club and Monterio resort and both these stays my family still cherish the timd spent there for everything right from comfort to...


More  




“A wasted property !”


The first impression when you see the property is not so bad, the rooms, the pool, the garden, they all look ok for a family resort. As you start interacting with the staff, the nightmare begins, a resort is basically for a relaxing break for families. The staff do not seem to care about the objective. The interiors are quite...


More  




“Beautiful Hotel”


This is my second visit to royal plams . It was really a nice experience . Rooms are now more beautiful. Staff is very friendly . Room service is good. Every room is now equipped with safe deposit locker. i enjoyed a lot in swimming pool & activites around swimming pool area.My family loved this trip.


“Don't ever think of going here”


First of all, all seems nice and shiny here until you scratch a small bit of skin. The every smiling staffs become un-helping, unwelcoming and in my case they started off a verbal fight. I had a booking of this hotel but they asked me to go some Simxa holiday counter which has few old dirty smelly room and operates...


More  




“Nice place”


We 7families stay here, and we feel like familiar atmosphere and nice cooperative staff.
We specialy thanks to mr.migal fernadiz.
He is the man behind the all good atmosphere.
Also room , pool, and daily activities was very wonderful.
I feel that again again came back here.
Thanks
Thank you.


“Enjoyed a very wonderful holiday in spite of rains.”


We had a very pleasant 5 days from 22 Oct 2014 - 27 Oct 2014. We came from Pune by Goa Express which reaches Madgaon early morning. Thanks to Miguel we had a room ready for us on arrival. Also Betty at front desk whom we had met when we were here last time about 10 yrs ago was as...


More  




“Nice quiant hotel”


I was a little apprehensive before check-in as the earlier reviews are mixed. Frankly i found this hotel quite nice. The blue and white color combination gives it a Mediterranean feel.
I got a 1 BR. It was nice,clean and spacious. I could sea from my balcony. It was on the top floor (4th). Not a problem for me but...


More  




“Royal resorts: royal goan beach club at royal palms”


Wonderful resort to relax unwind and de stress yourself.
Nice clean well maintained club with all facilities available.
Nice clean and good swimming pool, indoor table tennis and gym available.
Rooms well maintained clean with all facilities available for comfortable stay, kitchen equipped with latest induction cooker and all other facilities.
Good friendly and nice staff, restaurant clean and serves...


More  




“Comfort more than Ambience, Great for families with small children”


I recommend this place to all families travelling with small children. Complete kitchen with all the groceries available at the MRP rate, you dont have to worry when your kids prefer playing in the pool during restaurant lunch hours.Tasty and reasonably priced food and excellent service in the restaurant. Friendly helpful staff, each one of them makes a special effort....


More  




“Perfect Resort for Family”


This resort is located in South Goa which quite peaceful palce without crowded. Its a right place for stay.
Resort rooms are quite good and well maintained. They have a kitchen in the room which is very useful for having quick coffee or milk for kids or sandwich etc.
Hotel staff are very friendlier and nice. There is a small...


More  




“Great place and great service”


This place has been revamped recently, and it is really comfortable now. The location is very convenient, just 4-5 km from Margao, making it very easy to get around. The overall service was great, and the games/ contests in the recreation room livened up a rainy holiday. The food was excellent: the chef, Bruno (who was "on loan" from Beach...


More  




“Out of season but PERFECT”


My partner and i stayed here for one week mid september, Goa was mostly still shut for the monsoon season but was slowly coming back to life. The hotel was a range of block type units which were very spacious! Comes with kitchen, TV, A/C all the bells and whistles. Was comfortable and outstanding service from all staff.
I would...


More  




“Family destination resort”


We stayed in this resort in room No 30(6) single bedroom unit from 30thSept-2014 to 4th Oct-2014. The location is outstanding as it is located very close to Benaulim beach( one of the most beautiful ,clean and silent beach). we can hire motor bike or car easily and it’s very convenient with good prices.
The Unit was very spacious and...


More  




“Perfectly Fine Resort by a Quiet Beach”


We're 3 adults staying at their renovated two-bedroom villa. The villa was above expectations. It is furnished well and the kitchenette is equipped excellently.
Free WiFi was available for only 2 devices which is a shame since it's a family villa. But the connection was great.
The location of the resort is very good for Goa. There's a quiet, pretty...


More  




“4rth visit to Goa”


Had a very enjoyable stay and would like to go again next year. I have read reviews of other travellers and are very true. The photographs on the trip advisor website from the owner are very much of real picture in existence.
The surroundings are beautiful, it is very near to the beach and has lot of restaurants near by.


“Excellent location average Resort”


We stayed this resort with 1 kid in a 1 bedroom unit, location of Royal Palm is excellent as its very near to beach and good option of good restaurant and grocery shops, we can hire motor bike or car easily and it’s very convenient with good prices.
About the resort: when we reached there for check in they took...


More  




“At Home in Goa”


Location:The RGBC at Royal Palms has been designed in a colonial style with attractive facades and balconies. Landscaped with lovely gardens that also feature the palm trees that give the resort its name, this resort is near Benaulim beach. Margao city the commercial hub of South Goa is just a 10 minute drive away .
Living arrangements:It features a selection...


More  




“Best holiday Resort”


This is our 9th trip to Royal Palms in Benaulim - goa and every year we have enjoyed our stay thoroughly, with excellent service,co-operation from the staff and pleasant environment. The new G.M Neha has a pleasant personality and positive approach to all the guests and her staff. the front desk manager Mr.Miguel and Betty are ever so co-operative, helpful,...


More  




“Very Good Experience, specially if you are travelling with Kids”


I have got a deal from Groupon and stayed in this property in last week of Aug-14 and it was a very good experience, I really liked the concept, it has kitchen, all the necessary utensils, Fridge, Electric Gas, and Microwave (on Request) , it has small store inside the property from where you can buy grocery items on MRP,...


More  




“Royal Goan Beach Club at Royal Palma Goa.”


We had the opportunity to stay in this Club Resort. As appears from the name it is really a Royal Resort with all modern facilities. The staff is courteous and helpful. Neat at Clean Rooms/ suites with all modern facilities located out of City in South Goa, If any one really wants to enjoy the peace away from crowd and...


More  




“Nice property but not the most courteous staff”


We planned Goa for the third time this July and we decided to go for RGBC @ Royal Palms. To start with, the room wasn't clean, which was disappointing! We had our first meal there and were happy with the food. Benaulim beach is b'ful and serene and very different from the beaches in the North part of Goa. There...


More  




“Decent family retreat”


This is a service apartment style resort. Its well maintained and well staffed. It has a good swimming pool. It also has a basic spa. There is a mini store from where one can pick up basic essentials for cooking and they also have a decent collection of alcoholic beverages and cigarettes. The also store some ready to eat packaged...


More  




“Nice resort for family”


The resort is around 1 km from benaulim beach. if you dont mind walking that much you will like the resort.
Pros:
Rooms are clean and with a fully equipped kitchen.
Pool is big and clean
Ministore within the resort is very handy.
Bike/Car rentals are available opposite to the resort ( DREAM RIDE)
Housekeeping staffs are very good.
Front...


More  




“Excellent stay”


Very good resort. We stayed here for 3 nights and the stay was fantastic. The food was also good and rooms are also big. There are many facilities over there. The recreation center is also nice and I will stay there again


“Nice place to chill out”


We went to the RGBC - Royal Palms during the monsoon. Maybe because they were having some kind of promotion, there were quite a few guests in the property. The property is not brand new. However they are doing renovations, and hopefully it will get a face-lift.
The restaurant and spa are good. So is the swimming pool (not temperature...


More  




“The best ever holiday and dining experience !! *_*”


The place is really calm, beautiful and definitely worth a visit ! :)
The restaurant in there is the best in goa :) that's for sure :)
The staff here are so Co operative and awesome :")
They are simply wow
They Made Me Look Forward To EVERY Meal ! :)
Especially Bruno sir, Deepak, Sunny, and Patrick sir !!...


More  




“Fantastic Holiday”


This was my first trip to Goa. I had booked the Royal Palms at Benaulim from the 17th to the 24th of May'14. Guys we had a great time there. The staff were very helpful, friendly and filled with hospitality. The front office staff were always ready to help. the ambiance of the resort was great. the pool always had...


More  




“Return vacation”


We have recently returned from a second vacation to Goa. Our first week was spent at Royal Palms where we had a great restful break. Accommodation was excellent as was the service.We love Benaulim and everything about it and will be returning as soon as possible.


“Home away from home”


Goa is a wonderful holiday. destination. My husband & me stayed at Royal palms in March 2014 & had a wonderful time. The resort is of international standard and with all the requirement for a peaceful and happy time. There is a pool , a mini supermarket, a gym and a Spa on site and the rooms are very spacious...


More  




“Fantastic first trip to Goa”


The hotel was very clean and tidy. The staff were very helpful and friendly and made you feel very welcome.
We got engaged whilst in Goa and the hotel staff were exceptionally nice in organising our evening meal with flowers and wine.


“Real Holiday Moods”


Stay and FACILITIES are very good. All amenities available, are well thought of .
Real value for money. The rooms are spacious and Comfortable. Kitchen is well equipped with modern facilities. Store contains all essential items.


“Excellent Property...”


we have been goa on these month of may,2014 we have been 16members including child we are 5 Family.. we book Royal Goan Beach Club at Royal Palms from 9May to 16May,2014..
in every room you have kitchen ready made only thing you have to do is to buy food,oil,milk etc etc. dont worry in inner these property you have...


More  




“An extremely disappointing experience with the most impolite staff!”


We bought a deal through Groupon for 4 nights stay for a couple and 2 children. It looked like a deal not to be missed BUT looks are really deceptive! This deal includes stay in a 1 bed room apartment but they don't mention that the hall of the apartment is not air conditioned for people who take such deals....


More  




“Value for money”


Hi, I travelled to Goa this May-2014 and had a fantastic time at this resort. Overall the stay was very good, the staff have a great ability to serve the guests. The front office manager Mr. Miguel Fernandes and his team offers fantastic service quality. I think it is a great value for what we pay.


“Good Stay”


The Royal Goan beach club is an excellent place to stay especially if your have kids around. Every room is equipped with kitchen which is very handy if u have a toddler around.
not a very great location as it is approx 10 minutes walk from the beach nevertheless the stay was worth while, There is a mini store at...


More  




“AWESOME EXPERIENCE”


The Royal Goan Beach Club Was Very Good. I really enjoyed my stay there . Every Room Has a Kithcen. you will feel like other home. Rooms are well airconditioned.Near by market is also good. you can buy any thing. Pool is also big.Child acivities are also good I would like to go back there soon.

