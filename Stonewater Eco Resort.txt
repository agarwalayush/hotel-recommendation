
“Amazingly beautiful. ”


Fantastic location n Seaview. If u r having a vacation near Vasco, dis is d place to be. Restaurant is also too gud with poolside, place to have bonfires n lots more. Is gud for pocket too. Gr8 place for eco lovers.


“expierence the real beauty of goa”


Food which was a real treat for my pallete. Warm an friendly service. Relaxin ambience which served as a well deserved break we were lookin for.surely visit u again to experince d bliss we felt this time. if u r in goan than u have to visit this place to expierence the real beauty of goa.


“GOA ENGG COLLEGE 95-BATCH REUNION AFTER 20 YRS”


This was truly one of the most beautiful resorts to stay along with a group of friends away from the city and work hustles. Lovely wooden cottages and the interiors done up really well.
My stay over there was basically for 3 days and 2 nights, the occasion being our 95-GEC BATCH reunion.
It was really fantastic to meet almost...


More  




“Destination wedding at Stonewater Eco resort Goa”


Having booked all the 25 cottages of StoneWater Eco Resort in November for my Destination Wedding was amongst the finest decisions that I ever made. Right from the perfect eco resort setting to the courteous and attentive staff my experience with the hotel has been outstanding. With a hectic, back to back pre wedding functions and almost 50 very important...


More  




“Good... But frustrating. So easy to make it amazing but management not heeding feedback”


Wonderful location and clean, simple cabins that offered good value. Staffing is poor in that there is barely any, we were never told about the area, the complex or any resort information and we had to research online. Despite it mentioning an onsite spa clearly on the website, this is a £5 return cab to the local town and is...


More  




“Great views, nice rooms, meh service”


We spent 2 nights at the resort. It's a nice place but has seen better days. The cabins were clean and comfortable. There's not much to do there apart from enjoying the views and the pool.
After exploring the grounds we did see a great spot with a table setup with a fantastic view of the sea. It would have...


More  




“Simply amazing!!”


This was truly one of the most beautiful resorts I've been to. The private beach they have is very different and pretty. And the resort grounds too..way over my expectations! Lovely wooden cottages and the interiors were done up really well. Loved the bathrooms as well which had a bath tub. Going back there again in Feb with my family!


“Quiet and peaceful place....”


Nice quiet place to relax and very peaceful place. There are wooden chat lets with direct sea view. Great view of the sea as well as the wooden cabins. They have swimming pools and as the name suggests a stony beach. The wooden rooms were nice and we enjoyed every moment of our stay. Food at the restaurant was tasty....


More  




“Simply stunning..”


Cant wait to get back to this naturistic hotel with spacious wooden cottages had a magical time with my fiancee.. Good food n spacious swimming pool great Staff.. beautiful location.. Amazing memories attached


“Calm n subtle...”


Amidst a rocky waters situated is this lovely n quiet beautiful resort.
It is Hard to locate n find it's location if u r coming on your own.
Directions- Ask for bogmalo beach.. It's about 4 km before you reach bogmalo beach on the left side opposite a bus stop. There's a small signboard of Stonewater resort which may be...


More  




“Amazing.............”


Pros: Super stay for family/couple. Nice cosy wooden cottages, spacious bathrooms with most required amenities. Resort location is awesome away from hustle of Calangute and Baga.
Cons: Choice/availability of food is limited within the resort. No nearby eating joints.
Advise to have a vehicle to move around.
Overall, amazing experience and one of the best property in Goa.


“Amazing, Cozy place away from the city bustle”


Stayed at the place for two/3 nights.
Its well maintained, clean and helpful staff.
Breakfast is alright but the good thing is they also deliver it in the room if wanted.
There are 2 pools in the place. One is a small infinity pool over looking the sea which is very pleasant.
The sea side is also at a walking...


More  




“Great views”


This place is set away from the party scene and is very beautiful and peaceful. Lovely pool
Over looking the ocean and very cute and cosy but well AC'd cabins to stay in with great views of the sea. The good wasnt the best and the restaurant atmosphere was poor but we weren't there in the height of the season...


More  




“Best resort with an excellent view”


If anyone is looking for a quiet and peaceful place in Goa then Stonewater eco resort is the place, it has got amazing wooden cottages with excellent service, very friendly staff and great food at its restaurant. This resort is away from the busy city. I would recommend this resort to all who plan to come to Goa for a...


More  




“Deceptive prices. Beware of booking it online”


We booked through booking.com. We recieved a confirmation at the rate of INR 4500 per night. When we called up the hotel they refused to oblige the booking and said they will charge INR 8500 per night!
The hotel staff is very arrogant. They can dupe anybody through fradulent rates online. Please be careful while dealing with this hotel.


“Our destination wedding....”


When I was researching for a place to have our destination wedding, Stonewater was the place I found to be the perfect venue. Every arrangement was taken care of and we had an indeed memorable and fulfilling experience...breath taking view from all the villas and we had an amazing time....Staff members were very hospitable and they provided service with a...


More  




“Excellent property for a Conference as well as a personal get-away!”


This is a truly amazing property hidden away in Vasco near the Bogmallo Beach! The property itself is magnificent in its design, location, view, accommodation and so is the staff who are warm and efficient as well as the food which is wonderful. We were there in a group of 20 for a company conference and it was an amazing...


More  




“An interesting experience”


A very interesting getaway place, smartly managed and tastefully done up. Very cordial and hospitable crew. To begin with, the setting of the location itself is quite an impression, and then the pleasure in being in that place is easily sustained and enhanced by the way the place is done up, and then the hospitality of the Management. Suneet Ahuja


“Beautiful property”


This has to be one of the best non beach resorts in Goa. In saying that, I mean the resort doesn't have an actual sandy beach, but is located on the rocky edge of Bogmalo beach. Trust me, the whole property is amazing. The view of the calm, peaceful cove, and of the whole Colva benaulim white sands far off...


More  




“Good location, Cozy cottages, slow service”


We were at stone water for just a day and this review reflects our experience during this short stay.
Location and cottages:
The location is just Amazing. The resort is built on a downhill that faces sea. There are levels created on the downhill and a group of cottages exist on each of these levels. The sea shore is a...


More  




“WOW!!”


This resort is simply gorgeous, the log cabins camouflage themselves so beautifully amongst the lush green landscape, i can safely say that this venue is one of the best romantic resorts in india.the cabins are sooo cozy but the view from them is like no other. The view of the ocean from the cabin is just breath-taking.im gonna return to...


More  




“Romantic is the word !!!”


A perfect place for honeymoon. If you are looking for a peaceful stay, get to this place and inhale the fresh air. Beautiful landscape with breathtaking view of sea and lush green surroundings. Very good house keeping. Cleanliness was good. Don't miss this place in Goa.


“Lovely property; beautiful views; well priced”


Stonewater is a beautifully landscaped resort with some of the most magnificent views I have seen at a beach property. It's located very close to the airport (~5 mins drive), but is thankfully bereft of the associated noisiness somehow. The rooms are of a comfortable size, and done up extremely tastefully, and each comes with its own viewing deck. The...


More  




“Treat for the 4 senses”


Feast your eyes on the view, inhale the really fresh air, tranquility for your city-beaten ears. Well, the fourth sense is touch and this resort touches you in various ways. The landscaping is gorgeous, the log cabins are so authentic. It's almost like a beautiful but abandoned castle.
The castle metaphor should help. Staff are scarce, service is slow and...


More  




“come for the view”


had been there for the goaq project, they managed the event wit 200 ppl rather well. what would strike first is the breath taking view of the sea. I loved the landscape and the privacy. The cabins are comfortable, looking forward to visit again.


“Dream Venue”


Landscaped gardens, perfectly manicured lawns, chirping birds, TWO pools, lovely wooden cabins and beautiful sea views. Its a absolutely a dream venue for me. Was there 7-8 Feb, 2014 and the calm was so palpable, inspite of our conference going on, i could hear chirping birds and and the speaker clearly in the distance from my room.
The property is...


More  




“Beautiful to the core”


I did not really stay here but spent the better part of 2 days here as I was attending the weekend. The property is located slightly remotely and is very beautiful.You will be besotted by the view of the sea, the cottages and way this resort looks the moment you will step in. The food served was also quite nice...


More  




“Anything for the view!”


The first thing that you will see, as you reach and enter the place, is the view of the beach, with a neat, white boat floating around aimlessly!
The lush greenery, and clean winding roads that take you to the different parts of the resort are a sight too, and instill a sense of tranquility and calmness in you... I...


More  




“Wedding Bliss”


I attended a friends wedding at this resort and actually stayed in Bogmallo Beach resort as all the cabins here were full. However this was a similar price but much better the cabins were very nice and were set in beautiful tropical gardens. The wedding ceremony was held there in the jetty area and so were the reception and other...


More  




“The food is pathetic and overpriced.”


I stayed here for two nights. The log cabin is cute and new but it was a struggle to shut the front door. Also there is no menu in the rooms and anyway the food is yuk and500 rs for toast eggs puri bhaji corn flakes and poha is way over the top. Besides the rest staff is very take...


More  




“Picture Perfect”


Stonewater is the perfect place if you're looking to celebrate something in goa. The resort seamlessly blends with the best of nature to provide you with a stunning and unparalleled holiday. We were an assorted group of 10, friends and family.. And we couldn't have asked for a better venue. The log cabins are semi luxurious and comfortable, the view...


More  




“A Reunion to remember”


We celebrated our school reunion (20years) at this resort and had a great time. The surrounding is serene and lush green.Very hospitable staff. I would definitely come here again with my better half next time


“A great experience”


We had booked the entire resort for a wedding celebration. The resort is beautiful. Each and every staff member did their best to ensure a very smooth stay. A special mention of Swetha and her team who were totally involved in taking care of the guests. Filu and her team from house keeping, were always smiling and did an excellent...


More  




“Our Fairy tale Wedding!”


We chose to get married at Stonewater Eco Resort, Goa because it was nothing short of a magical venue for a Fairy-Tale wedding that we wanted. It was an ideal location tucked away in the South of Goa that 99% of our friends had not even heard about. A resort by the waterfront (Arabian sea) with your own private jetty...


More  




“COOL CALM & LOVELY”


i have been n stayed in this resort it has nice rooms all made of wood from flooring to everything except the things in bathroom, one can get good view of arabian sea and see the south goa full costal line if weather is in your faver. its a nice place to spend over night with family n friends n...


More  




“In love with this place..”


We were planning a trip to Goa for our 1st wedding anniversary and Stonewater Eco Resort was one of the options as my best friend who got married here had highly recommended this place.
We got in touch with the reservation and spoke to shweta who took care of our reservations. We were highly impressed with the amenities offered and...


More  




“Awesomness..”


What can i say about this place, it is a superb place to be at. I was very lucky to be there as my sister exchanged vows there, calm,serene,beautiful the place has such positivity to it unbelievable.
The host there a very lovely young lady who is doing a great job.
The staff is very helpful and will make sure...


More  




“Best resort in Goa”


We were at the resort for few nights, excellent views from the rooms. Excellent service , staff was friendly. If you love sea food ..thats the place. Resort has a personal beach which was an extra benefit. Close to airport (10 mins) therfore don't have to travel long distance after flight. I highly recommend this resort to Goa travellers.


“A nice resort to relax!”


Stayed at Stonewater Eco Resort in the first week of August 2013. We were a group of around 25 people. I was pleasantly surprised when I reached the resort. I was not expecting the wonderful view it presents. My room provided a partial view of the sea. The room was on a bit smaller size but neatly done. The washbasin...


More  




“Wonderful”


We had a reunion of our schoolmates over the last weekend. The arrangements were perfect, and the management kept every word of the promises they had made. Every cottage has been ensured a sea-view. The menu is vast, and the food is yum!!! The staff is very friendly and enthusiastic. They had also given us a menu of the various...


More  




“Resort with a nice view - the service offered left much to be desired”


Last year we had a terrific holiday @ Wildernest Goa. After such a mind blowing experience the benchmark of service we expected out of any place at the same price range had increased considerably. This year when we were searching for a weekend gateway for the monsoons we wanted some eco resort near to Pune. The eco resorts at Goa...


More  




“Bliss !”


This place is just simply amazing :) it has just the right view , food , luxury and a beautiful host . I have definitely been shimmered with a pleasant experience ! I would recommend this place to all the locals and tourist as it truly is heaven on earth !


“StoneWater... Amazin plc to b!!”


Initially wen I made d bookings, continously bck of my mind, I was thinkin like did I made d right deal, or was it a bit overpriced? But as soon as I along wid a huge family of mine for a special occasion, checked in, v all were like...WOW... The rooms are beautiful, n d view is amazin. Location of...


More  




“very good location and view with excellent service and hospitality.”


We were 24 people, were happy with the service and enjoyed the pure veg.food. good
experience and waiting to visit the same place very soon. we went for the marriage function and to
manage with all the person and look after every one was difficult without the help of such a lovely
staff of stonewater eco resort. even the owner...


More  




“Not happy with the hotel”


I had reserved a room at Stonewater from 21st to 26th May 2012 and paid an advance of Rs.20,000/-. On arrival i.e. around 1 p.m., I realised there was no phone in the room, the hand shower was not working and they were unable to repair the same, neither did they give me another room. The lunch was good. However,...


More  




“lovely stay @stonewater!!!”


Stonewater resort is a perfect place to spend a lovely time with our friends and family.....and the ambience is excellent!!! Enjoyed our stay :-)
Regards: Regan & Curie Rodrigues.


“Stunning View”


My wife and me spent a night here and were completely destressed at the end of our stay. The wooden cottages are very cosy. Great landscapping. Free kayaking is a bonus. Best of all though is the stunning location of the place and the view from the cottages which is breathtaking. The only negative is the restaraunt, where the service...


More  




“Superb blend of nature and luxury”


Spent 2 nights here and loved every bit of it. There are 17 wooden cottages comprising this beautiful resort. It is on the way to Bogmollo beach. You have to take a left turn which is marked by a small sign.
The resort is spread over a slope reaching down to the sea where the cottages are spread out amidst...


More  



