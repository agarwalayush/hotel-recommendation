
“Sunset Beach Hotel Candolim”


On arrival no water to drink the room we were allocated had a terrible smell. Dilapidated fridge in room froze the water we had brought with us. No glasses to drink out of. The bathroom was disgusting the room was shabby. The pool area was a disgrace as were the grounds. I would not recommend this hotel to anyone.


“Good hotel”


Stayed here for 28 nights over Christmas and new year and found the place to be good. The rooms are cleaned daily and towels are changed every other day. The place needs a bit of TLC however this adds to the charm of the place. Location is ideal for both the beach and the main road. We paid extra for...


More  




“enjoyable stay at the sunset beach candolim”


we arrived 18th dec and stayed untill 24th dec 2014,
the room we had was spacious and had a ceiling fan and air con,these are a must in goa.
the room was cleaned daily and linen and towels changed every other day.it had a full size fridge and in room safe and sat tv.
the air con,safe and tv are...


More  




“fantastic family christmas holiday”


We arrived around 3am, We got shown to our rooms, It would have been nice to have had a bottle of water in the fridge to drink after a long day travelling but had to go to bed very thirsty and wait until morning, The pool area is ok but some wobbly paving slabs that need sorting out, shady most...


More  




“Happy holiday”


Stayed at the sunset beach for the second time from November into December and again had a great time. The rooms are spacious and cleaned daily. The location is great and the staff are all really friendly and helpful and can't do enough for you. Nelish also known as Neil never seems to go home and remembers everybody's drink. I...


More  




“Great base whilst staying in Goa.”


Me and a group of 11 others stayed here in November.
The hotel is clean and a 2 minute walk from the beach. (For people who are familiar with Goa, it is near to the Calamari Shack).
We stayed in Goa last year in the Silver Sands Holiday Village on Beach Road. We found that the Sunset Beach Hotel was...


More  




“Very Disappointing”


We have just returned to the UK having spent two weeks at the Sunset Beach. This will be the first time I have ever had to write a bad review on Tripadvisor. My husband and I stayed at the Sunset Beach for three weeks back in 2012. At that time we had a ground floor room 116 facing the pool....


More  




“A good basic hotel...”


If you have never been to Goa before .. north Goa is very different from south Goa, NORTH goa where sunset beach hotel is , is much busier , more going on...which i prefer .. the hotel is 2 mins walk from the beach, or the street..if you want to party a bit then head for Anjuna instead, i wanted...


More  




“Best ever place in Goa....”


It was a great stay over there, visited there in my Honeymoon....great place, very peaceful, and very supportive staff.
will visit again & again when ever i am in Goa.
Rooms are very clean & spacious.
regards, Sunil


“BRILLIANT”


Had a fantastic 2 weeks stay at the sunset beach, staff were fantastic couldn't do enough for you, very friendly and nothing was to much, hotel was nice and clean but could do with a little update, but this was my second time in goa and this was better hotel than last one we stayed in, beach was only 5...


More  




“Good property”


We stayed for 04 days. Location of hotel is really good; one way is main road & other way is the beach(2 mins walk). Staff put lots of emphasis on cleaning & that's the best part about this hotel . Staff is very supportive in terms of advising on visit locations etc.
In-house restaurant Bistro is one of the best...


More  




“Ok....but wouldn't stay again”


Stayed at Sunset Beach Feb 14 for two weeks.
I only have one negative about our stay so will get that out of the way first. We arrived at our hotel at 3am absolutely exhausted, as there was 14 in our party it took a while to check us all in. I asked the receptionist if I could have a...


More  




“Good base”


we had 2 weeks here and although we had a few nights elsewhere,mInfelt our property was nice and safe.
the hotel is in a great location, with the road leading right to the beach and tourist shops 30 secs away.
The breakfast was basic, only the toast & drinks were complimentary and the toast was a bit hard but you...


More  




“best place i have stayed in goa”


just returned from this hotel and would not hesitate staying again, from the cleaners to the management they were a pleasure to be with, these guys will do anything to help you,advice where to go and how to get there, local foods what to eat and what not to eat, local remedies, the food the service was out of this...


More  




“First timers in Goa”


We visited Candolim in Feb 2014, and had a really great holiday. Sunset Beach is an ideal location, close to beach, shops and restaurants.
Room was big and roomy with a fridge and air con included. Sheets and towels changed and room cleaned daily. What more could you ask for.
Staff were friendly, courteous and could not do enough for...


More  




“Basic,clean, lovely staff”


This was our first trip to Goa, I must admit when we first got to our room which was 111, I was a bit shocked to see in one of the patio doors a pane of glass missing and a piece of cardboard in it's place, we decided not to complain we just looked at one another and said, "well...


More  




“First time in Goa”


This hotel was a lovely surprise. All staff were brilliant and the rooms were cleaned daily. Fridges are now free and i wouldnt bother with aircon till youve stayed there a night or two....we had it but found we didnt really need it as the fan was enough.
Although we loved our room on the 1st floor i would have...


More  




“1st stay here ”


Stayed in other hotels in candolim and this is the best one yet
For goa this is a very good hotel clean rooms , comfy beds
Good food toast for breakfast but really cheap if you want anything else
Friendly staff . They are very helpful the rep is very good as well
The only downside is on a night...


More  




“Lovely Christmas stay at the sunset beach”


The sunset beach is a friendly, clean hotel with great staff and close to all the action in Candolim.
The staff are friendly and polite and we had no problems at all in dealing with them, they couldn't do enough for us during our stay. (Christmas and new year 2013)
The rooms are basic, we had a room at the...


More  




“Great Hotel”


I stayed here the 1st 2 weeks of December 2013.This is my 7th visit to Goa. However this was my 1st time at Sunset beach. This hotel is the best I have stayed in. Your rooms are cleaned everyday and the towels changed every other day. The rooms were spotless and the cleaning was done to a very high standard....


More  




“Holiday”


Stayed here 4 times now it only gets better staff so friendly nothing is a problem .great place to stay beach just down the road perfect location . Clean and tidy . Food is well nice and always served with a smile .
Roll on arch so we can return and have another great holiday .


“One of the best hotels in Candolim”


Stayed at the Sunset 30/11 for 2 weeks and having visited 5 hotels in Candolim this was the best. Room on the ground floor was dark and next time I will take a room without a balcony room 131 as very bright and airy and had a sea view and fantastic view of the sunset.
Paid for TV however the...


More  




“Our 3 stay at Sunset Beach”


We stayed at sunset beach 7-22 November 2013. This was our 3rd time there. Staff are very friendly and make you feel very welcome. We have ate in the bistro several times, very good and not too expensive. Candolim is a lovely village, the locals are so friendly, we have been going back there for 10years. Every time we go...


More  




“2 minutes from beach and street”


we stayed here from the 24th nov to the 8 dec 2013 and really enjoyed our stay. the room was basic but spotless, it was cleaned daily and bedding was changed every 2nd day. only a couple of minutes walk to either the beach or the main street with taxi office across street and enough eating places around to eat...


More  




“another nice stay at sunset beach”


my husband and myself stayed at this hotel from 6th to 21st november we had a lovely time here .the staff are very nice and very accomadating.our room was 107 on the ground floor it was very clean.we also ate at the bistro twice food was excellent.my only gripe is when you look on the website it states rooms have...


More  




“Our 2nd stay at Sunset Beach Hotel in Candolim”


We've just returned today from our second visit to Candolim and The Sunset Beach hotel. Again we had an absolutely brilliant time in Candolim. There's so much to do here, so many fab restaurants, bars and shacks on the beach. Last year I put a really good review on Tripadvisor for this hotel. This year was again lovely, but we...


More  




“very pleasant stay”


visited in November 2013 during Diwali. twin-bedded room with balcony on 1st floor overlooking pool was spacious and clean with good air con, ceiling fan, refrigerator, tv with multiple cable channels, updated bathroom. staff were pleasant and efficient. free coffee and toast in the morning. good location in candolim, with short walk to the beach and tons of restaurants and...


More  




“basic clean and tidy”


stayed in november 2012 with friends yes would reccomend pool nice and clean dont buy from tonys the jewellers next door will be ripped offbistro food was excellant breakfast not very good try to book rooms around the pool the rest of the buildind is dark


“holiday 2012”


Have holiday in north goa for many years and found the sunset beach one of the best. Stayed in november 2012. The staff were great ,the food great,try the bistro The rooms very clean and nice.In a good location near to beach and main road. Loved it.


“Fantastic, Yet Again”


Another very short and sweet review of this hotel, we stayed for the 3rd time at Sunset Beach in January and February 2013, and have booked again for January and February 2014.
The hotel is clean, basic, in a fantastic location, and is one which sells out very quickly.
The rooms are very spacious, always clean, yes they could all...


More  




“Never fails”


First 2 days back in the UK and wish i was back in Goa.
We stayed at the Sunset beach hotel a few years ago and tried it again, the staff are still as good as ever, the bistro is fab, the hotel rooms are cleaned daily. This time around the fridge is included which is a good thing.
One...


More  




“second time here very clean and lovely”


our second time at sunset beach.feb 21st to march the 8th 2013.. hotel very clean, and staff very helpful.. we didnt take the air conditioning as we felt the over head fan was enough..i had our laundry done at the hotel,for 30rs a item they came back washed and pressed fantastic.. the breakfast is juice tea or coffee with toast...


More  




“Lovely time at Sunset Beach... again”


This was my second time to Goa and Sunset Beach, we had room 119 (Third Block, 2nd Floor) - Arriving in the middle of the night, the night porter Sunjay and an accomplice carried our bags to the room, and the air-con remote was given to us aswell (Good job - it was boiling!)
As there was 3 of us,...


More  




“Unforgettable Goa”


Stayed at the Sunset Beach Resort for two weeks commencing 28/Feb/13 with a group of friends, This was the first time in India for all of us. After a 10 hour flight, 2 hours to clear immigration and 1 hour transfer, we were all a little fraught and not helped by the night porters refusal to give us the remote...


More  




“small hotel great location, staff very helpful(ALL)”


I can honestly say this is a great hotel, a little bit tired but very clean, towels changed very day bedding changed every other day.The only negative is breakfast,tea/ toast/marg/jam. 5 minutes to the beach turn right out of hotel follow the road,, pass the over priced trinket/clothes/towel stalls. The towels are that bad the dye comes off on your...


More  




“Good budget hotel”


We stayed at this hotel for 2 nights after our booking at another hotel got cancelled. The is very conveniently located near the beach. The rooms are spacious and clean. The only draw back of this hotel is there is no room service. There is a restaurant in the hotel with almost no vegetarian options.
The hotel owner is a...


More  




“Great hotel, great location, lovely staff, great value!”


We spent 3 weeks at Sunset Beach hotel in November and absolutely loved it! The hotel is in a great location, turn left and in a couple of mins you're on the fab beach, turn right and in a couple of mins you're on the way to bars, shops and restaurants.
The hotel itself is lovely, small and friendly, clean...


More  




“Xmas at sunset beach”


My husband and I stayed here 18th dec to 8th jan, the room was lovely, 18inch comfortable mattress, no 1 priority for us
Breakfast was a bit disappointing, juice coffee and toast(very hard) you will need to ask for it soft or it will cut your gums. You can order other items but you have to pay extra for them,...


More  




“Great Value for money, Great location”


This hotel was in a very good location, 5 mins from beach and 2 mins from the main road where all the shops and restaurants are. The staff are friendly and make you feel welcome.
We didn't use the pool as we are beach people.
The rooms are adequate and have air con ( but i think you may need...


More  




“Stay at Sunset Beach Hotel”


Stayed for 2 weeks December 2012. The Hotel is clean and comfortable. Rooms were cleaned every day with fresh bed linen and towels. Beds are quite hard which is common in Goa.
The pool is very small and is in the shade but the beach is five minutes away. There is a 10 percent surcharge on bar bills but we...


More  




“Fabulous three weeks at the Sunset Beach Hotel”


From the moment we arrived at this Hotel there was nothing too much trouble for any of the staff.
The breakfasts of juice, toast, tea or coffee (complimentary) was served on the terrace in the morning sun. There was a comprehensive list of breakfast choices to order and pay for which were all very reasonably priced. The Manager was friendly...


More  




“Love this hotel”


We stayed at Sunset 2007, 2008 & 2009. I found the hotel to be basic but clean (much better than the place we stayed on our first visit in 2006). The room was always clean, we never stayed around the hotel during the day as the beach is a 5 minute walk, so cannot comment on pool. The breakfast is...


More  




“Basic + Fantastic”


We've stayed at the Sunset Beach Hotel twice now, and have booked to return in January 2013.
The rooms are basic, clean, and nothing is too much trouble. If you're staying in the North of Goa, and aren't spending a fortune, then you aren't going to get the 6 star style hotels of Kuala Lumpur.
Location wise, for myself and...


More  




“a spruce up wud be good”


stayed in The Sunset last year, found the staff friendly and the owner is a really nice guy always got time to come and have a chat with you, the hotel itself is nice but it could do with a makeover its a bit tired looking, didnt use the pool as its quite tucked away and not very inviting but...


More  




“Good again”


Just returned from our 18th visit to Candolim staying at the Sunset Beach again We arrived late night and got a cheery welcome from all the staff, once again as in previous visits we were delighted with the room it was spotlessly clean and everything was in working order.
We met up with some friends we met on previous visits...


More  




“Tired Hotel”


Just got back from Candolim. This hotel is good but is now looking really tired now. The eating and bar area needs new lighting for the evening it looks dull and very uninviting. The bar/waiting staff could do with a carisma boost also. Reception and cleaning staff are great. The whole place needs a lick of paint and some needed...


More  




“so relaxing”


well just got back from Sunset and can say had a lovely relaxing time, the staff we very very helpful could not do enough for you, the breakfast was just enough for us who do not like to stuff our faces. Our room was cleaned everyday along with fresh towels which smelt lovely, two guys who were the ground staff...


More  




“nice hotel”


we have just returned from this hotel,our third visit,we really cant fault the place,good sized rooms,good wet room for your shower ,only two minutes to the beach,good bar area,great staff that will help you all they can,what more could you want.The only thing i dissagree with is having to pay for the television,but then i only watch the news whilst...


More  




“Mostly Great”


We stayed at Sunset Beach for 3 weeks in January, and found it mostly great. The beds are fantastic, especially compared to other places in India, and the solar system ensures there's always enough hot water. The staff are friendly, if occasionally a little 'creative' when adding service charges to bills. As others have said, there's no consistency. But that's...


More  




“Will be back”


Just returned from a fantastic 2 weeks - lovely rooms very large - lovely clean pool - old hotel but very well maintained All the staff were very friendly - Our flight home was 01.20 and we asked if we could keep the room - for only £10 per room we kept ours until 2130 until our taxi picked us...


More  




“Fantastic First time in Goa”


I was recommended this hotel by my sister who has stayed here 13 times!
The location is perfect for the beach, a five minute walk away.
The rooms are basic but ample for what you need. Your room is cleaned every day and fresh towels are provided daily. The beds are king size and very comfortable. Plenty of hot water...


More  




“SUNSET BEACH CANDOLIM”


Just returned from a 3 weeks holiday at the above hotel ,this was our 11th holiday in goa 1st time at the sunset stayed at other hotels and apartments found the sunset very good jus t a short stroll to the beach and very handy to get to the rest of candolim we found the staff very friendly and helpful...


More  




“Sunset Beach Hotel,Candolim,Goa”


On arrival booking in was dreadfully slow.Our room was large,clean and had 2 balcony's.The fridge was locked,so we had to pay 1400 rp for 2 weeks to use it (approx £18) we thought that was a bit of a rip off.We also had to pay for the safe,which worked out at 600 rp (approx £8).Our friends had to pay for...


More  




“Fabulous”


Went to Sunset Beach for xmas and new year, nice clean hotel, small and friendly. Beach is just at the end of the road and there is a great bar across called Toss & Grill where the lads are friendly and food is excellent. Have just booked again for next xmas same hotel, can't wait. Good tip for Goa, I...


More  




“Another friendly welcome”


Stayed here 2 years ago and the staff were great to my wife while I was hospitalised. Went again first 2 weeks December 2011 and the staff recognised us as we were waiting to check in. As usual rooms were clean and the staff friendly and Le Bistro restaurant connected to the hotel served excellent food


“Comfortable stay... lovely place!”


My family and I shifted hastily to this hotel after an online deal with hotel went bad. We just walked into Sunset beach hotel and booked a room right away. First off, we got a good deal on the place, since they were still getting the place ready for the season.
The rooms were clean and most importantly the bathroom...


More  




“lovely”


first time visit to goa and was recommended this hotel i did read the reviews on this hotel but to be honest i never really take them for face value. Arrived there at 3am and was shown to our room we was a bit tired so we basically just fell into bed, the bed was huge and very comfy best...


More  




“Brilliant, couple of niggles though!”


We stayed in room 104, which had 2 balconies- one overlooking the beach road with a slight sea view and one overlooking the restaurant next door and some other balconies. The room was larger than I imagined with a large wardrobe/cupboard/tv stand. Plenty of hangers! There was a fridge which cost 300 rupees a week, you could also buy boxes...


More  




“BEST holiday ever!!”


I went with my boyfriend who had been before and stayed at this hotel previously -he had absolutely loved it and wanted to go back. I have never been to India before and was quite unsure what i would make of it when we booked to go i.e concerned about cleanliness, feeling out of my depth! How wrong i was........


More  




“Small and extremely friendly”


For our second trip to Goa we chose the Sunset Beach and so glad we did. Just remember this is India and standards are not the same as Britain. However, the rooms were a good size and were cleaned every day with a change of towels. The staff are very friendly and always smiling and there to help. We paid...


More  



