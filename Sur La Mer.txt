
“Among the Best Food in Goa”


On a recent trip to Goa, while we didnt stay here, we spent 2 long and memorable evenings at this property and loved it.
Sur La Mer has perhaps one the finest kitchens in Goa. I say kitchen and not restaurant because it does not have a designated restaurant area on the premises and uses the lawn and pool area...


More  




“Nice rooms”


The rooms were very nice, and large as well. Service was very good. We were there with our young kids, and the loud music & karaoke was somewhat annoying.
Food was just OK and by the 4th day, I was sick of the breakfast.


“Shabby Chic Sanctuary in Goa”


Sur La Mer is a lovely, intimate, charming, pretty place to stay. Other guests are friendly, as is the very hospitable owner Aneel & his wife. A family atmosphere, you can do as you please, no rules & regulations which was a refreshing change from most other hotels around the world. The central swimming pool is fantastic, can get some...


More  




“Short trip to Goa”


While on a business trip I had the chance to visit Goa for a weekend. Just arrived, I instantly felt home. The architecture of the hotel, the very warm welcome and the openess of Raju, Nayan and all their friends was a real pleasure.
Best wishes for Raju, Nayan and their team for future!


“The guys in 'interstellar' should have discovered Sur la Mer as the new planet”


It is outstanding and amazing.... Anil alias Raju is the last of the Mohicans I have come across in creating boutique hospitality experiences.... The place calls you back!
The good part...
The owner and the staff are true rock stars
Amazing and prompt service
Good food, great breakfast.
Walking distance from the beach... 5 mins to morjim beach, which is...


More  




“amazing experience and warmest hospitality”


This hotel has relax written all over it ! Great place with an amazing vibe, beautiful decor and the yummiest prawn curry. The hospitality has been one of the warmest we experienced ! We truly enjoyed our 4 days at the hotel. The rooms are large and spacious and have charming decor and the food is to die for!


“Will make me go back to Goa earlier than I thought”


Raju and team is doing an amazing job there. This is the kind of address you want to come back to. Food is one of the best in India and fish are well prepared with unique creation of Raju. At the end you feel like staying to the hotel.
Don't miss it place
Laurent


“Quaint and quirky”


This hotel is a nice little oasis of its own in Morjim. The things going for it were as follows:
1) Food - Every item that was served to us over the 4 days was excellent. The chef was a master across cuisines, and I dont remember eating such good food in Goa anywhere
2) Service: Gets full marks for...


More  




“nice hotel”


The room, public areas and all facilities are very well maintained
Staff are very helpful and friendly. Generalyy, excellent service levels.
Transport arrangements to/from hotel were very efficient
Food range was varied with a wide selection available from the buffe


“Chic not Shabby”


I planned to stay at Sur Le Mar for a few days last January and ended up staying for 2 weeks.
The rooms and facility's are on par with a European 4 star hotel but with a charming mix of local culture.
The staff are super helpfull, completely invisable but instantly there at a ring of a bell and the...


More  




“Muchas gracias Raju!”


You know you’ve had a great holiday when you come back home feeling desolate. Sur La Mer isn’t your usual holiday resort. It’s like home. A comfortable haven. A place where you make friends and have experiences you want to remember for a long time to come.
In terms of the infrastructure, you have everything you need for your creature...


More  




“I miss my family at Sur La Mer !!!”


My season is over right and I’m away from Goa. I miss my Goa family… right on the top of the my list is, The General -Raju, his lovely wife Nayan and their 2 super doggies- Marlin and Sunday- Woof Woof !!
Sur La Mer is a gorgeous paradise tucked away in a private corner at the foothills of Asvem...


More  




“Great Memories”


We visited Goa in 2013 (end of November and December) and stayed in Sur La Mer
Great memories, very friendly owner and all personnel. Food was good and we have overall very pleasant experience. Was nice to get some understanding of Indian people and their character through communication with owner, his wife, staff and guests in Sur La Mer.
Would...


More  




“One place that continues to hold its magic in Goa”


This was our second visit to goa in 5 years and we were drawn back to Sur La Mer. This is a cosy resort on the Ashwem road. The resort is about 500 mtrs away from the beach. The beach itself is very thinly populated and a great place to be. But theres a catch. Once you enter Sur La...


More  




“Beautiful, relaxing, rustic luxury in the heart of Morjim!”


I've been staying at Sur La Mer for the last 6 years and every stay has been as memorable as the first. For me, it is the only place to stay in Morjim. A beautiful boutique hotel built like a Spanish Villa with great food, great staff and the owners Rajoo and Nayan are absolutely delightful. Being in the heart...


More  




“Great Hotel in Goa”


Sur La Mer is a wonderful place to stay in Morjim .My wife and i chose to book 3 nights after reading good reviews on trip advisor and we ended up staying 10 days it was so great Raju & Nayan(the owners) are the best hosts you could ever dream of !!!!The staff are really great and very helpful and...


More  




“Highly recommend ! :)”


Sur La Mer has great ambience- beautiful pool and rooms (I personally prefer the ones on ground floor) and amazing, very personalised service.
Its the only place I stay at when I visit Goa- highly recommend.


“Peaceful and worth to get away from crowd....”


The hotel ambiance is peaceful and typical Spanish style villa.The pool view is worth to enjoy. The menu had everything and good. The suites are worth booking and staff are very assisting. Must add on your next upcoming trip...parking is bit of a problem and bit narrow road but overall the trip is worth..


“Welcoming and special”


I stayed at Sur Le Mer with a close friend and we had the best time.
The hotel decor is a great mix of rustic chic, a lot of thought has been put into making the surrounding special and comfortable. Whether it was having chai at the poolside or a drink at the host's bar in the evening, the atmosphere...


More  




“Amazing place that keeps on calling us back”


This has been our fourth stay at Sur La Mer. And each time we come back, we get more and more impressed. Aneel is an amazing host and leaves no stone unturned to take care of his guests. He personally steps into the kitchen to make some of his exquisite cuisine. The food is outstanding - sometimes it takes a...


More  




“A slice of awesomeness in a magnificent setting!”


While Sur La Mer, and it's fantastic food, and peaceful setting tucked away off Aswem, has always been a place we visited for various meals across our many years of visiting Goa; this was the first time we actually stayed there, and such a homely experience it was that i felt almost shortchanged in my other stays around the region...


More  




“Wish I could've stayed longer”


We were fortunate enough to be given the penthouse, which is nothing short of amazing - and very memorable. Absolutely beautiful!
The owner I liken to Dos Equis' most interesting man in the world. Very friendly and inclusive.
It's most definitely the best place to stay - fabulous service, delicious food (and really anything you want at any time), and...


More  




“Vic's birthday”


Best place to stay in Goa, very chilled out and the food is amazing :) the vibe is great. It's quite central and a Bike ride from most of the beaches. It's open bathrooms so don't go with someone you're shy with. Definitely recommend everyone to stay here.


“Sur La Mer - read the fine print”


The property is a hidden gem up in north goa, i was blown away by the quaint very spanish looking structures.
The proprietor a very slick, typical sales guy type person took us on a tour. It really is a fantastic property to be at.
But thats from where it goes all down hill, here as some of the highlights...


More  




“Not just a hotel for us ..”


According to Oxford English dictionary, a hotel is an establishment providing accommodations, meals, and other services for travelers and tourists. Now can you expect to get the same accommodation (meaning room), meals that you prefer (at any hour of the night), your favorite drinks every time waiting for you and even delivered to your room at one in the morning,...


More  




“Seamless Celebrations! Hidden Gem in Morjim.”


We recently hosted our post wedding celebrations at Sur La Mer and the experience was phenomenal. The vibe to this property is magical. Everything about the Portuguese style villa property comes together in perfect harmony and the 30+ guests who stayed here had a great time.
Aneel and his team do everything to make you feel at home. The rooms...


More  




“Excellent Stay !!”


We stayed for 2 nights at this extremely lovely property at Morjim and we cannot stop raving about it since then. There were 6 of us (3 couples) who had booked at another property close by for 5 nights. Due to some date mix up at the other property, they gave us an option to stay at Sur La Mer...


More  




“Great if you're homesick!”


This place is awesome, but maybe not for you if you're looking for total peace and quiet at night, as it has a really social atmosphere. The owner, Aneel, loves for everyone to feel comfortable and happy, so staying at this quiet hotel feels more like you're a personal guest in his home. The hospitality is unbelievable and we opted...


More  




“Not Really by the Sea but close enuff and Fantastique”


We went there last year and absolutely loved it.A friend is getting married in October this year and wanted to have it in Goa.We recomended Surlamer and they checked it out and we beleive we were instrumental in getting them a great deal(right Ben!!!).This place is really marvellous,its kinda hidden and it seems they have their personalised guests who cme...


More  




“Absolutely Fantastic”


you want a home away from home,where your every need is looked after personally then surlamer is the place to go.i wanted a special holiday with my boyfriend and surlamer was recommended to me...the best decision we made...what a holiday...aneel the owner made the coolest whisky sours and his stories made us never want to leave the bar....a special area...


More  




“A great place to stay in Goa!!”


First time in Goa and we couldn't feel more at home than at Sur La Mer. The staff went out of the way to make us feel welcome and comfortable. The rooms are spacious, cosy and we had a great night's sleep every night. Chilling at the pool and in the beautiful setting of the hotel was the perfect way...


More  




“Bliss in Goa”


I travel to Goa easily 2-3 times in a year but this trip was definitely the best so far. Loved our time at Sur La Mer. It's a very romantic, laid back and quaint boutique hotel in Morjim. The hotel owners - Nayan and Aneel are a super cool couple and fantastic hosts. They went out of their way to...


More  




“Like the cocktails served, Sur La Mer is perfectly mixed and very cool”


The outstanding feature of the extraordinary hotel Sur La Mer is the balance it maintains between formality and friendliness. It succeeds in maintaining an impressive standard of service: my towel appears within seconds of choosing a sun bed, my drink within two minutes of ordering it, and the beautifully designed and comfortable rooms are serviced to the highest standard, and...


More  




“Fantabulous place in north goa with an extremely charming host aneel to take care of you.”


Perfect place to celebrate my husband's 40th b'day. A truy special place for a special occasion.
Aneel is truly the finest host I have come across. The word 'no' does not exist in his dictionary. A master chef, a charming host , a fab singer... Infact he cooked up some amazing meal for us which is still being talked about...


More  




“Hospitality at its best”


Sur La Mer takes hospitality to a new level and that is just but one of its charms. Beautiful sprawling rooms with spacious attics to accommodate more guests or even just a change of space, rustic furniture, soothing spanish style architecture and a mean vindaloo. The whole enchilada. The icing on the cake is of course the lovely Marlon, the...


More  




“Most amazing place”


Entering this Spanish style villa, with rooms over looking a serene pool we knew that this was going to be just lovely. Big rooms, spacious, impeccably kept. Beautifully styled and fresh bright bathrooms. Very child friendly and best of all Pet friendly. Your dogs if you travel with them or wanna take them on holidays will be treated like kings....


More  




“Family hospitality! Charming place!”


We spent a great time in December 2012 at Sur la Mer. Planed to stay just 5 days, but at the end had to cancel other hotel to extended for 4 days more.
We were happy about attention, how the crew took care about all our needs. Kind and lovely atmosphere in this accommodation.
Rooms made with taste and charm....


More  




“Goa for Grown ups”


Sur La Mer remains by far our favorite destination in North Goa. Perched around an expansive outdoor pool, white table cloth tables sprawl out onto the grass which in turn is flanked by archways each framing super large rooms decorated with unique hand-picked antique furniture and rugs all fronting the Moorish minimalist commodes, dressing rooms and showers. This idyllic oasis...


More  




“Wonderful!”


SUR LA MER is a real pearl in GOA.
It is located in a short walking distance from Ashvem beach but still peaceful and surrounded by green, making us feel somehow in the countryside.
The building design is impressive, as well as the lovely swimming pool in the center of the complex.
It is full of different antique pieces, both...


More  




“Amazing but shocking”


Stopped off for dinner tonight, amazing venue with a lovely setting. Sadly a few points let the place down:
Constant banging of the bass from a near by club.
Service was dire, lacked multiple meals on the menu, was servered variations of what I actually ordered.
My partners starter and main course arrives after mine to the point she had...


More  




“Amazing”


Out of the world experience, the owner is a wonderful host, absolute value for money. It is like owning your own resort. The food is too good. The best in goa for sure. Pork, fish, prawns .... Too delicious. The rooms are amazing ... Different concept. Our children had a ball of a time.


“I luv Sur LA Mer”


I luv Sur La Mer. It’s a home away from home. If you stay once you feel like coming back again. You can't miss sitting at the bar and enjoying your evening drink. It's a well-designed and well maintained place. It has great staff that is ready to serve you any time.
Aneel (Owner) has really made a exclusive place...


More  




“Be amazed”


Taking a taxi in the black of the night up a narrow pot hole ridden lane you could be forgiven for thinking you are in the wrong place and the taxi driver is having you over. Stay with it for another 10 minutes and an oasis opens in front of your eyes......
Should we say more or leave the rest...


More  




“Superlative Food and Hospitality”


Spent 4 nights here and did not want to leave. Tranquil and peaceful by day, lively and entertaining by night. Just what we wanted. Thoroughly enjoyed the owner's hospitality and magnificent cooking. Did not eat an ordinary meal while we were there.
The location is perfect - 10 minute walk to the beach, close to La Plage and other great...


More  




“magic weekend getaway...”


Magic place, Aneel made sure that this was to become a weekend to remember...the place is cosy and with character, food unbelievable and Aneel...what a gem. He cooked, chatted us up and even djed for us...we left feeling we were leaving home in Goa...Don't miss this place, you will be truly looked after.


“incredible”


although i did not stay at the hotel ...which is truly fabulous in its laid back style ...i was there as a guest of friends and had the ..great fortune ..of dining there ...and the food ..was ..to die for ...just superb ..indian..western cuisine ..made to perfection..
really i will not say more...just that even after having been there over...


More  




“This is it !”


This was our fifth visit to Sur La Mer. For me it's the perfect place to relax, enjoy my holidays and completely unwind, away from my hectic work schedule. Our son had fun in the swimming pool with me and my wife. We will be coming soon again as this is the perfect resort for us.The service is prompt and...


More  




“Goa Perfect!”


Sur La Mer is Ashvem's well kept secret (literally). An absolutely stunning resort with just 12 rooms makes it exclusive and the tastefully done room adorned with antique furniture add to the delight. It is a five star resort in every way! The warmth and hospitality of the staff and owner Aneel Verman's hospitality more than make up for the...


More  




“Beautiful property and wonderful hosts”


Stayed at this wonderful hotel early April this year with family.
The location is perfect literally a 5 min walk to Ashvem beach which is a great beach.
As for Sur le mer itself the owner Aneel and his family make you feel at home and the staff are excellent. The whole place has a boutique feel and the food...


More  




“Perfect place to relax”


We stayed here for 2 nights in one of the rooms which open up directly into the pool. The whole aura of the place is just so relaxing that the moment you enter you become lazy. The pool is the centre point of the hotel and to have the privilege to jump into it at night is just amazing. This...


More  




“Food Heaven”


Should you wish to find a tranquil and extremely comfortable place to stay in Goa,look no further.
You will be mollycoddled as a personal guest by the owners--Aneel and Nayan Verman.
The place is a gastronomic delight and that is where it really scores.
The hotel itself is designed in the local style and blends in perfectly with the environs.The...


More  




“A piece of Heaven....”


Sur La Mer located in Morjim, heads my list of favourite getaways! The rooms are airy, the food is FAB and the ambiance is relaxed and homely. Everything about it is warm and hospitable.To top it all, Sur La Mer's owner Aneel Verman aka General not only whips up great signature dishes but is charm personified! He is an encyclopedia...


More  




“An exquisite meal for a romantic date night”


Being foodies my husband and I spent most of our time in Morjim scourging delectable eats both down the beach and up the hill. Sur La Mer was recommended to us by the lovely lady at Ku, another nice stay option in Morjim. We took our little baby and rickety bike on the short ride up e hill and there...


More  




“Lovely property, great location, excellent service - perfect!”


We came here for a long weekend in a big group and this was the perfect venue for the occasion! Sur La Mer is a delightful mix of old-world charm and modern facilities. Colonial facade, large, airy rooms with everything you need - a chilling air con, comfy beds, free wi-fi, fridge, tea/coffee, TV, movies, pool - the works. Breezy...


More  




“Always the most superb food”


Just thoroughly enjoyed our fourth dinner at Sur la Mer during our stay in Goa. As always the restaurant continues to serve consistently excellent food. The ambience of the hotel and the outdoor dining area enhances the enjoyment of the wonderful food. We never look at the menu but rely on the excellent advice and culinary skills of the owner...


More  




“Sur La Sux”


We had great many recommendations to go eat/stay at Sur La Mer .. so we decided to try it out for lunch.. being a veggie in goa does have its perils but i had managed to get delish food at most shacks and resto ..till i came here.. at first we were sitted on one table then asked to move...


More  




“The perfect little getaway”


I have been going to Goa for 11 years now and have always stayed in the same part of Goa (Candolim) and have always been weary to try any other place.
We decided to be adventurous and stay in Sur La Mer in Morjim and were we pleased!!!
The owners of the hotel are so friendly and we spent every...


More  




“Truly an awesome experience!”


We found this treasure just by luck, hotels.com had a mix up with our reservation at another hotel and found this for us last moment. We had been traveling in India for 6 weeks on a holiday and this was our last stop before returning home.
We were very impressed by the hotel, the rooms and the staff. Sur la...


More  




“Nice, no frills getaway in Goa with very good food”


I spent a couple days in Goa at Sur La Mer over New Years 2011. Here are my thoughts:
-beach access: the hotel is located about 8 min walk from the beach. You have to walk down the main access road, cross the "highway" and walk down another dirt path to get to the beach. The walk itself may seem...


More  




“Beautiful boutique hotel in Goa”


We had heard and read a lot about Sur La Mer and im usually skeptical about beleiving everything. But Sur La Mer is everything and more! One of the best hotel experiences in India. First of all Morjim is the idle location. One of the best beaches in goa and close proximity to Anjuna & Vagator as well as Ashwem...


More  



