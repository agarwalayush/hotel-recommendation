
“Spacious room with poor service”


Apartment was spacious and the kitchen was fully equipped. It is located in an area with no shops or local attractions closeby. You would need to rent a vehicle if you are staying here.
It is a bit noisy at at times.


“Amazing stay”


I went to surya sangolda over new year. The apartment I was staying in was amazing, furnished to a really high standard and was really clean. The staff go above and beyond what is expected to make your stay really comfortable. They knocked on my door after I arrived with breakfast, whenever I needed to go somewhere they offered to...


More  




“noisy, noisy, with no service!”


We booked on the net, and were led to a nice apartment 2 room flat on the 2nd floor, however with the bedroom facing the main highway. The highway literally runs through the rooms on this side, and it is busy, busy busy - all day and all night! We could not sleep a wink with motorcycles and trucks screaming...


More  




“Nice Apartment but poor service”


I reached Goa in the night around 9:00 PM and when I called the number provided by cleartrip, I didn't get any response. Somehow managed to get to the apartment but came to shock that they weren't aware of my booking after 15 minutes dilly dollying finally I got the room. When we entered the apartment, it was stinking as...


More  




“perfect stay at GOA”


we stayed there in mid Feb 2013 and this place is as good as home.i will highly recommend to anybody going to GOA. the location of place is very good and all major beach are near to hotel.
furniture is of top class and of best wooden quality . bathroom are top clean and kitchen is perfect to cook Indian...


More  




“Excellent Apartment Stay”


Stayed with my Family. Agree with the earlier reviewer, you would require your own transport, rented car. Not on the beach or any of the happening party places. But, again, not far also. Couple of kilometers drive to great beaches as well as Panjim.
Great rooms and an amazing penthouse. Stayed in both. Large rooms which are well furnished with...


More  




“Amazing apartments with some small gaps”


I haven't stayed in a better appointed or more giant room than at Surya Sangolda. Two bathrooms, a couch (!) The pool was great too - cold and clean, perfect for the Goa heat. On the other hand, travelers should know that this place is ideal for people who know Goa a little and have their own transportation (a car...


More  




“A wonderful stay in Goa”


Surya Sangolda is a wonderful hotel. Its USP is its great location, wonderful wonderful apartment like rooms, interiors and amazing staff service.
I stayed at this place in November 2012 and would recommend is strongly as a great place to live while in Goa. I had reviewed the hotel on tripadvisor and stayed in room number 2 based on a...


More  




“Perfection is everywhere”


Wonderful location, excellent service, extreme care for each and every detail...
Rooms are superb, stilish, with every comfort and always clean. All the area around is always tidy and there are flowers everywhere.
Spa is fantastic and they even provided yoga lessons in front of the beach, at sunset..The total ambiance and the property is excellent.


“As perfect as home!!!”


Nestled in the hills of one of Goa's most beautiful villages, Sangolda, Surya Sangolda is a 'home away from home'. An upmarket offering of service apartments, it provides a traveler with the option of doing things their own way whilst on holiday in Goa.
I've stayed there a few times and enjoy the freedom of cooking my own meals while...


More  




“Great experience”


We stayed in the surya sangolda after booking through sunmaster. The apart hotel is set within a little village outside of the main areas of calangute and baga. Although it was a little remote there is a supermarket nearby and 2 restaurants within easy walking distance. A taxi ride was 2-3 pounds to the busier areas. The apartments are a...


More  



