
“Nice hotel, slightly far from beach, totally worth it”


Spent 4 nights here, lovely rooms, very comfortable. Rooms are a bit exposed since one entire wall is a floor-to-ceiling window, but the blackout drapes are great for privacy. Just don't forget to draw them :)
Each room is also pretty isolated so you're not disturbed by other more party-oriented guests.


“Sweet,simple,relaxing”


Banyan soul is pretty close to the Anjuna beach (walking distance) and right next to German Bakery which is awesome. The owner and staff are very nice. Rooms are clean and cozy, though room service is not available. Rates are very attractive.
Overall, much recommended in Anjuna.


“Best choice in Anjuna”


This was my second visit to banyan soul and I am very much satisfied as before. It is the same no frills hotel with a peaceful and cosy atmosphere. Best part is the rates which were still reasonable when everything in Goa was offered for more than double cost during an extended weekend in October. Just a short walk from...


More  




“Neat, Clean & Simple”


One of my favourite spots to stay in goa. It's economical, close to Anjuna and Vagator and the owner is always happy to help no matter where he is.
The rooms are simple and clean and so are the toilets. There isn't any room service but free wi fi kinda seals the deal. It's the perfect spot for trips where...


More  




“Just the type of relaxation needed”


Great staff and lovely simple decor. Right near german bakery which is ace. Walking distance from anjuna wednesday market which is fun but very hectic in terms of hard sellers. Well recommended. Walking distance from beach. Hard to find though.


“Just what we needed”


My partner and I were feeling run down from a stressful couple of weeks in Karnataka, and this was a perfect refresher. Clean and very peaceful. No restaurant but we made nightly use of the delivery flyers in the room and were very happy with that. Only 10-15 minute walk to the beach.
Highly recommend :)


“Good”


Nice homely place. If your idea is to chill and relax...this is a perfect place. Clean and nicely maintained. No interference from hotel staff and u feel u r at home. Only drawback - no restaurant. No not even basic tea/coffee. However, the German Bakery is just next doors and it's a nice place to have breakfast and generally chill....


More  




“Peaceful hideaway!”


Was looking for a peaceful hideaway for a short vacation with my spouse. We chose The Banyan Soul in Anjuna because of its location. Nicely private, at the same time very close to all the action points in Anjuna. Very efficient staff, clean and tidy rooms and just the right amount of comfort. Don't expect luxury but just efficient comfort...


More  




“A Quaint Delight !”


A Quaint Delight !
Recently did a 10 day vacation in Anjuna, Goa .The fact that I was headed to Goa after a nearly 6 year long hiatus had made me keen to squeeze the most out of this trip.
Landed in the Evening and an hour long cab ride through rain soaked greenery and finally found myself looking at...


More  




“Soul refresher”


I was extremely stressed out recently with my work. And was looking for destresser.
I stayed here for 4 days.
Very good ambience. Very friendly people. Highly recommended. Look forward to visit you people soon


“Great place great staff and great Manager/Owner”


Twelve great units all with exactly the same amenities and nice verandas, airconditioned, free wifi, tv's and very nice grounds and quite places to relax and read and plenty of place close by to eat and drink and the beaches just a short walk


“Home is where the soul is”


Heart warming serenity and just the right ambiance for a perfect holiday, The Banyan Soul can make you feel at home. A holiday gets better when you can leave all your worries to someone else and that's precisely what this place did for me. Warm homely food, clean cute rooms, and brilliantly friendly staff and owner, added to my sweet...


More  




“Good place to stay for couples and families”


Stayed here in November 2013 during Ridermania and had a good experience. The rooms are good and clean. You have a good lawn outside every room to relax and enjoy the climate. It offers good value for money and is a must try for couples.


“Do Not Hire a Bike Through This Hotel!!!”


This hotel in itself is just a set of rooms which lacks all the basic amenities--No desk-phone, no room service, no front desk, no food, scanty cleaning, no laundry, no drinking water, no hot water kettle...the list is way too long, just Do Not Expect Anything and yes the rooms are also very very small.
But, one can make one's...


More  




“Amazing place!!! A must stay”


Stayed for 4 nights in banyan soul and it was Amazing.Highly Recommended for couples/honeymoon.Sit -out is very good.
Owner of the hotel is friendly and accessible, guided us the route to come to hotel.
Near Attraction- German bakery, Flea market, Curlie's , vagator beach, Anjuna Beach,Morjim,Club cubana.
Calungate and baga at distance of 8-10 Km, Rent a bike and enjoy....


More  




“Calm and Beautiful”


Stayed here for 2 nights, 30th and 31st August with the wife to celebrate our anniversary. Booked this place on a hunch ""NO REGRETS"". A beautifuly calm and quiet place. Nice clean and decently big rooms. Bathroom was clean and no leakages. Few things which are not there makes this more of an experience like there is no intercom but...


More  




“Not worth the effort at all...”


So, here is a boutique hotel -The Banyan Soul that is situated in one corner of Anjuna. There is no kitchen, sooo many mosquitoes to keep you company and an average property with no pool. The staff is helpful, thank God for that. The place is difficult to find and far away from the beach. German bakery, which was situated...


More  




“Good budget friendly place, a shout away from curlies”


We recently stayed at Banyan soul for a night, after an impromptu decision to move to North Goa from the south for the last night of our trip. The owner was friendly and immediately blocked two rooms for us (we called him the day before). The rooms were clean and decently big and so was the bathroom. They did have...


More  




“Good place”


The rooms are beautiful, very clean and the best part very affordable.
The hotel is located in a quiet and peaceful surrounding.
It's close to Anjuna Beach, Curlies and right next to the German Bakery.
My overall experience was pretty good and satisfactory.


“A big diasspointment”


Me and my wife stayed here for 3 nights and it was an utter diasspointment. The pros and cons are as follows.
PROS
1. Located near the Anjuna beach
2. Tariff rates reasonable
3. German bakery nearby
CONS
1. Unproffesional staff. Reluctant to take any requests and orders. Very very carefree attitude.
2. Rooms were not cleaned regularly. Had to...


More  




“Worth Staying”


nice rooms, nice staff, slightly location problem. maybe it was off session so it was little scary because it was surrounded by jungle. otherwise, very good hotel. room service was excellent though you have go to reception for anything (no inter call). its a kind of hotel were u can relax. no traffic, no horns, no pollution surrounded by greenery,...


More  




“very good”


nice rooms, nice staff, slightly location problem. maybe it was off session so it was little scary because it was surrounded by jungle. otherwise, very good hotel. room service was excellent though you have go to reception for anything (no inter call). its a kind of hotel were u can relax. no traffic, no horns, no pollution surrounded by greenery,...


More  




“Hidden away, nice place”


States here with friends and was kind off perfect in all senses. Food is a limitation, but you seldom eat at the lodging. The rooms are clean, although our ac started leaking water, was taken care off.
Friendly staff. Close to the beaches.
A big thumbs up and will be returning again.


“overpriced and out of the way”


Clean, small rooms with zero service that are very far away from anything. Booked a car from the airport through the hotel but no one was there, sort of sums up the staff here.
Rooms overpriced for everything, would stay somewhere else (which is what we did and found much better value)


“Lovely place for long stays in Goa.”


Staying in Banyan Soul is itself an adventure in the Paradise called Goa. 12 beautifully appointed rooms with small garden and a huge banyan tree. Modest pantry is available but you will certainly not be eating at your hotel anyway in Goa. Just Next to it is one of the best places to eat in Goa, German Bakery. Open from...


More  




“Relaxing and good value”


We only stayed @ the banyan soul one night as Anjuna was dead, but loved it all the same.
There's a little route to the beach which takes about 10-15 mins to walk (we didn't get a scooter but the hotel can sort this out for you). The rooms are very light and feel airy as they have big glass...


More  




“Good value hotel near Anjuna”


After reading extensive reviews of all North Goa beaches, Anjuna was the second choice behind Mandrem as the base point for me and couple of my cousins. Since we could not find a decent hotel room at Mandrem, we zeroed in on Anjuna. After a quick search, I was introduced to The Banyan Soul.
Locating the hotel wasn't exactly straightforward...


More  




“Awesome place”


Our group of 7 friends stayed here for two nights and we were really happy with our decision. Its slightly away from the beach but the elegance makes up for it. The rooms and bathroom are sufficiently sized and were very clean. The open area in front of rooms is worth admiration. The owner is very friendly and loves chatting...


More  




“Tucked Away in a quiet corner”


Nice hotel tucked away in a corner of Anjuna. All Anjuna hotspots within 5-10 mins. Take note that there is not restaurant in this hotel but then thats what makes it quiet. For the price one pays its VFM for lodging . Dont expect too much and you will not be disappointed. Rooms are fine, a bit on the smaller...


More  




“Budget hotel near Anjuna”


Stayed there for 3 nights from Feb 16-19. Nice budget hotel which is a bit difficult to locate initially. Landmark German Bakery. The rooms are well maintained. Comfortable bed.
Get a bike on rental and you will find everything very near. Anjuna beach will be 4 mins by bike and same is flea market on Wednesday.
There is no complimentary...


More  




“Very nice place in Anjuna”


This is a place for people who want the best of both Goas - close proximity to the beach while enjoying Goa's peace, calm & scenic beauty. The owner, Sumit, has turned a charming bungalow into a hotel. The location is fantastic, right next to German Bakery so you are always assured of great food :-) Anjuna Beach is also...


More  




“Love the new Banksy”


Awesome yet again. Stayed at this magical property for the 5th or 6th time. Never failed to satisfy. The new Banksy print on the facade wall is a refreshing touch. Sumit & his boys always make you feel at home & take care of all your needs. Thanks guys until next time.


“Most unethical person i ever met”


We booked this hotel via booking.com. person called Sumit called me a day before our arrival and confirm the booking. Hr was worried that as he has not received any advance we might not turn up there so I asked him to forward me his ac no and assured him that our booking is confirm. But toy surprise when I...


More  




“Stylish and comfortable place in Anjuna”


Stayed here a couple of times over the past few years. It is set back a bit from Anjuna itself- but secretly in the best location! Quiet and relaxed! Next door is the German bakery (see reviews because it is one of the best places in Anjuna!) It is the most relaxed place on earth. Great food, atmosphere and service,...


More  




“Quiet, relaxed and well kept”


I spent three weeks at the Banyan Soul in Jan 2014 and I was very pleased with the stay. The rooms are spacious, clean, air conditioned, and located in a quiet part of Anjuna just by the German Bakery and a 10 minute walk to the beach.
The owner Sumit was extremely helpful and quick to respond to any questions...


More  




“A Good Find”


If you are looking for a clean, nice, affordable and yet modern place to stay while in North Goa, you should consider the Banyan Soul. It's a good hour's drive from the Panjim airport and is closer to the less crowded yet beautiful beaches of North Goa. The well known German Bakery is its neighbours and a great landmark. The...


More  




“Very good rooms but remote location”


Rooms are too good, and the room service is also very nice and helpful people.
Just that the hotel is located very inside from the main happening places, so if you like to be in a remote place its the best you can have.
Beach is just 10 minutes walk from the hotel, and the major landmark is "Near German...


More  




“Good value hotel and close proximity to happening places”


Excellent boutique concept value hotel stay with 12 rooms with limited services. Close proximity to most happening places such as Club Cabanna, Curlies, Beaches- Anjuna, Vagator and Baga is just 4 kms away (approx). Sumit the owner, who actively takes care of the services is a great guy and very helpful on any queries you may have. Slightly difficult to...


More  




“Brilliant place to stay if you need peace around you.”


I along with my 4 friends reached Goa on 6th January 2014 and was there till 11th January 2014. I had no idea about this place even though the booking was done in advance. Initially we had a hard time tracking this place down but when found, I immediately liked this place. Uphill, greenery around and a well maintained place....


More  




“Cute and Beautiful hotel that needs management's attention”


This hotel has 12 spacious rooms and it is undoubtedly cute and beautiful, however it needs management attention, The rooms are good and spacious with good powerful A.C. and a nice sit out. All the rooms have little garden in front. The shower curtains and curtains in the rooms are not clean, also the blankets are not covered with the...


More  




“Once again a great place to stay”


I stayed at the Banyan Soul 2 years ago and it was great. Went back this November and better
still now that the owner Sumit has also added free wifi and a lovely sitting and lounge area at the
front. Again the position of the Banyan soul is perfect and the hospitality up to par. Great job...see you soon!


“Spectacular Location”


This hotel was located at a very interior location at Anjuna. Initially we had to hunt this location with the help of GPS on our phones. I traveled here with my friends. The hotel was very small with limited number of rooms but had all the expected amenities. It was extremely reasonable in terms of the tariff. It is very...


More  




“Well Maintained Budget Hotel”


I stayed at the Banyan Soul with a friend during the first week of Nov, 2013. I read about it here on tripadvisor.in. After reading few reviews, I decided to book a room immediately as rooms in all the hotels were fast filling. Booking was hassle-free. After booking a room, I read few more reviews. Few days later I started...


More  




“Neat Stay near the Beach”


Tucked under the shadow of a banyan tree, the Banyan Soul is pleasantly located in the rustic calm of Anjuna. Yet it's a stones throw from the action packed beach (5 min walking) and Flea Market (2 min walking).
The right sized rooms are neat with good air-conditioning and well equipped with cable TV and attached bathrooms which are clear...


More  




“Good place to stay”


In Goa, where one spends very less time in the room, the only thing I look for is cleanliness and good staff. This place serves this the best! The rooms & bathroom are very clean and overall very nice (no toiletries provided though). The hotel is at a very secluded place. However, the landmark German bakery helps a lot to...


More  




“excellent value for money, and the owner's a great guy!”


I stayed at the Banyan tree in October 13 for about 4 days and was very impressed with the low cost and clean, simple rooms. Great value for money, all rooms have cable TV (tho if you're going to Goa to watch cable you definitely have the wrong idea), good air conditioning and, most important, CLEAN bathrooms with hot water....


More  




“Even better the 2nd time!”


Visited The Banyan Soul for the second time and it was great again. The place is greener than the last time, which adds to the overall calmness and beauty of the place. The staff was courteous as ever and the owner as helpful. Will definitely visit again!


“Go when the owner is there”


I decided on this place after talking to the owner, Sumit. Very accommodating and reasonable gentleman. Unfortunately for me he wasn't at the place when we landed. The banyan library was closed which is understandable but unclean rooms unacceptable. Stink and filthy linen is what makes me give it average rating for what could have been 4 stars. Service is...


More  




“Soulful”


If you are looking for a pleasant place to stay with a very casual atmosphere with friends you must check this out. Its a place for people who come to enjoy Goa in the Goan way. Me and my friends stayed there over the weekend and did we love it :) The room was roomy, pleasantly cool and the beds...


More  




“A bad experience.”


I spent one night in Banyan Soul and that was it :-)
Pros:
Near to the beach
Cheap
Cons:
In a street where it is a night mare to take car in.
Hard to search, everyone knows where German Bakery is, but no idea about Banyan Soul.
Stinking rooms.
Pillows had black fungus on them.
Bedspreads were dirty.
For me...


More  




“Feel the soul of Goa”


This is a no frills hotel which could be called the best ever bed and breakfast you can find in Goa. Rooms are hotel like and deluxe, kitchen is more like that of a resort and service is courteous and friendly. Adjacent to German bakery and walking distance from anjuna are it's primary usp. The owner manages the hotel himself...


More  




“Peaceful, Cheap and a Short Walk to the Beach”


Our good friend from India booked this place for us. Down the street from the German Bakery, the Banyan Soul is a hidden hide-away that's simple and beautiful. It is easier to stay here if you have a friend that knows the area. The walk to the beach is short, but a little difficult to find if you're new to...


More  




“The Perfect Retreat!!!!”


Everything is nice about this quaint little place!!! Starting from the greenery that surrounds it to the reading place under the banyan tree!! Never feel like leaving the place and going anywhere! the staff is very helpful and so is the management. Sumit, the owner of the property always brings out a solution if you need help with regards to...


More  




“Best hotel service ..Amazingly caring owner and staff !!”


Surrounded by natural greens, this hotel is like an oasis with clean and neat rooms and the campus. The owner of this hotel is very nice and polite and flexible with guests. No hard n fast rules regarding check out / check in. Their main purpose is to serve is what i felt.Every room as some green space in front...


More  




“Calm & Peaceful!!!!!”


This hotel is recommended only if you are good with roads because this hotel is situated in the middle of nowhere and is difficult to locate, especially at night. The property in itself is very beautifully done and has descent rooms and a comparatively small bathroom. The breakfast is average. The best part about this hotel is the peace and...


More  




“Simple yet excellent”


Our trip to Goa in the month of March was totally unplanned. We ended up going there for a long weekend. Sumit who owns the place was really accommodating and we enjoyed our stay here. Each room has an entrance from the lawn and a restroom. It's a simple no fuss place. It is right behind German Bakery so there...


More  




“Epitome of Excellence and Tranquility”


By an off chance on recommendation from Tripadvisor and friends we contacted this place for a room on our trip to goa. We were lucky to get a room availability as it seems they are always full up (no doubt with the elegance of this place).
This place is a paradise in every sence. I have to say and compliment...


More  




“Good basic lodging - everything you need from an Anjuna stay”


We turned up at Banyan Soul having wandered around Anjuna and decided to stay a little longer. We walked up and the staff were immediately accommodating - we were given a room immediately. Clean, basic, and you're left to get on with your holiday, which was exactly what we wanted. The bathroom wasn't the most comfortable or ventilated, and it...


More  




“A little piece of heaven”


It is a little piece of heaven in a heavenly place: Goa. The staff is really nice and helpful even if some of them cannot speak English at all, which can be a problem sometimes ;-)
The rooms are really spacious and clean.
The manager is really nice and so is the owner.
It is very close to flee-market and...


More  




“Great value for money”


nice and quiet location, away from major roads but still close to the beach. the hotel has only 12 rooms so the service is much more personalized compared to large hotels. They took us to our room as soon as we arrived so that we could get refreshed, rather than making us wait at the reception to finish their paperwork....


More  



