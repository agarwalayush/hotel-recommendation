
“amazing time!”


just what we had thought n more..... though check-in took a while but every bit worth the wait!
beautifully done up. suites r clean to say the least spacious n luxurious.aesthetically done-up Mediterranean theme. the shuttle service to white sands zuri take u to the private white sand beach! perfect to relax n enjoy.....


“Wonderful memorable trip”


It was a wonderful trip. The hotel was simply awesome with facilities like Table Tennis, Bar, casino, gym, swimming pool, private beach, snooker table etc. the gardens were excellent. Another property of the same owner, Zuri white sands allows use of its swimming pool too if you are staying in this hotel. The swimming pool of Zuri White Sands is...


More  




“Value for money for a family”


This is 1 of the best 4 star resort ive been in Goa. Its designed in portugese style. It has 2 huge rooms which can easily accomodate a family of 4. The hotel has all the required amenities like swimming pool, club house. Its very near to the Varca beach and guests staying here can also visit the Zuri white...


More  




“Great hotel in GOA. Stayed with a party of 70 whilst we get married in GOA”


My wife and I visited a couple of places to choose from where we had our 4 day big fat indian wedding in GOA. Zuri retreat is where we all stayed (70+ people) and I would like to say thanks to all the staff for welcoming us and assisting with all our needs. We utilised one of the conference rooms...


More  




“Veg resort”


I am writing as I staying at the resort
They sell a package deal with all meals
But they don't mention it as a veg deal
The moring breakfast is continental as the westerns eat with the Indians but the buffet meals are only fit for the Jains as we go out to select the no veg meals as outcasts...


More  




“NICE !!!”


We visited Goa in April 2013.We had a three night package. We were total six people. Although we had a three night package but for the first day we were given a room in retreat by Zuri as the room was not available in Zuri white sands. We reached retreat by zuri at 1.30 p.m. We were given a welcome...


More  




“" the royal zuri experience "”


We were 3 couples stayed at zuri for 3 days, its located at south goa ,very far from the happening places In goa,it don't have a near by beach, we have to go to the other zuri resort 3 km to get to the beach. The food and bar is very expensive, the service was good, the rate also was...


More  




“good place for holiday with kids”


i had stayed in club mahindra varca beech resort twice in the past hence decided to sty in emarald palms.location is bit interior,ask for kolva police station and go straight.resort is much smaller than varca.we had a royal welcome,staff are polite,ambience is good.rooms are verywell furnished and maintained.i stayed in 1 bedroom apartment,which is truly 5star.food had variety,kids had variety...


More  




“Perfect”


This resort was amazing. The breakfast was delicious, the location ideal (a short walk down to the beach and past some little restaurants), the pool a paradise, and the rooms were gigantic. I have nothing bad to say about this hotel at all. A lot of Russian guests were staying here. Wonderful!


“Superb,would love to go back once more..”


We were in The retreat in June 2011, simply enjoyed everyday there.Rooms and Room Service was excellent. The breakfast buffet was too good. The pool was neat and tidy. For a bigger pool and a private beach they would take you to Zuri white sands just a km away in their bus /car as required.
Disadvantage: Far from the market...


More  




“the best thing that can happen in goa”


the rooms are excellent. the pool area is clean n tidy.every alternate day there would be live music and barbecue.the best thing when you holiday everything is organised for you.Staff is very helpfull with a smile on their face.Front office would always make sure that we are having comfortable stay and Reena and Menakshi would always guide us to the...


More  




“A wonderful experience, only that it was not much like a stay in Goa”


I had planned a vacation with my family to Goa through Club Mahindra. Unfortunately (due to Ganesh Chaturthi) Club Mahindra Varca was full, so we went to Emerald Palms instead. I had read the reviews and as Varca Palms was rated far better, I was dejected. Furthermore, there was renovation going on in the lobby. But I was in 4...


More  




“A Good Hotel to Sleep”


If you go to Goa to travel around , go beach hopping , visit various eat-outs , go for casino night outs then this Hotel Makes perfect sense. Bottom Line if you are in the hotel to just sleep and eat the buffet breakfast then its a very good bargain for the price at which it comes.
In case you...


More  




“Worth it..... for the South Goa visitors”


I was in Zuri retreat Sept 2011 along with my family ...its supposed to be of season in GOA during these days and i got excellent offer from Zuri .
This property dont have an adjacent beach but they take you to Zuri white sand on request via car which is just 2 Km away and you can enjoy the...


More  




“Pleasant stay”


we visited this place as club mahindra emerald palms. considering they have taken over the place, it should be renamed on the website too.
Following is the review:
Approach to the hotel - confusing as there are various boards on the way, same say varca and some emerald palms while both are on the same way which splits later on....


More  




“Fantastic Stay would love to go back.”


Very well appointed rooms when we visited it was thru a tie up with Club Mahindra. We are extremely particular about cleanliness in a room and we had absolutely no complaints though we were with an infant. There were two washrooms in our suite and that was an added advantage. We absolutely enjoyed our stay. The ambience and architecture of...


More  




“Good”


The hotel is great! Staff polite and chief executive. The kitchen is delicious, as elsewhere in the GOA. A small, but very green area. Very spacious and comfortable rooms.
Advantages:
High-quality Indian Four
Disadvantages:
Relatively far from the beach and shops, a little fun, noise from the pumping station.


“awesome time!”


The hotel is located in a colonial-style building and looks fabulous in the photos. The hotel seems to target local guests most of all, which is well-reflected in the cuisine. I like Indian food so I was quite pleased, but it is definitely a question of personal taste and/or digestive possibilities. There is very little around – what I can...


More  




“A great place for vacation!”


Located away from the hustle and crowd of the town, its a perfect place to unwind. The rooms are just perfect. The staffs are very friendly and food is good. the pool, bar, sports actitivity makes it a perfect place for a weekend holiday. The beach is accessible from the hotel and is about a km away... The beach is...


More  




“Goa, A Good Break in the Summer at Club Mahindra Emerald Palms”


The Retreat by Zuri, Currently Club Mahindra Emerald Palms(CMEP), is a nice place to relax. We (my wife, daughter & myself), enjoyed thoroughly as we had our own car, else, it would have been a nightmare reaching this place. We had no option as Club Mahindra Varca (CMV) was completely full and we were lucky enough to get accommodation at...


More  




“Not bad”


Our friend is a member of Club Mahindra and was eligible for 3 rooms at a Mahindra Resort.
We opted for Goa - Would have preferred to stay at the Varca Resort but no rooms there.
The place was quite nice - Airy reception area, quick and hassle free check-in, rooms were not very large but not too small either....


More  




“Bad Hospitality by Travel Potter Noida”


I will just say never go with Travel Potter.They are only interested in your money not bothered about your feelings with which you plan your vacations.Once they will receive money from you they will not entertain any complaint about any inconvenience you faced.


“good experiance”


had a good experiance during my stay here... and i would definitely consider staying there during my next goa trip. the resort was build like a portuguese village... g+1 villa kind of rooms. rooms were decent sized and well maintained. the beds comfortable and balcony comfortable for 5-6 ppl to sit and have drinks during evening. the staff was very...


More  




“Club Mahindra Resort - A pathetic excuse”


All people that wish to attend expecting Club Mahindra quality and service, please DO NOT go to this "resort". Old unmaintained rooms, with LEAKING toilets and faulty AC control units. Extremely poor customer service - in room dining forget half your order each time. And server in the restaurant are not prompt or attentive. Also no facilities what so ever...


More  




“Back to the time of the Portuguese”


Beautiful Portuguese style design and wonderful ambiance. Very silent atmosphere , near to Varca beach, the white sand beach. The only minus being its location, South Goa, where you've nothing much to see, other than the white sand beach. All the other travel hotspots are to the Northern side, which demands a long journey from where you're staying. But if...


More  




“Great Place but bad Location”


This Place Beautiful - literary beautiful.. but thats about it! Rooms are nice and spacious. One goes to Goa to chill and relax at the beach but this place even after having all facilities has a major drawback that its far from the beach. There are not too many recreational facilities in the hotel apart from the pool. Unless and...


More  




“Pl don't visit club mahindra emerald palms Goa if you look at value for your hard earned money”


I visited emerald palms between 22-25 may 2012 just because there were no rooms available in club mahindra Varca. The rooms & service was pretty good but I never expected that this would be my worst Goa trip (already been here thrice but at better beach touching hotels). We were literally confined to our room for 3 days since the...


More  




“Nice place, service could be improved upon”


Club Mahindra's Emerald Palms seems almost exclusively for visitors with timeshare, obviously all Indian. The foreigners used to visit when the hotel was known as The Retreat by Zuri. This change hasn't gone down well with the unionised taxi guys waiting outside the hotel, who did good business when the foreigners were around. They lament the fact that several Indian...


More  




“Changed ownership & name...not the same!”


The Retreat by Zuri is now owned by Club Mahindra & is called The Emerald Palms & is purely for Timeshare people now.
No longer a quiet getaway from the world...it is now almost exclusively for large Indian timeshare families & at times very noisy with children especially around the pool & dining areas...so perhaps not suitable for couples or...


More  




“There are better places to stay in this area”


My stay at this hotel got off to the wrong start when I emailed them a month or so in advance about something to do with my booking, only to be told the hotel had changed hands and no longer had the same name. I would have expected to be told this and presumably the customers who didn't email them...


More  




“Great Hotel and very nice experience”


I visted Zuri Retreat(Now Club Mahindra Emrald Palms) on 11/01/2012.I was travelling as a guest member of Club Mahindra. My check in time was 2:00 pm and I reached Hotel at 09:00 am even.When I reached the hotel I was satisfied lloking at the entrance .The reception was nice one with circular counter as I reached the staff gave a...


More  




“Good Hotel & Good Service - contrary to all what I read....”


The hotel has some very good features such as the spacious rooms, the architecture of the place, the awesome number of plams in full bloom etc.
The pool is good and the best part is that it is not more than 1.25 mts deep, so infact the adults can easliy lounge in the water and not have to keep aflot...


More  




“For the price I expected a more luxurious hotel”


After an hours drive we arrived at this hotel from the airport on the 2nd of Jan 2012. Entered the reception/lobby to be greeted with a disgruntled guest having an argument with the reception staff which became very loud. We were asked to have seat in the lobby with temperatures well above 30 degrees celsius with no air on in...


More  




“Bring your own water, towels esrplugs and toilet paper - for only $400 usd per day!”


I was looking forward to staying here for months. I have never spent this much on a hotel and thought my money would go much further in India. The front desk staff were aloof and unresponsive. The restaurant staff fantastic. Room service had left items from the guests, like coffee in the coffee cups. Kids screamed through dinner and the...


More  




“Good stay with decent staffs”


I stayed 1 night on Christmas season. Room is clean and has 2 bathrooms. There is a pool but no beach. It was a good stay in Christmas season.


“Mixed review”


I stayed there for 2 nights. The rooms are big and the space gives a sense of luxury. The room had a balcony and made for some relaxing time with my morning tea. The beach was not close to the hotel ... 1 km walk from the hotel. For the money they charge I could find better locations. The food...


More  




“Sheer bliss!”


generally people would want to go to North Goa where all the action is! This place is for those people who would want to relax peacefully, be away from the hustle bustle of the city!
Beautiful property, great facilities, very calm & peaceful! we had a suite which was beautiful. The breakfast options are great. Good place for a couple...


More  




“Excellent Place for nice break”


The property is really good with spacious clean rooms. The room I stayed in had a separate living area (perhaps its there in all rooms) and that was the best part of it. Huge living area with a separate bath. The swimming pool is nice and so is the lovely breakfast spread. The private beach is a small distance next...


More  




“Excellent Stay”


I stayed at The Retreat in the month of May during an official conference. Although during my 3 nights stay I was stuck to the conference room for 2 days, the hotel offered many comforts to make me enjoy Goa.
Hotel swimming pool is amazing and looks even more beautiful at night. The food at the property is good too....


More  




“Excellent spot for hanging out in silence, yet happening”


Nice place in budget to spend a long weekend. Approachable to the places and beaches nearby. Excellent for couples and families how want to spend time slowly and peacefully. The best part is that they have pick and drop to their big sister resort - white sands where you are allowed to use all facilities like spa, private beach, casino...


More  




“Good for a Small Break”


Visited this resort in last week of September 2011.
Pros,
Got a steal deal for a suite room, the price was inclusive of breakfast.
Suites are huge, clean and plush with all modern amenities that one would need in a hotel room.
Swimming Pool is small yet clean and good for unwinding.
Breakfast has variety but lacks that oomph factor...


More  




“Excellent rooms, Boring Food”


We took the all-inclusive package for 3 nights
What we liked about the place
1. 2 Big rooms with 2 TV’s and 2 bathrooms + plus dressing room with huge wardrobes, my 9 year old boy loved having his private room and TV. Rooms and bathrooms are of superior quality and stainless
2. Pool is good though not very large,...


More  




“Good hotel for family vacation and relaxing but not so much for sightseeing”


We stayed in this property for 3n/4d in last week of Sept 2011. We had got a very good deal online so decided to visit the place. The property is far away from the hustle bustle of North Goa and Panjim (35kms).
All the rooms here are suites so you get more space and have good layout. The wi-fi is...


More  




“Refreshing, Relaxing, PERFECT.”


Our room had two bath rooms!! with perfect shower jets. The kind of shower, where you feel relaxed instantly!. Great room, nice decor and furniture, great bed...Overall great for a couple !! We didn't feel like going out of the room for Goa sight seeing.
The beach is a 10 mins walk from the hotel. The pool was cozy enough...


More  




“Very tasteful rooms and surroundings”


We stayed at The Retreat just a couple of weeks back. Have to say that the rooms were very tastefully designed and decorated. Our kids just loved the rooms and so did we. Only thing lacking was a bath tub. The swimming pool is well designed and suitable for kids too. There is a seperate kids pool as also the...


More  




“Great Hotel, Bad Food”


The hotel is very excusite for the money you are paying. Its a great property but sadly the service is poor and food horrible. Far away from the beach makes it more unviable. Would had a been a great hotel but for its service and food quality. Stay and have food outside. Amenities are good with pool, spa and playing...


More  




“Great experience”


We stayed at this hotel from 2nd Nov to 4th Nov 2011 and we had a great stay. The room was fantastic, the place was good and the service was excellent. The staff were very prompt and quick with whatever we needed. The breakfast was good. Only the menu prices were way too high which was made to suit Russian...


More  




“great gateaway”


i had the pleasure of staying at this wonderful hotel last year-september-2010 to be precise.
in fact it was for my nephew's wedding,who had booked the entire hotel for 3 days.
we had a great time,my family,neices,nephews,sisters and friends.
good comfortable rooms,suites,great breakfast,lunch,dinner,and of course drinks.
children enjoyed the swimming pool.wedding ceremony was conducted in the ball room,good facilities.
staff,managers,in...


More  




“Real Retreat”


We visited the Retreat by Zuri in September 2011 and it was a very pleasant expereince.The staff was very freindly and attentive to the needs. The portuguese architecture was beautiful...food too tasted great and had a lot of variety. The best part was the experience at their private beach at whitesands /varca which shall remain in our memories forever. The...


More  




“Worst place in goa”


this place has been my worst experience in goa. They should change their tag line from "welcome back" to dont come back.. I had booked this place for 3 nights and 4 days.
Our shocker came on the 2nd night when we found a lizzard in our room. the request to remove the lizzard was acknowledged after a good 45...


More  




“Zuri Retreat”


I was here a day before. This is another affiliate resort of Club Mahindra.This resort is lacking with entertainment. Swimming pool is centre of the room blocks. though it is a small property rooms are good. this is about one kilometre away from Club Mahindra Varca beach resort.


“Good value in a quiet location”


We got suites for Rs. 3000 per night which was an excellent deal for the property. The rooms are big. Just a couple of watch-outs -1. It is not a beach-front property, so don't expect that 2. It is quite far from the nearest main road - ensure you have transportation lined up if you're living here. Apart from this,...


More  




“The Perfect Place”


Perfect place, lovely property, great rooms, good service. Located 10 minutes away from the beach. The restaurants where I enjoyed their food are Ton Cia - 5 minutes away on the way to the Varca beach, Loco and then Travellers Rest.


“Undoubtedly Recreational!”


It was a lovely stay. The Suit was really comfortable with perfectly matching ambience that will make you feel the environment of Goa. I like the floor tiles particularly also my suit one side balcony was opening towards east giving perfect sunrise view. During night, standing at west side balcony was a different lovely experience.
I will strongly suggest readers...


More  




“Awesome...”


I stayed at the retreat during the diwali week and i must say it was one of my best holidays.i was with my wife and kid....firstly i got a good package for 4 nites5 days....i would say the deal was a steal...besides that..all the promises as per the package were delivered wirhout any room for cribbing....so here are the highlights......


More  




“The worst exp ever”


Staff not helpful at all and they do not give any correct information till you shout at them and on a holiday you don't want to do that at all


“Fab rooms, bad spa”


this is a late review as we stayed in july 2011.the suite room was fantastic.spacious & clean. its doors opened right into the courtyard.what i found missing was a bath tub. but thats coz i just love them. the bed was decent sized. not huge.......big problem was that i felt that the AC was too cold at 18 deg.also whenever...


More  




“A very good stay at Retreat by Zuri”


Oct-Feb may be the season to visit Goa but “OFF” season trip during last 2 weeks of September was quite good. You get all the fun at a lower price because it’s the “OFF” Season.
This blog is for a relaxed trip in south Goa.
We booked Zuri-The Retreat in south Goa for Rs1500/day. It costs around Rs 5000/day during...


More  




“"Good deal for money but still not worth for service"”


I and my husband had visited this property in July 2011 and stayed for 5 days at the hotel. The hotel interiors were appealing and well maintained. The 1st rule of good customer service is to welcome the guest with a smile. But the staff on the reception gave a look as if saying "Why have you come?" Talking about...


More  




“Absolutely fantastic”


Stayed in the last week of sept 2011 just before the holiday season starts in Goa. The place is in South Goa. Loved the surroundings. Beautiful location. bout 12 km from Madgaon station. Excellent breakfast. Apart from that, everything else is exorbitantly priced.. well wont blame them since its a five star property. Rent a bike option is available from...


More  



