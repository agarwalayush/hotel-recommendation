
“Relaxing eco resort beside the beach.”


We stayed for a week in one of the Honey Pods, with its patio table on which to enjoy the delicious breakfast with a sea-view, and a shady hammock overlooking the lush gardens. There were sea eagles nesting in pines above our heads, and we saw just one tiny frog during our 7 night stay. It sat in the bowl...


More  




“the good, the bad and the ugly”


We loved it when we got there but that changed pretty quick. We had booked for 5 nights and left after the first night.
The good stuff:
the place is right on the beach, quiet and serene
we were in the family dome – nice large room
Great mohitos
The bad stuff:
the rooms were very dark
the food was...


More  




“A true escape...quite and relaxing place”


It's located in Ashvem beach, my favorite part of Goa. The huts are large and right in front of the beach, so you could hear the waves while sleeping can't ask for a better sleep. The whole resort has I think 14 huts surrounded by palm trees. You will get used to the crows eventually they will be after you...


More  




“Really nice place”


The place is really nice: surrounded by palm trees, in front of a wild beach, with very few restaurants. The resort is calm: no music, no noise...
We spent the 1st night in a standard dome which was quite large and really nice. Then we moved to the double suite Pod with two other friends: this one was not so...


More  




“Wonderful Resort”


We stayed in lovely Yab Yum for 1 week and it was a totally relaxing time. We are usually city travellers and therefore staying in an almost rural resort is a completely new experience for us. The resort is being well kept and the restaurant offers lovely food.
However, there could be some things the resort could do to legitimate...


More  




“A hidden gem”


Ecologically sustainable tourism is very important to me and my wife, and Yab Yum provided just that. We stayed for four nights, in cute huts inspired from Star Wars planets (guess which one). The staff was very friendly and complaint. Clean drinking water was available all the time, which was great. Wifi worked, a bit slow but worked. Restaurant/bar/lounge area...


More  




“Amazing stay”


Wonderful location in the middle of Ashwem and Arambol beach..Clean , peaceful environment with comfortable, large, airy huts.
We stayed 5 days;2 couples and one single at the beginning of the season : there were some small things to be repaired inside the domes but when we informed them, they immediately fixed. They were very sensitive for our comfort.
They...


More  




“Peaceful beachside idyll”


This is the second time I have been to YabYum, and I have also stayed an adjacent properties on the same beach. Yabyum is approx 1 hour's drive north along the coast from the Goa airport.
I keep going back as the cottages are comfortable (NB no A/C). The food is good (ie perfectly fine, but you can also get...


More  




“A brilliant Goan experience! ”


Good resort. Lovely huts. Staff very helpful with on site needs and going off site. Breakfast was good but other food needs taking up a notch to keep customers from wandering along to other eateries on the beach. Overall a good experience.


“Beautiful, but food & service are not enough”


When we reached this place out hearts got happy. It is a beautiful place, well taken care of, the resort looks like a green oasis and jungle with super cute huts. We stayed in the Honey Pod and our friends had a Suite Pod. The location is practically on the beach, the sound of the waves is the best you...


More  




“seclusion in a gorgeous part of Goa”


Looking for paradise in Goa? Look no further - but get in early to ensure they aren't booked!
Yab Yum has received deservedly high reviews on TA and we weren't disappointed. This is in a beautiful part of North Goa, just south of Arambol. The beach is lovely and secluded; devoid of the noisy bar scene, package tour groups and...


More  




“An idyllic place to unwind and relax”


The moment you enter from the gates you feel like walking in a paradise. I and my husband stayed in a HoneyPod. The place was very calm and quiet with no interference at all. The only thing you can hear is the chirping of birds and sounds made by waves as the beach is nearby. Its an eco-resort and hats...


More  




“Beach paradise”


What a nice place. The view opened up to the beach and couldn't dream of a more comfortable spot. Simple, but elegant designed cottages. Beds had mosquito nets, but we still had issues with our toddler getting bites. The beach very close and there's a lot of food options nearby. The french place is the best and we ended up...


More  




“Basic but peaceful”


If you're looking for something quiet and serene this place is for you. Very family friendly and great access to beach! Food was good too but there wasn't much atmosphere in the restaurant it was pretty much empty the whole week we were there. We stayed in a beach house which was very basic but really fine for what we...


More  




“Quirky”


This place is awesome. As far as huts on a beach go Yab Yum is quite a piece of art. The rooms are comfortable and the bathroom is clean. Even 'Larry' out resident frog made us feel welcome. Breakfast was good (best masala omellete I are in India). There was sun loungers for guests on the beach, which was almost...


More  




“What a place.”


I'll keep this short and sweet. If you want an off the beaten track, Goa'n experience with a beautiful beach, stunning and unusual sleeping pods, amazing food and drinks and great prices: Come to yab yum. We stayed for 3 nights and we're so relaxed I'm surprised I can write this review. Laying in the double hammock with my gf...


More  




“A pleasant stay in paradise”


This was the perfect place to spend some quiet days in a beautiful environment. Right on the beach amongst palm trees and flowers I spent four nights in a specious and clean hut. Never mind the frog that liked to go swimming in my toilet, the hotel handsoap that kept dissappearing (might be the frog) or the crowling creatures on...


More  




“This Place was Awesome”


I spent a week here with family and friends. The huts were comfortable and roomy. There was no AC but the fans provided enough air flow to keep things cool. It is nicely located right on a great section of beach and within walking distance of plenty of restaurants. The food was excellent and each night the bar had a...


More  




“Another amazing return to beautiful tranquil YabYum”


We returned to YabYum this year and loved it. We stayed in a beachside honeypod with a seaview for breakfast. Huts are clean and atmospheric and set in the most amazing lush gardens, all very well maintained. Great fresh food. A very peaceful and tranquil spot, Lulled to sleep by the sound of the sea. The beach is fabulous and...


More  




“It's a little Oasis.”


AMAZING huts set in lush landscaped gardens. We stayed in the family pod in December 2013. Really enjoyed our time in Mandrem and at Yab Yum resort.
The breakfast option is something i haven't come across before. You get a menu delivered to your door the night before, you tick boxes for the relevant items you want (i usually had...


More  




“Almost perfect”


If, what you’re looking for are polished marble floors, fluffy bath towels and slick service, Yab Yum isn’t for you. If, however, you want to find a little gem of a hotel, seemingly in the middle of nowhere, on a beautiful, peaceful stretch of powder sand, I wouldn’t hesitate to recommend Yab Yum.
It’s set in the north of Goa...


More  




“Glamping in Goa”


Yabyum is a great retreat from the world. Right on the beach; excellent locally sourced food; and away from the commercial development of other beaches. The huts are perfect for families with kids who like to be with their parents. Ashvem is the nicest, quietest beech of this northern stretch with enough great food options and some nice boutiques, but...


More  




“Very beautiful resort at Ashvem beach in north Goa”


The resort is absolutly beautiful with direct beach access. The beach itself is clean, long and wide. You can walk for miles. There are many nice restaurants on the beach, outside of Yab Yum which offer great dishes. Small "supermarkets" are in walkable distance, maybe five minutes along the road. We really enjoyed our stay.


“Amazing getaway”


I loved everything about this place, from the hobbit huts, the nightly frogs in the bathroom, the food (the thali, which changes each day, is excellent) and the amazing massages!
The standard pods were booked up, so we (a couple) stayed in a family pod. It was a great size and done out beautifully. Breakfast was delivered each morning outside...


More  




“Great stay but take earplugs!”


We stayed here for 5 nights at the beginning of December (3 in standard dome, 2 in Honeypod2). The photographs on the website don't do the grounds justice! It is so green and tropical, it really does feel like paradise. The food is excellent. The staff were good but I think do struggle with understanding English. We went on various...


More  




“GOA BEFORE IT'S TOO LATE”


The setting is idyllic,staff great,the food is really, really good and so are the cocktails. Best accomodation is 'Honey Pod 2' where we stayed which is right by the beach and they serve your fantastic breakfast at your own table looking over the beautiful beach. Other accomodation looks fine but just doesn't have such good sea views. My wife found...


More  




“A fabulous stay just by the beach”


My girlfriend and I had a great time at Yab Yum. The yurts are large, airy and clean and the resort is next to a stunning, quiet beach with amenities nearby. The food in the restaurant was the best we found in the area and the cocktails were also great! The yoga class was relaxing and the staff were always...


More  




“comfortable accommodation but lacking charm”


I stayed here in November 2013. The room we were in was for 3 though we were only 2. The room was very clean and spacious and the bathroom was the best I have had in India. The only point which was lacking was the general atmosphere. Whilst not bad it just felt very stagnant or lacklustre; staff were polite...


More  




“Wonderful stay!”


We stayed at the Yab Yum at the end of our trip around India. Perfect place to chill out and enjoy the sea. Good food. Cool location. Yoga classes. Good service. Friendly staff. I would recommend this hotel.


“Prettycool!”


Great soft sand, right on beach, groovy, no a/c not a problem. Rooms were clean, mosquito free, extremely quiet, and surprisingly airy. Stayed in both cabin and dome, and cabin might be a bit better than dome (maybe), but both are cool. Breakfast was very late all three days but it's not a big deal.
Two quibbles:
1. Please put...


More  




“Closest you'll get to a luxury beach hut in Goa”


This charming tranquil resort is perfect for a week of r and r. Any more than a week and Id have been bored, we moved down south to Palolem for the 2nd week. I mist admit i enjoyed the 'life' in Palolem after a week in Yab yum, however it depends what sort of holiday you are after.
Large domed...


More  




“Lovely rooms. A smile from the staff would be good.”


These pods look fantastic from the outside and the inside. Very organic. Other plus points include:
- Large rooms
- Breakfast delivered to your pod
- Rights on the sea
- Good bar area with excellent cocktails and good food
- Very close to the best restaurant in the area - La Plage (see our other reviews).
- Staff will...


More  




“Another amazing stay at Yab Yum”


Our second trip of the season and pleased to report that the service and experience was every bit as good at the end of the season as the beginning. Every request and concern was managed with the utmost professionalism.
We'll certainly return.
Roll on next season.


“Kickback and enjoy !”


If you really want to get away from the whole sterile hotel setup with swimming pools surrounded by lager consuming tatooed beasts, large screen TVs showing EPL games and the obligatory all you can eat buffet....you may want to think about this place.
You will see reviews complaining about slow room service and "wildlife," I think they may have missed...


More  




“Pure Bliss!”


This resort is the perfect place to kick back and relax. I stayed in one of the domes and was amazed at how large it was. The dome had a little seating area, king sized bed with mosquito net and a large en-suite. Outside is a table and chairs and a hammock. You are only 1 minute walk from the...


More  




“Paradise”


I love this place and would recommend it to all who want to get away from sanitised hotel corridors and relax in nature .This is our second trip to this place with children and I have to say that this place is very child friendly and has a great menu and I love the breakfast which you have outside your...


More  




“Peace”


YabYum was the very first stop on our 6 month break from the corporate world. It is a great place to switch off and re-charge your batteries. The beach is beautiful and the hotel's grounds are great as well. The place has a nice vibe and it was cool to see the swarms of dragonflies.


“very nice place. but...”


This is indeed a very nice palce. we stayed in the Suite pod. I loved that you are really in the middle of nature, the views to the sea are fantastic. It is a very relaxing place and the pod was very cool and wonderful to sleep. The whole place is in a paradise environment.
BUT.
there are some things...


More  




“Practically paradise”


We arrived at Yab Yum frazzled from work, and our stay completely re-energised us. Their 'pods' are simple but extremely comfortable and cool, with clean bathrooms and quality bedding that helped us sleep like logs.
This was our first time in India, and I think Yab Yum is perfect for people who might be a little wary of the country...


More  




“Nice intimate resort on the beach”


I've just returned from a weekend in Goa with my partner and we stayed for two nights at Yab Yum and over we were pretty pleased.
What we liked: The pods are great, they are well built, comfortable and clean. The resort has been been constructed with a great amount of care and consideration to the environment. When you're standing...


More  




“A perfect hide away from the crowd”


We spent 4 nights at Yab Yum and enjoyed every minute. A perfect combination of lush, green, peace, sun, privacy simple but yet luxurius. Very friendly and helpfull staff. The beach was great and only with very few people, the food was also recommendable, and we loved the fact that our breakfast was served on our private "terasse" just outside...


More  




“Amazing, loved it.”


Yab Yum felt like the jewel in Goa's crown. A really peaceful, stylish and very unique place with a quiet beach outside and all the other guests were really nice and chilled too. The staff were wonderful and we just got the impression that the place was very well run and everyone was happy!
I can't say we have ever...


More  




“I would not recommend this place”


I visited Yam Yum for the first time in 2011. At that time our room was flooding after a rain storm and after that we were evacuated with all the guests in the hotel because the rooms were not "water isolated", anyhow the staff managed the situation. This time we decided to try again (you know.....is better a bad place...


More  




“The best in North Goa”


We were oringinally booked for 5 nights and ended up staying nine. This hotel is a marvelous refuge from the noise and pollution of India. The staff at Yab Yum was the best trained we found in India.They went out of their way to make us dinner reservations, book us drivers to go sightseeing, and even taught us to use...


More  




“Highly recommended”


We've just spent 9 glorious days at Yab Yum, staying in Honeypod 2. The accommodation was very comfortable and clean. The resort is quiet and secluded and we were lulled to sleep at night by the sound of the waves. We were served breakfast on our deck, looking out onto the beach. The beach itself is the best for miles,...


More  




“It just gets better”


This was our second stay at Yab Yum, and we were a little apprehensive that we would not enjoy it as much as the first time. I need not have worried, it was a fantastic stay.
We stayed over the Christmas period with our daughter and although the resort was at full capacity, it still felt tranquil and discreet.
Since...


More  




“Christmas bliss on the beach”


We loved yabyum right from the minute we stepped through the gate. It was like a fairy land with beautifully lit paths. We stayed in a spacious, clean eco dome which looked like a giant coconut. The restaurant served complimentary mulled wine on Christmas eve which was cute. The beach was gorgeous and was practically empty. We spent a very...


More  




“Peaceful and tranquil”


We really enjoyed our 5 nights here. The beach was very quiet and peaceful with a short stroll to places to eat in the evening. The included breakfast has some vegan options and the staff were diligent. I would have liked more information from the hotel on how they minimise environmental impact. The couple who run the yoga classes were...


More  




“Busy doing nothing”


Swim sun breakfast swim sun lunch swim sun swim sun swim sun dinner swim sun sleep. Repeat for 7 days.
This place was quiet, relaxed and the beach was empty! Perfect!
Thank you so much for the wonderful food and cottage.


“Escape to paradise”


Concrete jungle dwellers, this is the place to wash away all your stress and get a huge dose of relaxation.
The Good:
- The Pods are designed beautifully and you get a great 'close to nature' feel throughout your stay.
- Ashwem beach is arguably Goa's best and cleanest.
- The 'flying carpet' hammocks from Arambol outside your pod are...


More  




“Bliss”


We just returned yesterday from a 2 week holiday at Yab Yum and I am still on a cloud.
The place is Bliss. As soon as you enter the resort you know it is going to be a very relaxing holiday. And we fell under the spell very quickly. The Domes are brilliant. Unique in their Design as the owner...


More  




“Everyone should Stay here”


This is one of the best places both myself and my partner have ever stayed. We arrived here after being in another part of Goa which was a disappointing experience. We stayed in a standard dome which was a great size for 2 people. It was spotless clean and when they cleaned it everyday they some how managed to get...


More  




“Tropical Igloos and a private beach.”


You can live in a tropical igloo (the dome shared huts of various sizes) using thatched palm and coconut leaves with bob lights and without a fridge or TV or A/C, get the feel of an extended beach as the two acre property is completely done with sand. And on this sandy floor they have created an amazing variety of...


More  




“Great place for family's”


Me my daughter who is five and my mum stayed here for 2 nights. My daughter loved it, on arrival they gave my daughter a painting set and our pod had a small little pool next to it. This is an excellent place for family's with small children, warm and friendly.


“Podtastic!”


What a fantastic, idyllic place and very luxurious for a pod on the beach. We stayed in many places in North Goa and this would definately be one of two of our favourite places. I would definitley come back. My only complaint would be the service is unbelievably slow and the staff are not very welcoming apart from the owner...


More  




“a little oasis”


only stayed one night as we were heading north to explore but this was so nice it was touhg to move on. We stayed in one of the cottages and couldnt beleive how nice it was inside. Warm showers in goa were a rarity on our budget but the bathroom was really nice, bed was a good size and had...


More  




“Yab Yum - the positives and the negatives!”


My family and I enjoyed a fantastic holiday in Goa over Easter, we spent 3 weeks in 3 different resorts, Luxury Villa, Yab Yum and 5 Star Hotel in the North and South of Goa.
Yab Yum was our middle week and my husband enjoyed and missed this one the most!
We stayed in the only 2 bedroom suite/pod......it looked...


More  




“We are going to make this a regular destination”


We have been coming to Goa for 15 years or more and we have often walked past Yab Yums on beach walks and wondered what it was like to stay there. I am glad that we tried it as we will certainly return and have already booked for next year.
We stayed with our seven year old daughter and had...


More  




“better every time”


Over the past two years, I've stayed at Yab Yum on three separate occasions, and I intend to keep returning there for the next two years, and for as long as I keep coming to India. The resort stands out as an island of professionalism and grace, in the otherwise loud, chaotic, and touristy beach culture of North Goa. If...


More  




“An oasis of tranquilty”


I had the great pleasure of staying at Yam Yum in October 2011 for three nights, at the end of my first trip to India. We stayed at cottage No.3, which was spacious, airy and clean, and just a short stroll to the beach.I have fond memories of waking to the sound of the sea under to cocoon of the...


More  



