#convex hull code from http://www.ics.uci.edu/~eppstein/161/python/diameter.py
from __future__ import generators

import ner
import sys
import urllib
import simplejson
import csv
import math
import operator
from treetagger import TreeTagger
from array import *
lat1 = 25.0 #delhi lat long range
lat2 = 35.0
long1 = 76.0
long2 = 78.0
latindex = 1
lonindex = 2
crimeindex = 3
interpretation_distance = 5.0
diameter_threshold = 5.0
prior_record_file = 'crimerecords.csv'
time_record_path = "../temporal_analysis/"


def main():
	tagger = ner.SocketNER(host='localhost', port=1234)

	total = len(sys.argv)
	cmdargs = str(sys.argv)
	filename = str(sys.argv[1])
	with open(filename) as myfile:
	    data="".join(line.rstrip() for line in myfile).replace('"','')

	nertagged = tagger.get_entities(data)
	#print nertagged
	nertaglatlong = []
	location = []
	if "LOCATION" in nertagged:
		location = [x.title() for x in nertagged["LOCATION"]]
		location = list(set(location)) #removes dublicates
		for item in location:
			checklatlong(item, nertaglatlong)
	else:
		print "\nNo NER tagged location\n"
	#print location
	#print nertaglatlong

	taggedlist = []
	postaglatlong = []
	tt = TreeTagger(encoding='latin-1',language='english')
	a = tt.tag(data)
	#print a

	#print "pos tagged np---------------------"       #need only proper noun
	for item in a:
		if item[1] == 'NP':
			taggedlist.append(item[0].title())
	taggedlist = list(set(taggedlist))
	#print taggedlist

	with open(filename, 'r') as inF:
	    for line in inF:
	    	for i in xrange(len(taggedlist)):
				if taggedlist[i] in line and taggedlist[i] not in location:
					#print taggedlist[i]
					checklatlong(taggedlist[i], postaglatlong)
	#print postaglatlong

	#I have now two list nertaglatlong and postaglatlong with locations, lat n long..Now, I have to do fuzzy geptagging..use prior
	#probability n blah blah
	#find the maximmum distance b/w two police stations, for each toponym find the police station within a range(here taking the radius as max
	#distance) and for all those interpretations assign wt as distance bw the toponym and police station. Now, sort in increasing order of
	#wt. Now, do convex hull thing. Get the lexicon. 
	#for now, increase no of crime by 1 in all those lexicons(police stations)

	crimerecord = []
	maxdistance = 0.0
	totalcrime = 0.0
	with open(prior_record_file, 'rb') as f:
		reader = csv.reader(f,dialect='excel')
		for row in reader:
			a = row[0].split('\t')
			crimerecord.append(a)
	#print crimerecord
	#print len(crimerecord)

	for i in xrange(0, len(crimerecord)):
		#print i
		for j in xrange(i+1, len(crimerecord)):
			d = distance(float(crimerecord[i][latindex]),float(crimerecord[i][lonindex]),float(crimerecord[j][latindex]),float(crimerecord[j][lonindex]))
			if d > maxdistance:
				maxdistance = d
			totalcrime+=float(crimerecord[i][crimeindex])
				#print d

	Point = []
	L = []
	stop = 0

	R = []
	for t in xrange(0,len(nertaglatlong)):
		for i in xrange(0,len(crimerecord)):
			#print float(nertaglatlong[t][1]),float(nertaglatlong[t][2]),float(crimerecord[i][latindex]),float(crimerecord[i][lonindex])
			d = distance(float(nertaglatlong[t][1]),float(nertaglatlong[t][2]),float(crimerecord[i][latindex]),float(crimerecord[i][lonindex]))
			if d < interpretation_distance:
				#R[t].append((i,d,float(crimerecord[i][2]),float(crimerecord[i][3])))
				R.append((i,d))

	for t in xrange(0,len(postaglatlong)):
		for i in xrange(0,len(crimerecord)):
			#print float(nertaglatlong[t][1]),float(nertaglatlong[t][2]),float(crimerecord[i][latindex]),float(crimerecord[i][lonindex])
			d = distance(float(postaglatlong[t][1]),float(postaglatlong[t][2]),float(crimerecord[i][latindex]),float(crimerecord[i][lonindex]))
			if d < interpretation_distance:
				#R[t].append((i,d,float(crimerecord[i][2]),float(crimerecord[i][3])))
				R.append((i,d))

	R.sort(key = operator.itemgetter(1))

	# print R

	d = 0.0
	for item in R:
		#print item[item2]
		i = item[0]
		Point.append((float(crimerecord[i][latindex]),float(crimerecord[i][lonindex])))
		d = diameter(Point)
		#dist = distance(d[0][0],d[0][1],d[1][0],d[1][1])
		if d > diameter_threshold:
			break
		else:
			L.append((i,item[1]))

	# for item in L:
	# 	i = item[0]
	# 	print crimerecord[i][0]

	for i in xrange(0,len(L)-1):
		for j in xrange(i+1,len(L)-1):
#			print L[i], L[j]
			if L[i][0]==L[j][0]:
				L.remove(L[j])

	if(len(L)>2):
		if(L[len(L)-2][0]==L[len(L)-1][0]):
			L.remove(L[len(L)-1])

	#print crimerecord[L[len(L)-2][0]][0]

	for item in L:
		i = item[0]
		print crimerecord[i][0]

	#now update crime
	# totalcrime, crimeindex
	prior = []
	sum =0 
	for item in L:
		i = item[0]
		#print crimerecord[0][crimeindex]
		alpha = float(crimerecord[i][crimeindex])/totalcrime
		prior.append((i,alpha))
		#print i," ",alpha
		sum += alpha

	for i in xrange(0,len(prior)):
		fileread2 = []
		j = 0
		a = ()
		b = []
		filename = time_record_path+str(prior[i][0])+".csv"
		with open(filename, 'rb') as f:
			reader = csv.reader(f,dialect='excel')
			for row in reader:
				if(j!=0):
					fileread2.append(b)
				else:
					j = 1
				b = row
				a = row[0].split('\t')
		temp = prior[i][1]/sum
		a[1] = str(temp + float(a[1]))
		b = a[0] + '\t' + a[1]
		fileread2.append([b])
		writer = csv.writer(open(filename, 'w'))
		writer.writerows(fileread2)
		crimerecord[prior[i][0]][crimeindex] = str(float(crimerecord[prior[i][0]][crimeindex]) + temp)
#		print crimerecord[prior[i][0]][crimeindex]
	c = []
	for row in crimerecord:
		q = "\t".join(row)
		c.append([q])
	writer = csv.writer(open(prior_record_file, 'w'))
	writer.writerows(c)

def orientation(p,q,r):
    '''Return positive number if p-q-r are clockwise, neg if ccw, zero if colinear.'''
    return (q[1]-p[1])*(r[0]-p[0]) - (q[0]-p[0])*(r[1]-p[1])

def hulls(Points):
    '''Graham scan to find upper and lower convex hulls of a set of 2d points.'''
    U = []
    L = []
    Points.sort()
    for p in Points:
        while len(U) > 1 and orientation(U[-2],U[-1],p) <= 0: U.pop()
        while len(L) > 1 and orientation(L[-2],L[-1],p) >= 0: L.pop()
        U.append(p)
        L.append(p)
    return U,L

# U,L = hulls([(0,0),(1,1),(0,1),(1,0),(1,3),(3,1),(2,2)])
# returns
# U = [(0, 0), (0, 1), (1, 3), (3, 1)]
# L = [(0, 0), (1, 0), (3, 1)]

def rotatingCalipers(Points):
    '''Given a list of 2d points, finds all ways of sandwiching the points between
two parallel lines that touch one point each, and yields the sequence of pairs of
points touched by each pair of lines.'''
    U,L = hulls(Points)
    i = 0
    j = len(L) - 1
    while i < len(U) - 1 or j > 0:
        yield U[i],L[j]
        
        # if all the way through one of top or bottom, advance the other
        if i == len(U) - 1: j -= 1
        elif j == 0: i += 1
        
        # still points left on both lists, compare slopes of next hull edges
        elif (U[i+1][1]-U[i][1])*(L[j][0]-L[j-1][0]) > \
                (L[j][1]-L[j-1][1])*(U[i+1][0]-U[i][0]):
            i += 1
        else: j -= 1

# rotatingCalipers([(0,0),(1,1),(0,1),(1,0),(1,3),(3,1),(2,2)])
# yields:
# (0, 0) (3, 1)
# (0, 1) (3, 1)
# (1, 3) (3, 1)
# (1, 3) (1, 0)
# (1, 3) (0, 0)

def diameter(Points):
    '''Given a list of 2d points, returns the pair that's farthest apart.'''
    bestPair = None
    bestDist = 0
    # dialat1=0.0
    # dialat2=0.0
    # dialon1=0.0
    # dialon2 = 0.0
    def square(x): return x*x
    for p,q in rotatingCalipers(Points):
    	#dist = distance(dialat1,dialon1,dialat2,dialon2)
    	dist = distance(p[0],p[1],q[0],q[1])
        #dist = square(q[0]-p[0]) + square(q[1]-p[1])
        if dist > bestDist:
            bestDist = dist
            bestPair = (p,q)
            # dialat1 = p[0]
            # dialat2 = q[0]
            # dialon1 = p[1]
            # dialon2 = q[1]
    return bestDist

# diameter([(0,0),(1,1),(0,1),(1,0),(1,3),(3,1),(2,2)])
# returns: ((0, 0), (3, 1))

def distance(lat1,lon1,lat2,lon2):
	radius = 6371 # km
	#print lat1,lat2,lon1,lon2
	dlat = math.radians(lat2-lat1)
	dlon = math.radians(lon2-lon1)
	a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
		* math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
	d = radius * c
	return d

def checklatlong(toponym,taglatlong):
	googleGeocodeUrl = 'http://maps.googleapis.com/maps/api/geocode/json?'

	#print ("The total numbers of args passed to the script: %d " % total)a
	#print ("Args list: %s " % cmdargs)
	# Pharsing args one by one 
	#print ("Script name: %s" % str(sys.argv[0]))
	#print toponym
	
	#toponym = "Kashmere Gate"
	from_sensor=False
	toponym = toponym.encode('utf-8')
	params = {
	'address': toponym,
	'sensor': "true" if from_sensor else "false"
	}
	url = googleGeocodeUrl + urllib.urlencode(params)
	json_response = urllib.urlopen(url)
	response = simplejson.loads(json_response.read())
	if response['results']:
		location = response['results'][0]['geometry']['location']
		latitude, longitude = location['lat'], location['lng']
		if ((latitude>lat1) & (latitude<lat2) & (longitude>long1) & (longitude < long2)):
			taglatlong.append((toponym,latitude,longitude))
		#print toponym, latitude, longitude
	else:
		latitude, longitude = None, None
		#print toponym, "<no results>"

#def toponymresolution():


if __name__=='__main__':
	main()
